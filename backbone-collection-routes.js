// backbone/AJAX routes
// modify all the posts, puts and deletes to handle more than one document
// modify gets to take additional parameters (for finds)
// modify these routes with user checks
peercover.get('/users', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    User.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/users', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new User(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/users', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    User.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/users', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    User.find(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// ious Routes
peercover.get('/ious', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Iou.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/ious', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Iou(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/ious', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Iou.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/ious', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Iou.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// rippleInsuredWallets Routes
peercover.get('/rippleInsuredWallets', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    RippleInsuredWallet.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/rippleInsuredWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new RippleInsuredWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/rippleInsuredWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuredWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/rippleInsuredWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuredWallet.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// bitcoinInsuredWallets Routes
peercover.get('/bitcoinInsuredWallets', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    BitcoinInsuredWallet.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/bitcoinInsuredWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new BitcoinInsuredWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/bitcoinInsuredWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuredWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/bitcoinInsuredWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuredWallet.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// bitcoinInsuranceWallets Routes
peercover.get('/bitcoinInsuranceWallets', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    BitcoinInsuranceWallet.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/bitcoinInsuranceWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new BitcoinInsuranceWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/bitcoinInsuranceWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuranceWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/bitcoinInsuranceWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuranceWallet.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// rippleInsuranceWallets Routes
peercover.get('/rippleInsuranceWallets', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    RippleInsuranceWallet.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/rippleInsuranceWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new RippleInsuranceWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/rippleInsuranceWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuranceWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/rippleInsuranceWallets', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuranceWallet.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// facebookData Routes
peercover.get('/facebookData', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    FacebookDatum.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/facebookData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new FacebookDatum(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/facebookData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FacebookDatum.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/facebookData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FacebookDatum.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// dwollaData Routes
peercover.get('/dwollaData', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    DwollaDatum.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/dwollaData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new DwollaDatum(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/dwollaData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    DwollaDatum.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/dwollaData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    DwollaDatum.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// linkedinData Routes
peercover.get('/linkedinData', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    LinkedinDatum.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/linkedinData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new LinkedinDatum(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/linkedinData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LinkedinDatum.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/linkedinData', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LinkedinDatum.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// fundingSources routes
peercover.get('/fundingSources', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    FundingSource.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/fundingSources', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new FundingSource(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/fundingSources', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FundingSource.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/fundingSources', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FundingSource.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// images Routes
peercover.get('/images', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Imaged.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/images', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Imaged(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/images', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Imaged.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/images', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Imaged.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// documents Routes
peercover.get('/documents', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Documented.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/documents', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Documented(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/documents', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Documented.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/documents', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Documented.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// videos Routes
peercover.get('/videos', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Video.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/videos', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Video(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/videos', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Video.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/videos', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Video.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// claims Routes
peercover.get('/claims', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Claim.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/claims', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Claim(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/claims', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Claim.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/claims', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Claim.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// lossGroups Routes
peercover.get('/lossGroups', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    LossGroup.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/lossGroups', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new LossGroup(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/lossGroups', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LossGroup.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/lossGroups', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LossGroup.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// usersReferred Routes
peercover.get('/usersReferred', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    UserReferred.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/usersReferred', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new UserReferred(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/usersReferred', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    UserReferred.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/usersReferred', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    UserReferred.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// terms Routes
peercover.get('/terms', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Term.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/terms', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Term(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/terms', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Term.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/terms', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Term.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// signatures Routes
peercover.get('/signatures', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Signature.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/signatures', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Signature(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/signatures', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Signature.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/signatures', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Signature.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// termOptions Routes
peercover.get('/termOptions', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    TermOption.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/termOptions', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new TermOption(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/termOptions', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TermOption.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/termOptions', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TermOption.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// trustedGateways Routes
peercover.get('/trustedGateways', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    TrustedGateway.find().exec(function (err,docs) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(docs);
    });
});
peercover.post('/trustedGateways', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new TrustedGateway(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/trustedGateways', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TrustedGateway.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/trustedGateways', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TrustedGateway.findById(req.params.id,function(err,docs){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});
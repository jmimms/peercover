/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'wires1',
            type:'image',
            rect:['43px','25px','212px','295px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"wires1.svg",'0px','0px']
         },
         {
            id:'wires2',
            type:'image',
            rect:['29px','324px','278px','276px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"wires2.svg",'0px','0px']
         },
         {
            id:'wires3',
            type:'image',
            rect:['388px','460px','163px','130px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"wires3.svg",'0px','0px']
         },
         {
            id:'wires4',
            type:'image',
            rect:['327px','27px','246px','357px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"wires4.svg",'0px','0px']
         },
         {
            id:'L',
            type:'image',
            rect:['450px','525px','18px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"L.svg",'0px','0px']
         },
         {
            id:'drops',
            type:'image',
            rect:['163px','20px','13px','14px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"drops.svg",'0px','0px']
         },
         {
            id:'bitcoin',
            type:'image',
            rect:['71px','141px','18px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"bitcoin.svg",'0px','0px']
         },
         {
            id:'purple_cloud1',
            type:'image',
            rect:['62px','71px','97px','32px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"purple_cloud.svg",'0px','0px']
         },
         {
            id:'purple_cloud2',
            type:'image',
            rect:['498px','204px','97px','32px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"purple_cloud.svg",'0px','0px']
         },
         {
            id:'purple_cloud3',
            type:'image',
            rect:['45px','519px','97px','32px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"purple_cloud.svg",'0px','0px']
         },
         {
            id:'blue_cloud1',
            type:'image',
            rect:['468px','25px','92px','30px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"blue_cloud.svg",'0px','0px']
         },
         {
            id:'blue_cloud2',
            type:'image',
            rect:['16px','299px','92px','30px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"blue_cloud.svg",'0px','0px']
         },
         {
            id:'blue_cloud3',
            type:'image',
            rect:['486px','526px','92px','30px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"blue_cloud.svg",'0px','0px']
         },
         {
            id:'house8',
            type:'image',
            rect:['388px','150px','31px','121px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house8.svg",'0px','0px']
         },
         {
            id:'house7',
            type:'image',
            rect:['377px','124px','27px','131px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house7.svg",'0px','0px']
         },
         {
            id:'house5',
            type:'image',
            rect:['308px','76px','34px','163px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house5.svg",'0px','0px']
         },
         {
            id:'house6',
            type:'image',
            rect:['331px','93px','46px','177px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house6.svg",'0px','0px']
         },
         {
            id:'house3',
            type:'image',
            rect:['267px','42px','37px','191px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house3.svg",'0px','0px']
         },
         {
            id:'house4',
            type:'image',
            rect:['290px','152px','37px','74px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house4.svg",'0px','0px']
         },
         {
            id:'house1',
            type:'image',
            rect:['214px','123px','29px','149px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house1.svg",'0px','0px']
         },
         {
            id:'house2',
            type:'image',
            rect:['240px','87px','47px','159px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house2.svg",'0px','0px']
         },
         {
            id:'house9',
            type:'image',
            rect:['430px','346px','109px','133px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house9.svg",'0px','0px']
         },
         {
            id:'house10',
            type:'image',
            rect:['352px','495px','70px','78px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house10.svg",'0px','0px']
         },
         {
            id:'house11',
            type:'image',
            rect:['203px','490px','72px','76px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house11.svg",'0px','0px']
         },
         {
            id:'house12',
            type:'image',
            rect:['123px','407px','76px','82px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"house12.svg",'0px','0px']
         },
         {
            id:'tree1',
            type:'image',
            rect:['165px','219px','48px','52px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree1.svg",'0px','0px']
         },
         {
            id:'tree2',
            type:'image',
            rect:['123px','268px','61px','43px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree2.svg",'0px','0px']
         },
         {
            id:'tree3',
            type:'image',
            rect:['103px','336px','68px','29px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree3.svg",'0px','0px']
         },
         {
            id:'tree4',
            type:'image',
            rect:['101px','396px','73px','33px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree4.svg",'0px','0px']
         },
         {
            id:'tree5',
            type:'image',
            rect:['247px','514px','36px','66px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree5.svg",'0px','0px']
         },
         {
            id:'tree6',
            type:'image',
            rect:['331px','514px','37px','61px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree6.svg",'0px','0px']
         },
         {
            id:'tree7',
            type:'image',
            rect:['422px','466px','58px','43px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree7.svg",'0px','0px']
         },
         {
            id:'tree8',
            type:'image',
            rect:['377px','184px','23px','65px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"tree8.svg",'0px','0px']
         },
         {
            id:'circle',
            type:'image',
            rect:['165px','222px','299px','300px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"circle.svg",'0px','0px']
         },
         {
            id:'people1',
            type:'image',
            rect:['143px','308px','38px','38px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people1.svg",'0px','0px']
         },
         {
            id:'people2',
            type:'image',
            rect:['135px','359px','37px','37px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people2.svg",'0px','0px']
         },
         {
            id:'people3',
            type:'image',
            rect:['178px','459px','50px','40px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people3.svg",'0px','0px']
         },
         {
            id:'people4',
            type:'image',
            rect:['399px','472px','41px','31px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people4.svg",'0px','0px']
         },
         {
            id:'people5',
            type:'image',
            rect:['437px','433px','28px','23px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people5.svg",'0px','0px']
         },
         {
            id:'people6',
            type:'image',
            rect:['458px','356px','27px','27px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people6.svg",'0px','0px']
         },
         {
            id:'people7',
            type:'image',
            rect:['448px','307px','39px','37px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people7.svg",'0px','0px']
         },
         {
            id:'people8',
            type:'image',
            rect:['370px','209px','30px','33px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"people8.svg",'0px','0px']
         },
         {
            id:'bycicle1',
            type:'image',
            rect:['170px','263px','35px','33px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"bycicle1.svg",'0px','0px']
         },
         {
            id:'car2',
            type:'image',
            rect:['281px','518px','51px','32px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"car2.svg",'0px','0px']
         },
         {
            id:'car',
            type:'image',
            rect:['206px','220px','44px','38px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"car.svg",'0px','0px'],
            transform:[[],[],[],[],['50%','49.9999%']]
         },
         {
            id:'bus',
            type:'image',
            rect:['391px','216px','88px','90px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"bus.svg",'0px','0px']
         },
         {
            id:'text',
            type:'image',
            rect:['220px','371px','190px','62px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"text.svg",'0px','0px']
         },
         {
            id:'big_person',
            type:'image',
            rect:['267px','47px','75px','184px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"big_person.svg",'0px','0px']
         },
         {
            id:'logo',
            type:'image',
            rect:['191px','312px','244px','50px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
         }],
         symbolInstances: [

         ]
      },
   states: {
      "Base State": {
         "${_house8}": [
            ["style", "left", '388px'],
            ["style", "top", '150px']
         ],
         "${_drops}": [
            ["style", "top", '79px'],
            ["transform", "rotateZ", '0deg'],
            ["motion", "location", '93.5px 82.999980163574px'],
            ["style", "opacity", '0'],
            ["style", "left", '87px']
         ],
         "${_people1}": [
            ["style", "top", '308px'],
            ["style", "left", '143px']
         ],
         "${_purple_cloud2}": [
            ["style", "left", '498px'],
            ["style", "top", '204px']
         ],
         "${_tree8}": [
            ["style", "left", '377px'],
            ["style", "top", '184px']
         ],
         "${_people3}": [
            ["style", "left", '178px'],
            ["style", "top", '459px']
         ],
         "${_blue_cloud1}": [
            ["style", "left", '468px'],
            ["style", "top", '25px']
         ],
         "${_bus}": [
            ["style", "left", '391px'],
            ["style", "top", '216px']
         ],
         "${_bitcoin}": [
            ["style", "top", '141px'],
            ["style", "left", '71px']
         ],
         "${_logo}": [
            ["style", "left", '191px'],
            ["style", "top", '312px']
         ],
         "${_tree3}": [
            ["style", "left", '103px'],
            ["style", "top", '336px']
         ],
         "${_wires1}": [
            ["style", "left", '43px'],
            ["style", "top", '25px']
         ],
         "${_blue_cloud3}": [
            ["style", "left", '486px'],
            ["style", "top", '526px']
         ],
         "${_house2}": [
            ["style", "left", '240px'],
            ["style", "top", '87px']
         ],
         "${_L}": [
            ["style", "left", '450px'],
            ["style", "top", '525px']
         ],
         "${_text}": [
            ["style", "left", '220px'],
            ["style", "top", '371px']
         ],
         "${_wires2}": [
            ["style", "left", '29px'],
            ["style", "top", '324px']
         ],
         "${_purple_cloud3}": [
            ["style", "left", '45px'],
            ["style", "top", '519px']
         ],
         "${_bycicle1}": [
            ["style", "left", '170px'],
            ["style", "top", '263px']
         ],
         "${_tree2}": [
            ["style", "left", '123px'],
            ["style", "top", '268px']
         ],
         "${_house7}": [
            ["style", "left", '377px'],
            ["style", "top", '124px']
         ],
         "${_people2}": [
            ["style", "left", '135px'],
            ["style", "top", '359px']
         ],
         "${_blue_cloud2}": [
            ["style", "left", '16px'],
            ["style", "top", '299px']
         ],
         "${_tree1}": [
            ["style", "left", '165px'],
            ["style", "top", '219px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '612px'],
            ["style", "width", '612px']
         ],
         "${_tree6}": [
            ["style", "left", '331px'],
            ["style", "top", '514px']
         ],
         "${_car}": [
            ["style", "-webkit-transform-origin", [247.73,402.63], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [247.73,402.63],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [247.73,402.63],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [247.73,402.63],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [247.73,402.63],{valueTemplate:'@@0@@% @@1@@%'}],
            ["transform", "rotateZ", '0deg'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '206px'],
            ["style", "top", '220px']
         ],
         "${_big_person}": [
            ["style", "left", '267px'],
            ["style", "top", '47px']
         ],
         "${_people5}": [
            ["style", "left", '437px'],
            ["style", "top", '433px']
         ],
         "${_house1}": [
            ["style", "left", '214px'],
            ["style", "top", '123px']
         ],
         "${_house9}": [
            ["style", "left", '430px'],
            ["style", "top", '346px']
         ],
         "${_house3}": [
            ["style", "left", '267px'],
            ["style", "top", '42px']
         ],
         "${_house12}": [
            ["style", "left", '123px'],
            ["style", "top", '407px']
         ],
         "${_car2}": [
            ["style", "left", '281px'],
            ["style", "top", '518px']
         ],
         "${_people6}": [
            ["style", "left", '458px'],
            ["style", "top", '356px']
         ],
         "${_tree5}": [
            ["style", "top", '514px'],
            ["style", "left", '247px']
         ],
         "${_circle}": [
            ["style", "left", '165px'],
            ["style", "top", '222px']
         ],
         "${_purple_cloud1}": [
            ["style", "left", '62px'],
            ["style", "top", '71px']
         ],
         "${_tree7}": [
            ["style", "left", '422px'],
            ["style", "top", '466px']
         ],
         "${_tree4}": [
            ["style", "left", '101px'],
            ["style", "top", '396px']
         ],
         "${_house5}": [
            ["style", "top", '76px'],
            ["style", "left", '308px']
         ],
         "${_people4}": [
            ["style", "left", '399px'],
            ["style", "top", '472px']
         ],
         "${_house10}": [
            ["style", "top", '495px'],
            ["style", "left", '352px']
         ],
         "${_wires3}": [
            ["style", "left", '388px'],
            ["style", "top", '460px']
         ],
         "${_house11}": [
            ["style", "left", '203px'],
            ["style", "top", '490px']
         ],
         "${_people7}": [
            ["style", "left", '448px'],
            ["style", "top", '307px']
         ],
         "${_house6}": [
            ["style", "top", '93px'],
            ["style", "left", '331px']
         ],
         "${_house4}": [
            ["style", "top", '152px'],
            ["style", "left", '290px']
         ],
         "${_wires4}": [
            ["style", "left", '327px'],
            ["style", "top", '27px']
         ],
         "${_people8}": [
            ["style", "left", '370px'],
            ["style", "top", '209px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 100000,
         autoPlay: true,
         labels: {
            "loop": 3854
         },
         timeline: [
            { id: "eid17", tween: [ "style", "${_drops}", "opacity", '0.984375', { fromValue: '0'}], position: 187, duration: 684 },
            { id: "eid19", tween: [ "style", "${_drops}", "opacity", '0', { fromValue: '0.984375'}], position: 4103, duration: 622, easing: "easeInQuad" },
            { id: "eid25", tween: [ "style", "${_drops}", "opacity", '0.984375', { fromValue: '0'}], position: 6000, duration: 684 },
            { id: "eid28", tween: [ "style", "${_drops}", "opacity", '0', { fromValue: '0.984375'}], position: 7500, duration: 622, easing: "easeInQuad" },
            { id: "eid33", tween: [ "style", "${_drops}", "opacity", '0.984375', { fromValue: '0'}], position: 9000, duration: 684 },
            { id: "eid34", tween: [ "style", "${_drops}", "opacity", '0', { fromValue: '0.984375'}], position: 11250, duration: 622, easing: "easeInQuad" },
            { id: "eid42", tween: [ "transform", "${_car}", "rotateZ", '190deg', { fromValue: '0deg'}], position: 3871, duration: 12000, easing: "easeInOutQuad" },
            { id: "eid43", tween: [ "transform", "${_car}", "rotateZ", '360deg', { fromValue: '190deg'}], position: 15871, duration: 7000, easing: "easeInOutQuad" },
            { id: "eid40", tween: [ "style", "${_car}", "-webkit-transform-origin", [247.73,402.63], { valueTemplate: '@@0@@% @@1@@%', fromValue: [247.73,402.63]}], position: 13371, duration: 0, easing: "easeInCubic" },
            { id: "eid96", tween: [ "style", "${_car}", "-moz-transform-origin", [247.73,402.63], { valueTemplate: '@@0@@% @@1@@%', fromValue: [247.73,402.63]}], position: 13371, duration: 0, easing: "easeInCubic" },
            { id: "eid97", tween: [ "style", "${_car}", "-ms-transform-origin", [247.73,402.63], { valueTemplate: '@@0@@% @@1@@%', fromValue: [247.73,402.63]}], position: 13371, duration: 0, easing: "easeInCubic" },
            { id: "eid98", tween: [ "style", "${_car}", "msTransformOrigin", [247.73,402.63], { valueTemplate: '@@0@@% @@1@@%', fromValue: [247.73,402.63]}], position: 13371, duration: 0, easing: "easeInCubic" },
            { id: "eid99", tween: [ "style", "${_car}", "-o-transform-origin", [247.73,402.63], { valueTemplate: '@@0@@% @@1@@%', fromValue: [247.73,402.63]}], position: 13371, duration: 0, easing: "easeInCubic" },
            { id: "eid2", tween: [ "transform", "${_drops}", "rotateZ", '18000deg', { fromValue: '0deg'}], position: 0, duration: 100000 },
            { id: "eid41", tween: [ "style", "${_car}", "opacity", '1', { fromValue: '0.000000'}], position: 871, duration: 2000, easing: "easeInCubic" },
            { id: "eid10", tween: [ "style", "${_drops}", "top", '20px', { fromValue: '79px'}], position: 0, duration: 1243, easing: "easeOutQuad" },
            { id: "eid21", tween: [ "style", "${_drops}", "top", '18px', { fromValue: '20px'}], position: 1243, duration: 1507, easing: "easeInOutQuad" },
            { id: "eid22", tween: [ "style", "${_drops}", "top", '20px', { fromValue: '18px'}], position: 2750, duration: 1105, easing: "easeInOutQuad" },
            { id: "eid14", tween: [ "style", "${_drops}", "top", '142px', { fromValue: '20px'}], position: 3855, duration: 1243, easing: "easeInQuad" },
            { id: "eid24", tween: [ "style", "${_drops}", "top", '143px', { fromValue: '142px'}], position: 5098, duration: 1902, easing: "easeOutQuad" },
            { id: "eid26", tween: [ "style", "${_drops}", "top", '86px', { fromValue: '143px'}], position: 7000, duration: 1250, easing: "easeInQuad" },
            { id: "eid32", tween: [ "style", "${_drops}", "top", '312px', { fromValue: '86px'}], position: 8500, duration: 3000, easing: "easeInOutQuad" },
            { id: "eid39", tween: [ "style", "${_drops}", "top", '312px', { fromValue: '312px'}], position: 12139, duration: 0 },
            { id: "eid8", tween: [ "style", "${_drops}", "left", '87px', { fromValue: '87px'}], position: 0, duration: 0 },
            { id: "eid12", tween: [ "style", "${_drops}", "left", '248px', { fromValue: '87px'}], position: 1243, duration: 2611, easing: "easeInOutQuad" },
            { id: "eid20", tween: [ "style", "${_drops}", "left", '249px', { fromValue: '248px'}], position: 3855, duration: 1243, easing: "easeInOutQuad" },
            { id: "eid23", tween: [ "style", "${_drops}", "left", '131px', { fromValue: '249px'}], position: 5098, duration: 1902, easing: "easeInOutQuad" },
            { id: "eid29", tween: [ "style", "${_drops}", "left", '102px', { fromValue: '131px'}], position: 8250, duration: 250, easing: "easeInOutQuad" },
            { id: "eid31", tween: [ "style", "${_drops}", "left", '101px', { fromValue: '102px'}], position: 8500, duration: 3000, easing: "easeInOutQuad" },
            { id: "eid38", tween: [ "style", "${_drops}", "left", '177px', { fromValue: '101px'}], position: 11647, duration: 492 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-126484");

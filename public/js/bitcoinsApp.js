var socket;
var creditCardFee = 1.105;
var countryCodes = {
    AF:	 "AFG",
    AX : "ALA",
    AL : "ALB",
    DZ : "DZA",
    AS : "ASM",
    AD : "AND",
    AO : "AGO",
    AI : "AIA",
    AQ : "ATA",
    AG : "ATG",
    AR : "ARG",
    AM : "ARM",
    AW : "ABW",
    AU : "AUS",
    AT : "AUT",
    AZ : "AZE",
    BS : "BHS",
    BH : "BHR",
    BD : "BGD",
    BB : "BRB",
    BY : "BLR",
    BE : "BEL",
    BZ : "BLZ",
    BJ : "BEN",
    BM : "BMU",
    BT : "BTN",
    BO : "BOL",
    BA : "BIH",
    BW : "BWA",
    BV : "BVT",
    BR : "BRA",
    VG : "VGB",
    IO : "IOT",
    BN : "BRN",
    BG : "BGR",
    BF : "BFA",
    BI : "BDI",
    KH : "KHM",
    CM : "CMR",
    CA : "CAN",
    CV : "CPV",
    KY : "CYM",
    CF : "CAF",
    TD : "TCD",
    CL : "CHL",
    CN : "CHN",
    HK : "HKG",
    MO : "MAC",
    CX : "CXR",
    CC : "CCK",
    CO : "COL",
    KM : "COM",
    CG : "COG",
    CD : "COD",
    CK : "COK",
    CR : "CRI",
    CI : "CIV",
    HR : "HRV",
    CU : "CUB",
    CY : "CYP",
    CZ : "CZE",
    DK : "DNK",
    DJ : "DJI",
    DM : "DMA",
    DO : "DOM",
    EC : "ECU",
    EG : "EGY",
    SV : "SLV",
    GQ : "GNQ",
    ER : "ERI",
    EE : "EST",
    ET : "ETH",
    FK : "FLK",
    FO : "FRO",
    FJ : "FJI",
    FI : "FIN",
    FR : "FRA",
    GF : "GUF",
    PF : "PYF",
    TF : "ATF",
    GA : "GAB",
    GM : "GMB",
    GE : "GEO",
    DE : "DEU",
    GH : "GHA",
    GI : "GIB",
    GR : "GRC",
    GL : "GRL",
    GD : "GRD",
    GP : "GLP",
    GU : "GUM",
    GT : "GTM",
    GG : "GGY",
    GN : "GIN",
    GW : "GNB",
    GY : "GUY",
    HT : "HTI",
    HM : "HMD",
    VA : "VAT",
    HN : "HND",
    HU : "HUN",
    IS : "ISL",
    IN : "IND",
    ID : "IDN",
    IR : "IRN",
    IQ : "IRQ",
    IE : "IRL",
    IM : "IMN",
    IL : "ISR",
    IT : "ITA",
    JM : "JAM",
    JP : "JPN",
    JE : "JEY",
    JO : "JOR",
    KZ : "KAZ",
    KE : "KEN",
    KI : "KIR",
    KP : "PRK",
    KR : "KOR",
    KW : "KWT",
    KG : "KGZ",
    LA : "LAO",
    LV : "LVA",
    LB : "LBN",
    LS : "LSO",
    LR : "LBR",
    LY : "LBY",
    LI : "LIE",
    LT : "LTU",
    LU : "LUX",
    MK : "MKD",
    MG : "MDG",
    MW : "MWI",
    MY : "MYS",
    MV : "MDV",
    ML : "MLI",
    MT : "MLT",
    MH : "MHL",
    MQ : "MTQ",
    MR : "MRT",
    MU : "MUS",
    YT : "MYT",
    MX : "MEX",
    FM : "FSM",
    MD : "MDA",
    MC : "MCO",
    MN : "MNG",
    ME : "MNE",
    MS : "MSR",
    MA : "MAR",
    MZ : "MOZ",
    MM : "MMR",
    NA : "NAM",
    NR : "NRU",
    NP : "NPL",
    NL : "NLD",
    AN : "ANT",
    NC : "NCL",
    NZ : "NZL",
    NI : "NIC",
    NE : "NER",
    NG : "NGA",
    NU : "NIU",
    NF : "NFK",
    MP : "MNP",
    NO : "NOR",
    OM : "OMN",
    PK : "PAK",
    PW : "PLW",
    PS : "PSE",
    PA : "PAN",
    PG : "PNG",
    PY : "PRY",
    PE : "PER",
    PH : "PHL",
    PN : "PCN",
    PL : "POL",
    PT : "PRT",
    PR : "PRI",
    QA : "QAT",
    RE : "REU",
    RO : "ROU",
    RU : "RUS",
    RW : "RWA",
    BL : "BLM",
    SH : "SHN",
    KN : "KNA",
    LC : "LCA",
    MF : "MAF",
    PM : "SPM",
    VC : "VCT",
    WS : "WSM",
    SM : "SMR",
    ST : "STP",
    SA : "SAU",
    SN : "SEN",
    RS : "SRB",
    SC : "SYC",
    SL : "SLE",
    SG : "SGP",
    SK : "SVK",
    SI : "SVN",
    SB : "SLB",
    SO : "SOM",
    ZA : "ZAF",
    GS : "SGS",
    SS : "SSD",
    ES : "ESP",
    LK : "LKA",
    SD : "SDN",
    SR : "SUR",
    SJ : "SJM",
    SZ : "SWZ",
    SE : "SWE",
    CH : "CHE",
    SY : "SYR",
    TW : "TWN",
    TJ : "TJK",
    TZ : "TZA",
    TH : "THA",
    TL : "TLS",
    TG : "TGO",
    TK : "TKL",
    TO : "TON",
    TT : "TTO",
    TN : "TUN",
    TR : "TUR",
    TM : "TKM",
    TC : "TCA",
    TV : "TUV",
    UG : "UGA",
    UA : "UKR",
    AE : "ARE",
    GB : "GBR",
    US : "USA",
    UM : "UMI",
    UY : "URY",
    UZ : "UZB",
    VU : "VUT",
    VE : "VEN",
    VN : "VNM",
    VI : "VIR",
    WF : "WLF",
    EH : "ESH",
    YE : "YEM",
    ZM : "ZMB",
    ZW : "ZWE"
}

var countryFromCode = {
    AF : "Afghanistan",
    AL : "Albania",
    DZ : "Algeria",
    AS : "American Samoa",
    AD : "Andorra",
    AO : "Angola",
    AI : "Anguilla",
    AQ : "Antarctica",
    AG : "Antigua and Barbuda",
    AR : "Argentina",
    AM : "Armenia",
    AW : "Aruba",
    AU : "Australia",
    AT : "Austria",
    AZ : "Azerbaijan",
    BS : "Bahamas",
    BH : "Bahrain",
    BD : "Bangladesh",
    BB : "Barbados",
    BY : "Belarus",
    BE : "Belgium",
    BZ : "Belize",
    BJ : "Benin",
    BM : "Bermuda",
    BT : "Bhutan",
    BO : "Bolivia",
    BA : "Bosnia and Herzegovina",
    BW : "Botswana",
    BV : "Bouvet Island (Bouvetoya)",
    BR : "Brazil",
    IO : "British Indian Ocean Territory (Chagos Archipelago)",
    VG : "British Virgin Islands",
    BN : "Brunei Darussalam",
    BG : "Bulgaria",
    BF : "Burkina Faso (was Upper Volta)",
    BI : "Burundi",
    KH : "Cambodia",
    CM : "Cameroon",
    CA : "Canada",
    CV : "Cape Verde",
    KY : "Cayman Islands",
    CF : "Central African Republic",
    TD : "Chad",
    CL : "Chile",
    CN : "China",
    CX : "Christmas Island",
    CC : "Cocos (Keeling) Islands",
    CO : "Colombia",
    KM : "Comoros",
    CD : "Congo",
    CG : "Congo",
    CK : "Cook Islands",
    CR : "Costa Rica",
    CI : "Cote D'Ivoire (Ivory Coast)",
    CU : "Cuba",
    CY : "Cyprus",
    CZ : "Czech Republic",
    DK : "Denmark",
    DJ : "Djibouti",
    DM : "Dominica",
    DO : "Dominican Republic",
    EC : "Ecuador",
    EG : "Egypt",
    SV : "El Salvador",
    GQ : "Equatorial Guinea",
    ER : "Eritrea",
    EE : "Estonia",
    ET : "Ethiopia",
    FO : "Faeroe Islands",
    FK : "Falkland Islands (Malvinas)",
    FJ : "Fiji",
    FI : "Finland",
    FR : "France",
    GF : "French Guiana",
    PF : "French Polynesia",
    TF : "French Southern Territories",
    GA : "Gabon",
    GM : "Gambia",
    GE : "Georgia",
    DE : "Germany",
    GH : "Ghana",
    GI : "Gibraltar",
    GR : "Greece",
    GL : "Greenland",
    GD : "Grenada",
    GP : "Guadaloupe",
    GU : "Guam",
    GT : "Guatemala",
    GN : "Guinea",
    GW : "Guinea-Bissau",
    GY : "Guyana",
    HT : "Haiti",
    HM : "Heard and McDonald Islands",
    VA : "Holy See (Vatican City State)",
    HN : "Honduras",
    HK : "Hong Kong",
    HR : "Hrvatska (Croatia)",
    HU : "Hungary",
    IS : "Iceland",
    IN : "India",
    ID : "Indonesia",
    IR : "Iran",
    IQ : "Iraq",
    IE : "Ireland",
    IL : "Israel",
    IT : "Italy",
    JM : "Jamaica",
    JP : "Japan",
    JO : "Jordan",
    KZ : "Kazakhstan",
    KE : "Kenya",
    KI : "Kiribati",
    KP : "Korea",
    KR : "Korea",
    KW : "Kuwait",
    KG : "Kyrgyz Republic",
    LA : "Lao People's Democratic Republic",
    LV : "Latvia",
    LB : "Lebanon",
    LS : "Lesotho",
    LR : "Liberia",
    LY : "Libyan Arab Jamahiriya",
    LI : "Liechtenstein",
    LT : "Lithuania",
    LU : "Luxembourg",
    MO : "Macao",
    MK : "Macedonia",
    MG : "Madagascar",
    MW : "Malawi",
    MY : "Malaysia",
    MV : "Maldives",
    ML : "Mali",
    MT : "Malta",
    MH : "Marshall Islands",
    MQ : "Martinique",
    MR : "Mauritania",
    MU : "Mauritius",
    YT : "Mayotte",
    MX : "Mexico",
    FM : "Micronesia",
    MD : "Moldova",
    MC : "Monaco",
    MN : "Mongolia",
    MS : "Montserrat",
    MA : "Morocco",
    MZ : "Mozambique",
    MM : "Myanmar",
    NA : "Namibia",
    NR : "Nauru",
    NP : "Nepal",
    AN : "Netherlands Antilles",
    NL : "Netherlands",
    NC : "New Caledonia",
    NZ : "New Zealand",
    NI : "Nicaragua",
    NE : "Niger",
    NG : "Nigeria",
    NU : "Niue",
    NF : "Norfolk Island",
    MP : "Northern Mariana Islands",
    NO : "Norway",
    OM : "Oman",
    PK : "Pakistan",
    PW : "Palau",
    PS : "Palestinian Territory, Occupied",
    PA : "Panama",
    PG : "Papua New Guinea",
    PY : "Paraguay",
    PE : "Peru",
    PH : "Philippines",
    PN : "Pitcairn Island",
    PL : "Poland",
    PT : "Portugal",
    PR : "Puerto Rico",
    QA : "Qatar",
    RE : "Reunion",
    RO : "Romania",
    RU : "Russian Federation",
    RW : "Rwanda",
    SH : "St. Helena",
    KN : "St. Kitts and Nevis",
    LC : "St. Lucia",
    PM : "St. Pierre and Miquelon",
    VC : "St. Vincent and the Grenadines",
    WS : "Samoa",
    SM : "San Marino",
    ST : "Sao Tome and Principe",
    SA : "Saudi Arabia",
    SN : "Senegal",
    CS : "Serbia and Montenegro",
    SC : "Seychelles",
    SL : "Sierra Leone",
    SG : "Singapore",
    SK : "Slovakia",
    SI : "Slovenia",
    SB : "Solomon Islands",
    SO : "Somalia",
    ZA : "South Africa",
    GS : "South Georgia and the South Sandwich Islands",
    ES : "Spain",
    LK : "Sri Lanka",
    SD : "Sudan",
    SR : "Suriname",
    SJ : "Svalbard & Jan Mayen Islands",
    SZ : "Swaziland",
    SE : "Sweden",
    CH : "Switzerland",
    SY : "Syrian Arab Republic",
    TW : "Taiwan",
    TJ : "Tajikistan",
    TZ : "Tanzania",
    TH : "Thailand",
    TL : "Timor-Leste",
    TG : "Togo",
    TK : "Tokelau (Tokelau Islands)",
    TO : "Tonga",
    TT : "Trinidad and Tobago",
    TN : "Tunisia",
    TR : "Turkey",
    TM : "Turkmenistan",
    TC : "Turks and Caicos Islands",
    TV : "Tuvalu",
    VI : "US Virgin Islands",
    UG : "Uganda",
    UA : "Ukraine",
    AE : "United Arab Emirates",
    GB : "United Kingdom of Great Britain & N. Ireland",
    UM : "United States Minor Outlying Islands",
    US : "United States of America",
    UY : "Uruguay",
    UZ : "Uzbekistan",
    VU : "Vanuatu",
    VE : "Venezuela",
    VN : "Vietnam",
    WF : "Wallis and Futuna Islands",
    EH : "Western Sahara",
    YE : "Yemen",
    ZM : "Zambia",
    ZW : "Zimbabwe"
}

var stateCode = {
    Alabama : "AL",
    Alaska : "AK",
    Arizona : "AZ",
    Arkansas : "AR",
    California : "CA",
    Colorado : "CO",
    Connecticut : "CT",
    Delaware : "DE",
    Florida : "FL",
    Georgia : "GA",
    Hawaii : "HI",
    Idaho : "ID",
    Illinois : "IL",
    Indiana : "IN",
    Iowa : "IA",
    Kansas : "KS",
    Kentucky : "KY",
    Louisiana : "LA",
    Maine : "ME",
    Maryland : "MD",
    Massachusetts : "MA",
    Michigan : "MI",
    Minnesota : "MN",
    Mississippi : "MS",
    Missouri : "MO",
    Montana : "MT",
    Nebraska : "NE",
    Nevada : "NV",
    "New Hampshire" : "NH",
    "New Jersey" : "NJ",
    "New Mexico" : "NM",
    "New York" : "NY",
    "North Carolina" : "NC",
    "North Dakota" : "ND",
    "Ohio" : "OH",
    "Oklahoma" : "OK",
    "Oregon" : "OR",
    "Pennsylvania" : "PA",
    "Rhode Island" : "RI",
    "South Carolina" : "SC",
    "South Dakota" : "SD",
    Tennessee : "TN",
    Texas : "TX",
    Utah : "UT",
    Vermont : "VT",
    Virginia : "VA",
    Washington : "WA",
    "West Virginia" : "WV",
    Wisconsin : "WI",
    Wyoming : "WY",
    "American Samoa" : "AS",
    "District of Columbia" : "DC",
    "Federated States of Micronesia" : "FM",
    Guam : "GU",
    "Marshall Islands" : "MH",
    "Northern Mariana Islands" : "MP",
    Palau : "PW",
    "Puerto Rico" : "PR",
    "Virgin Islands" : "VI",
    "Armed Forces Americas" : "AA",
    "Armed Forces" : "AE",
    "Armed Forces Pacific" : "AP"
}

App = Ember.Application.create({
    //LOG_TRANSITIONS: true,
    rootElement: '#app',
    ready: function(){
        socket = io.connect();
        socketIOListeners();
    }
});

App.Router.map(function() {
    this.route('about');
    this.route('register');
    this.route('login');
    this.route('loader');
    this.route('buyBitcoin');
    this.route('inputEmail');
    this.route('termsConditions');
    this.route('recoverPassword');
    this.route('administrateBankAccounts');
    this.resource('profile');
});

// Routes
App.AuthenticatedRoute = Ember.Route.extend({
    beforeModel: function(transition) {
        if (!this.controllerFor('application').get('currentUserId')) {
            var applicationController = this.controllerFor('application');
            applicationController.set('attemptedTransition', transition);
            this.transitionTo('index');
        }
    }
});

App.IndexRoute = Ember.Route.extend({
    beforeModel: function(transition) {
        if (this.controllerFor('application').get('currentUserId')) {

        }
    },
    model: function() {
    },
    afterModel: function(){
    },
    setupController: function(controller, model) {
        controller.set('model', model);
    }
});

App.BuyBitcoinRoute = App.AuthenticatedRoute.extend({
    model: function(){
        var self = this;
        return App.User.find(self.controllerFor('application').get('currentUserId'));
    },
    setupController: function(controller,model){
        controller.set('contented', model);
        socket.emit('getUSDBTCRate',{});
    },
    renderTemplate: function() {
        this.render('buyBitcoin');
    }
});

App.InputEmailRoute = Ember.Route.extend({
});

App.RecoverPasswordRoute = Ember.Route.extend({
});

App.TermsConditionsRoute = Ember.Route.extend({
    renderTemplate: function() {
        this.render('termsConditions');
    }
});

App.LoaderRoute = Ember.Route.extend({
    renderTemplate: function() {
        this.render('loader');
    }
});

App.ProfileRoute = App.AuthenticatedRoute.extend({
    model: function() {
        socket.emit('user:acquire', {});
        var id = this.controllerFor('application').get('currentUserId');
        this.controllerFor('profile').set('jumioActive', false);
        return App.User.find(id);
    }
});

App.ApplicationRoute = Ember.Route.extend({
    beforeModel: function(){

    },
    model: function() {

    },
    setupController: function (controller, model) {
        var self = this;
        var store = DS.get("defaultStore");
        Ember.Instrumentation.subscribe("twoFactorRegister.notificationOccured", {
            before: function(name, timestamp, payload) {
                var userId = self.controllerFor('application').get('currentUserId');
                if (userId){
                    var user = App.User.find(userId);
                    user.set('hasTwoFactor', payload.hasTwoFactor);
                    self.store.commit();
                    user.set('twoFactorDate', payload.twoFactorDate);
                    self.store.commit();
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("bankAccounts.notificationOccured", {
            before: function(name, timestamp, payload) {
                var walletId = self.controllerFor('application').get('currentUserId');
                if (walletId){
                    var wallet = App.User.find(walletId);
                    wallet.set('bankAccounts', payload);
                    self.store.commit();
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("auth.notificationOccured", {
            before: function(name, timestamp, payload) {
                var alreadyLogged = self.controllerFor('application').get('currentUserId');
                self.controllerFor('application').set('currentUserId', payload.session.passport.user);
                self.controllerFor('application').set('haveWallet', payload.haveWallet);
                var attemptedTransition = self.controllerFor('application').get('attemptedTransition');
                if (attemptedTransition) {
                    attemptedTransition.retry();
                    self.controllerFor('application').set('attemptedTransition', null);
                } else if (!alreadyLogged){
                    self.transitionTo('profile');
                }
                $('#app').fadeIn();
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("logout.notificationOccured", {
            before: function(name, timestamp, payload) {
                if (self.controllerFor('application').get('currentUserId')){
                    window.location = "/";
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("user.notificationOccured", {
            before: function(name, timestamp, context) {
                self.controllerFor('application').set('haveWallet', context.haveWallet);
                self.controllerFor('application').set('verified', context.verified);
                var facebookImage = '';
                self.store.commit();
                if (context.facebookData){
                    if(context.facebookData.username){
                        facebookImage = 'https://graph.facebook.com/'+context.facebookData.username+'/picture?type=large';
                    }
                }

                context.facebookImage = facebookImage;

                context.id = context._id;

                store.adapterForType(App.User).load(store, App.User, context);
                self.store.commit();
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("jumioVerified.notificationOccured", {
            before: function(name, timestamp, payload) {
                if (self.controllerFor('application').get('currentUserId')){
                    if (App.User.find(self.controllerFor('application').get('currentUserId') && App.User.find(self.controllerFor('application').get('currentUserId')))) {
                        App.User.find(self.controllerFor('application').get('currentUserId')).set('jumioVerified', payload.jumioVerified);
                        self.store.commit();
                        App.User.find(self.controllerFor('application').get('currentUserId')).set('jumioData', payload.jumioData);
                        self.store.commit();
                    }
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("bitcoinUSDQuote.notificationOccured", {
            before: function(name, timestamp, payload) {
                if (self.controllerFor('buyBitcoin')){
                    self.controllerFor('buyBitcoin').set('bitcoinUSDQuote', payload.bitcoinUSDQuote);
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("xrpReserve.notificationOccured", {
            before: function(name, timestamp, payload) {
                if (self.controllerFor('buyBitcoin')){
                    self.controllerFor('buyBitcoin').set('xrpReserve', payload.xrpReserve);
                    var reserveMet = (payload.xrpBalance >= payload.xrpReserve) ? 'met' : 'not met';
                    self.controllerFor('buyBitcoin').set('reserveMet', reserveMet);
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("reservesInfo.notificationOccured", {
            before: function(name, timestamp, payload) {
                if (self.controllerFor('buyBitcoin')){
                    var reserve = Number(payload.result.account_data.Balance) / 1000000 - 5000;
                    if (reserve < 0){
                        reserve = 0;
                    }
                    self.controllerFor('buyBitcoin').set('reserves', reserve);
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("fbFriends.notificationOccured", {
            before: function(name, timestamp, payload) {
                controller.send('fbFriendsNotificationOccured', payload);
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("linkedinConnections.notificationOccured", {
            before: function(name, timestamp, payload) {
                controller.send('linkedinConnectionsNotificationOccured', payload);
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("XRPDollarConversion.notificationOccured", {
            before: function(name, timestamp, payload) {
                var counter = 0;
                payload.result.offers.forEach(function(offer){
                    if (offer.taker_pays_funded || offer.taker_gets_funded){
                        counter += Number(offer.taker_pays_funded.value)/Number(offer.taker_gets_funded);
                    }
                    else {
                        counter += Number(offer.quality);
                    }
                });

                var averageRate = counter/payload.result.offers.length * 1000000;
                if (self.controllerFor('buyBitcoin')){
                    self.controllerFor('buyBitcoin').set('conversionRate', averageRate);
                }

            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("uxRate.notificationOccured", {
            before: function(name, timestamp, payload) {
                if (payload){
                    if (self.controllerFor('buyBitcoin')){
                        self.controllerFor('buyBitcoin').set('conversionRate', payload.rate);
                    }
                }
            },
            after: function() {}
        });
        Ember.Instrumentation.subscribe("uxBTCRate.notificationOccured", {
            before: function(name, timestamp, payload) {
                if (payload){
                    if (self.controllerFor('buyBitcoin')){
                        self.controllerFor('buyBitcoin').set('conversionBTCRate', payload.rate);
                    }
                }
            },
            after: function() {}
        });
    }
});

App.AdministrateBankAccountsRoute = App.AuthenticatedRoute.extend({
    renderTemplate: function() {
        this.render('administrateBankAccounts');
    },
    model: function(params) {
        var currentUserId = this.controllerFor('application').get('currentUserId');
        return App.User.find(currentUserId);
    },
    setupController: function(controller,model){
        controller.set('user', model);
    }
});

App.ApplicationController = Ember.Controller.extend({
    loginFailed: false,
    loginProcessing: false,
    currentUserId: null,
    profileImageUpload: false,
    twoFactor: false,
    chatOpen: false,
    email : null,
    model : null,
    password: null,
    verified : null,
    haveWallet: null,

    doNothing: function(){},
    profileComplete: function() {
        return this.get('verified') && this.get('haveWallet');
    }.property('verified', 'haveWallet'),
    fbFriendsNotificationOccured: function(context) {
        var self = this;
        var store = DS.get("defaultStore");
        var facebookids = [];
        for (i = 0; i < context.length; i++) {
            store.adapterForType(App.FbFriends).load(store, App.FbFriends, context[i]);
            self.store.commit();
        }
    },
    linkedinConnectionsNotificationOccured: function(context) {
        var self = this;
        var store = DS.get("defaultStore");
        for (i = 0; i < context.values.length; i++) {
            store.adapterForType(App.LinkedinConnections).load(store, App.LinkedinConnections, context.values[i]);
            self.store.commit();
        }
    },
    login: function() {
        this.setProperties({
            loginProcessing: true
        });

        var self = this, data = this.getProperties('email', 'password', 'verify');

        $.post('/login', data)
            .fail(function(response) {
                $('#noticeSlide').attr('class', 'alert alert-error').html('Email and password mismatch.').fadeIn(300, function() {
                    setTimeout(function(){$('#noticeSlide').fadeOut(300);}, 1500);
                });
            })
            .done(function(response) {
                if (response.message == 'Must verify.'){
                    self.set('twoFactor', true);
                    self.transitionToRoute('login');
                }
                else if (response.message == 'Login successful.'){
                    $('#noticeSlide').attr('class', 'alert alert-success').html('Authentication successful.').fadeIn(300, function() {
                        setTimeout(function(){$('#noticeSlide').fadeOut(300);}, 1500);
                    });
                    if (!socket.socket.connected) {
                        socket.socket.connect();
                    }
                    else {
                        socket.disconnect();
                    }
                }
                else {
                    $('#noticeSlide').attr('class', 'alert alert-error').html(response.message).fadeIn(300, function() {
                        setTimeout(function(){$('#noticeSlide').fadeOut(300);}, 1500);
                    });
                }
            }.bind(this));

        this.set("loginProcessing", false);
    },
    logout: function() {
        var data = null;
        $.post('/logout', data).done(function(response) {
            if (response.success) {
                window.location = "/";
            }
        });
    },
    dwollaLogin: function () {
        var left = (screen.width/2)-(400/2);
        var top = (screen.height/2)-(350/2);
        var myWindow = window.open(window.location.protocol + "//" + window.location.host + '/auth/dwolla', 'Dwolla Login', 'width=400, height=350, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

        var pollTimer = window.setInterval(function() {
            if (myWindow.closed !== false) {
                window.clearInterval(pollTimer);
                if (!socket.socket.connected) {
                    socket.socket.connect();
                }
                else {
                    socket.disconnect();
                }
            }
        }, 200);

    },
    facebookLogin: function () {
        var left = (screen.width/2)-(1100/2);
        var top = (screen.height/2)-(600/2);
        var myWindow = window.open(window.location.protocol + "//" + window.location.host + '/auth/facebook', 'Facebook Login', 'width=1100, height=600, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

        var pollTimer = window.setInterval(function() {
            if (myWindow.closed !== false) {
                window.clearInterval(pollTimer);
                if (!socket.socket.connected) {
                    socket.socket.connect();
                }
                else {
                    socket.disconnect();
                }
            }
        }, 200);

    },
    linkedinLogin: function () {
        var left = (screen.width/2)-(400/2);
        var top = (screen.height/2)-(700/2);
        var myWindow = window.open(window.location.protocol + "//" + window.location.host + '/auth/linkedin', 'LinkedIn Login', 'width=400, height=700, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

        var pollTimer = window.setInterval(function() {
            if (myWindow.closed !== false) {
                window.clearInterval(pollTimer);
                if (!socket.socket.connected) {
                    socket.socket.connect();
                }
                else {
                    socket.disconnect();
                }
            }
        }, 200);

    },
    connect: function() {
        socket.socket.connect();
    }

});

App.LoginController = Ember.Controller.extend({
    needs: ['application'],
    emailBinding: 'controllers.application.email',
    passwordBinding: 'controllers.application.password',
    verifyBinding: 'controllers.application.verify'
});

App.IndexController = Ember.Controller.extend({
    needs: ['application']
});

App.BuyBitcoinController = Ember.Controller.extend({
    needs: ['application'],
    bitcoinAddress: null,
    amountBTC: null,
    isOtherAddress: false,
    address: null,
    recipient: null,
    enteredKey: null,
    userId: null,
    card: null,
    year: null,
    month: null,
    code: null,
    conversionBTCRate: null,
    conversionRate: null,
    validCreditCard: null,
    again: false,

    // TODO :
    // add more fields for address, name and social security
    // implement frontend and backend Netswipe
    // create confirmation messages on frontend and follow up emails on backend
    // complete credit card route
    // return RT exchange rates to frontend
    // do fee calculations and pegging
    // translate to Chinese
    validCreditCard: function(){
        var self = this;
        var indicator = balanced.card.validate({
            card_number: self.get('card'),
            expiration_month: self.get('month'),
            expiration_year: self.get('year'),
            security_code: self.get('code')
        });
        if ((self.get('user.jumioVerified') && isEmpty(indicator)) || (self.get('user.jumioVerified') && self.get('again'))){
            return true;
        }
        else {
            return false;
        }
    }.property('card', 'year', 'month', 'code', 'again', 'user.jumioVerified'),
    validNumber: function(){
        var self = this;
        return balanced.card.isCardNumberValid(self.get('card'));
    }.property('card'),
    validExpiry: function(){
        var self = this;
        return balanced.card.isExpiryValid(self.get('month'), self.get('year'));
    }.property('year', 'month'),
    validCode: function(){
        var self = this;
        return balanced.card.isSecurityCodeValid(self.get('card'), self.get('code'));
    }.property('code', 'card'),
    buyBitcoinCreditCard: function(){
        var self = this;
        if (self.get('validBitcoinAddress') && self.get('amountBTC') && Number(self.get('amountBTC')) > 0){
            var sendPacket = {};
            sendPacket.bitcoinAmount = self.get('amountBTC');
            sendPacket.code = self.get('code');
            sendPacket.card = self.get('card');
            sendPacket.month = self.get('month');
            sendPacket.year = self.get('year');
            sendPacket.again = self.get('again');
            var encodedBitcoinAddress = baseDecode(self.get('bitcoinAddress'), 'bitcoin');
            encodedBitcoinAddress = sjcl.codec.bytes.toBits(encodedBitcoinAddress);
            encodedBitcoinAddress = sjcl.codec.hex.fromBits(encodedBitcoinAddress).toUpperCase();
            sendPacket.bitcoinAddress = encodedBitcoinAddress;
            socket.emit('balancedCreditBitcoin', sendPacket);
            self.setProperties({
                amountBTC: null,
                bitcoinAddress: null,
                isOtherAddress: false,
                address: null,
                recipient: null,
                enteredKey: null,
                userId: null,
                card: null,
                year: null,
                month: null,
                code: null
            });
//                $.post('/netswipe', sendPacket)
//                    .fail(function() {
//                        $('#noticeSlide').attr('class', 'alert alert-error').html('Quote failed.').fadeIn(300, function() {
//                            setTimeout(function(){$('#noticeSlide').fadeOut(300);}, 1500);
//                        });
//                    })
//                    .done(function(response) {
//                        if (response.error){
//                            $('#noticeSlide').attr('class', 'alert alert-error').html(response.error).fadeIn(300, function() {
//                                setTimeout(function(){$('#noticeSlide').fadeOut(300);}, 1500);
//                            });
//                        }
//                        else {
//                            JumioClient.setVars({
//                                merchantAPIToken: "4b924ede-8d46-4a1d-a5ca-c56d6f3b26db",
//                                locale: "en",
//                                amount: response.price,
//                                currency: "USD",
//                                merchantPaymentReference: response.merchant,
//                                billingFullname: "",
//                                billingStreet: "",
//                                billingZipcode: "",
//                                billingCity: "",
//                                billingState: "",
//                                billingCountry: ""}).initForm("NETSWIPE");
//                        }
//                    }.bind(this));
        }
    },
    validAmountToBuy: function(){
        if (this.get('amountToBuy') && !isNaN(Number(this.get('amountToBuy')))){
            return true;
        }
        else {
            return false;
        }
    }.property('amountToBuy'),
    validBitcoinAddress: function(){
        var self = this;
        if (self.get('bitcoinAddress') && check_address(self.get('bitcoinAddress'))){
            return true;
        }
        else {
            return false;
        }
    }.property('bitcoinAddress'),
    priceBTCFormatted: function(){

    }.property(),
    conversionFormatted: function(){

    }.property()

});

App.IndexController = Ember.Controller.extend({
    needs: ['application'],

    walkthrough: function(){
        if ($('.walkthrough') && $('.walked') && $('.walkthrough').length && $('.walkthrough').length){
            $('.walkthrough').fadeIn();
            $('.walked').fadeOut();
        }
    }
});

App.AdministrateBankAccountsController = Ember.Controller.extend({
    needs: ['application'],
    name: null,
    routingNumber: null,
    bankAccount: null,
    validInfo: false,
    routingNumberValid: false,
    bankAccountValid: false,
    fullNameValid: false,
    paths: null,
    amounted: null,
    amountValid: false,
    reserves: null,
    user: null,
    recipient: null,
    enteredKey: null,
    cashOuts: null,
    userId: null,
    currentPage: 0,
    start: 0,
    rows: 5,
    isSelected: 1,
    bankAccountSelected: null,
    verifyOne: null,
    verifyTwo: null,
    verify: function(){
        var self = this;
        socket.emit('cashout:verifyBankAccount', {id: self.get('bankAccountSelected'),confirmOne: self.get('verifyOne'), confirmTwo: self.get('verifyTwo')});
    },
    addBankAccount: function(){
        var self = this;
        var bankInfo = {};
        bankInfo.name = self.get('recipient');
        bankInfo.routing_number = self.get('routingNumber');
        bankInfo.account_number = self.get('bankAccount');
        balanced.bankAccount.create(bankInfo, function(response) {
            switch (response.status) {
                case 201:
                    var finalPack = {};
                    finalPack.bankToken = response.data;
                    socket.emit('cashout:addBankAccount', finalPack);
                    self.setProperties({
                        paths: null,
                        amounted: null,
                        name: null,
                        bankAccount: null,
                        routingNumber: null,
                        routingNumberValid: false,
                        validInfo: false,
                        amountValid: false,
                        recipient: null,
                        enteredKey: null,
                        userId: null,
                        currentPage: 0,
                        start: 0,
                        rows: 5
                    });
                    break;
                case 400:
                    break;
                case 402:
                    break
                case 404:
                    break;
                case 500:
                    break;
            }
        });
    }
});

App.RegisterController = Ember.Controller.extend({
    registerError: null,

    register : function () {
        this.set('registerError', null);

        var self = this, data = this.getProperties('email', 'password', 'password2');

        $.post('/register', data).done(function(response) {
            if (response.error) {
                self.set('registerError', response.error);
            }
            else {
                $('#noticeSlide').fadeOut();
                if (!socket.socket.connected &&
                    !socket.socket.connecting) {
                    socket.socket.connect();
                }
                else {
                    socket.socket.connect();
                    socket.disconnect();
                }
            }
        });
    }
});

App.InputEmailController = Ember.ObjectController.extend({
    email: null,
    recover: function () {
        var self = this, data = this.getProperties('email');
        this.store.commit();
        socket.emit('passwordChange:request', data);
        socket.once('passwordChange', function(payload){
            if (payload.success){
                self.setProperties({ email: null });
                self.transitionToRoute('recoverPassword');
            }
        });
    }
});

App.RecoverPasswordController = Ember.ObjectController.extend({
    password: null,
    password2: null,
    recoverCode: null,

    changePassword: function () {
        var self = this, data = this.getProperties('password', 'password2', 'recoverCode');
        this.store.commit();
        socket.emit('passwordChange:loggedOut', data);

        this.setProperties({
            password: null,
            password2: null,
            recoverCode: null
        });

        self.transitionToRoute('login');
    }
});

App.ProfileController = Ember.ObjectController.extend({
    needs: ['application'],
    isViewing : true,
    isEditing : false,
    isEditingEmail : false,
    isChangingPassword : false,
    isEditingImage : false,
    jumioActive : false,
    isTwoFactor : false,
    country : null,
    cellPhone : null,
    userModel: null,
    password : null,
    password2 : null,
    formerPassword : null,
    accredited: null,
    founderCode: null,
    monthJoin: null,
    yearJoin: null,
    currentUserId: null,
    currentUserIdBinding: 'controllers.application.currentUserId',
    monthJoinBinding : 'controllers.application.monthJoin',
    yearJoinBinding : 'controllers.application.yearJoin',
    size: 200,

    twoFactorRegister: function(){
        var self = this;
        if ($('#country-code-0') && $('#country-code-0').length && $('#authy-cellphone') && $('#authy-cellphone').length){
            socket.emit('twoFactor:register', {country : $('#country-code-0').val(), phone : $('#authy-cellphone').val()});
            socket.once('twoFactorRegister',function(res){
                this.set("isEditing", false);
                this.set("isViewing", true);
                this.set("isEditingImage", false);
                this.set("isChangingPassword", false);
                this.set("isEditingEmail", false);
                this.set("isTwoFactor", false);
            });
        }
    },
    disableTwoFactor: function(){
        var self = this;
        socket.emit('twoFactor:remove', {});
        socket.once('twoFactorRegister',function(res){
            this.set("isEditing", false);
            this.set("isViewing", true);
            this.set("isEditingImage", false);
            this.set("isChangingPassword", false);
            this.set("isEditingEmail", false);
            this.set("isTwoFactor", false);
        });
    },
    gravatarUrl: function() {
        var email = this.get('email'),
            size = this.get('size');

        return 'https://www.gravatar.com/avatar/' + hex_md5(email) + '?s=' + size + '&d=retro';
    }.property('email', 'size'),
    founderDealed: function(){
        var self = this;
        socket.emit('founderInvite', {code: self.get('founderCode')});
    },
    verify: function () {
        var self = this, data = this.getProperties('verifyCode');
        socket.emit('verifyCode', data);
        this.store.commit();
    },
    activeJumio: function () {
        var self = this;
        if (!self.get('jumioActive')){
            self.set('jumioActive', true);
        }
        else {
            self.set('jumioActive', false);
        }
    },
    nullJumio: function () {
        var self = this;
    },
    disconnectJumio: function () {
        var self = this;
        socket.emit('disconnectJumio',{});
    },
    resendVerification: function(){
        this.set('verifyCode', '');
        socket.emit('resendVerification', {});
    },
    dqFacebook: function(){
        socket.emit('dqFacebook', {});
    },
    dqLinkedin: function(){
        socket.emit('dqLinkedin', {});
    },
    dqDwolla: function(){
        socket.emit('dqDwolla', {});
    },
    doesNotHaveName: function(){
        if (this.get('fullName') == 'Peercover Member'){
            return true;
        }
        else {
            return false;
        }
    }.property('fullName', 'facebookData', 'linkedinData', 'linkedinToken', 'facebookToken'),

    doesNotHaveAvatar: function() {
        return this.get('profileImage') == this.get('defaultImage');
    }.property('profileImage'),

    facebookPic: function (){
        var dataurls = this.get('facebookImage');
        socket.emit('user:update', {facebookImage : dataurls, currentProfileImageUrl : dataurls});
        this.set('currentProfileImageUrl', dataurls);
        this.store.commit();
    },

    linkedinPic: function (){
        var dataurls2 = this.get('linkedinImage');
        socket.emit('user:update', {linkedinImage : dataurls2, currentProfileImageUrl : dataurls2});
        this.set('currentProfileImageUrl', dataurls2);
        this.store.commit();
    },

    dwollaConnect: function () {
        var left = (screen.width/2)-(400/2);
        var top = (screen.height/2)-(350/2);
        myWindow = window.open(window.location.protocol + "//" + window.location.host + '/auth/dwolla', 'Dwolla Login', 'width=400, height=350, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

        var pollTimer = window.setInterval(function() {
            if (myWindow.closed !== false) {
                window.clearInterval(pollTimer);
                socket.emit('user:acquire');
            }
        }, 200);

    },

    facebookConnect: function () {
        var left = (screen.width/2)-(400/2);
        var top = (screen.height/2)-(600/2);
        myWindow = window.open(window.location.protocol + "//" + window.location.host + '/auth/facebook', 'Facebook Login', 'width=1100, height=600, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

        var pollTimer = window.setInterval(function() {
            if (myWindow.closed !== false) {
                window.clearInterval(pollTimer);
                socket.emit('user:acquire');
            }
        }, 200);

    },

    linkedinConnect: function () {
        var left = (screen.width/2)-(400/2);
        var top = (screen.height/2)-(700/2);
        myWindow = window.open(window.location.protocol + "//" + window.location.host + '/auth/linkedin', 'LinkedIn Login', 'width=400, height=700, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

        var pollTimer = window.setInterval(function() {
            if (myWindow.closed !== false) {
                window.clearInterval(pollTimer);
                socket.emit('user:acquire');
                socket.emit('getLinkedinConnections');
            }
        }, 200);

    },

    editProfile: function () {
        this.set("isEditing", true);
        this.set("isViewing", false);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
        this.set("isTwoFactor", false);
    },

    editEmail: function () {
        this.set('password', null);
        this.set("isEditing", false);
        this.set("isViewing", false);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", true);
        this.set("isTwoFactor", false);
    },

    changePassword: function () {
        this.set('password', null);
        this.set('password2', null);
        this.set('formerPassword', null);
        this.set("isEditing", false);
        this.set("isViewing", false);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", true);
        this.set("isEditingEmail", false);
        this.set("isTwoFactor", false);
    },

    savePassword: function () {
        this.set("isEditing", false);
        this.set("isViewing", true);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
    },

    stopEditing: function () {
        this.set("isEditing", false);
        this.set("isViewing", true);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
        this.set("isTwoFactor", false);
    },

    passwordSubmit: function () {
        var formerPassword = this.get('formerPassword')
            , password = this.get('password')
            , password2 = this.get('password2');
        socket.emit("passwordChange:loggedIn", { password : password, password2 : password2, formerPassword : formerPassword });
        this.set("isEditing", false);
        this.set("isViewing", true);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
    },

    emailSubmit: function () {
        var email = this.get('email')
            ,   password = this.get('password');
        this.store.commit();
        socket.emit("user:update", { email : email, password : password });
        this.store.commit();
        this.set("isEditing", false);
        this.set("isViewing", true);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
    },

    saveProfile: function () {
        var firstName = this.get('firstName')
            , lastName = this.get('lastName')
            , username = this.get('username');
        this.store.commit();
        socket.emit("user:update", { username : username, firstName : firstName, lastName : lastName });
        this.store.commit();
        this.set("isEditing", false);
        this.set("isViewing", true);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
        this.set("isTwoFactor", false);
    },

    editImage: function() {
        this.set("isEditing", false);
        this.set("isViewing", false);
        this.set("isEditingImage", true);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
        this.set("isTwoFactor", false);
    },

    twoFactor: function() {
        this.set("isEditing", false);
        this.set("isViewing", false);
        this.set("isEditingImage", false);
        this.set("isChangingPassword", false);
        this.set("isEditingEmail", false);
        this.set("isTwoFactor", true);
    },

    saveImage: function() {
        this.store.commit();
        var currentProfileImageUrl = this.get('currentProfileImageUrl');
        socket.emit("user:update", { currentProfileImageUrl : currentProfileImageUrl });
        this.store.commit();
        this.set("isViewing", true);
        this.set("isEditingImage", false);
        this.set("isEditingEmail", false);
        this.set("isChangingPassword", false);
    },

    selectFacebookImage: function() {
        this.set("currentProfileImageUrl", this.get("facebookImage"));
        this.store.commit();
    },

    selectGravatarImage: function() {
        this.set("currentProfileImageUrl", this.get("gravatarUrl"));
        this.store.commit();
    },

    selectLinkedinImage: function() {
        this.set("currentProfileImageUrl", this.get("linkedinImage"));
        this.store.commit();
    },

    selectUploadImage: function() {
        this.set("currentProfileImageUrl", this.get("uploadImage"));
        this.store.commit();
    },

    selectDefaultImage: function() {
        this.set("currentProfileImageUrl", 'https://s3-us-west-2.amazonaws.com/peercover/images/profileHolder.jpg');
        this.store.commit();
    }
});

// Models
App.Store = DS.Store.extend({
    adapter: 'DS.FixtureAdapter'
});

App.User = DS.Model.extend({
    email                  : DS.attr('string'),
    username               : DS.attr('string'),
    password               : DS.attr('string'),
    joinDate               : DS.attr('date'),

    firstName              : DS.attr('string'),
    lastName               : DS.attr('string'),

    uploadImage            : DS.attr('string'),
    facebookImage          : DS.attr('string'),
    linkedinImage          : DS.attr('string'),
    defaultImage           : 'https://s3-us-west-2.amazonaws.com/peercover/images/profileHolder.jpg',

    verified               : DS.attr('boolean'),
    jumioVerified          : DS.attr('boolean'),

    jumioData              : DS.attr('string'),
    facebookData           : DS.attr('string'),
    linkedinData           : DS.attr('string'),
    creditCard             : DS.attr('string'),
    bankAccounts           : DS.attr('string'),
    customer               : DS.attr('string'),
    dwollaData             : DS.attr('string'),
    hasTwoFactor           : DS.attr('boolean'),
    twoFactorDate          : DS.attr('date'),

    connectionsSent        : DS.attr('string'),
    connectionsReceived    : DS.attr('string'),
    bankAccount            : DS.attr('string'),
    connections            : DS.hasMany('App.User'),

    facebookToken          : DS.attr('string'),
    linkedinSecret         : DS.attr('string'),
    dwollaToken            : DS.attr('string'),
    publicKey              : DS.attr('string'),

    verificationBadge      : DS.attr('boolean'),
    founderDeal            : DS.attr('boolean'),
    superadmin             : DS.attr('boolean'),
    superadminDate         : DS.attr('date'),

    currentProfileImageUrl : DS.attr('string'),
    updateDate	           : DS.attr('date'),

    haveWallet             : DS.attr('boolean'),

    invoices               : DS.attr('string'),
    dues                   : DS.attr('string'),

    dailyInstantLimit      : DS.attr('number'),
    dailyInstantDate       : DS.attr('date'),

    referredByUser         : DS.attr('string'),
    invitedByUser          : DS.attr('string'),

    fbfriends              : DS.hasMany('App.FbFriends'),
    inconnections          : DS.hasMany('App.LinkedinConnections'),

    fullName: function() {
        var fullName;
        if (this.get('firstName') || this.get('lastName')){
            fullName = [this.get('firstName'), this.get('lastName')].join(' ').trim();
        }
        else if (typeof this.get('linkedinData') != 'undefined' && this.get('linkedinData') != null && this.get('linkedinData').firstName && this.get('linkedinData').lastName){
            fullName = [this.get('linkedinData').firstName, this.get('linkedinData').lastName].join(' ').trim();
        }
        else if (this.get('facebookData')){
            fullName = this.get('facebookData').name;
        }
        else if (this.get('dwollaData')){
            fullName = this.get('dwollaData').Name;
        }
        return fullName ? fullName : 'Peercover Member';
    }.property('firstName', 'lastName', 'facebookData', 'linkedinData', 'dwollaData'),

    profileImage: function() {
        var currentProfileImageUrl;
        if (this.get('currentProfileImageUrl')){
            currentProfileImageUrl = this.get('currentProfileImageUrl');
        }
        else if (this.get('uploadImage')){
            currentProfileImageUrl = this.get('currentProfileImageUrl');
        }
        else if (this.get('linkedinImage')){
            currentProfileImageUrl = this.get('linkedinImage');
        }
        else if (this.get('facebookImage')){
            currentProfileImageUrl = this.get('facebookImage');
        }
        else {
            currentProfileImageUrl = this.get('currentProfileImageUrl');
        }
        var defaultImage = this.get('defaultImage');
        return currentProfileImageUrl ? currentProfileImageUrl : defaultImage;
    }.property('currentProfileImageUrl'),

    profileImageBackgroundStyle: function() {
        return 'background-image: url("' + this.get('profileImage') + '");';
    }.property('currentProfileImageUrl'),

    facebookProfileLink: function() {
        if (this.get('facebookData') && this.get('facebookData').username){
            return 'https://www.facebook.com/' + this.get('facebookData').username;
        }
        else {
            return '';
        }
    }.property('facebookData'),

    activeFacebook: function() {
        if (this.get('facebookToken')){
            return 'width:40px !important;background-color: #18bc9c !important;background-image: none;';
        }
        else {
            return 'width:40px !important;background-color: #33485d !important;';
        }
    }.property('facebookToken'),

    activeLinkedin: function() {
        if (this.get('linkedinSecret')){
            return 'width:40px !important;background-color: #18bc9c !important;background-image: none;';
        }
        else {
            return 'width:40px !important;background-color: #33485d !important;';
        }
    }.property('linkedinSecret'),

    activeDwolla: function() {
        if (this.get('dwollaToken')){
            return 'width:40px !important;background-color: #18bc9c !important;background-image: none;';
        }
        else {
            return 'width:40px !important;background-color: #33485d !important;';
        }
    }.property('dwollaToken'),

    accredit: function() {
        if (this.get('dwollaToken') && this.get('linkedinSecret') && this.get('facebookToken')){
            return true;
        }
        else {
            return false;
        }
    }.property('dwollaToken', 'linkedinSecret', 'facebookToken')
});

App.FbFriends = DS.Model.extend({
    name                         : DS.attr('string'),
    gender                       : DS.attr('string'),
    locale                       : DS.attr('string'),
    facebook                     : DS.attr('string'),
    username                     : DS.attr('string'),

    fullName: function() {
        return this.get('name');
    }.property('name'),

    profileImageBackgroundStyle: function (){
        return 'background-image: url("' + 'https://graph.facebook.com/' + this.id + '/picture?type=large' + '");';
    }.property('id'),

    link: function (){
        return true;
    }.property('id')

});

App.LinkedinConnections = DS.Model.extend({
    headline                     : DS.attr('string'),
    firstName                    : DS.attr('string'),
    lastName                     : DS.attr('string'),
    pictureUrl                   : DS.attr('string'),
    publicProfileUrl             : DS.attr('string'),
    positions                    : DS.attr('string'),
    linkedin                     : DS.attr('string'),

    fullName: function() {
        return this.get('firstName') + ' ' + this.get('lastName');
    }.property('firstName', 'lastName'),

    profileImageBackgroundStyle: function (){
        if (this.get('pictureUrl')){
            return 'background-image: url("' + this.get('pictureUrl') + '");';
        }
        else {
            return 'background-image: url("https://s3-us-west-2.amazonaws.com/peercover/images/profileHolder.jpg");';
        }
    }.property('pictureUrl')
});

App.User.FIXTURES = [];

App.FbFriends.FIXTURES = [];

App.LinkedinConnections.FIXTURES = [];

// Views
App.Popover = Ember.View.extend({
    parentSelector: '',
    contentSelector: '',
    title: '',
    didInsertElement: function () {
        var self = this;
        if (self.parentSelector && self.contentSelector){
            $(self.parentSelector).popover({
                html: true,
                title: self.title,
                placement: 'bottom',
                content: function() {
                    if ($(self.contentSelector) && $(self.contentSelector).length){
                        var $content = $(self.contentSelector);
                        return $content.html();
                    }
                    else {
                        return '';
                    }
                }
            });
            $(window).on('resize', function () {
                if ($(self.parentSelector) && $(self.parentSelector).length){
                    $(self.parentSelector).popover('hide');
                }
            });
        }
        else {
            $(self.$('.getClassDotSelector').val()).popover({
                html: true,
                title: self.title,
                placement: 'bottom',
                content: function() {
                    var $content = $(self.$('.getContentSelector').val());
                    return $content.html();
                }
            });
            $(window).on('resize', function () {
                if ($('.label') && $('.label').length){
                    $('.label').popover('hide');
                }
            });
        }
    },
    close: function() {
        var self = this;
        if (self.parentSelector && self.contentSelector && $(this.parentSelector) && $(this.parentSelector).length){
            $(this.parentSelector).popover('hide');
        }
        else {
            if ($(self.$('.getClassDotSelector').val()) && $(self.$('.getClassDotSelector').val()).length){
                $(self.$('.getClassDotSelector').val()).popover('hide');
            }
        }
    }
});

App.TradeView = Ember.View.extend({
    didInsertElement: function() {
        new TradingView.MiniWidget({
            "container_id": "tv-miniwidget",
            "symbols": [

                "BITSTAMP:BTCUSD",
                "MTGOX:BTCEUR",
                "MTGOX:BTCCNY"

            ],
            "width": 352,
            "height": 300
        });
    }
});

App.FadeInBigView = Ember.View.extend({
    didInsertElement: function(){
        var self = this;
        if (self.$('.fadeInAll') && self.$('.fadeInAll').length){
            self.$('.fadeInAll').hide();
            self.$('.fadeInAll').fadeIn(500);
        }
    }
});

App.ApplicationView = Ember.View.extend({
    elementId: "application",
    didInsertElement: function() {
        var self = this;
        if (self.$("a") && self.$("a").length){
            self.$("a").tooltip();
        }
    }
});

App.ToolTipView = Ember.View.extend({
    country : '',
    flag : '',
    place: '',
    toolType: '',
    countryTitle: null,
    didInsertElement: function() {
        var self = this;
        if (self.$('.flagger') && self.$('.flagger').length && self.$('.tooled') && self.$('.tooled').length){
            self.$('.tooled').tooltip({title: self.country});
        }
        else if (self.get('place') == 'body'){

        }
        else if (self.get('toolType') == 'html'){
            self.$('.tooled').tooltip({html:true, title:self.get('countryTitle')});
        }
        else if (self.$('.tooled') && self.$('.tooled').length){
            self.$('.tooled').tooltip();
        }
        if (self.flag && self.$('.flagger') && self.$('.flagger').length){
            self.$('.flagger').addClass(self.flag);
        }
    },
    willDestroyElement: function(){
        var self = this;
        if (self.$('.tooled') && self.$('.tooled').length){
            self.$('.tooled').tooltip('destroy');
        }
        if (self.$('.flagger') && self.$('.flagger').length){
            self.$('.flagger').tooltip('destroy');
        }
    }
});

App.NXTView = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        if (self.$('.nxtForm') && self.$('.nxtForm').length){
            self.$('.nxtForm').keyup(function(){
                $.post('/validNXT', {address : self.get('controller.nextcoinAddress')})
                    .fail(function() {
                    })
                    .done(function(response) {
                        self.set('controller.validNXTPacket', response);
                    }.bind(this));
            });
        }
    },
    willDestroyElement: function(){
        var self = this;
        if (self.$('.nxtForm') && self.$('.nxtForm').length){
            self.$('.nxtForm').off();
        }
    }
});

App.MembersChatView = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        if (self.$('.outThere') && self.$('.outThere').length){
            self.$('.outThere').keyup(function(){
                var finalArray = [];
                if (self.$('.outThere') && self.$('.outThere').length){
                    var finalArray = [];
                    var query = '';
                    var splitQuery = self.$('.outThere').val().split(" ");
                    if (splitQuery && splitQuery[0]){
                        query = ' AND (';
                        var enumed = [];
                        splitQuery.forEach(function(quer){
                            if (quer){
                                enumed.push(quer);
                            }
                        });
                        for (i=0; i < enumed.length; i++) {
                            var lenEnd = enumed.length - 1;
                            if (i == lenEnd){
                                query += 'finalName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'firstName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'lastName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'username_t:' + valueEscape(enumed[i].trim()) + '* OR publicKey_s:' + valueEscape(enumed[i].trim()) + ' OR email_t:' + valueEscape(enumed[i].trim());
                            }
                            else {
                                query += 'finalName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'firstName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'lastName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'username_t:' + valueEscape(enumed[i].trim()) + '* OR ';
                            }
                        }
                        query += ')';
                    }
                    else {
                        query = '';
                    }
                    self.set('controller.currentPage', 1);
                    $.post('/connectionSearch', {query: query, listAll: true, page: self.get('controller.currentPage'), start: self.get('controller.currentPage') * self.get('controller.rows') - self.get('controller.rows'), rows: self.get('controller.rows'), searchTerm: self.$('.outThere').val()})
                        .fail(function() {
                        })
                        .done(function(response) {
                            if (response.totalResults < self.get('controller.totalResults') && $('.outThere') && $('.outThere').length && !$('.outThere').val()){

                            }
                            else {
                                if (self.get('controller.userToRefer') == response.searchTerm){
                                    self.set('controller.totalPages', response.pages);
                                    self.set('controller.totalResults', response.totalResults);
                                    self.set('controller.start', response.start);
                                    self.set('controller.sOLRresults', response.results);
                                }
                            }
                        }.bind(this));
                }
            });
        }
    },
    willDestroyElement: function(){
        var self = this;
        if (self.$('.outThere') && self.$('.outThere').length){
            self.$('.outThere').off();
        }
    }
});

App.MembersListView = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        if (self.$('.referInvite') && self.$('.referInvite').length){
            self.$('.referInvite').keyup(function(){
                var finalArray = [];
                if (self.$('.referInvite') && self.$('.referInvite').length){
                    var finalArray = [];
                    var query = '';
                    var splitQuery = self.$('.referInvite').val().split(" ");
                    if (splitQuery && splitQuery[0]){
                        query = ' AND (';
                        var enumed = [];
                        splitQuery.forEach(function(quer){
                            if (quer){
                                enumed.push(quer);
                            }
                        });
                        for (i=0; i < enumed.length; i++) {
                            var lenEnd = enumed.length - 1;
                            if (i == lenEnd){
                                query += 'finalName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'firstName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'lastName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'username_t:' + valueEscape(enumed[i].trim()) + '*';
                            }
                            else {
                                query += 'finalName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'firstName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'lastName_t:' + valueEscape(enumed[i].trim()) + '* OR ' + 'username_t:' + valueEscape(enumed[i].trim()) + '* OR ';
                            }
                        }
                        query += ')';
                    }
                    else {
                        query = '';
                    }
                    self.set('controller.currentPage', 1);
                    $.post('/userSearch', {query: query, listAll: true, page: self.get('controller.currentPage'), start: self.get('controller.currentPage') * self.get('controller.rows') - self.get('controller.rows'), rows: self.get('controller.rows'), searchTerm: self.$('.referInvite').val()})
                        .fail(function() {
                        })
                        .done(function(response) {
                            if (response.totalResults < self.get('controller.totalResults') && $('.referInvite') && $('.referInvite').length && !$('.referInvite').val()){

                            }
                            else {
                                if (self.get('controller.userToRefer') == response.searchTerm){
                                    self.set('controller.totalPages', response.pages);
                                    self.set('controller.totalResults', response.totalResults);
                                    self.set('controller.start', response.start);
                                    self.set('controller.sOLRresults', response.results);
                                }
                            }
                        }.bind(this));
                }
            });
        }
    },
    willDestroyElement: function(){
        var self = this;
        if (self.$('.referInvite') && self.$('.referInvite').length){
            self.$('.referInvite').off();
        }
    }
});


App.CashOutFormView = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        if (self.$('.cashoutInput') && self.$('.cashoutInput').length){
            self.$('.cashoutInput').keyup(function(){
                var validate = {error:'error'};
                var validateRouting = {error:'error'};
                if (self.get('controller.routingNumber') && self.get('controller.bankAccount') && self.get('controller.recipient')){
                    validate = balanced.bankAccount.validate({
                        routing_number: self.get('controller.routingNumber').toString(),
                        account_number: self.get('controller.bankAccount').toString(),
                        name: self.get('controller.recipient').toString()
                    });

                }
                if (self.get('controller.routingNumber')){
                    validateRouting = balanced.bankAccount.validateRoutingNumber(self.get('controller.routingNumber'));
                }
                if (isEmpty(validate) && self.get('controller.amounted') && Number(self.get('controller.amounted')) && !isNaN(Number(self.get('controller.amounted')))){
                    self.set('controller.validInfo', true);
                }
                else if (self.get('controller.enteredKey')){
                    self.set('controller.validInfo', true);
                }
                else {
                    self.set('controller.validInfo', false);
                }
                if (isEmpty(validateRouting)){
                    self.set('controller.routingNumberValid', true);
                }
                else {
                    self.set('controller.routingNumberValid', false);
                }
            });
        }
    },
    willDestroyElement: function(){
        var self = this;
        if (self.$('.cashoutInput') && self.$('.cashoutInput').length){
            self.$('.cashoutInput').off();
        }
        if (self.$('.amountInput') && self.$('.amountInput').length){
            self.$('.amountInput').off();
        }
    }
});

App.CountDownView = Ember.View.extend({
    countDownDate: '',
    handler: '',
    didInsertElement: function() {
        var self = this;
        var oldDateObj = new Date();
        var timerId = countdown(
            new Date(self.countDownDate),
            function(ts) {
                if (new Date() > new Date(self.countDownDate)){
                    if ($('.lastBet') && $('.lastBet').length){
                        $('.lastBet').html('<strong>No more bets.</strong>');
                        if ($('.predictionEnter') && $('.predictionEnter').length){
                            $('.predictionEnter').hide();
                        }
                    }
                    window.clearInterval(timerId);
                }
                else {
                    if (self.$('.countDown') && self.$('.countDown').length){
                        self.$('.countDown').html(ts.toHTML("strong"));
                    }
                }
            },
            countdown.YEARS|countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS);
        self.handler = timerId;
    },
    willDestroyElement: function(){
        var self = this;
        window.clearInterval(self.handler);
    }
});

App.ConnectionIndicator = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        if (self.$('.connection-indicator') && self.$('.connection-indicator').length){
            if (socket.socket.connected){
                self.$('.connection-indicator').attr( "style", "color: #16a085");
            }
            else {
                self.$('.connection-indicator').attr( "style", "color: #c0392b");
            }
            self.$(".connection-indicator").tooltip();
        }
    }
});

App.ProfileView = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        if (self.$("a") && self.$("a").length){
            self.$("a").tooltip();
        }
    }
});

// Helpers
Ember.Handlebars.registerBoundHelper('fromNowDate', function(date) {
    return moment(date).fromNow();
});

Ember.Handlebars.registerBoundHelper('dateLast', function(date) {
    return moment(date).fromNow();
});

Ember.Handlebars.registerBoundHelper('toTitle', function(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
});

Ember.Handlebars.registerBoundHelper('displayDate', function(date) {
    return moment(date).format('MMMM D, YYYY');
});

Ember.Handlebars.registerBoundHelper('displayDateTime', function(date) {
    return moment(date).format('MMMM D, YYYY, h:mm:ss a');
});

Ember.Handlebars.registerBoundHelper('roundBalance', function(balance) {
    var returnBalance = 0;
    if (typeof balance != 'number'){
        returnBalance = precise_round(Number(balance),2);
    }
    else {
        returnBalance = precise_round(balance,2);
    }
    return returnBalance;
});

Ember.Handlebars.registerBoundHelper('roundBalanceFour', function(balance) {
    var returnBalance = 0;
    if (typeof balance != 'number'){
        returnBalance = precise_round(Number(balance),4);
    }
    else {
        returnBalance = precise_round(balance,4);
    }
    return returnBalance;
});

Ember.Handlebars.registerBoundHelper('displayPercent', function(perc) {
    return (perc * 100).toFixed(0) + '%'
});

//When using the pluralize helper, it seems like a good idea to limit
//the scope to a particular context. Otherwise, later helpers seem to be
//able to infect the output.

Ember.Handlebars.registerBoundHelper('pluralize', function(value, options) {
    var count = options.hash.count;
    var string = value ? value : options.data.properties[0];
    return (count > 1 ? string+"s" : string);
});

App.RadioButton = Ember.View.extend({
    tagName : "input",
    type : "radio",
    attributeBindings : [ "name", "type", "value", "checked:checked:" ],
    click : function() {
        this.set("selection", this.$().val())
    },
    checked : function() {
        return this.get("value") == this.get("selection");
    }.property()
});

App.AuthyFormatView = Ember.View.extend({
    didInsertElement: function() {
        (function(){window.Authy={};if(document.getElementsByClassName==null){document.getElementsByClassName=function(className,parentElement){var child,children,elements,i,length;children=(parentElement||document.body).getElementsByTagName("*");elements=[];child=void 0;i=0;length=children.length;while(i<length){child=children[i];if((" "+child.className+" ").indexOf(" "+className+" ")!==-1){elements.push(child)}i++}return elements};HTMLDivElement.prototype.getElementsByClassName=function(className){return document.getElementsByClassName(className,this)}}window.Authy.UI=function(){var absolutePosFor,buildItem,countriesList,disableAutocompleteAuthyToken,findAndSetupCountries,getKeyCode,hideAutocompleteList,processKey13,processKey38,processKey40,self,setActive,setCountryField,setupAuthyTokenValidation,setupCellphoneValidation,setupCountriesDropdown,setupCountriesDropdownPosition,setupEvents,setupTooltip,setupTooltipPosition,tooltipMessage,tooltipTitle;self=this;tooltipTitle="Authy Help Tooltip";tooltipMessage='This is a help tooltip for your users. You can set the message by doing: authyUI.setTooltip("title", "message");';countriesList=[{country:"United States of America (+1)",code:"1"},{country:"Canada (+1)",code:"1"},{country:"Russia (+7)",code:"7"},{country:"Kazakhstan (+7)",code:"7"},{country:"Egypt (+20)",code:"20"},{country:"South Africa (+27)",code:"27"},{country:"Greece (+30)",code:"30"},{country:"Netherlands (+31)",code:"31"},{country:"Belgium (+32)",code:"32"},{country:"France (+33)",code:"33"},{country:"Spain (+34)",code:"34"},{country:"Hungary (+36)",code:"36"},{country:"Italy (+39)",code:"39"},{country:"Romania (+40)",code:"40"},{country:"Switzerland (+41)",code:"41"},{country:"Austria (+43)",code:"43"},{country:"United Kingdom (+44)",code:"44"},{country:"Guernsey (+44)",code:"44"},{country:"Isle of Man (+44)",code:"44"},{country:"Jersey (+44)",code:"44"},{country:"Denmark (+45)",code:"45"},{country:"Sweden (+46)",code:"46"},{country:"Norway (+47)",code:"47"},{country:"Poland (+48)",code:"48"},{country:"Germany (+49)",code:"49"},{country:"Peru (+51)",code:"51"},{country:"Mexico (+52)",code:"52"},{country:"Cuba (+53)",code:"53"},{country:"Argentina (+54)",code:"54"},{country:"Brazil (+55)",code:"55"},{country:"Chile (+56)",code:"56"},{country:"Colombia (+57)",code:"57"},{country:"Venezuela (+58)",code:"58"},{country:"Malaysia (+60)",code:"60"},{country:"Australia (+61)",code:"61"},{country:"Indonesia (+62)",code:"62"},{country:"Philippines (+63)",code:"63"},{country:"New Zealand (+64)",code:"64"},{country:"Singapore (+65)",code:"65"},{country:"Thailand (+66)",code:"66"},{country:"Japan (+81)",code:"81"},{country:"Korea (+South) (+82)",code:"82"},{country:"Vietnam (+84)",code:"84"},{country:"China (+86)",code:"86"},{country:"Turkey (+90)",code:"90"},{country:"India (+91)",code:"91"},{country:"Pakistan (+92)",code:"92"},{country:"Afghanistan (+93)",code:"93"},{country:"Sri Lanka (+94)",code:"94"},{country:"Myanmar (+95)",code:"95"},{country:"Iran (+98)",code:"98"},{country:"Morocco (+212)",code:"212"},{country:"Algeria (+213)",code:"213"},{country:"Tunisia (+216)",code:"216"},{country:"Libya (+218)",code:"218"},{country:"Gambia (+220)",code:"220"},{country:"Senegal (+221)",code:"221"},{country:"Mauritania (+222)",code:"222"},{country:"Mali Republic (+223)",code:"223"},{country:"Guinea (+224)",code:"224"},{country:"Ivory Coast (+225)",code:"225"},{country:"Burkina Faso (+226)",code:"226"},{country:"Niger (+227)",code:"227"},{country:"Togo (+228)",code:"228"},{country:"Benin (+229)",code:"229"},{country:"Mauritius (+230)",code:"230"},{country:"Liberia (+231)",code:"231"},{country:"Sierra Leone (+232)",code:"232"},{country:"Ghana (+233)",code:"233"},{country:"Nigeria (+234)",code:"234"},{country:"Chad (+235)",code:"235"},{country:"Central African Republic (+236)",code:"236"},{country:"Cameroon (+237)",code:"237"},{country:"Cape Verde Islands (+238)",code:"238"},{country:"Sao Tome and Principe (+239)",code:"239"},{country:"Gabon (+241)",code:"241"},{country:"Congo, Democratic Republ (+243)",code:"243"},{country:"Angola (+244)",code:"244"},{country:"Guinea-Bissau (+245)",code:"245"},{country:"Seychelles (+248)",code:"248"},{country:"Sudan (+249)",code:"249"},{country:"Rwanda (+250)",code:"250"},{country:"Ethiopia (+251)",code:"251"},{country:"Somalia (+252)",code:"252"},{country:"Djibouti (+253)",code:"253"},{country:"Kenya (+254)",code:"254"},{country:"Tanzania (+255)",code:"255"},{country:"Uganda (+256)",code:"256"},{country:"Burundi (+257)",code:"257"},{country:"Mozambique (+258)",code:"258"},{country:"Zambia (+260)",code:"260"},{country:"Madagascar (+261)",code:"261"},{country:"Reunion (+262)",code:"262"},{country:"Zimbabwe (+263)",code:"263"},{country:"Namibia (+264)",code:"264"},{country:"Malawi (+265)",code:"265"},{country:"Lesotho (+266)",code:"266"},{country:"Botswana (+267)",code:"267"},{country:"Swaziland (+268)",code:"268"},{country:"Mayotte Island (+269)",code:"269"},{country:"Aruba (+297)",code:"297"},{country:"Faroe Islands (+298)",code:"298"},{country:"Greenland (+299)",code:"299"},{country:"Gibraltar (+350)",code:"350"},{country:"Portugal (+351)",code:"351"},{country:"Luxembourg (+352)",code:"352"},{country:"Ireland (+353)",code:"353"},{country:"Iceland (+354)",code:"354"},{country:"Albania (+355)",code:"355"},{country:"Malta (+356)",code:"356"},{country:"Cyprus (+357)",code:"357"},{country:"Finland (+358)",code:"358"},{country:"Bulgaria (+359)",code:"359"},{country:"Lithuania (+370)",code:"370"},{country:"Latvia (+371)",code:"371"},{country:"Estonia (+372)",code:"372"},{country:"Moldova (+373)",code:"373"},{country:"Armenia (+374)",code:"374"},{country:"Belarus (+375)",code:"375"},{country:"Andorra (+376)",code:"376"},{country:"Monaco (+377)",code:"377"},{country:"San Marino (+378)",code:"378"},{country:"Ukraine (+380)",code:"380"},{country:"Serbia (+381)",code:"381"},{country:"Montenegro (+382)",code:"382"},{country:"Croatia (+385)",code:"385"},{country:"Slovenia (+386)",code:"386"},{country:"Bosnia-Herzegovina (+387)",code:"387"},{country:"Macedonia (+389)",code:"389"},{country:"Czech Republic (+420)",code:"420"},{country:"Slovakia (+421)",code:"421"},{country:"Liechtenstein (+423)",code:"423"},{country:"Falkland Islands (+500)",code:"500"},{country:"Belize (+501)",code:"501"},{country:"Guatemala (+502)",code:"502"},{country:"El Salvador (+503)",code:"503"},{country:"Honduras (+504)",code:"504"},{country:"Nicaragua (+505)",code:"505"},{country:"Costa Rica (+506)",code:"506"},{country:"Panama (+507)",code:"507"},{country:"Haiti (+509)",code:"509"},{country:"Guadeloupe (+590)",code:"590"},{country:"Bolivia (+591)",code:"591"},{country:"Guyana (+592)",code:"592"},{country:"Ecuador (+593)",code:"593"},{country:"French Guiana (+594)",code:"594"},{country:"Paraguay (+595)",code:"595"},{country:"Martinique (+596)",code:"596"},{country:"Suriname (+597)",code:"597"},{country:"Uruguay (+598)",code:"598"},{country:"Netherlands Antilles (+599)",code:"599"},{country:"Timor-Leste (+670)",code:"670"},{country:"Guam (+671)",code:"671"},{country:"Brunei (+673)",code:"673"},{country:"Nauru (+674)",code:"674"},{country:"Papua New Guinea (+675)",code:"675"},{country:"Tonga (+676)",code:"676"},{country:"Solomon Islands (+677)",code:"677"},{country:"Vanuatu (+678)",code:"678"},{country:"Fiji Islands (+679)",code:"679"},{country:"Cook Islands (+682)",code:"682"},{country:"Samoa (+685)",code:"685"},{country:"New Caledonia (+687)",code:"687"},{country:"French Polynesia (+689)",code:"689"},{country:"Korea (+North) (+850)",code:"850"},{country:"HongKong (+852)",code:"852"},{country:"Macau (+853)",code:"853"},{country:"Cambodia (+855)",code:"855"},{country:"Laos (+856)",code:"856"},{country:"Bangladesh (+880)",code:"880"},{country:"International (+882)",code:"882"},{country:"Taiwan (+886)",code:"886"},{country:"Maldives (+960)",code:"960"},{country:"Lebanon (+961)",code:"961"},{country:"Jordan (+962)",code:"962"},{country:"Syria (+963)",code:"963"},{country:"Iraq (+964)",code:"964"},{country:"Kuwait (+965)",code:"965"},{country:"Saudi Arabia (+966)",code:"966"},{country:"Yemen (+967)",code:"967"},{country:"Oman (+968)",code:"968"},{country:"Palestine (+970)",code:"970"},{country:"United Arab Emirates (+971)",code:"971"},{country:"Israel (+972)",code:"972"},{country:"Bahrain (+973)",code:"973"},{country:"Qatar (+974)",code:"974"},{country:"Bhutan (+975)",code:"975"},{country:"Mongolia (+976)",code:"976"},{country:"Nepal (+977)",code:"977"},{country:"Tajikistan (+992)",code:"992"},{country:"Turkmenistan (+993)",code:"993"},{country:"Azerbaijan (+994)",code:"994"},{country:"Georgia (+995)",code:"995"},{country:"Kyrgyzstan (+996)",code:"996"},{country:"Uzbekistan (+998)",code:"998"},{country:"Bahamas (+1242)",code:"1242"},{country:"Barbados (+1246)",code:"1246"},{country:"Anguilla (+1264)",code:"1264"},{country:"Antigua and Barbuda (+1268)",code:"1268"},{country:"Virgin Islands, British (+1284)",code:"1284"},{country:"Cayman Islands (+1345)",code:"1345"},{country:"Bermuda (+1441)",code:"1441"},{country:"Grenada (+1473)",code:"1473"},{country:"Turks and Caicos Islands (+1649)",code:"1649"},{country:"Montserrat (+1664)",code:"1664"},{country:"Saint Lucia (+1758)",code:"1758"},{country:"Dominica (+1767)",code:"1767"},{country:"St. Vincent and The Gren (+1784)",code:"1784"},{country:"Puerto Rico (+1787)",code:"1787"},{country:"Dominican Republic (+1809)",code:"1809"},{country:"Dominican Republic2 (+1829)",code:"1829"},{country:"Dominican Republic3 (+1849)",code:"1849"},{country:"Trinidad and Tobago (+1868)",code:"1868"},{country:"Saint Kitts and Nevis (+1869)",code:"1869"},{country:"Jamaica (+1876)",code:"1876"},{country:"Congo (+2420)",code:"2420"}];setupCellphoneValidation=function(){var cellPhone;cellPhone=document.getElementById("authy-cellphone");if(!cellPhone){return}cellPhone.onblur=function(){if(cellPhone.value!==""&&cellPhone.value.match(/^([0-9][0-9][0-9])\W*([0-9][0-9]{2})\W*([0-9]{0,5})$/)){return cellPhone.style.backgroundColor="white"}else{return cellPhone.style.backgroundColor="#F2DEDE"}}};setupAuthyTokenValidation=function(){var token;token=document.getElementById("authy-token");if(!token){return}token.onblur=function(){if(token.value!==""&&token.value.match(/^\d+$/)){return token.style.backgroundColor="white"}else{return token.style.backgroundColor="#F2DEDE"}}};disableAutocompleteAuthyToken=function(){var token;token=document.getElementById("authy-token");if(!token){return}token.setAttribute("autocomplete","off")};setupTooltip=function(){var authy_help,tooltip;authy_help=document.getElementById("authy-help");if(!authy_help){return}tooltip=document.createElement("div");tooltip.setAttribute("id","authy-tooltip");tooltip.innerHTML='<a id="authy-tooltip-close"></a><h3 class="tooltip-title">'+tooltipTitle+'</h3><p class="tooltip-content">'+tooltipMessage+"</p>";document.body.appendChild(tooltip);document.getElementById("authy-help").onclick=function(){tooltip=document.getElementById("authy-tooltip");setupTooltipPosition(this,tooltip);return tooltip.style.display="block"};document.getElementById("authy-tooltip-close").onclick=function(){return document.getElementById("authy-tooltip").style.display="none"};setupTooltipPosition(authy_help,tooltip)};setupTooltipPosition=function(helpLink,tooltip){var pos,tooltipLeft,tooltipTop;pos=absolutePosFor(helpLink);tooltipTop=pos[0];tooltipLeft=pos[1]+helpLink.offsetWidth+5;return tooltip.setAttribute("style","top:"+tooltipTop+"px;left:"+tooltipLeft+"px;")};processKey40=function(listId){var activeElement,caId,countriesArr,countriesDropdown,i,li,_i,_len;caId="countries-autocomplete-"+listId;countriesDropdown=document.getElementById(caId);countriesArr=countriesDropdown.getElementsByTagName("li");i=0;for(_i=0,_len=countriesArr.length;_i<_len;_i++){li=countriesArr[_i];if(li.className==="active"&&countriesArr.length>(i+1)){activeElement=countriesArr[i+1];li.className="";setActive(activeElement);self.autocomplete(activeElement,false);break}i++}return false};processKey38=function(listId){var activeElement,caId,countriesArr,i;caId="countries-autocomplete-"+listId;countriesArr=document.getElementById(caId).getElementsByTagName("li");i=countriesArr.length-1;while(i>=0){if(document.getElementById(caId).getElementsByTagName("li")[i].className==="active"){document.getElementById(caId).getElementsByTagName("li")[i].className="";activeElement=null;if(i===0){activeElement=document.getElementById(caId).getElementsByTagName("li")[countriesArr.length-1]}else{activeElement=document.getElementById(caId).getElementsByTagName("li")[i-1]}setActive(activeElement);self.autocomplete(activeElement,false);return false}i--}document.getElementById(caId).getElementsByTagName("li")[0].setAttribute("class","active")};processKey13=function(listId){var obj;obj=document.getElementById("countries-autocomplete-"+listId).getElementsByClassName("active")[0];self.autocomplete(obj,true);return false};setActive=function(liElement){var li,liElements,listId,_i,_len;listId=liElement.getAttribute("data-list-id");liElements=document.getElementById("countries-autocomplete-"+listId).getElementsByTagName("li");for(_i=0,_len=liElements.length;_i<_len;_i++){li=liElements[_i];li.className=""}return liElement.className="active"};setupEvents=function(countriesInput,listId){if(!countriesInput){return}countriesInput.onblur=function(event){return processKey13(listId)};countriesInput.onfocus=function(){var countriesDropdown;countriesDropdown=document.getElementById("countries-autocomplete-"+listId);setupCountriesDropdownPosition(countriesInput,countriesDropdown);countriesDropdown.style.display="block"};countriesInput.onkeyup=function(event){var keyID;document.getElementById("countries-autocomplete-"+listId).style.display="block";keyID=getKeyCode(event);switch(keyID){case 13:processKey13(listId);return false;case 40:if(processKey40(listId)===false){return false}break;case 38:if(processKey38(listId)===false){return false}}return self.searchItem(listId)};countriesInput.onkeypress=function(event){if(getKeyCode(event)===13){processKey13(listId);return false}};document.getElementById("countries-autocomplete-"+listId).onclick=function(e){if(e&&e.stopPropagation){hideAutocompleteList(listId);return e.stopPropagation()}else{e=window.event;return e.cancelBubble=true}};document.getElementById("countries-input-"+listId).onclick=function(e){if(e&&e.stopPropagation){e.stopPropagation();countriesInput.focus();return countriesInput.select()}else{e=window.event;return e.cancelBubble=true}};return document.onclick=function(){hideAutocompleteList(listId)}};hideAutocompleteList=function(listId){return document.getElementById("countries-autocomplete-"+listId).style.display="none"};buildItem=function(classActive,country,listId){var cc,flag,li,name;cc=country.country.substring(0,2).toLowerCase()+country.code;li=document.createElement("li");li.setAttribute("class",classActive);li.setAttribute("data-list-id",listId);li.setAttribute("rel",country.code);li.setAttribute("data-name",country.country);li.onmouseover=function(event){return setActive(li)};flag=document.createElement("span");flag.setAttribute("class","aflag flag-"+cc);li.appendChild(flag);name=document.createElement("span");name.innerHTML=country.country;li.appendChild(name);return li};absolutePosFor=function(element){var absLeft,absTop;absTop=0;absLeft=0;while(element){absTop+=element.offsetTop;absLeft+=element.offsetLeft;element=element.offsetParent}return[absTop,absLeft]};setupCountriesDropdown=function(countriesSelect,listId){var buf,classActive,countries,countriesAutocompleteList,countriesDropdown,countriesInput,countryCodeValue,i,name,placeholder;if(!countriesSelect){return}countries=[];i=0;while(i<countriesSelect.getElementsByTagName("option").length){buf=[];buf[0]=countriesSelect.getElementsByTagName("option")[i].value;buf[1]=countriesSelect.getElementsByTagName("option")[i].innerHTML;countries.push(buf);i++}countriesSelect.setAttribute("style","display:none");name=countriesSelect.getAttribute("name");countriesSelect.removeAttribute("name");countriesDropdown=document.createElement("div");countryCodeValue=document.createElement("input");countryCodeValue.setAttribute("type","hidden");countryCodeValue.setAttribute("id","country-code-"+listId);countryCodeValue.setAttribute("name",name);classActive="";countriesAutocompleteList=document.createElement("ul");i=0;while(i<countriesList.length){classActive=(i===0?"active":"");countriesAutocompleteList.appendChild(buildItem(classActive,countriesList[i],listId));i++}countriesDropdown.innerHTML="";countriesDropdown.appendChild(countriesAutocompleteList);document.body.appendChild(countriesDropdown);countriesInput=document.createElement("input");countriesInput.setAttribute("id","countries-input-"+listId);countriesInput.setAttribute("class","countries-input");countriesInput.setAttribute("type","text");countriesInput.setAttribute("autocomplete","off");placeholder=countriesSelect.getAttribute("placeholder");if(placeholder!=null){countriesSelect.removeAttribute("placeholder");countriesInput.setAttribute("placeholder",placeholder)}countriesSelect.parentNode.insertBefore(countriesInput,countriesSelect);countriesSelect.parentNode.appendChild(countryCodeValue);countriesDropdown.setAttribute("id","countries-autocomplete-"+listId);countriesDropdown.setAttribute("class","countries-autocomplete");setupCountriesDropdownPosition(countriesInput,countriesDropdown);setupEvents(countriesInput,listId)};setupCountriesDropdownPosition=function(countriesInput,countriesDropdown){var pos,width;pos=absolutePosFor(countriesInput);width=countriesInput.offsetWidth;if(width<220){width=220}return countriesDropdown.setAttribute("style","width: "+(width-5)+"px; top: "+(pos[0]+2+countriesInput.offsetHeight)+"px; left: "+(pos[1]-2)+"px;")};findAndSetupCountries=function(){var countries,i;setupCountriesDropdown(document.getElementById("authy-countries"),0);countries=document.getElementsByClassName("authy-countries");i=0;while(i<countries.length){setupCountriesDropdown(countries[i],i+1);i++}};setCountryField=function(){var country,countryCode,defaultListId,field,_i,_len,_results;defaultListId=0;field=document.getElementById("authy-countries");if(!field){return}countryCode=field.value;if(countryCode!==""){_results=[];for(_i=0,_len=countriesList.length;_i<_len;_i++){country=countriesList[_i];if(country.code===countryCode){self.autocomplete(buildItem("active",country,defaultListId),true);break}else{_results.push(void 0)}}return _results}};getKeyCode=function(event){var keyCode;if(event&&event.which){keyCode=event.which}else{if(window.event){keyCode=window.event.keyCode}}return keyCode};this.init=function(){setupAuthyTokenValidation();disableAutocompleteAuthyToken();setupTooltip();findAndSetupCountries();setCountryField();return setupCellphoneValidation()};this.searchItem=function(listId){var classActive,countriesAutocompleteList,countriesInput,countryItem,countryWords,cw,dropdownMenu,firstCountryCodeFound,i,matches,reg,str;classActive="active";countriesInput=document.getElementById("countries-input-"+listId);str=countriesInput.value;countriesAutocompleteList=document.createElement("ul");firstCountryCodeFound=null;matches=false;str=str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g,"\\$&");reg=new RegExp("^"+str,"i");i=0;while(i<countriesList.length){countryItem=countriesList[i];countryWords=countryItem.country.toLowerCase().split(/\s+/);cw=0;while(cw<countryWords.length){if((countryWords[cw].length>2&&countryWords[cw].match(reg))||(""+countryItem.code).match(reg)){countriesAutocompleteList.appendChild(buildItem(classActive,countryItem,listId));classActive="";matches=true;if(firstCountryCodeFound==null){firstCountryCodeFound=countryItem.code}break}cw++}i++}if(matches){dropdownMenu=document.getElementById("countries-autocomplete-"+listId);dropdownMenu.innerHTML="";dropdownMenu.appendChild(countriesAutocompleteList);return self.setCountryCode(listId,firstCountryCodeFound)}};this.autocomplete=function(obj,hideList){var listId;listId=obj.getAttribute("data-list-id");document.getElementById("countries-input-"+listId).value=obj.getAttribute("data-name");self.setCountryCode(listId,obj.getAttribute("rel"));if(hideList){hideAutocompleteList(listId)}};this.setCountryCode=function(listId,countryCode){return document.getElementById("country-code-"+listId).value=countryCode};this.setTooltip=function(title,msg){var tooltip;tooltip=document.getElementById("authy-tooltip");if(!tooltip){return}tooltip.getElementsByClassName("tooltip-title")[0].innerHTML=title;tooltip.getElementsByClassName("tooltip-content")[0].innerHTML=msg}};Authy.UI.instance=function(){if(!this.ui){this.ui=new Authy.UI();this.ui.init()}return this.ui};window.onload=function(){return Authy.UI.instance()}}).call(this);
        Authy.UI.instance();
    }
});

App.ProfileImageView = Ember.View.extend({
    uploadProfileImage: function (){
        var self = this;
        self.$("#files").bind('touchstart mousedown', function(){
            self.$("#files").val(null);
        });
        self.$("#files").change(function(){
            var filed = this.files[0];
            var dataUrl = callFileUpload(filed, self.get('controller'));
        });
        self.$('#files').click();
    }
});

App.NetverifyView = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        $.post('/jumioAuthentication', {})
            .fail(function() {
            })
            .done(function(data) {
                JumioClient.setVars({authorizationToken: data.authorizationToken,locale: "en"}).initVerify("JUMIOIFRAME");
            }.bind(this));
    }
});

App.AutoGrowView = Ember.View.extend({
    didInsertElement: function() {
        var self = this;
        self.$('.autoGrow').autoGrowInput({
            comfortZone: 5,
            minWidth: 300,
            maxWidth: 350
        });
    },
    willDestroyElement: function(){
        var self = this;
        self.$('.autoGrow').off();
    }
});

function socketIOListeners(){

    socket.on('getLinkedinConnections', function(result){
        Ember.Instrumentation.instrument("linkedinConnections.notificationOccured", result);
    });

    // Sockets
    socket.on('auth', function(data) {
        if (data && data.session && data.session.passport){
            if (!data.session.passport.user){
                Ember.Instrumentation.instrument("logout.notificationOccured", data);
                $('#app').fadeIn();
            }
            if (data.session.passport.user){
                Ember.Instrumentation.instrument("user.notificationOccured", data.user);
                Ember.Instrumentation.instrument("auth.notificationOccured", data);
            }
        }
        else {
            $('#app').fadeIn();
        }
    });
    socket.on('cryptoDeposit', function(payload){
        Ember.Instrumentation.instrument("cryptoDeposit.notificationOccured", payload);
    });
    socket.on('cryptoWithdraw', function(payload){
        Ember.Instrumentation.instrument("cryptoWithdraw.notificationOccured", payload);
    });
    socket.on('sentDeposit', function(payload){
        Ember.Instrumentation.instrument("sentDeposit.notificationOccured", payload);
    });
    socket.on('sentWithdraw', function(payload){
        Ember.Instrumentation.instrument("sentWithdraw.notificationOccured", payload);
    });
    socket.on('getRooms', function(payload) {
        socket.emit('joinRooms');
    });
    socket.on('masterAccountInfo', function(payload) {
        Ember.Instrumentation.instrument("reservesInfo.notificationOccured", payload);
    });
    socket.on('cashoutTransactionAcquireAll', function(data){
        Ember.Instrumentation.instrument("cashoutTransactionAcquireAll.notificationOccured", data);
    });
    socket.on('cashoutTransactionAcquire', function(data){
        Ember.Instrumentation.instrument("cashoutTransactionAcquire.notificationOccured", data);
    });
    socket.on('inEscrow', function(data){
        Ember.Instrumentation.instrument("inEscrow.notificationOccured", data);
    });
    socket.on('superadmin', function(data){
        Ember.Instrumentation.instrument("superadmin.notificationOccured", data);
    });
    socket.on('bankAccounts', function(data){
        Ember.Instrumentation.instrument("bankAccounts.notificationOccured", data);
    });
    socket.on('xrpReserve', function (data) {
        Ember.Instrumentation.instrument("xrpReserve.notificationOccured", data);
    });
    socket.on('bitcoinUSDQuote', function (data) {
        Ember.Instrumentation.instrument("bitcoinUSDQuote.notificationOccured", data);
    });
    socket.on('uxRate', function(data) {
        Ember.Instrumentation.instrument("uxRate.notificationOccured", data);
    });
    socket.on('uxBTCRate', function(data) {
        Ember.Instrumentation.instrument("uxBTCRate.notificationOccured", data);
    });
    socket.on('userAcquire', function(data){
        Ember.Instrumentation.instrument("user.notificationOccured", data);
    });
    socket.on('card', function(data) {
        Ember.Instrumentation.instrument("card.notificationOccured", data);
    });
    socket.on('jumioVerified', function(data) {
        Ember.Instrumentation.instrument("jumioVerified.notificationOccured", data);
    });
    socket.on('twoFactorRegister', function(data) {
        Ember.Instrumentation.instrument("twoFactorRegister.notificationOccured", data);
    });
    socket.on('connect', function(){
        socket.emit('auth');
        var trigger = {};
        if ($('.connection-indicator') && $('.connection-indicator').length){
            $('.connection-indicator').attr( "style", "color: #16a085");
        }
    });
    socket.on('disconnect', function(){
        if ($('.connection-indicator') && $('.connection-indicator').length){
            $('.connection-indicator').attr( "style", "color: #c0392b");
        }
        socket.socket.connect();
    });
    socket.on('validation', function(){

    });
    socket.on('register', function(data){
    });
    socket.on('error', function(data){
        if (data.error && $('#noticeSlide') && $('#noticeSlide').length && $('#noticeSlide2') && $('#noticeSlide2').length){
            $('#noticeSlide').attr('class', 'alert alert-error').html(data.error).fadeIn(300, function() {
                setTimeout(function(){$('#noticeSlide').fadeOut(300);}, 1500);
            });
            $('#noticeSlide2').attr('class', 'alert alert-error').html(data.error).fadeIn(300, function() {
                setTimeout(function(){$('#noticeSlide2').fadeOut(300);}, 1500);
            });
        }
        else {
            socket.socket.connect();
        }
    });
    socket.on('connect_failed', function () {

    });
    socket.on('reconnect_failed', function () {

    });
    socket.on('success', function(data){
        if ($('#noticeSlide') && $('#noticeSlide').length && $('#noticeSlide2') && $('#noticeSlide2').length){
            $('#noticeSlide').attr('class', 'alert alert-success').html(data.success).fadeIn(300, function() {
                setTimeout(function(){$('#noticeSlide').fadeOut(300);}, 1500);
            });
            $('#noticeSlide2').attr('class', 'alert alert-success').html(data.success).fadeIn(300, function() {
                setTimeout(function(){$('#noticeSlide2').fadeOut(300);}, 1500);
            });
        }
    });
    socket.on('verifyCode', function(data){
        socket.emit('user:acquire');
    });

}

// Extra Functions (eventually move to new file)

function findKey(obj, value){
    var key;

    _.find(obj, function(v, k) {
        if (v === value) {
            key = k;
            return true;
        } else {
            return false;
        }
    });

    if (key){
        return key;
    }
    else {
        return value;
    }
}

function repositionTooltip( e, ui, self ){
    var div = $(ui.handle).data("tooltip").$tip[0];
    var pos = $.extend({}, $(ui.handle).offset(), { width: $(ui.handle).get(0).offsetWidth,
        height: $(ui.handle).get(0).offsetHeight
    });

    var actualWidth = div.offsetWidth;

    var tp = {left: pos.left + pos.width / 2 - actualWidth / 2}
    $(div).offset(tp);

    $(div).find(".tooltip-inner").text( ($(self.selector).slider("value") * 10).toFixed(0) + '%' );
}

function compare(a,b) {
    if (a.score > b.score)
        return -1;
    if (a.score < b.score)
        return 1;
    return 0;
}

var isMobile = {
    Android : function() {
        return navigator.userAgent.match(/Android/i) ? true : false;
    },
    BlackBerry : function() {
        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
    },
    iOS : function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
    },
    Windows : function() {
        return navigator.userAgent.match(/IEMobile/i) ? true : false;
    },
    any : function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
};

function are_cookies_enabled() {
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;

    if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled)
    {
        document.cookie="testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
    }
    return (cookieEnabled);
}

function valueEscape (query) {
    return query.replace(/\\?([&|+\-!(){}[\]^"~*?\:]{1})/g, function(str, c) {
        return '\\' + c;
    });
}

function precise_round(num,decimals){
    return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
}

function convertRippledDate(date){
    var rippledDate = new Date("2000-01-01T00:00:00");
    rippledDate = rippledDate.setSeconds(rippledDate.getSeconds() + Number(date) + (60*60));
    rippledDate = new Date(rippledDate);
    return rippledDate;
}

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

BigInteger.valueOf = nbv;
BigInteger.prototype.toByteArrayUnsigned = function () {
    var ba = this.toByteArray();
    if (ba.length) {
        if (ba[0] == 0)
            ba = ba.slice(1);
        return ba.map(function (v) {
            return (v < 0) ? v + 256 : v;
        });
    } else
        return ba;
};
var Bitcoin = {};
(function () {
    var B58 = Bitcoin.Base58 = {
        alphabet: "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",
        base: BigInteger.valueOf(58),
        decode: function (input) {
            bi = BigInteger.valueOf(0);
            var leadingZerosNum = 0;
            for (var i = input.length - 1; i >= 0; i--) {
                var alphaIndex = B58.alphabet.indexOf(input[i]);
                if (alphaIndex < 0) {
                    throw "Invalid character";
                }
                bi = bi.add(BigInteger.valueOf(alphaIndex)
                    .multiply(B58.base.pow(input.length - 1 - i)));
                if (input[i] == "1") leadingZerosNum++;
                else leadingZerosNum = 0;
            }
            var bytes = bi.toByteArrayUnsigned();
            while (leadingZerosNum-- > 0) bytes.unshift(0);
            return bytes;
        }
    };
})();
Bitcoin.Address = function (bytes) {
    if ("string" == typeof bytes)
        bytes = Bitcoin.Address.decodeString(bytes);
    this.hash = bytes;
    this.version = Bitcoin.Address.networkVersion;
};
Bitcoin.Address.networkVersion = 0x00; // mainnet
Bitcoin.Address.decodeString = function (string) {
    var bytes = Bitcoin.Base58.decode(string);
    var hash = bytes.slice(0, 21);
    var checksum = Crypto.SHA256(Crypto.SHA256(hash, { asBytes: true }), { asBytes: true });
    if (checksum[0] != bytes[21] ||
        checksum[1] != bytes[22] ||
        checksum[2] != bytes[23] ||
        checksum[3] != bytes[24])
        throw "Checksum validation failed!";
    var version = hash.shift();
    if (version != 0)
        throw "Version " + version + " not supported!";
    return hash;
};
var Ripple = {};
(function () {
    var B58 = Ripple.Base58 = {
        alphabet: "rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz",
        base: BigInteger.valueOf(58),
        decode: function (input) {
            bi = BigInteger.valueOf(0);
            var leadingZerosNum = 0;
            for (var i = input.length - 1; i >= 0; i--) {
                var alphaIndex = B58.alphabet.indexOf(input[i]);
                if (alphaIndex < 0) {
                    throw "Invalid character";
                }
                bi = bi.add(BigInteger.valueOf(alphaIndex)
                    .multiply(B58.base.pow(input.length - 1 - i)));
                if (input[i] == "1") leadingZerosNum++;
                else leadingZerosNum = 0;
            }
            var bytes = bi.toByteArrayUnsigned();
            while (leadingZerosNum-- > 0) bytes.unshift(0);
            return bytes;
        }
    };
})();
Ripple.Address = function (bytes) {
    if ("string" == typeof bytes)
        bytes = Ripple.Address.decodeString(bytes);
    this.hash = bytes;
    this.version = Ripple.Address.networkVersion;
};
Ripple.Address.networkVersion = 0x00; // mainnet
Ripple.Address.decodeString = function (string) {
    var bytes = Ripple.Base58.decode(string);
    var hash = bytes.slice(0, 21);
    var checksum = Crypto.SHA256(Crypto.SHA256(hash, { asBytes: true }), { asBytes: true });
    if (checksum[0] != bytes[21] ||
        checksum[1] != bytes[22] ||
        checksum[2] != bytes[23] ||
        checksum[3] != bytes[24])
        throw "Checksum validation failed!";
    var version = hash.shift();
    if (version != 0)
        throw "Version " + version + " not supported!";
    return hash;
};
var Litecoin = {};
(function () {
    var B58 = Litecoin.Base58 = {
        alphabet: "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",
        base: BigInteger.valueOf(58),
        decode: function (input) {
            bi = BigInteger.valueOf(0);
            var leadingZerosNum = 0;
            for (var i = input.length - 1; i >= 0; i--) {
                var alphaIndex = B58.alphabet.indexOf(input[i]);
                if (alphaIndex < 0) {
                    throw "Invalid character";
                }
                bi = bi.add(BigInteger.valueOf(alphaIndex)
                    .multiply(B58.base.pow(input.length - 1 - i)));
                if (input[i] == "1") leadingZerosNum++;
                else leadingZerosNum = 0;
            }
            var bytes = bi.toByteArrayUnsigned();
            while (leadingZerosNum-- > 0) bytes.unshift(0);
            return bytes;
        }
    };
})();
Litecoin.Address = function (bytes) {
    if ("string" == typeof bytes)
        bytes = Litecoin.Address.decodeString(bytes);
    this.hash = bytes;
    this.version = Litecoin.Address.networkVersion;
};
Litecoin.Address.networkVersion = 0x00; // mainnet
Litecoin.Address.decodeString = function (string) {
    var bytes = Litecoin.Base58.decode(string);
    var hash = bytes.slice(0, 21);
    var checksum = Crypto.SHA256(Crypto.SHA256(hash, { asBytes: true }), { asBytes: true });
    if (checksum[0] != bytes[21] ||
        checksum[1] != bytes[22] ||
        checksum[2] != bytes[23] ||
        checksum[3] != bytes[24])
        throw "Checksum validation failed!";
    var version = hash.shift();
    return hash;
};
function check_address(address) {
    try {
        Bitcoin.Address(address);
        return true;
    } catch (err) {
        return false;
    }
}
function checkLitecoinAddress(address) {
    try {
        Litecoin.Address(address);
        return true;
    } catch (err) {
        return false;
    }
}
function checkNextcoinAddress(address) {
    if (!isNaN(address) && address.length >= 15){
        return true;
    }
    else {
        return false;
    }
}
function checkRippleAddress(address) {
    return(ripple.UInt160.is_valid(address));
}
function arraySet(count, value) {
    var a = new Array(count);

    for (var i=0; i<count; i++) {
        a[i] = value;
    }

    return a;
};

var alphabets = {
    ripple  :  "rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz",
    tipple  :  "RPShNAF39wBUDnEGHJKLM4pQrsT7VWXYZ2bcdeCg65jkm8ofqi1tuvaxyz",
    bitcoin :  "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
};
function baseDecode(input, alpha) {
    if (typeof input !== 'string') {
        return void(0);
    }

    var alphabet = alphabets[alpha || 'ripple'];
    var bi_base  = new BigInteger(String(alphabet.length));
    var bi_value = new BigInteger();
    var i;

    for (i = 0; i != input.length && input[i] === alphabet[0]; i += 1){

    }

    for (; i !== input.length; i += 1) {
        var v = alphabet.indexOf(input[i]);

        if (v < 0) {
            return void(0);
        }

        var r = new BigInteger();
        r.fromInt(v);
        bi_value  = bi_value.multiply(bi_base).add(r);
    }

    // toByteArray:
    // - Returns leading zeros!
    // - Returns signed bytes!
    var bytes =  bi_value.toByteArray().map(function (b) { return b ? b < 0 ? 256+b : b : 0; });
    var extra = 0;

    while (extra != bytes.length && !bytes[extra]) {
        extra += 1;
    }

    if (extra) {
        bytes = bytes.slice(extra);
    }

    var zeros = 0;

    while (zeros !== input.length && input[zeros] === alphabet[0]) {
        zeros += 1;
    }

    return [].concat(arraySet(zeros, 0), bytes);
};
function facebookInit(){
    FB.Event.subscribe('auth.statusChange', function(response) {
        if (response.status === 'connected') {
            if (response && response.authResponse && response.authResponse.userID && response.authResponse.accessToken){
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;
                FB.api('/me/friends', function(response){
                    if (response && response.data){
                        Ember.Instrumentation.instrument("fbFriends.notificationOccured", response.data);
                    } else {
                    }
                });
            }
        }/* else if (response.status === 'not_authorized') {

         } else {

         }*/
    });
}
function formatDollar(num) {
    var p = num.toFixed(2).split(".");
    return ["$", p[0].split("").reverse().reduce(function(acc, num, i) {
        return num + (i && !(i % 3) ? "," : "") + acc;
    }, "."), p[1]].join("");
}
function isEmpty(ob){
    for(var i in ob){ if(ob.hasOwnProperty(i)){return false;}}
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}        //

function signtx(secret, tx_in) {
    var tx_JSON = tx_in;
    var tx = new ripple.Transaction();
    tx.tx_json = tx_JSON;
    tx._secret = secret;
    tx.complete();
    tx.sign();
    return (tx.tx_json);
}
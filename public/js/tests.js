/**
 * Re-open App, and modify setupForTesting to set `Ember.testing = false`
 * so that the run-loop's autorun is not disabled.
 * The App currently has asynchronous side-effects that do not respect
 * Ember.run, such as $.post() calls to register.
 */
App.reopen({
  testHelpers: {},

  setupForTesting: function() {
    Ember.testing = false;

    this.deferReadiness();

    this.Router.reopen({
      location: 'none'
    });

    Ember.Test.adapter = Ember.Test.QUnitAdapter.create();
  },

});

// Standard Ember Testing set-up
App.rootElement = '#app';
App.setupForTesting();
App.injectTestHelpers();

/**
 * Do not re-order tests. Always run them in the specified order.
 * This is necessary because the testing suite has non-atomic tests.
 */
QUnit.config.reorder = false;

// Reset the app between modules
QUnit.moduleStart(function() {
	App.reset();
});

// Log out after all tests are complete
QUnit.done(function() {
	$.post('/logout', null).done(function(response) {

	});
});

/**
 * Basic page tests examine the pages and functions available to logged out users.
 */
module("Basic Page Tests", {
  setup: function() {
    App.reset();
  }
});

test("app exists", function() {

  visit("/").then(function() {
	ok(find(".brand:contains('Peercover')").length, "The navbar brand should read Peercover");
	equal(find("#main").length, 1, "The app should have a #main element");
	equal(find("#alerts").length, 1, "The app should have an #alerts section");
  });

});

test("about page exists", function() {
  visit("/about").then(function() {
	ok(find("p").length, "The About page should have some text on it.");
  });

});

test("login page exists", function() {
  visit("/login").then(function() {
	ok(find(".page-title:contains('Login')").length, "The login page should have a title");
	ok(find("form").length, "The login page should have a form");
  });

});

test("send password recovery page exists", function() {
  visit("/inputEmail").then(function() {
	ok(find(".page-title:contains('Recover Password')").length, "The send password recovery page should have a title");
	ok(find("form").length, "The login page should have a form");
  });
  //TODO test error message for no user found
  //TODO test behavior for correct form submission

});

test("set new password page exists", function() {
  visit("/recoverPassword").then(function() {
	ok(find(".page-title:contains('Recover Password')").length, "The password reset page should have a title");
	ok(find("form").length, "The password reset should have a form");
  });
  //TODO test error message for no user found
  //TODO test behavior for correct form submission
});

test("registration page exists", function() {
  visit("/register").then(function() {
	ok(find(".page-title:contains('Register')").length, "The registration page should have a title");
	ok(find("form").length, "The registration page should have a form");
  });

});

test("registration page gives error on no input", function() {
	visit("/register")
	.click("#btn-register")
	.then(function() {
		ok(find(".alert-error:contains('No inputs.')").length, "The registration should show an error with no inputs.");
	});

});

test("registration page gives error on mismatched passwords", function() {
	visit("/register")
	.fillIn("#email", "pcqunit1@dayrep.com")
	.fillIn("#password", "ab")
	.fillIn("#password2", "abcd")
	.click("#btn-register")
	.then(function(){
			ok(find(".alert-error:contains('The passwords you entered do not match.')").length, "The registration should show an error if passwords do not match.");
	});
});

/**
 * The Membership tests register a new user and steps through basic membership functions.
 */

module("Membership Tests", {
  setup: function() {

  }
});

test("login exists", function() {

  visit("/").then(function() {
	equal(find("#navbarInputEmail").length, 1, "The app should have an email login element");
	equal(find("#navbarInputPassword").length, 1, "The app should have a password login element");
  });

});

var registerEmail = "qunitprcover0"
	+ Math.random().toString(36).substring(2, 10)
	+ '@mailinator.com';
var registerPassword = "pcpc"
	+ Math.random().toString(36).substring(2, 10);
var registerTestName = "test registration using "
	+ registerEmail
	+ " with password " 
	+ registerPassword;

asyncTest(registerTestName, function() {

	visit("/register")
	.fillIn("#email", registerEmail)
	.fillIn("#password", registerPassword)
	.fillIn("#password2", registerPassword)
	.click("#btn-register")
	.then(function(){
		ok(true, "Registration should have logged us in." );

			setTimeout(function() {
				ok( true, "Pause while registration completes.");
				start();
			}, 3000);
	});

});

test("profile page exists", function() {

	visit("/profile").then(function() {
		ok(find("#profile").length, "The profile page should exist");
		ok(find("#profile-email:contains('" + registerEmail + "')").length, "The user's email should be set on the profile");
	});

});

var memberTestName = {
	firstName: 'Member',
	lastName: 'Tester-' + Math.random().toString(36).substring(2, 10)
};

var memberFullName = memberTestName.firstName + ' ' + memberTestName.lastName;

var memberTestTitle = "member can change name using " + memberFullName;

test(memberTestTitle, function() {
	visit("/profile")
	.click("#profile-edit-btn")
	.fillIn("#inputFirstName", memberTestName.firstName)
	.fillIn("#inputLastName", memberTestName.lastName)
	.click("#save-profile-btn")
	.then(function() {
		ok(find(".page-title:contains('" + memberFullName + "')").length, "The user should be able to change the name.");
		ok(find("#profile-full-name:contains('" + memberFullName + "')").length, "The user's full name should be displayed");
	});

});

test("members page exists", function() {
	visit('/members')
	.then(function() {
		ok(find(".page-title:contains('Members')").length, "The members page should have a title");
		ok(find("a:contains('" + memberFullName + "')").length, "The user should appear in the members list");
	})
	.click("a:contains('" + memberFullName + "')")
	.then(function() {
		ok(find(".page-title:contains('" + memberFullName + "')").length, "The user have a member page with the full name listed");
	});

});

/* //Comment out groups tests, which require member verification and ripple wallet
test("groups pages exists", function() {

	visit('/groups')
	.then(function() {
		ok(find(".page-title:contains('Groups')").length, "The groups page should have a title");
		ok(find("#btn-groups-create:contains('Create a Group')").length, "The groups page should have a button to create a new group");
	})
	.click('#btn-groups-create')
	.then(function() {
		ok(find("#form-groups-create").length, "The groups create page should have a form.");
	});

});

var testGroupName = 'Test Group-'
	+ Math.random().toString(36).substring(2, 10);

asyncTest("create groups works", function() {

	visit('/groups/create')
	.fillIn("#inputName", testGroupName)
	.click("#group-create-btn")
	.then(function() {
		ok(true, 'Creating group');
	});

	setTimeout(function() {
		ok( true, "Pause while group create completes.");
		start();
	}, 3000);
});

test("new group exists", function() {

	visit('/groups')
	.then(function() {
		ok(find("a:contains('" + testGroupName + "')").length, "The group should appear in the groups list")
	})
	.click("a:contains('" + testGroupName + "')")
	.then(function() {
		ok(find(".page-title:contains('" + testGroupName + "')").length, "The group name should be listed on the page");
	})
	.click(".subnav a:contains('Terms')")
	.then(function() {
		ok(find("h3:contains('Create New Terms')").length, "The terms page exists");
	});

});
*/

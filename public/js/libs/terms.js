
$(document).ready(function(){

    var rawMd, fileName;
    var editingAllowed = false; //dont allow editing until something is loaded

    socket.on('navLinks', function (data){
        $('#wikiNavigation').html(data.links);
        changewikiContentHeight();
    });

    ///////////////////////////////////////////////////////////
    // opening files and wikiNavigation
    ///////////////////////////////////////////////////////////
    var canSendReadFile = true;
    $(document).on('click', '#wikiNavigation a.link', function(a){
        if (canSendReadFile == true){
            canSendReadFile = false;
            socket.emit('readFile', {name: $(a.currentTarget).text()});
            $('#wikiNavigation').children().attr('class', 'link');
            $('#wikiContent #markdown_wikiContent').html('<em>Loading...</em>');
            $('#wikiContent_header h1').html('Node Wiki');
            $(a.currentTarget).attr('class', 'selected link');
            changewikiContentHeight();

            if (creatingNewFile){
                cancelNewFile();
            }
        }
    });

    socket.on('readFileReply', function(data){
        canSendReadFile = true;
        if (data.error.error == true){
            console.warn('error: ' + data.error.reason);
            $('#notification').html('error reading file: ' + data.error.reason);
            $('#notification').slideDown('fast', function(){
            window.setTimeout(function(){$('#notification').slideUp()}, 7000);
            });
            $('#wikiContent #markdown_wikiContent').html('');
            changewikiContentHeight();
        } else {
            $('#wikiContent #markdown_wikiContent').html(data.filewikiContents);
            $('#wikiContent #wikiContent_header h1').html(data.fileName);
            rawMd = data.rawMd;
            fileName = data.fileName;
            editingAllowed = true;
            creatingNewFile = false;
            showButtons(true);
            changewikiContentHeight();
            $('#notification').slideUp();
        }
    });

    socket.on('readFolderReply', function(data){
        canSendReadFile = true;
        $('#wikiContent #markdown_wikiContent').html('');
        changewikiContentHeight();
        editingAllowed = false;
        creatingNewFile = false;
        showButtons(false);
    });

    $(document).on('click', '#wikiNavigation a#go_back', function(){
        socket.emit('goBackFolder');
        $('#wikiContent #markdown_wikiContent').html('');
        $('#wikiContent_header h1').html('Node Wiki');
        //editingAllowed = true;
    })

    ///////////////////////////////////////////////////////////
    // saving files
    ///////////////////////////////////////////////////////////

    $(document).on('click', '#edit_save_buttons a#save', function(){
        if (editingAllowed == false){ //if user is currently editing
            if (creatingNewFile == true){
                if ($('#wikiContent #wikiContent_header input').val() != '' && $('#wikiContent #markdown_wikiContent textarea').val() != ''){
                    socket.emit('saveFile', {name: $('#wikiContent #wikiContent_header input').val(), wikiContent: $('#wikiContent #markdown_wikiContent textarea').val()});
                } else {
                    $('#notification').html('The title or the wikiContent cannot be empty. Also, the title needs to end with ".md"');
                    $('#notification').slideDown('fast', function(){
                        window.setTimeout(function(){$('#notification').slideUp()}, 4000);
                    });
                }
                } else {
                    socket.emit('saveFile', {name: fileName, wikiContent: $('#wikiContent #markdown_wikiContent textarea').val()});
                }
        }
    });

    socket.on('saveFileReply', function(data){
        if (data.error.error == true){
            console.warn('there was an error saving');
            $('#notification').html('there was an error: ' + data.error.reason);
            $('#notification').slideDown('fast', function(){
              window.setTimeout(function(){$('#notification').slideUp()}, 7000);
            });
            } else {
                $('#wikiContent #markdown_wikiContent').html(data.filewikiContents);
                rawMd = data.rawMd;
                fileName = data.fileName;
                editingAllowed = true;
                changewikiContentHeight();
                $('#notification').html('Saved');
                $('#notification').slideDown('fast', function(){
                window.setTimeout(function(){$('#notification').slideUp()}, 2000);
            });

            if (creatingNewFile == true){
              creatingNewFile = false;
              $('#wikiContent #wikiContent_header h1').html(fileName);
              tempFile = '';
              socket.emit('refreshNav');
            }
        }
    });

    ///////////////////////////////////////////////////////////
    // editing files
    ///////////////////////////////////////////////////////////
    $(document).on('click', '#edit_save_buttons a#edit', function(){
        if (editingAllowed == true){
            editingAllowed = false;
            $('#wikiContent').height('auto');
            $('#wikiContent #markdown_wikiContent').html('<textarea>' + rawMd + '</textarea>');
        }
    });


    ///////////////////////////////////////////////////////////
    // creating new files
    ///////////////////////////////////////////////////////////
    var tempFile; // hold the <a> tag for the new file being created
    var creatingNewFile = false;

    $(document).on('click', '#wikiNavigation a#new_file', function(){
        fileName = '';
        rawMd = '';
        creatingNewFile = true;
        editingAllowed = false;

        $('#wikiNavigation #tri_buttons').before('<a href="#" class="link"><code>New File...</code></a>');
        tempFile = $('#wikiNavigation a.link:last');
        $('#wikiNavigation').children().attr('class', 'link');
        tempFile.attr('class', 'link selected');
        $('#wikiNavigation a#new_file').attr('id', 'new_file_inactive');
        $('#wikiContent #markdown_wikiContent').html('<textarea></textarea>');
        $('#wikiContent #wikiContent_header h1').html('<form><input type="text" /></form>');
        $('#wikiContent #wikiContent_header input').focus();
        showButtons(true, true);
        changewikiContentHeight();
    });

    $(document).on('click', '#edit_save_buttons a#cancel', function(){
        cancelNewFile();
    });

    function cancelNewFile(){
        tempFile.remove();
        tempFile = '';
        creatingNewFile = false;
        editingAllowed = true;
        $('#wikiContent #markdown_wikiContent').html('');
        $('#wikiContent #wikiContent_header h1').html('Node Wiki');
        $('#wikiNavigation a#new_file_inactive').attr('id', 'new_file');
        showButtons(false);
        changewikiContentHeight();
    }

    ///////////////////////////////////////////////////////////
    // creating new folder
    ///////////////////////////////////////////////////////////

    creatingNewFolder = false;
    $(document).on('click', '#wikiNavigation a#new_folder', function(){
        creatingNewFolder = true;
        fileName = '';
        rawMd = '';
        editingAllowed = false;

        if (creatingNewFile){
            cancelNewFile();
        }

        $('#wikiContent #wikiContent_header h1').html('Node Wiki');
        $('#wikiContent #markdown_wikiContent').html('Creating new folder, press enter to create the folder');
        $('#wikiNavigation').children().attr('class', 'link');
        $('#wikiNavigation #tri_buttons').before('<div id="temp_new_folder"><form><input type="text" /></form></div>');
        tempFile = $('#wikiNavigation #temp_new_folder');
        $('#wikiNavigation input').focus();
        $('#wikiNavigation a#new_folder').attr('id', 'new_folder_inactive');
        $('#wikiNavigation #tri_buttons').html('<a href="#" id="cancel_folder">Cancel</a><a href="#" id="save_folder">Create Folder</a>');
        changewikiContentHeight();
    });

    $(document).on('click', '#wikiNavigation a#cancel_folder', function(){
        cancelNewFolder();
    });

    function createFolder(){
    if ($('#wikiNavigation input').val() != ''){
        socket.emit('newFolder', $('#wikiNavigation input').val());
        cancelNewFolder();
    } else {
        $('#notification').html('Please have at least 1 character for the folder name');
        $('#notification').slideDown('fast', function(){
            window.setTimeout(function(){$('#notification').slideUp()}, 4000);
        });
    }
    }

    $(document).on('click', '#wikiNavigation a#save_folder', function(){
        createFolder();
    });


    $('#wikiNavigation').submit(function(){
        createFolder();
    });


    function cancelNewFolder(){
        if (creatingNewFolder){
            creatingNewFolder = false;
            tempFile.remove();
            tempFile = '';
            $('#wikiNavigation #tri_buttons').html('');
            socket.emit('refreshNav');
            $('#wikiContent #markdown_wikiContent').html('');
        }
    }

    socket.on('newFolderReply', function(err){
        $('#notification').html('There was an error making the folder, error code: ' + err.code);
        $('#notification').slideDown('fast', function(){
            window.setTimeout(function(){$('#notification').slideUp()}, 4000);
        });
    });


    ///////////////////////////////////////////////////////////
    // functions for the layout
    ///////////////////////////////////////////////////////////

    function showButtons(show, newFile){
        var buttons = '<a id="edit" href="#">Edit</a>\n<a id="save" href="#">Save</a>';
        if (show){
          if (newFile){
            $('#edit_save_buttons').html('<a id="cancel" href="#">Cancel</a>\n<a id="save" href="#">Save</a>');
          } else {
            $('#edit_save_buttons').html(buttons);
          }
        } else {
          $('#edit_save_buttons').html('');
        }
    }


    function changewikiContentHeight(){
        $('#wikiContent').height('auto');
        var offset = $('#wikiContent').offset();
        var wikiContentBottom = $('#wikiContent').height() + offset.top;

        var offsetNav = $('#wikiNavigation').offset();
        var navBottom = $('#wikiNavigation').height() + offsetNav.top;

        if (navBottom > wikiContentBottom){
          $('#wikiContent').height($('#wikiNavigation').height() + 20 + 'px');
        }
    }


});

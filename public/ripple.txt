[validation_public_key]
n94af2MGXg5KTuWcgEkFZeAoG6mDC2M2Tt3Uqh61qaYVFnYszQQ5

[accounts]
ra9eZxMbJrUcgV8ui7aPc161FgrqWScQxV

[stock_address]
rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif

[domain]
peercover.com

[federation_url]
https://peercover.com/bridge

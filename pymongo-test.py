import csv
from pymongo import MongoClient

if __name__ == '__main__':
    with open('mongodb.csv') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        params = list(reader)[0]
    db = MongoClient(params[0], int(params[4])).peercover_production
    db.authenticate(params[1], params[2])
    print db.collection_names()
    # print
    # print db.dividends.find_one()
    # print
    # print db.outsidecashouts.find_one()
    # for item in db.users.find():
    #     print item
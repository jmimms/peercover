var transactions = new Schema({
    id    : ObjectId
    , userTo : String // displayed on transaction view
    , userFrom : String // display on transaction view
    , embeddedTransaction : Schema.Types.Mixed
    , transaction : String // display on transaction view
    , description : String // CRUD and display on transaction view
    , date : Date // automatically update
    , verified : Boolean // used in conjunction with websocket to display verified transaction in green
    , currency : String
    , amount : String
});
var messages = new Schema({
    id    : ObjectId
    , userFrom : String
    , userTo : String
    , ip : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , dateSent : Date
    , dateUpdated : Date
    , dateDeleted : Date
    , deleted : Boolean
    , message : String
    , invoices : [invoices]
    , transactions : [transactions]
    , files : [fileUploads]
});
var chatThreads = new Schema({
    id    : ObjectId
    , users : Array
    , userInitiated : String
    , group : String
    , isGroup : Boolean
    , dateStarted : Date
    , dateUpdated : Date
    , dateDeleted : Date
    , deleted : Boolean
    , messages : [messages]
});
var invoices = new Schema({
    id    : ObjectId
    , dueId : String
    , claim : String
    , group : String
    , failToPay : Boolean
    , amount : Number
    , currencyDesired: String
    , currencyClaimedIn : String
    , houseAmount : Number
    , currency : String
    , fundingSources : [fundingSources]
    , to : String
    , from : String
    , proportionOfClaim : Number
    , paid : Boolean
    , receivedDate : Date
    , paidDate : Date
    , dueDate : Date
    , failToPayDate : Date
    , forgiven : Boolean
    , invoiceId : String
    , type : String
    , txCurrency : String
    , txAmount : String
    , txAmountMax : String
    , txState : String
    , finalStatus : String
    , txHash : String
    , txBlob : String
    , txCurrencyHouse : String
    , txAmountHouse : String
    , txAmountHouseMax : String
    , txStateHouse : String
    , finalStatusHouse : String
    , txHashHouse : String
    , txBlobHouse : String
});

CREATE TABLE orderbook (
    orderid BIGSERIAL PRIMARY KEY,
    userid BIGINT NOT NULL,
    fromcurrency VARCHAR(10) NOT NULL,
    tocurrency VARCHAR(10) NOT NULL,
    price NUMERIC(25,10) NOT NULL,
    volume NUMERIC(25,10) NOT NULL,
    orderdate TIMESTAMP DEFAULT statement_timestamp() NOT NULL
);

CREATE TABLE transactions (
    txid BIGSERIAL PRIMARY KEY,
    fromuser BIGINT NOT NULL,
    touser BIGINT NOT NULL,
    currency VARCHAR(10) NOT NULL,
    price NUMERIC(25,10) NOT NULL,
    volume NUMERIC(25,10) NOT NULL,
    txdate TIMESTAMP DEFAULT statement_timestamp() NOT NULL
);

CREATE TABLE pendingTransactions (
    txid BIGSERIAL PRIMARY KEY,
    incTxid VARCHAR(300),
    depTxid VARCHAR(300),
    uuid VARCHAR(32) NOT NULL,
    type VARCHAR(32) NOT NULL,
    currency VARCHAR(10),
    issuer VARCHAR(40),
    amount NUMERIC,
    toAddress VARCHAR(300),
    validationUrl VARCHAR(300),
    validated BOOLEAN,
    successSubmit BOOLEAN,
    initDate TIMESTAMP DEFAULT statement_timestamp() NOT NULL,
    lastSubmitDate TIMESTAMP DEFAULT statement_timestamp()
);

CREATE TABLE balance (
    userid BIGINT PRIMARY KEY,
    usd NUMERIC(25,10),
    ils NUMERIC(25,10),
    eur NUMERIC(25,10),
    gbp NUMERIC(25,10),
    aud NUMERIC(25,10),
    chf NUMERIC(25,10),
    cny NUMERIC(25,10),
    jpy NUMERIC(25,10),
    nok NUMERIC(25,10),
    btc NUMERIC(25,10),
    ltc NUMERIC(25,10),
    nxt NUMERIC(25,10)
);

GRANT CONNECT ON DATABASE peercover TO peercover;
GRANT SELECT, INSERT, UPDATE, DELETE ON orderbook TO peercover;
GRANT SELECT, INSERT, UPDATE, DELETE ON transactions TO peercover;
GRANT SELECT, INSERT, UPDATE, DELETE ON balance TO peercover;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, UPDATE, INSERT, DELETE ON TABLES TO peercover;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO peercover;
var peercoverConfig = require('../config'),
    pg = require('pg'),
    bitcoin = require('bitcoin'),
    mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment'),
    paginate = require('mongoose-paginate');

mongoose.connect(peercoverConfig.mongodb);
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
autoIncrement.initialize(mongoose);
var users = new Schema({
    id    : ObjectId
    , email     : String // used for login confirmation
    , username  : String // optional CRUD on profile view // if added display on all headings
    , password  : String // U on profile view
    , joinDate  : Date // display on profile view
    , activeDate : Date // display on profile view
    , firstName : String // CRUD on profile view // if no user name display on all headings
    , lastName  : String // CRUD on profile view // if no user name display on all headings
    , dob       : Date // CRUD on profile
    , superadminDate : Date
    , uploadImage : String // CRUD profile image or select url from facebook, linkedin (make sure SSL intact)
    , facebookImage : String
    , customer : String
    , dailyInstantLimit : Number
    , dailyInstantDate : Date
    , founderDeal: Boolean
    , twoFactorUser : String
    , hasTwoFactor : Boolean
    , walletRecoverCode : String
    , twoFactorDate : Date
    , founderDealDate: Date
    , founderDealDateEnd: Date
    , linkedinImage : String
    , customerServiceAvailable : Boolean
    , haveWallet    : Boolean
    , superadmin : Boolean
    , jumioVerified : Boolean
    , bankAccounts : [bankAccounts]
    , offers: [inviteCodes]
    , cashOuts: [cashOuts]
    , deposits: [cashOuts]
    , creditReportUrl : String // for V2.0
    , identityProofUrl : String // for V2.0
    , verified: Boolean // must be verified to access any views other than home and about
    , verifyCode : String // used on backend
    , recoverCode : String
    , rippleInsuredWallet : [rippleInsuredWallets] // CRUD on group view after accepted to group
    , bitcoinInsuredWallet : [bitcoinInsuredWallets]
    , bitcoinWallet : [bitcoinWallets] // CRUD on group view after accepted to group V2.0
    , rippleInsuranceWallet : [rippleInsuranceWallets] // CRUD on profile view // required
    , facebookData : Schema.Types.Mixed // display on profile view
    , dwollaData : Schema.Types.Mixed   // display on profile view
    , linkedinData : Schema.Types.Mixed // display on profile view
    , jumioData : Schema.Types.Mixed
    , permissions : String // three levels of permissions for profile display and edit on profile view
    , verificationBadge : Boolean // is identity verified (dwolla bank account valid in name) - display verification badge on profile view
    , facebookToken : String
    , linkedinToken : String
    , customer : String
    , creditCard : String
    , dwollaToken : String
    , facebookRefresh : String
    , linkedinSecret : String
    , dwollaRefresh : String
    , profileImageSource : String
    , currentProfileImageUrl : String
    , updateDate : Date
    , connectionsSent : [connections]
    , connectionsReceived : [connections]
    , connections : Array
    , formerEmail : String
    , chatThreads : [chatThreads]
    , groupHistory : [groupHistory]
    , invites : [invites]
    , invitesSent : [invites]
    , invoices : [invoices]
    , dues : [invoices]
    , signature : [signatures]
});
var BitcoinWallet = mongoose.model('BitcoinWallet', bitcoinWallets),
    User = mongoose.model('User', users);

pg.connect(peercoverConfig.pgParams, function (err, client, done) {
    if (err) return console.error('Error fetching client from pool', err);
    var orderbook = [
        [1, 'xrp', 'btc', 1000, 100],
        [2, 'xrp', 'btc', 999, 10000],
        [3, 'xrp', 'btc', 998, 1000000],
        [4, 'xrp', 'btc', 997, 2000000],
        [5, 'btc', 'xrp', 1002, 1000],
        [6, 'btc', 'xrp', 1003, 10000],
        [7, 'btc', 'xrp', 1004, 500000],
        [8, 'btc', 'xrp', 1005, 3000000],
    ];
    var sql = 'INSERT INTO orderbook (userid, fromcurrency, tocurrency, price, volume) '+
              'VALUES ($1::int, $2::text, $3::text, $4::int, $5::int)';
    client.query('BEGIN');
    for (var i = 0; i < orderbook.length; i++) {
        client.query(sql, [orderbook[i][0], orderbook[i][1], orderbook[i][2], orderbook[i][3], orderbook[i][4]], function (err, qry) {
            if (err) console.log(err);
        });
    }
    client.query('COMMIT', function (err, qry) {
        if (err) console.log(err);
        done();
    });
});

var bitcoinClient = new bitcoin.Client({
    host: 'localhost',
    port: 8332, // 18332 for testnet, 8332 for live
    user: peercoverConfig.bitcoinUsername,
    pass: peercoverConfig.bitcoinPassword
});
User.find({}, function (err, users) {
    if (err) return console.log(err);
    pg.connect(peercoverConfig.pgParams, function (err, client, done) {
        if (err) return console.error(err);
        client.query('BEGIN');
        var sql = "INSERT INTO balance (userid) VALUES ($1::int)";
        users.forEach(function (user) {
            client.query(sql, [user.id], function (err) {
                if (err) return console.log(err);
            });
        });
        client.query('COMMIT', function (err) {
            if (err) return console.log(err);
        });
        bitcoinClient.getBalance(account, 6, function (err, balance) {
            if (err) return console.log(err);
            var sql = 'UPDATE TABLE balance SET btc = $1 WHERE userid = $2';
            client.query(sql, [balance, user.id], function (err) {
                if (err) return console.log(err);
            });
        });
    });
});
module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-ember-templates');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-closurecompiler');

	grunt.initConfig({
	cssmin: {
		combine: {
			files: {
				'public/css/vendor.css' : [
					'public/css/bootstrap.css',
					'public/css/bootstrap-responsive.css',
					'public/css/terms.css',
					'public/css/slider.css',
                    'public/css/avgrund.css',
                    'public/css/animate.min.css',
					'public/css/jquery.signature.css',
					'public/css/dropzone.css',
                    'public/css/bootstrap-datetimepicker.min.css',
                    'public/css/bootstrap-formhelpers-countries.flags.css',
                    'public/css/select2.css',
                    'public/css/bootstrap-combobox.css',
                    'public/css/pick-a-color-1.1.7.min.css',
                    'public/css/stroll.min.css',
                    'public/css/authy.min.css'
				]
			}
		}
	},
	concat: {
		ember: {
			files: {
				'public/js/ember.js': [
					'public/js/libs/handlebars.runtime-1.0.0.js',
					'public/js/libs/ember-1.0.0-rc.7.min.js',
					'public/js/libs/ember-data-0.13.min.js'
				]
			}
		}
	},
	closurecompiler: {
		application: {
			files: {
				'public/js/app.min.js' : [
					'public/js/app.js'
				]
			}
		}
	},
	emberTemplates: {
      watch: {
          emberTemplates: {
              files: 'views/templates/*.hbs',
              tasks: ['emberTemplates', 'livereload']
          }
      },
	  compile: {
	    options: {
	      templateName: function(sourceFile) {
		      return sourceFile.replace('views/templatesFirst/', '').replace('views/templates/', '');
	      }
	    },
	    files: {
	      "public/js/templates.js": "views/templates/*.hbs"
	    }
	  }
	}
	});

	grunt.registerTask('default', ['emberTemplates', 'concat', 'cssmin', 'closurecompiler']);
	grunt.registerTask('templates', ['emberTemplates']);
	// grunt.registerTask('app', ['closurecompiler:application']);

};

var fundingSources = new Schema({
    id    : ObjectId
    , sourceId : String
    , name : String
    , type : String
    , verified : Boolean
    , processingType : String
    , publicKey : String
});
var inviteCodes = new Schema({
    id    : ObjectId
    , code : String
    , user : String
    , used : Boolean
    , usedDate: Date
    , expirationDate : Date
    , issuedDate: Date
    , type : String
    , duration : Schema.Types.Mixed
    , group : String
    , email : String
});
var offers = new Schema({
    id    : ObjectId
    , description : String
    , startDate : Date
    , expirationDate : Date
    , endDate : Date
    , createdDate : Date
    , updateDate : Date
    , type : String
    , title : String
    , active : Boolean
    , duration : Schema.Types.Mixed
    , amountLimit : Number
    , offerContact : String
    , price : Number
    , currencyPrice : String
    , codes : [inviteCodes]
    , groupTypesAvailable : Array
});
var ious = new Schema({
    id    : ObjectId
    , gateway : String // displayed on wallet view // can issue manually // automatically issued if wallet contains balance claimant trusts at time of claim approval
    , amount : Number // displayed on wallet view
    , currency : String // displayed on wallet view
});
var trust = new Schema({
    id    : ObjectId
    , gateway : String // displayed on wallet view // can issue manually // automatically issued if wallet contains balance claimant trusts at time of claim approval
    , amount : Number // displayed on wallet view
    , currency : String // displayed on wallet view
});
var trustedGateways = new Schema({
    id    : ObjectId
    , name : String // CRUD by users and displayed on terms/profile views
    , publicKey : String // CRUD by users and displayed on terms/profile views
    , currencies : Array
    , date : Date
});
var rippleInsuredWallets = new Schema({
    id    : ObjectId
    , user : String // identify owner of wallet
    , publicKey : String // verify how much insured/collateral // display on wallet view
    , xrp : Number // amount of XRP, display on wallet view
    , ious : [ious] // display on wallet view
    , trust : [trust] // display on wallet view
    , updateDate : Date
});
var bitcoinInsuredWallets = new Schema({
    id    : ObjectId
    , user : String
    , publicKey : String // used to manually or automatically send BTC when claim approved // display on wallet view
    , btc : String // display on wallet view
    , updateDate : Date
});
var pendingTransactions = new Schema({
    id    : ObjectId
    , invoiceId : String
    , sequenceNumber : Number
    , txHash : String
    , date : Date
});
var invoices = new Schema({
    id    : ObjectId
    , dueId : String
    , claim : String
    , group : String
    , failToPay : Boolean
    , amount : Number
    , currencyDesired: String
    , currencyClaimedIn : String
    , houseAmount : Number
    , currency : String
    , fundingSources : [fundingSources]
    , to : String
    , from : String
    , proportionOfClaim : Number
    , paid : Boolean
    , receivedDate : Date
    , paidDate : Date
    , forgiven : Boolean
    , invoiceId : String
    , txCurrency : String
    , txAmount : String
    , txAmountMax : String
    , txState : String
    , finalStatus : String
    , txHash : String
    , txBlob : String
    , txCurrencyHouse : String
    , txAmountHouse : String
    , txAmountHouseMax : String
    , txStateHouse : String
    , finalStatusHouse : String
    , txHashHouse : String
    , txBlobHouse : String
});
var rippleInsuranceWallets = new Schema({
    id    : ObjectId
    , user : String
    , publicKey : String  // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , privateKey : String // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , walletName : String // CRUD on wallet view
    , password : String // CRUD on wallet view
    , passphrase : String // display on wallet view
    , privateKeyHex : String
    , fundingSources : [fundingSources]
    , xrp : String // display on wallet view
    , ious : [ious] // display on wallet view // issue on wallet view
    , trust : [trust] // display on wallet view // issue on wallet view
    , creationDate : Date // display on wallet view
    , updateDate : Date
    , primary : Boolean
    , funded : Boolean
    , trustedGateways : [trustedGateways]
    , qrcode : String
});
var fileUploads = new Schema({
    id    : String
    , user : String
    , date : Date
    , type : String
    , name : String
    , url : String
    , original : String
    , folder : String
    , permissions : String
    , type : String
    , success : Boolean  // set by S3 response
});
var videos = new Schema({
    id    : ObjectId
    , upload : String
    , user : String
    , title : String // CRUD on claim upload view
    , url : String // claim upload view
    , description : String // CRUD on claim upload view
    , creationDate : Date
    , updateDate : Date
    , type : String
    , date : Date
});
var images = new Schema({
    id    : ObjectId
    , upload : String
    , user : String
    , title : String // CRUD on claim upload view
    , url : String // claim upload view
    , description : String // CRUD on claim upload view
    , creationDate : Date
    , updateDate : Date
    , type : String
    , date : Date
});
var documents = new Schema({
    id    : ObjectId
    , upload : String
    , user : String
    , title : String // CRUD on claim upload view
    , url : String // claim upload view
    , description : String // CRUD on claim upload view
    , creationDate : Date
    , updateDate : Date
    , type : String
    , date : Date
});
// reference or embed?
var transactions = new Schema({
    id    : ObjectId
    , user : String // displayed on transaction view
    , user2 : String // display on transaction view
    , transaction : String // display on transaction view
    , description : String // CRUD and display on transaction view
    , date : Date // automatically update
    , verified : Boolean // used in conjunction with websocket to display verified transaction in green
});
var votes = new Schema({
    id    : ObjectId
    , vote : Boolean
    , user : String
    , date : Date
});
var comments = new Schema({
    id    : ObjectId
    , user : String
    , comment : String
    , attachmentUrl : String
    , creationDate : Date
    , updateDate : Date
});
var claims = new Schema({
    id    : ObjectId
    , user : String
    , images : [images] // display as attachment on claim view
    , documents : [documents] // display as attachment on claim view
    , videos : [videos] // display as attachment on claim view
    , files : [fileUploads]
    , title : String // CRUD on claim view
    , description : String // CRUD on claim view
    , amountClaimed : Number // CRUD on claim view privileges change depending on state of claim
    , currencyClaimedIn : String // CRUD on claim view privileges change depending on state of claim
    , currencyDesired : String // CRUD on claim view privileges change depending on state of claim
    , voteApprove : Boolean // votes displayed on claims
    , votes : [votes]
    , comments : [comments]
    , forceEndDate : Date
    , vestDate : Date
    , dateVoteEnd : Date
    , creationDate : Date
    , updateDate : Date
    , groupMembersNumberOnFile : Number
    , group: String
});
var groupHistory = new Schema({
    id    : ObjectId
    , user : String
    , group : String
    , dateJoined : Date
    , dateLeft : Date
    , honorableDischarge : Boolean
    , commentsOnMembership : [comments]
    , reviewsOfMember : [comments]
    , voteJoin : [votes]
    , voteJoinDate : Date
    , voteJoinDateEnd : Date
    , voteRescindMembership : [votes]
    , voteRescindMembershipDate : Date
    , voteRescindMembershipDateEnd : Date
    , dateValueEntered : Date
    , roundVoteRescindMembership : Number
    , roundVoteJoin : Number
    , member : Boolean
    , foundingUser : Boolean
    , admin : Boolean
    , voteApprove : Boolean
    , portion : Number
    , forceEndDate : Date
    , voteThresholdMemberApproval : Number
    , deleted : Boolean
    , dateAcceptedReferral : Date
    , valueInsured : Number
});
var invites = new Schema({
    id : ObjectId
    , sentDate : Date
    , acceptedDate : Date
    , declinedDate : Date
    , acceptedDeclined : Boolean
    , sentBy : String
    , receivedBy : String
    , group : String
    , retract : Boolean
    , type : String
});
var signatures = new Schema({
    id    : ObjectId
    , sig : String // signature displayed in profile view
    , user : String // user identification
    , date : Date // current date displayed when signature selected on terms and claims view
    , valueInsured : Number
    , currency : String
    , portion : Number
    , description : String
});
var beforeSignatures = new Schema({
    id    : ObjectId
    , sig : String // signature displayed in profile view
    , user : String // user identification
    , date : Date // current date displayed when signature selected on terms and claims view
    , valueInsured : Number
    , destinationTag : Number
    , currency : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , portion : Number
    , awarded : Number
    , fee : Number
    , address : String
    , ip : String
    , awardedDate : Date
    , paidDate : Date
    , paidOutDate : Date
    , feePaidOutDate : Date
    , convertedDate : Date
    , refundedDate : Date
    , amount : Number
    , paidTo : String
    , description : String
    , userInput : Number
    , userStringInput : String
});
var messages = new Schema({
    id    : ObjectId
    , userFrom : String
    , userTo : String
    , ip : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , dateSent : Date
    , dateUpdated : Date
    , dateDeleted : Date
    , deleted : Boolean
    , message : String
    , files : [fileUploads]
});
var chatThreads = new Schema({
    id    : ObjectId
    , users : Array
    , userInitiated : String
    , group : String
    , dateStarted : Date
    , dateUpdated : Date
    , dateDeleted : Date
    , deleted : Boolean
    , messages : [messages]
});
var connections = new Schema({
    id    : ObjectId
    , sentBy : String
    , receivedBy : String
    , dateSent : Date
    , dateAccepted : Date
    , dateRevoked : Date
    , dateDenied : Date
    , messageAttached : String
    , isAccepted : Boolean
});
var users = new Schema({
    id    : ObjectId
    , email     : String // used for login confirmation
    , username  : String // optional CRUD on profile view // if added display on all headings
    , password  : String // U on profile view
    , joinDate  : Date // display on profile view
    , activeDate : Date // display on profile view
    , firstName : String // CRUD on profile view // if no user name display on all headings
    , lastName  : String // CRUD on profile view // if no user name display on all headings
    , dob       : Date // CRUD on profile
    , uploadImage : String // CRUD profile image or select url from facebook, linkedin (make sure SSL intact)
    , facebookImage : String
    , founderDeal: Boolean
    , founderDealDate: Date
    , founderDealDateEnd: Date
    , linkedinImage : String
    , haveWallet    : Boolean
    , superadmin : Boolean
    , offers: [inviteCodes]
    , creditReportUrl : String // for V2.0
    , identityProofUrl : String // for V2.0
    , verified: Boolean // must be verified to access any views other than home and about
    , verifyCode : String // used on backend
    , recoverCode : String
    , rippleInsuredWallet : [rippleInsuredWallets] // CRUD on group view after accepted to group
    , bitcoinInsuredWallet : [bitcoinInsuredWallets] // CRUD on group view after accepted to group V2.0
    , rippleInsuranceWallet : [rippleInsuranceWallets] // CRUD on profile view // required
    , facebookData : Schema.Types.Mixed // display on profile view
    , dwollaData : Schema.Types.Mixed   // display on profile view
    , linkedinData : Schema.Types.Mixed // display on profile view
    , permissions : String // three levels of permissions for profile display and edit on profile view
    , verificationBadge : Boolean // is identity verified (dwolla bank account valid in name) - display verification badge on profile view
    , facebookToken : String
    , linkedinToken : String
    , dwollaToken : String
    , facebookRefresh : String
    , linkedinSecret : String
    , dwollaRefresh : String
    , profileImageSource : String
    , currentProfileImageUrl : String
    , updateDate : Date
    , connectionsSent : [connections]
    , connectionsReceived : [connections]
    , connections : Array
    , formerEmail : String
    , chatThreads : [chatThreads]
    , groupHistory : [groupHistory]
    , invites : [invites]
    , invitesSent : [invites]
    , invoices : [invoices]
    , dues : [invoices]
    , signature : [signatures]
});
var usersReferred = new Schema({
    id    : ObjectId
    , user  : String // displayed in new member inbox view
    , referredBy : String // displayed in new member inbox view
    , date : Date // displayed in new member inbox view
    , acceptedDeclined : Boolean
    , dateDecision : Date
});
var termOptions = new Schema({
    id    : ObjectId
    , minInsured : String // CRUD in terms view
    , maxInsured : String // CRUD in terms view
    , trustedGateways : [trustedGateways] // universally group trusted gateways
    , currenciesAllowed : Array // CRUD in terms view
});
var proposedTermsChanges = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , vestDate : Date // group CRUD displayed in terms/group view
    , overDate : Date // group CRUD displayed in terms/group view
    , voteEnd : Date
    , contract : String // group CRUD displayed in terms/group view
    , changesList : String // group CRUD displayed in terms/group view
    , voteApprove : Boolean
    , durationVote : Date
    , comments : [comments]
    , votes : [votes]
    , stashIndex : String
    , commitIndex : String
});
var templates = new Schema({
    id    : ObjectId
    , name : String
    , type : String
    , markdown : String
    , user : String
    , date : Date
});
var terms = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , updateDate : Date // displayed in terms/group view
    , vestDate : Date // group CRUD displayed in terms/group view
    , overDate : Date // group CRUD displayed in terms/group view
    , contract : String // group CRUD displayed in terms/group view
    , contractUrl : String // group CRUD displayed in terms/group view
    , claimWeight : String // group CRUD displayed in terms/group view
    , typeOfInsurance : String // group CRUD displayed in terms/group view
    , durationGroup : String // group CRUD displayed in terms/group view
    , termOptions : [termOptions] // group CRUD displayed in terms/group view
    , signature : [signatures] // CRUD displayed in terms/group view // signature pad integration // electronic signature
    , durationVote : Date
    , comments : [comments]
    , selectedTerms : Boolean
});
var lossGroups = new Schema({
    id    : ObjectId
    , creationDate : Date // display on group view
    , updateDate : Date // display on group view
    , vestDate : Date // display on group view
    , overDate : Date // display on group view
    , dissolveDate : Date
    , minVestDate : Date
    , endDate : Date
    , willVestDate : Date
    , durationOfGroup : Number
    , minimumUsers : Number
    , rangeStart : Number
    , rangeEnd : Number
    , multiplier : Number
    , chatThreads : [chatThreads]
    , dateRangeStart : Date
    , dateRangeEnd : Date
    , eventEndDate : Date
    , signedLabels : Array
    , finalValue : Number
    , promoteAmount : Number
    , promoteCurrency : String
    , finalValueString : String
    , duration : Schema.Types.Mixed
    , promotion : Boolean
    , promotionEnd : Date
    , type : String
    , name : String
    , founderDeal : Boolean
    , description : String
    , rewardLabel : String
    , terms : [terms] // display on group view
    , beforeSignatures : [beforeSignatures]
    , proposedTermsChanges : [proposedTermsChanges]
    , queueTermsProposedChanges : [terms]
    , rippleInsuredWallet : [rippleInsuredWallets] // display on group view
    , bitcoinInsuredWallet : [bitcoinInsuredWallets] // display on group view
    , rippleInsuranceWallet : [rippleInsuranceWallets]
    , userReferred : [usersReferred] // display on group view, display in group members' new member inbox view
    , voteThresholdNewMembers : Number // display on group view // CRUD on terms view
    , voteThresholdTermVest : Number // display on group view
    , voteThresholdGroupVest : Number // display on group view
    , voteThresholdClaimApproval : Number // display on group view
    , voteThresholdAdminApproval : Number // display on group view
    , gracePeriod : Number
    , adminNominations : [usersReferred] // invited users by admins (separate admin vote count)
    , isPublic : Boolean
    , vested : Boolean
    , didVest : Boolean
    , didNotVest : Boolean
    , claims : [claims]
    , vestVote : [votes]
    , isAdmin : Boolean
    , claimVote : Boolean
    , portionInput : Number
    , deleted: Boolean
});

//================================================ Models for MongoDB ================================================//

beforeSignatures.plugin(autoinc.plugin, { model: 'BeforeSignature', field: 'destinationTag', start: 1, step: 1 });

var FundingSource = mongoose.model('FundingSource', fundingSources)
    , Iou = mongoose.model('Iou', ious)
    , Trust = mongoose.model('Trust', trust)
    , TrustedGateway = mongoose.model('TrustedGateway', trustedGateways)
    , RippleInsuredWallet = mongoose.model('RippleInsuredWallet', rippleInsuredWallets)
    , BitcoinInsuredWallet = mongoose.model('BitcoinInsuredWallet', bitcoinInsuredWallets)
    , PendingTransaction = mongoose.model('PendingTransaction', pendingTransactions)
    , Invoice = mongoose.model('Invoice', invoices)
    , RippleInsuranceWallet = mongoose.model('RippleInsuranceWallet', rippleInsuranceWallets)
    , FileUpload = mongoose.model('FileUpload', fileUploads)
    , Video = mongoose.model('Video', videos)
    , Imaged = mongoose.model('Imaged', images)
    , InviteCode = mongoose.model('InviteCode', inviteCodes)
    , Offer = mongoose.model('Offer', offers)
    , Documented = mongoose.model('Documented', documents)
    , Transaction = mongoose.model('Transaction', transactions)
    , Vote = mongoose.model('Vote', votes)
    , Comment = mongoose.model('Comment', comments)
    , Claim = mongoose.model('Claim', claims)
    , GroupHistory = mongoose.model('GroupHistory', groupHistory)
    , Invite = mongoose.model('Invite', invites)
    , Signature = mongoose.model('Signature', signatures)
    , BeforeSignature = mongoose.model('BeforeSignature', beforeSignatures)
    , Message = mongoose.model('Message', messages)
    , ChatThread = mongoose.model('ChatThread', chatThreads)
    , Connection = mongoose.model('Connection', connections)
    , User = mongoose.model('User', users)
    , UserReferred = mongoose.model('UserReferred', usersReferred)
    , TermOption = mongoose.model('TermOption', termOptions)
    , ProposedTermsChange = mongoose.model('ProposedTermsChange', proposedTermsChanges)
    , Template = mongoose.model('Template', templates)
    , Term = mongoose.model('Term', terms)
    , LossGroup = mongoose.model('LossGroup', lossGroups);
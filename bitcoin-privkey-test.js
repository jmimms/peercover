// Unlock wallet for 120 sec: bitcoind walletpassphrase [password] 120
// Get private key: bitcoind dumpprivkey [address]
// Re-lock wallet: bitcoind walletlock
var bitcoin = require('bitcoin');
var prompt = require('prompt');
var peercoverConfig = require('./config');
var bitcoinClient = new bitcoin.Client({
    host: 'localhost',
    port: 8332, // 18332 for testnet, 8332 for live
    user: peercoverConfig.bitcoinUsername,
    pass: peercoverConfig.bitcoinPassword
});
var schema = {
    properties: {
        name: {
            pattern: /^[a-zA-Z\s\-]+$/,
            required: true
        },
        password: {
            hidden: true
        }
    }
};
prompt.start();
prompt.get(schema, function (err, result) {
    bitcoinClient.walletPassphrase(result.password, 120, function (err) {
        if (err) return console.log(err);
        bitcoinClient.getAccountAddress(result.name, function (err, address) {
            if (err) return console.log(err);
            bitcoinClient.dumpPrivKey(address, function (err, privkey) {
                if (err) return console.log(err);
                console.log('Private key:', privkey);
            });
        });
    });
    bitcoinClient.walletLock(function (err) { if (err) return console.log(err); });
});
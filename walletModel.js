var outsideCashouts = new Schema({
    id    : ObjectId
    , sig : String // signature displayed in profile view
    , user : String // user identification
    , date : Date // current date displayed when signature selected on terms and claims view
    , valueInsured : Number
    , destinationTag : Number
    , currency : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , portion : Number
    , awarded : Number
    , fee : Number
    , address : String
    , type : String
    , ip : String
    , awardedDate : Date
    , paidDate : Date
    , paidOutDate : Date
    , feePaidOutDate : Date
    , convertedDate : Date
    , refundedDate : Date
    , amount : Number
    , paidTo : String
    , description : String
    , userInput : Number
    , userStringInput : String
});
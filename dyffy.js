//================================================= Modules Required =================================================//

var cluster = require('cluster')
    , numCPUs = require('os').cpus().length;

var http = require('http')
    , dyffyConfig = require('./config')
    , backbone = require('backbone')
    , chokidar = require('chokidar')
    , countryData = require('country-data')
    , crypto = require('crypto-js')
    , cryptoSha = require('crypto')
    , request = require('request')
    , express = require('express')
    , geoip = require('geoip-native')
    , hbs = require('hbs')
    , fx = require('money')
    , ripple = require('ripple-lib')
    , sessions = require('sessions')
    , stylus = require('stylus')
    , _ = require('underscore')
    , btoa = require('btoa')
    , validation = require('backbone-validation')
    , xmlhttp = require('xmlhttprequest')
    , jsdom = require('jsdom')
    , $ = require('jQuery')
    , nodemailer = require('nodemailer')
    , bruteAForce = new (require('connect-bruteforce'))({banFactor: 2000, banMax: 30000})
    , flash = require('connect-flash')
    , https = require('https')
    , fs = require('fs')
    , url = require('url')
    , WebSocketClient = require('websocket').client
    , passportSocketIo = require('passport.socketio')
    , knox = require('knox')
    , mime = require('mime-magic')
    , cronJob = require('cron').CronJob
    , ioClient = require('socket.io-client')
    , path = require('path')
    , redis = require('redis')
    , bitcoin = require('bitcoin')
    , litecoin = require('node-litecoin')
    , solr = require('solr')
    , async = require('async')
    , jStat = require('jStat')
    , marked = require("marked")
    , paginate = require('mongoose-paginate')
    , child_process = require("child_process")
    , QRCode  = require('qrcode')
    , im = require('imagemagick')
    , OAuth= require('oauth').OAuth
    , RedisStore = require('connect-redis')(express)
    , cluster = require('cluster')
    , numCPUs = require('os').cpus().length
    , moment = require('moment')
    , gravatar = require('gravatar')
    , autoIncrement = require('mongoose-auto-increment')
    , jwt = require('jwt-simple')
    , bcrypt = require('bcrypt')
    , authy = require('authy')(dyffyConfig.authy)
    , pg = require('pg')
    , mod_getopt = require('posix-getopt');

//======================================  Gateways and Public Addresses We Trust ======================================//

function findKey(obj, value){
    var key;

    _.find(obj, function(v, k) {
        if (v === value) {
            key = k;
            return true;
        } else {
            return false;
        }
    });

    return key;
}

//=========================================== Amazon S3 Setup and Functions ===========================================//

function s3PolicyInstance(accessKey, secretKey) {

    if (!accessKey || !secretKey) {
    }

    this.accessKey = accessKey;
    this.secretKey = secretKey;

    this.readPolicy = function(key, bucket, duration, download, cb) {
        var dateObj = new Date;
        var expiration = new Date(dateObj.getTime() + duration * 1000);
        expiration = Math.round(expiration.getTime() / 1000);

        var policy = 'GET\n\n\n' + expiration + '\n';
        policy += '/' + bucket + '/' + key;
        if (download) {
            policy += '?response-content-disposition=attachment;filename=' + encodeURIComponent(download);
        }

        var signature = cryptoSha.createHmac("sha1", this.secretKey).update(policy);

        var url = 'https://s3.amazonaws.com/';
        url += bucket + '/';
        url += key;
        url += '?AWSAccessKeyId=' + this.accessKey;
        url += '&Expires=' + expiration;
        url += '&Signature=' + encodeURIComponent(signature.digest("base64"));
        if (download) {
            url += '&response-content-disposition=attachment;filename=' + encodeURIComponent(download);
        }
        if (cb) {
            cb(null, url);
        } else {
            return url;
        }
    };

    this.writePolicy = function(key, bucket, duration, filesize, permissions, cb) {
        var dateObj = new Date;
        var dateExp = new Date(dateObj.getTime() + duration * 1000);
        var policy = {
            "expiration":dateExp.getUTCFullYear() + "-" + dateExp.getUTCMonth() + 1 + "-" + dateExp.getUTCDate() + "T" + dateExp.getUTCHours() + ":" + dateExp.getUTCMinutes() + ":" + dateExp.getUTCSeconds() + "Z",
            "conditions":[
                { "bucket":bucket },
                ["eq", "$key", key],
                { "acl":permissions },
                ["content-length-range", 0, filesize * 1000000],
                ["starts-with", "$Content-Type", ""]
            ]
        };

        var policyString = JSON.stringify(policy);
        var policyBase64 = new Buffer(policyString).toString('base64');
        var signature = cryptoSha.createHmac("sha1", this.secretKey).update(policyBase64);
        var accessKey = this.accessKey;
        s3Credentials = {
            s3PolicyBase64:policyBase64,
            s3Signature:signature.digest("base64"),
            s3Key:accessKey
        };
        if (cb) {
            cb(s3Credentials);
        } else {
            return s3Credentials;
        }
    };

}

var s3 = require('aws2js').load('s3', dyffyConfig.s3Key, dyffyConfig.s3Secret);

function updateVitalFiles(){
    s3.setBucket('dyffy');

    s3.putFile('/js/vendor.js', '/home/ubuntu/dyffy/public/js/vendor.js', 'public-read', {}, function (error, result) {

    });
    s3.putFile('/js/ember.js', '/home/ubuntu/dyffy/public/js/ember.js', 'public-read', {}, function (error, result) {

    });
    s3.putFile('/js/app.min.js', '/home/ubuntu/dyffy/public/js/app.min.js', 'public-read', {}, function (error, result) {

    });
    s3.putFile('/js/templates.js', '/home/ubuntu/dyffy/public/js/templates.js', 'public-read', {}, function (error, result) {

    });
    s3.putFile('/js/templatesFirst.js', '/home/ubuntu/dyffy/public/js/templatesFirst.js', 'public-read', {}, function (error, result) {

    });
}

function createBucket (bucketName, kind){
    // kind 'public-read', 'private'
    s3.createBucket(bucketName, 'private', 'us-west-1', function (error, result) {

    });
}
// putFiles([{path:'/home/cis/Desktop/Work',file:'test.jpg'}]);
function putFiles(files){
    s3.putFiles(files, 'public-read', {}, function (error, result)
    {
    });
}
function saveFile(filed, destination){
    var uploader = s3.upload(filed, destination);
    uploader.on('error', function(err) {
    });
    uploader.on('progress', function(amountDone, amountTotal) {
    });
    uploader.on('end', function(url) {
    });
}
// [{key: '/path/to/object1'}, {key: '/path/to/object2'}]
// if versioning {key: '/path/to/object', versionId: 'version-id'}
function deleteFiles(files){
    s3.delMultiObjects(files, false, function (error, result) {
        // may want to leverage the results to set acl classification (private/public)
    });
}
var s3Client = knox.createClient({
    key: dyffyConfig.s3Key
    , secret: dyffyConfig.s3Secret
    , bucket: dyffyConfig.s3Bucket
});

function getS3Url(filename) {
    var expires = new Date();
    expires.setMinutes(expires.getMinutes() + 30);
    return s3Client.signedUrl(filename, expires);
}
function puttingDataPrivate(filename){
    var req = s3Client.put(filename, {
        'Content-Length': string.length
        , 'Content-Type': 'application/json'
    });
    req.on('response', function(res){
        if (200 == res.statusCode) {
        }
    });
    req.end(string);
}
function fetchingDataPrivate(filename){
    s3Client.get(filename).on('response', function(res){
        res.setEncoding('utf8');
        res.on('data', function(chunk){
        });
    }).end();
}
function deleteDataPrivate(filename){
    s3Client.del(filename).on('response', function(res){
    }).end();
}

function getExtension(filename) {
    var ext = path.extname(filename||'').split('.');
    return ext[ext.length - 1];
}

//==================================================== Send Email ====================================================//

function sendEmail(toEmail, plainText, html, subject) {
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: "Gmail",
        auth: {
            user: "admin@dyffy.com",
            pass: dyffyConfig.adminEmailPassword
        }
    });
    var from = "Dyffy " + "✔ " + "admin@dyffy.com";
    var mailOptions = {
        from: from, // sender address
        to: toEmail, // list of receivers
        subject: subject, // Subject line
        text: plainText, // plaintext body
        html: html // html body
    };
    smtpTransport.sendMail(mailOptions, function(error, response){
        if (!isEmpty(error)){
            console.log(error);
        }else{
        }
        smtpTransport.close();
    });
}

//=========================================== Password and Email Encryption ===========================================//

function hex2a(hex) {
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}
function shaValue(valueFixed){
    return require('crypto').createHash('sha512').update(valueFixed).digest('hex');
}
function shaValue256(valueFixed){
    return require('crypto').createHash('sha256').update(valueFixed).digest('hex');
}
function encryptToDb (input){
    return crypto.AES.encrypt(input, dyffyConfig.cryptoPhrase).toString();
}
function decryptFromDb (input){
    return hex2a(crypto.AES.decrypt(input, dyffyConfig.cryptoPhrase).toString());
}
function isEmpty(ob){
    for(var i in ob){ if(ob.hasOwnProperty(i)){return false;}}
    return true;
}

//=============================================== Express Configuration ===============================================//

var store = new express.session.MemoryStore;
var expressIo = require('express.io');
hbs = require('hbs');

dyffy = expressIo();

dyffy.http().io();
dyffy.io.set('match origin protocol', true);
dyffy.io.set('store', new expressIo.io.RedisStore({
    redisPub: redis.createClient(),
    redisSub: redis.createClient(),
    redisClient: redis.createClient()
}));

dyffy.engine('hbs', hbs.__express);
dyffy.set('view engine', 'hbs');
dyffy.set('views', __dirname + '/views');
dyffy.use(express.compress());
dyffy.use(express.bodyParser());
dyffy.use(function(req, res, next) {
    if(req.url == '/ripple.txt') {
        res.setHeader("Access-Control-Allow-Origin", "*");
    }
    return next();
});
dyffy.use(express.cookieParser());

dyffy.use(express.session({ secret: 'Gtahd87878sdDSF78dfggsdd73bde9', key: 'express.sid', store: new RedisStore }));

dyffy.use(flash());
dyffy.use(express.static(__dirname + '/public'));
dyffy.use(passport.initialize());
dyffy.use(passport.session());
dyffy.use(dyffy.router);

if ('development' == dyffy.get('env')) {
    dyffy.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    dyffy.locals.pretty = true;
    port  = process.env.PORT || 51299;
}
else {
    port  = process.env.PORT || 4545;
}

dyffy.listen(port);

dyffy.all('*',function(req,res,next) {
    if ('development' == dyffy.get('env')) {
        if(req.headers['x-forwarded-proto'] != 'http') {
            res.redirect(dyffyConfig.domain + req.url);
        }
        else {
            next();
        }
    }
    else {
        if(req.headers['x-forwarded-proto'] != 'https') {
            res.redirect(dyffyConfig.sslDomain + req.url);
        }
        else {
            next();
        }
    }
});

if ('development' == dyffy.get('env')) {
}
else {
    updateVitalFiles();
}

//==================================================== IO Security ====================================================//

function clientID(req) {
    return req.handshake.address.address;
}

function connectAuthBruteForce(options) {
    var self = this;
    options = options || {};
    options.banMax = options.banMax || 30 * 1000;
    options.banFactor = options.banFactor || 2 * 1000;
    this.db = {};

    function delayResponse(req) {
        var factor = Math.min(req.delayed.counter * options.banFactor, options.banMax);
        var error = 'Banned for ' + factor + ' ms';
        req.io.emit('error', {error : error});
    }
    this.prevent = function (req) {
        // need to check for user email verification as well with mongoose query
        if (req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user,function(err,user){
                if (user.verified){
                    req.delayed = self.db[clientID(req)];
                    if (req.delayed) {
                        delayResponse(req);
                        req.io.disconnect();
                    }
                }
                else {
                    req.io.disconnect();
                }
            });
        }
        else {
            req.io.disconnect();
        }
    };
}

connectAuthBruteForce.prototype.ban = function (req) {
    var client = clientID(req),
        delay = this.db[client] || (this.db[client] = {
            at: new Date(),
            counter: 0
        });
    delay.counter++;
    delay.lastTimeBanned = new Date();
};
connectAuthBruteForce.prototype.unban = function (req) {
    delete this.db[clientID(req)];
    delete req.delayed;
};

function connectBruteForce(options) {
    var self = this;
    options = options || {};
    options.banMax = options.banMax || 30 * 1000;
    options.banFactor = options.banFactor || 2 * 1000;
    this.db = {};

    function delayResponse(req) {
        var factor = Math.min(req.delayed.counter * options.banFactor, options.banMax);
        var error = 'Banned for ' + factor + ' ms';
        req.io.emit('error', {error : error});
    }
    this.prevent = function (req) {
        req.delayed = self.db[clientID(req)];
        if (req.delayed) {
            delayResponse(req);
            req.io.disconnect();
        }
    };
}

connectBruteForce.prototype.ban = function (req) {
    var client = clientID(req),
        delay = this.db[client] || (this.db[client] = {
            at: new Date(),
            counter: 0
        });
    delay.counter++;
    delay.lastTimeBanned = new Date();
};
connectBruteForce.prototype.unban = function (req) {
    delete this.db[clientID(req)];
    delete req.delayed;
};

var rippleForce = new connectAuthBruteForce({banFactor: 2000, banMax: 30000});

var walletSpam = new connectAuthBruteForce({banFactor: 2000, banMax: 30000});

var bruteForce = new connectBruteForce({banFactor: 2000, banMax: 30000});

var bruteAuthForce = new connectBruteForce({banFactor: 2000, banMax: 30000});

//=================================================== Simple Gateway ==================================================//

// Get Ripple reserve
dyffy.io.route('checkReserve', function (req) {
    if (req.data && req.data.rippleAddress && typeof req.data.rippleAddress == 'string') {
        var rippleAddress = req.data.rippleAddress;
        child_process.exec('cd ~/rippled && ./rippled server_state', function (err, stdout) {
            if (err) return console.log(err);
            var serverstate = JSON.parse(stdout);
            var reserveBase = serverstate.result.state.validated_ledger.reserve_base;
            var reserveInc = serverstate.result.state.validated_ledger.reserve_inc;
            child_process.exec('cd ~/rippled && ./rippled account_info ' + rippleAddress, function (err, stdout) {
                if (err) return console.log(err);
                var accountinfo = JSON.parse(stdout);
                var balance;
                if (accountinfo.result && accountinfo.result.account_data){
                    balance = accountinfo.result.account_data.Balance / 1000000;
                }
                else {
                    balance = 0;
                }
                var ledgerEntries;
                if (accountinfo.result && accountinfo.result.account_data){
                    ledgerEntries = accountinfo.result.account_data.OwnerCount;
                }
                else {
                    ledgerEntries = 0;
                }
                var reserve = (reserveBase + ledgerEntries*reserveInc) / 1000000;
                req.io.emit('xrpReserve', {xrpReserve: reserve, xrpBalance: balance});
            });
        });
    }
});

// Make sure bitcoind and litecoind are running and were started with the
// walletnotify option.  Stop/restart as needed.
//var bitcoindCheck, litecoindCheck;
//(bitcoindCheck = function () {
//    console.log("Checking for bitcoind");
//    child_process.exec("ps -ef | awk '/[b]itcoind/{print $2}'", function (err, procId) {
//        if (err) return console.log(err);
//        var spawn_bitcoind = function () {
//            setTimeout(function () {
//                child_process.spawn('bitcoind --daemon -walletnotify="' + dyffyConfig.path + 'bitcoinwalletnotify.sh %s');
//            }, 1500);
//        };
//        var startdaemon = function (restart) {
//            if (restart) {
//                console.log("Restarting bitcoind with walletnotify enabled");
//                child_process.exec("bitcoind stop", function (err) {
//                    if (err) return console.log(err);
//                    spawn_bitcoind();
//                });
//            }
//            spawn_bitcoind();
//        };
//        if (procId) {
//            child_process.exec("ps -ef | awk '/[b]itcoind/{print $9}'", function (err, flag) {
//                if (err) return console.log(err);
//                var walletnotify = function (flag) {
//                    var found = false;
//                    if (flag.indexOf('walletnotify') != -1) {
//                        console.log("bitcoind ok");
//                        found = true;
//                    }
//                    return found;
//                };
//                if (walletnotify(flag)) return;
//                child_process.exec("ps -ef | awk '/[b]itcoind/{print $10}'", function (err, flag) {
//                    if (err) return console.log(err);
//                    if (walletnotify(flag)) return;
//                    startdaemon(1);
//                });
//            });
//        }
//        else {
//            console.log("Starting bitcoind");
//            startdaemon(0);
//        }
//    });
//})();
//(litecoindCheck = function () {
//    console.log("Checking for litecoind");
//    child_process.exec("ps -ef | awk '/[l]itecoind/{print $2}'", function (err, procId) {
//        if (err) return console.log(err);
//        var spawn_litecoind = function () {
//            setTimeout(function () {
//                child_process.spawn('litecoind --daemon -walletnotify="' + dyffyConfig.path + 'litecoinwalletnotify.sh %s');
//            }, 1500);
//        };
//        var startdaemon = function (restart) {
//            if (restart) {
//                console.log("Restarting litecoind with walletnotify enabled");
//                child_process.exec("litecoind stop", function (err) {
//                    if (err) return console.log(err);
//                    spawn_litecoind();
//                });
//            }
//            spawn_litecoind();
//        };
//        if (procId) {
//            child_process.exec("ps -ef | awk '/[l]itecoind/{print $9}'", function (err, flag) {
//                if (err) return console.log(err);
//                var walletnotify = function (flag) {
//                    var found = false;
//                    if (flag.indexOf('walletnotify') != -1) {
//                        console.log("litecoind ok");
//                        found = true;
//                    }
//                    return found;
//                };
//                if (walletnotify(flag)) return;
//                child_process.exec("ps -ef | awk '/[l]itecoind/{print $10}'", function (err, flag) {
//                    if (err) return console.log(err);
//                    if (walletnotify(flag)) return;
//                    startdaemon(1);
//                });
//            });
//        }
//        else {
//            console.log("Starting litecoind");
//            startdaemon(0);
//        }
//    });
//})();

var bitcoinClient = new bitcoin.Client({
    host: 'localhost',
    port: 8332, // 18332 for testnet, 8332 for live
    user: dyffyConfig.bitcoinUsername,
    pass: dyffyConfig.bitcoinPassword
});
var litecoinClient = new litecoin.Client({
    host: 'localhost',
    port: 9332,
    user: dyffyConfig.litecoinUsername,
    pass: dyffyConfig.litecoinPassword
});
var namecoinClient = new bitcoin.Client({
    host: 'localhost',
    port: 9903,
    user: dyffyConfig.bitcoinUsername,
    pass: dyffyConfig.bitcoinPassword
});
var dogecoinClient = new bitcoin.Client({
    host: 'localhost',
    port: 9908,
    user: dyffyConfig.bitcoinUsername,
    pass: dyffyConfig.bitcoinPassword
});
var peercoinClient = new bitcoin.Client({
    host: 'localhost',
    port: 9902,
    user: dyffyConfig.bitcoinUsername,
    pass: dyffyConfig.bitcoinPassword
});

// Bitcoin and Litecoin Wallet Listeners
var bitcoinTxLog = dyffyConfig.path + 'bitcoinTransactions.log';
var bitcoinWatcher = chokidar.watch(bitcoinTxLog, {ignored: /[\/\\]\./, persistent: true});
bitcoinWatcher.on('error', function (error) {
    console.error(error);
});

var getTxInfoBitcoin = function (txId) {
    bitcoinClient.getTransaction(txId, function (err, tx) {
        if (err) return console.log(err);
        OutsideCashin.findOne({cashinAddress : tx.details[0].address},function(err,cashin){
            if (!isEmpty(cashin) && isEmpty(err)){
                var transaction;
                var doneTrans = true;
                if (cashin.transactions && cashin.transactions[0]){
                    cashin.transactions.forEach(function(transact){
                        if (transact.transactionId == tx.txid){
                            transaction = transact;
                        }
                    });
                }
                switch (tx.details[0].category) {
                    case 'send':
                        // Debit account
                        break;
                    case 'receive':
                        var amount = Number(tx.amount);
                        if (transaction && !transaction.paidOutDate && !isNaN(Number(tx.confirmations)) && Number(tx.confirmations) >= 3){
                            var transaction2 = {
                                "command" : "submit",
                                "id" : "3",
                                "tx_json" : {
                                    "TransactionType" : "Payment",
                                    "Account" : dyffyConfig.iouAddress,
                                    "Destination" : cashin.rippleAddress,
                                    "Amount" : {
                                        "currency" : "BTC",
                                        "value" : amount.toString(),
                                        "issuer" : dyffyConfig.iouAddress
                                    }
                                },
                                "secret" : dyffyConfig.iouSecret
                            }
                            transaction.paidOutDate = new Date();
                            cashin.save(function(err,cashined){
                                if (!isEmpty(cashined)){
                                    var rc = new WebSocketClient();
                                    rc.on('connect', function(connection){
                                        connection.on('message', function(message){
                                            message = JSON.parse(message.utf8Data);
                                            if (message.error){
                                                console.log(message.error);
                                            }
                                            else if (message.result && message.result.engine_result && message.result.engine_result == "tesSUCCESS"){
                                                var tranac = cashined.transactions.id(transaction._id);
                                                tranac.paidOutDate = new Date();
                                                tranac.rippleTransactionId = message.result.hash;
                                                cashined.save(function(err,finalCash){
                                                    if (!isEmpty(finalCash)){
                                                        depositSuccessEmail(finalCash.email, finalCash.rippleAddress, amount, 'BTC');
                                                        connection.close();
                                                    }
                                                });
                                            }
                                        });
                                        connection.send(JSON.stringify(transaction2));
                                    });
                                    rc.connect(dyffyConfig.rippled);
                                }
                            });
                        }
                        else if (!transaction){
                            var transac = new Transact({transactionId : tx.txid, amount : amount, initialDate : new Date()});
                            cashin.transactions.push(transac);
                            cashin.save(function(err,finalCash){
                                if (!isEmpty(finalCash)){
                                    dyffy.io.room(finalCash._id.toString()).broadcast('sentDeposit', {type:'BTC', amount:amount});
                                }
                            });
                        }
                        break;
                    default:

                }
            }
        });
    });
}
bitcoinWatcher.on('change', function (path, stats) {
    child_process.exec("tail -n1 " +  bitcoinTxLog, function (err, stdout) {
        if (err) return console.log(err);
        var lastline = stdout.replace(/(\r\n|\n|\r)/gm,'').split(' - ');
        //var txTime = lastline[0];
        var txId = lastline[1];
        getTxInfoBitcoin(txId);
    });
});

// Poll for transactions that walletnotify missed
setInterval(function () {
    bitcoinClient.listAccounts(0, function (err, accounts) {
        for (account in accounts) {
            bitcoinClient.listTransactions(account, function (err, txList) {
                if (txList){
                    for (var i = 0; i < txList.length; i++) {
                        getTxInfoBitcoin(txList[i].txid);
                    }
                }
            });
        }
    });
}, 60000);

var dogecoinTxLog = dyffyConfig.path + 'dogecoinTransactions.log';
var dogecoinWatcher = chokidar.watch(dogecoinTxLog, {ignored: /[\/\\]\./, persistent: true});
dogecoinWatcher.on('error', function (error) {
    console.error(error);
});

var getTxInfo = function (txId) {
    dogecoinClient.getTransaction(txId, function (err, tx) {
        if (err) return console.log(err);
        OutsideCashin.findOne({cashinAddress : tx.details[0].address},function(err,cashin){
            if (!isEmpty(cashin) && isEmpty(err)){
                var transaction;
                var doneTrans = true;
                if (cashin.transactions && cashin.transactions[0]){
                    cashin.transactions.forEach(function(transact){
                        if (transact.transactionId == tx.txid){
                            transaction = transact;
                        }
                    });
                }
                switch (tx.details[0].category) {
                    case 'send':
                        // Debit account
                        break;
                    case 'receive':
                        var amount = Number(tx.amount);
                        if (transaction && !transaction.paidOutDate && !isNaN(Number(tx.confirmations)) && Number(tx.confirmations) >= 3){
                            var transaction2 = {
                                "command" : "submit",
                                "id" : "3",
                                "tx_json" : {
                                    "TransactionType" : "Payment",
                                    "Account" : dyffyConfig.iouAddress,
                                    "Destination" : cashin.rippleAddress,
                                    "Amount" : {
                                        "currency" : "DOG",
                                        "value" : amount.toString(),
                                        "issuer" : dyffyConfig.iouAddress
                                    }
                                },
                                "secret" : dyffyConfig.iouSecret
                            }
                            transaction.paidOutDate = new Date();
                            cashin.save(function(err,cashined){
                                if (!isEmpty(cashined)){
                                    var rc = new WebSocketClient();
                                    rc.on('connect', function(connection){
                                        connection.on('message', function(message){
                                            message = JSON.parse(message.utf8Data);
                                            if (message.error){
                                                console.log(message.error);
                                            }
                                            else if (message.result && message.result.engine_result && message.result.engine_result == "tesSUCCESS"){
                                                var tranac = cashined.transactions.id(transaction._id);
                                                tranac.paidOutDate = new Date();
                                                tranac.rippleTransactionId = message.result.hash;
                                                cashined.save(function(err,finalCash){
                                                    if (!isEmpty(finalCash)){
                                                        depositSuccessEmail(finalCash.email, finalCash.rippleAddress, amount, 'DOG');
                                                        connection.close();
                                                    }
                                                });
                                            }
                                        });
                                        connection.send(JSON.stringify(transaction2));
                                    });
                                    rc.connect(dyffyConfig.rippled);
                                }
                            });
                        }
                        else if (!transaction){
                            var transac = new Transact({transactionId : tx.txid, amount : amount, initialDate : new Date()});
                            cashin.transactions.push(transac);
                            cashin.save(function(err,finalCash){
                                if (!isEmpty(finalCash)){
                                    dyffy.io.room(finalCash._id.toString()).broadcast('sentDeposit', {type:'DOG', amount:amount});
                                }
                            });
                        }
                        break;
                    default:

                }
            }
        });
    });
}
dogecoinWatcher.on('change', function (path, stats) {
    child_process.exec("tail -n1 " +  dogecoinTxLog, function (err, stdout) {
        if (err) return console.log(err);
        var lastline = stdout.replace(/(\r\n|\n|\r)/gm,'').split(' - ');
        //var txTime = lastline[0];
        var txId = lastline[1];
        getTxInfo(txId);
    });
});

// Poll for transactions that walletnotify missed
setInterval(function () {
    dogecoinClient.listAccounts(0, function (err, accounts) {
        for (account in accounts) {
            dogecoinClient.listTransactions(account, function (err, txList) {
                if (txList){
                    for (var i = 0; i < txList.length; i++) {
                        getTxInfo(txList[i].txid);
                    }
                }
            });
        }
    });
}, 60000);

var litecoinTxLog = dyffyConfig.path + 'litecoinTransactions.log';
var litecoinWatcher = chokidar.watch(litecoinTxLog, {ignored: /[\/\\]\./, persistent: true});
var getLTCTxInfo = function (txId) {
    litecoinClient.getTransaction(txId, function (err, tx) {
        if (err) return console.log(err);
        OutsideCashin.findOne({cashinAddress : tx.details[0].address},function(err,cashin){
            if (!isEmpty(cashin) && isEmpty(err)){
                var transaction;
                var doneTrans = true;
                if (cashin.transactions && cashin.transactions[0]){
                    cashin.transactions.forEach(function(transact){
                        if (transact.transactionId == tx.txid){
                            transaction = transact;
                        }
                    });
                }
                switch (tx.details[0].category) {
                    case 'send':
                        // Debit account

                        break;
                    case 'receive':
                        var amount = Number(tx.amount);
                        if (transaction && !transaction.paidOutDate && !isNaN(Number(tx.confirmations)) && Number(tx.confirmations) >= 3){
                            var transaction2 = {
                                "command" : "submit",
                                "id" : "3",
                                "tx_json" : {
                                    "TransactionType" : "Payment",
                                    "Account" : dyffyConfig.iouAddress,
                                    "Destination" : cashin.rippleAddress,
                                    "Amount" : {
                                        "currency" : "LTC",
                                        "value" : amount.toString(),
                                        "issuer" : dyffyConfig.iouAddress
                                    }
                                },
                                "secret" : dyffyConfig.iouSecret
                            }
                            transaction.paidOutDate = new Date();
                            cashin.save(function(err,cashined){
                                if (!isEmpty(cashined)){
                                    var rc = new WebSocketClient();
                                    rc.on('connect', function(connection){
                                        connection.on('message', function(message){
                                            message = JSON.parse(message.utf8Data);
                                            if (message.error){
                                                console.log(message.error);
                                            }
                                            else if (message.result && message.result.engine_result && message.result.engine_result == "tesSUCCESS"){
                                                var tranac = cashined.transactions.id(transaction._id);
                                                tranac.paidOutDate = new Date();
                                                tranac.rippleTransactionId = message.result.hash;
                                                cashined.save(function(err,finalCash){
                                                    if (!isEmpty(finalCash)){
                                                        depositSuccessEmail(finalCash.email, finalCash.rippleAddress, amount, 'LTC');
                                                        connection.close();
                                                    }
                                                });
                                            }
                                        });
                                        connection.send(JSON.stringify(transaction2));
                                    });
                                    rc.connect(dyffyConfig.rippled);
                                }
                            });
                        }
                        else if (!transaction){
                            var transac = new Transact({transactionId : tx.txid, amount : amount, initialDate : new Date()});
                            cashin.transactions.push(transac);
                            cashin.save(function(err,finalCash){
                                if (!isEmpty(finalCash)){
                                    dyffy.io.room(finalCash._id.toString()).broadcast('sentDeposit', {type:'LTC', amount:amount});
                                }
                            });
                        }

                        break;
                    default:

                }
            }
        });
    });
}
litecoinWatcher.on('error', function (error) {
    console.error(error);
});
litecoinWatcher.on('change', function (path, stats) {
    child_process.exec("tail -n1 " +  litecoinTxLog, function (err, stdout) {
        if (err) return console.log(err);
        var lastline = stdout.replace(/(\r\n|\n|\r)/gm,'').split(' - ');
        //var txTime = lastline[0];
        var txId = lastline[1];
        getLTCTxInfo(txId);
    });
});

// Poll for transactions that walletnotify missed
setInterval(function () {
    litecoinClient.listAccounts(0, function (err, accounts) {
        for (account in accounts) {
            litecoinClient.listTransactions(account, function (err, txList) {
                if (txList){
                    for (var i = 0; i < txList.length; i++) {
                        getLTCTxInfo(txList[i].txid);
                    }
                }
            });
        }
    });
}, 60000);

var peercoinTxLog = dyffyConfig.path + 'peercoinTransactions.log';
var peercoinWatcher = chokidar.watch(peercoinTxLog, {ignored: /[\/\\]\./, persistent: true});
var getPPCTxInfo = function (txId) {
    peercoinClient.getTransaction(txId, function (err, tx) {
        if (err) return console.log(err);
        OutsideCashin.findOne({cashinAddress : tx.details[0].address},function(err,cashin){
            if (!isEmpty(cashin) && isEmpty(err)){
                var transaction;
                var doneTrans = true;
                if (cashin.transactions && cashin.transactions[0]){
                    cashin.transactions.forEach(function(transact){
                        if (transact.transactionId == tx.txid){
                            transaction = transact;
                        }
                    });
                }
                switch (tx.details[0].category) {
                    case 'send':
                        // Debit account

                        break;
                    case 'receive':
                        var amount = Number(tx.amount);
                        if (transaction && !transaction.paidOutDate && !isNaN(Number(tx.confirmations)) && Number(tx.confirmations) >= 3){
                            var transaction2 = {
                                "command" : "submit",
                                "id" : "3",
                                "tx_json" : {
                                    "TransactionType" : "Payment",
                                    "Account" : dyffyConfig.iouAddress,
                                    "Destination" : cashin.rippleAddress,
                                    "Amount" : {
                                        "currency" : "PPC",
                                        "value" : amount.toString(),
                                        "issuer" : dyffyConfig.iouAddress
                                    }
                                },
                                "secret" : dyffyConfig.iouSecret
                            }
                            transaction.paidOutDate = new Date();
                            cashin.save(function(err,cashined){
                                if (!isEmpty(cashined)){
                                    var rc = new WebSocketClient();
                                    rc.on('connect', function(connection){
                                        connection.on('message', function(message){
                                            message = JSON.parse(message.utf8Data);
                                            if (message.error){
                                                console.log(message.error);
                                            }
                                            else if (message.result && message.result.engine_result && message.result.engine_result == "tesSUCCESS"){
                                                var tranac = cashined.transactions.id(transaction._id);
                                                tranac.paidOutDate = new Date();
                                                tranac.rippleTransactionId = message.result.hash;
                                                cashined.save(function(err,finalCash){
                                                    if (!isEmpty(finalCash)){
                                                        depositSuccessEmail(finalCash.email, finalCash.rippleAddress, amount, 'PPC');
                                                        connection.close();
                                                    }
                                                });
                                            }
                                        });
                                        connection.send(JSON.stringify(transaction2));
                                    });
                                    rc.connect(dyffyConfig.rippled);
                                }
                            });
                        }
                        else if (!transaction){
                            var transac = new Transact({transactionId : tx.txid, amount : amount, initialDate : new Date()});
                            cashin.transactions.push(transac);
                            cashin.save(function(err,finalCash){
                                if (!isEmpty(finalCash)){
                                    dyffy.io.room(finalCash._id.toString()).broadcast('sentDeposit', {type:'PPC', amount:amount});
                                }
                            });
                        }

                        break;
                    default:

                }
            }
        });
    });
}
peercoinWatcher.on('error', function (error) {
    console.error(error);
});
peercoinWatcher.on('change', function (path, stats) {
    child_process.exec("tail -n1 " +  peercoinTxLog, function (err, stdout) {
        if (err) return console.log(err);
        var lastline = stdout.replace(/(\r\n|\n|\r)/gm,'').split(' - ');
        //var txTime = lastline[0];
        var txId = lastline[1];
        getPPCTxInfo(txId);
    });
});

// Poll for transactions that walletnotify missed
setInterval(function () {
    peercoinClient.listAccounts(0, function (err, accounts) {
        for (account in accounts) {
            peercoinClient.listTransactions(account, function (err, txList) {
                if (txList){
                    for (var i = 0; i < txList.length; i++) {
                        getPPCTxInfo(txList[i].txid);
                    }
                }
            });
        }
    });
}, 60000);

var namecoinTxLog = dyffyConfig.path + 'namecoinTransactions.log';
var namecoinWatcher = chokidar.watch(namecoinTxLog, {ignored: /[\/\\]\./, persistent: true});
var getNMCTxInfo = function (txId) {
    namecoinClient.getTransaction(txId, function (err, tx) {
        if (err) return console.log(err);
        OutsideCashin.findOne({cashinAddress : tx.details[0].address},function(err,cashin){
            if (!isEmpty(cashin) && isEmpty(err)){
                var transaction;
                var doneTrans = true;
                if (cashin.transactions && cashin.transactions[0]){
                    cashin.transactions.forEach(function(transact){
                        if (transact.transactionId == tx.txid){
                            transaction = transact;
                        }
                    });
                }
                switch (tx.details[0].category) {
                    case 'send':
                        // Debit account

                        break;
                    case 'receive':
                        var amount = Number(tx.amount);
                        if (transaction && !transaction.paidOutDate && !isNaN(Number(tx.confirmations)) && Number(tx.confirmations) >= 3){
                            var transaction2 = {
                                "command" : "submit",
                                "id" : "3",
                                "tx_json" : {
                                    "TransactionType" : "Payment",
                                    "Account" : dyffyConfig.iouAddress,
                                    "Destination" : cashin.rippleAddress,
                                    "Amount" : {
                                        "currency" : "NMC",
                                        "value" : amount.toString(),
                                        "issuer" : dyffyConfig.iouAddress
                                    }
                                },
                                "secret" : dyffyConfig.iouSecret
                            }
                            transaction.paidOutDate = new Date();
                            cashin.save(function(err,cashined){
                                if (!isEmpty(cashined)){
                                    var rc = new WebSocketClient();
                                    rc.on('connect', function(connection){
                                        connection.on('message', function(message){
                                            message = JSON.parse(message.utf8Data);
                                            if (message.error){
                                                console.log(message.error);
                                            }
                                            else if (message.result && message.result.engine_result && message.result.engine_result == "tesSUCCESS"){
                                                var tranac = cashined.transactions.id(transaction._id);
                                                tranac.paidOutDate = new Date();
                                                tranac.rippleTransactionId = message.result.hash;
                                                cashined.save(function(err,finalCash){
                                                    if (!isEmpty(finalCash)){
                                                        depositSuccessEmail(finalCash.email, finalCash.rippleAddress, amount, 'NMC');
                                                        connection.close();
                                                    }
                                                });
                                            }
                                        });
                                        connection.send(JSON.stringify(transaction2));
                                    });
                                    rc.connect(dyffyConfig.rippled);
                                }
                            });
                        }
                        else if (!transaction){
                            var transac = new Transact({transactionId : tx.txid, amount : amount, initialDate : new Date()});
                            cashin.transactions.push(transac);
                            cashin.save(function(err,finalCash){
                                if (!isEmpty(finalCash)){
                                    dyffy.io.room(finalCash._id.toString()).broadcast('sentDeposit', {type:'NMC', amount:amount});
                                }
                            });
                        }

                        break;
                    default:

                }
            }
        });
    });
}
namecoinWatcher.on('error', function (error) {
    console.error(error);
});
namecoinWatcher.on('change', function (path, stats) {
    child_process.exec("tail -n1 " +  namecoinTxLog, function (err, stdout) {
        if (err) return console.log(err);
        var lastline = stdout.replace(/(\r\n|\n|\r)/gm,'').split(' - ');
        //var txTime = lastline[0];
        var txId = lastline[1];
        getNMCTxInfo(txId);
    });
});

// Poll for transactions that walletnotify missed
setInterval(function () {
    namecoinClient.listAccounts(0, function (err, accounts) {
        for (account in accounts) {
            namecoinClient.listTransactions(account, function (err, txList) {
                if (txList){
                    for (var i = 0; i < txList.length; i++) {
                        getNMCTxInfo(txList[i].txid);
                    }
                }
            });
        }
    });
}, 60000);

setInterval(function () {
    OutsideCashin.find({type : 'NXT'}, function(err,cashin){
        if (cashin){
            cashin.forEach(function(nxter){

                $.getJSON(
                    'http://localhost:7874/nxt?requestType=getAccountTransactionIds&account=' + nxter.cashinAddress + '&timestamp=0',
                    function(data) {

                        if (data.transactionIds){
                            data.transactionIds.forEach(function(transactionId){
                                $.getJSON(
                                    'http://localhost:7874/nxt?requestType=getTransaction&transaction=' + transactionId,
                                    function(tx) {
                                        if (tx.recipient != dyffyConfig.mainNXTAccount){
                                            OutsideCashin.findById(nxter._id,function(err,nxt){
                                                if (!isEmpty(nxt)){
                                                    var alreadyIn;
                                                    var transaction;
                                                    if (nxt.transactions){
                                                        nxt.transactions.forEach(function(trans){
                                                            if (trans.transactionId == transactionId){
                                                                transaction = trans;
                                                            }
                                                        });
                                                    }
                                                    var amount = Number(tx.amount);
                                                    var amountMinus = amount - 1;
                                                    $.getJSON(
                                                        'http://localhost:7874/nxt?requestType=sendMoney&secretPhrase=' + nxt.cashinSecret + '&recipient=' + dyffyConfig.mainNXTAccount + '&amount=' + amountMinus + '&fee=1&deadline=900&referencedTransaction=' + transactionId,
                                                        function(data) {

                                                        });
                                                    if (transaction && !transaction.paidOutDate && !isNaN(Number(tx.confirmations)) && Number(tx.confirmations) >= 10){
                                                        var transaction2 = {
                                                            "command" : "submit",
                                                            "id" : "3",
                                                            "tx_json" : {
                                                                "TransactionType" : "Payment",
                                                                "Account" : dyffyConfig.iouAddress,
                                                                "Destination" : nxt.rippleAddress,
                                                                "Amount" : {
                                                                    "currency" : "NXT",
                                                                    "value" : amount.toString(),
                                                                    "issuer" : dyffyConfig.iouAddress
                                                                }
                                                            },
                                                            "secret" : dyffyConfig.iouSecret
                                                        }
                                                        transaction.paidOutDate = new Date();
                                                        nxt.save(function(err,cashined){
                                                            if (!isEmpty(cashined)){
                                                                var rc = new WebSocketClient();
                                                                rc.on('connect', function(connection){
                                                                    connection.on('message', function(message){
                                                                        message = JSON.parse(message.utf8Data);
                                                                        if (message.error){
                                                                            console.log(message.error);
                                                                        }
                                                                        else if (message.result && message.result.engine_result && message.result.engine_result == "tesSUCCESS"){
                                                                            var tranac = cashined.transactions.id(transaction._id);
                                                                            tranac.paidOutDate = new Date();
                                                                            tranac.rippleTransactionId = message.result.hash;
                                                                            cashined.save(function(err,finalCash){
                                                                                if (!isEmpty(finalCash)){
                                                                                    depositSuccessEmail(finalCash.email, finalCash.rippleAddress, amount, 'NXT');
                                                                                    $.getJSON(
                                                                                        'http://localhost:7874/nxt?requestType=sendMoney&secretPhrase=' + finalCash.cashinSecret + '&recipient=' + dyffyConfig.mainNXTAccount + '&amount=' + amount + '&fee=1&deadline=900&referencedTransaction=' + transactionId,
                                                                                        function(data) {

                                                                                            connection.close();
                                                                                        });

                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                    connection.send(JSON.stringify(transaction2));
                                                                });
                                                                rc.connect(dyffyConfig.rippled);
                                                            }
                                                        });
                                                    }
                                                    else if (!transaction){
                                                        var transac = new Transact({transactionId : transactionId, amount : amount, initialDate : new Date()});
                                                        nxt.transactions.push(transac);
                                                        nxt.save(function(err,finalCash){
                                                            if (!isEmpty(finalCash)){
                                                                dyffy.io.room(finalCash._id.toString()).broadcast('sentDeposit', {type:'NXT', amount:amount});
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    });
                            });
                        }
                    });
            });
        }
    });
}, 100000);
//watcher.close();

//    var transacts = new Schema({
//        id    : ObjectId
//        , transactionId : String
//        , amount : Number
//        , rippleTransactionId : String
//        , paidOutDate : Date
//        , refundDate : Date
//        , initialDate : Date
//    });

// we will use one address for all IOUs and not https://ripple.com/wiki/Require_a_destination_tag_for_inbound_payments for now

// TODO : https://ripple.com/wiki/Returning_payments

function bitcoinLitecoinCashoutSubscribe(){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            message = JSON.parse(message.utf8Data);
            if (message.error){

            }
            else {
                if (message.engine_result && message.type == 'transaction' && message.validated && message.engine_result == "tesSUCCESS" && message.transaction && message.transaction.DestinationTag && message.transaction.InvoiceID && message.transaction.Amount){
                    OutsideCashout.findOne({destinationTag : message.transaction.DestinationTag}, function(err,cashoutObject){
                        if (!isEmpty(cashoutObject) && isEmpty(err)){
                            var transacter;
                            if (cashoutObject && cashoutObject.transactions){
                                cashoutObject.transactions.forEach(function(trans){
                                    if (trans.rippleTransactionId == message.transaction.hash){
                                        transacter = trans;
                                    }
                                });
                            }
                            if (message.transaction.Amount.currency == 'NXT'){
                                if (!transacter){
                                    var transacted = new Transact({rippleTransactionId : message.transaction.hash, amount : message.transaction.Amount.value, initialDate : new Date()});
                                    cashoutObject.transactions.push(transacted);
                                    cashoutObject.cashoutAddress = message.transaction.InvoiceID.replace(/^[0]+/g,"");
                                    cashoutObject.save(function(err,cashoutTwo){
                                        if (!isEmpty(cashoutTwo)){
                                            var finalTransaction = cashoutTwo.transactions.id(transacted._id);
                                            if (cashoutTwo.type == 'NXT' && message.transaction.Amount.currency == 'NXT' && !finalTransaction.paidOutDate){
                                                finalTransaction.paidOutDate = new Date();
                                                $.getJSON(
                                                    'http://localhost:7874/nxt?requestType=sendMoney&secretPhrase=' + dyffyConfig.mainNXTSecret + '&recipient=' + cashoutObject.cashoutAddress + '&amount=' + (Math.floor(Number(message.transaction.Amount.value) - 1)).toString() + '&fee=1&deadline=900',
                                                    function(data) {
                                                        finalTransaction.paidOutDate = new Date();
                                                        finalTransaction.rippleTransactionId = message.hash;
                                                        finalTransaction.transactionId = data.transaction;
                                                        finalTransaction.fromAddress = message.transaction.Account;

                                                        cashoutObject.save(function(err,cashout){
                                                            if (!isEmpty(cashout)){

                                                            }
                                                        });
                                                    });
                                            }
                                            else {
                                                // TODO : send back
                                                // TODO : failure notification
                                                // TODO : failure email
                                            }
                                        }
                                    });
                                }
                                else {
                                    if (cashoutObject.type == 'NXT' && message.transaction.Amount.currency == 'NXT' && !transacter.paidOutDate){
                                        transacter.paidOutDate = new Date();
                                        cashoutObject.cashoutAddress = message.transaction.InvoiceID.replace(/^[0]+/g,"");
                                        $.getJSON(
                                            'http://localhost:7874/nxt?requestType=sendMoney&secretPhrase=' + dyffyConfig.mainNXTSecret + '&recipient=' + cashoutObject.cashoutAddress + '&amount=' + (Math.floor(Number(message.transaction.Amount.value) - 1)).toString() + '&fee=1&deadline=900',
                                            function(data) {
                                                console.log(JSON.stringify(data));
                                                transacter.paidOutDate = new Date();
                                                transacter.rippleTransactionId = message.hash;
                                                transacter.transactionId = data.transaction;
                                                transacter.fromAddress = message.transaction.Account;

                                                cashoutObject.save(function(err,cashout){
                                                    if (!isEmpty(cashout)){

                                                    }
                                                });
                                            });
                                    }
                                    else {
                                        // TODO : send back
                                        // TODO : failure notification
                                        // TODO : failure email
                                    }
                                }
                            }
                            else if (message.transaction.Amount.currency == 'BTC'){
                                if (!transacter){
                                    var transacted = new Transact({rippleTransactionId : message.transaction.hash, amount : message.transaction.Amount.value, initialDate : new Date()});
                                    child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                        function (error, stdout, stderr) {
                                            if (!error) {
                                                cashoutObject.transactions.push(transacted);
                                                cashoutObject.cashoutAddress = stdout;
                                                cashoutObject.save(function(err,cashoutTwo){
                                                    if (!isEmpty(cashoutTwo)){
                                                        var finalTransaction = cashoutTwo.transactions.id(transacted._id);
                                                        if (cashoutTwo.type == 'BTC' && message.transaction.Amount.currency == 'BTC' && !finalTransaction.paidOutDate){
                                                            finalTransaction.paidOutDate = new Date();
                                                            bitcoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                                if (err) return console.log(err);
                                                                finalTransaction.paidOutDate = new Date();
                                                                finalTransaction.rippleTransactionId = message.hash;
                                                                finalTransaction.transactionId = sendFrom;
                                                                finalTransaction.fromAddress = message.transaction.Account;

                                                                cashoutObject.save(function(err,cashout){
                                                                    if (!isEmpty(cashout)){

                                                                    }
                                                                });
                                                            });
                                                        }
                                                        else {
                                                            // TODO : send back
                                                            // TODO : failure notification
                                                            // TODO : failure email
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }
                                else {
                                    if (cashoutObject.type == 'BTC' && message.transaction.Amount.currency == 'BTC' && !transacter.paidOutDate){
                                        child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                            function (error, stdout, stderr) {
                                                if (!error) {
                                                    transacter.paidOutDate = new Date();
                                                    cashoutObject.cashoutAddress = stdout;
                                                    bitcoinClient.sendFrom('dyffy', cashoutObject.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                        if (err) return console.log(err);
                                                        transacter.paidOutDate = new Date();
                                                        transacter.rippleTransactionId = message.hash;
                                                        transacter.transactionId = sendFrom;
                                                        transacter.fromAddress = message.transaction.Account;

                                                        cashoutObject.save(function(err,cashout){
                                                            if (!isEmpty(cashout)){

                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                    }
                                    else {
                                        // TODO : send back
                                        // TODO : failure notification
                                        // TODO : failure email
                                    }
                                }
                            }
                            else if (message.transaction.Amount.currency == 'DOG'){
                                if (!transacter){
                                    var transacted = new Transact({rippleTransactionId : message.transaction.hash, amount : message.transaction.Amount.value, initialDate : new Date()});
                                    child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                        function (error, stdout, stderr) {
                                            if (!error) {
                                                cashoutObject.transactions.push(transacted);
                                                cashoutObject.cashoutAddress = stdout;
                                                cashoutObject.save(function(err,cashoutTwo){
                                                    if (!isEmpty(cashoutTwo)){
                                                        var finalTransaction = cashoutTwo.transactions.id(transacted._id);
                                                        if (cashoutTwo.type == 'DOG' && message.transaction.Amount.currency == 'DOG' && !finalTransaction.paidOutDate){
                                                            finalTransaction.paidOutDate = new Date();
                                                            dogecoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                                if (err) return console.log(err);
                                                                finalTransaction.paidOutDate = new Date();
                                                                finalTransaction.rippleTransactionId = message.hash;
                                                                finalTransaction.transactionId = sendFrom;
                                                                finalTransaction.fromAddress = message.transaction.Account;

                                                                cashoutObject.save(function(err,cashout){
                                                                    if (!isEmpty(cashout)){

                                                                    }
                                                                });
                                                            });
                                                        }
                                                        else {
                                                            // TODO : send back
                                                            // TODO : failure notification
                                                            // TODO : failure email
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }
                                else {
                                    if (cashoutObject.type == 'DOG' && message.transaction.Amount.currency == 'DOG' && !transacter.paidOutDate){
                                        child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                            function (error, stdout, stderr) {
                                                if (!error) {
                                                    transacter.paidOutDate = new Date();
                                                    cashoutObject.cashoutAddress = stdout;
                                                    dogecoinClient.sendFrom('dyffy', cashoutObject.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                        if (err) return console.log(err);
                                                        transacter.paidOutDate = new Date();
                                                        transacter.rippleTransactionId = message.hash;
                                                        transacter.transactionId = sendFrom;
                                                        transacter.fromAddress = message.transaction.Account;

                                                        cashoutObject.save(function(err,cashout){
                                                            if (!isEmpty(cashout)){

                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                    }
                                    else {
                                        // TODO : send back
                                        // TODO : failure notification
                                        // TODO : failure email
                                    }
                                }
                            }
                            else if (message.transaction.Amount.currency == 'LTC'){
                                if (!transacter){
                                    var transacted = new Transact({rippleTransactionId : message.transaction.hash, amount : message.transaction.Amount.value, initialDate : new Date()});
                                    child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                        function (error, stdout, stderr) {
                                            if (!error) {
                                                cashoutObject.transactions.push(transacted);
                                                cashoutObject.cashoutAddress = stdout;
                                                cashoutObject.save(function(err,cashoutTwo){
                                                    if (!isEmpty(cashoutTwo)){
                                                        var finalTransaction = cashoutTwo.transactions.id(transacted._id);
                                                        if (cashoutTwo.type == 'LTC' && message.transaction.Amount.currency == 'LTC' && !finalTransaction.paidOutDate){
                                                            finalTransaction.paidOutDate = new Date();
                                                            litecoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                                if (err) return console.log(err);
                                                                finalTransaction.paidOutDate = new Date();
                                                                finalTransaction.rippleTransactionId = message.hash;
                                                                finalTransaction.transactionId = sendFrom;
                                                                finalTransaction.fromAddress = message.transaction.Account;

                                                                cashoutObject.save(function(err,cashout){
                                                                    if (!isEmpty(cashout)){

                                                                    }
                                                                });
                                                            });
                                                        }
                                                        else {
                                                            // TODO : send back
                                                            // TODO : failure notification
                                                            // TODO : failure email
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }
                                else {
                                    if (cashoutObject.type == 'LTC' && message.transaction.Amount.currency == 'LTC' && !transacter.paidOutDate){
                                        child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                            function (error, stdout, stderr) {
                                                if (!error) {
                                                    transacter.paidOutDate = new Date();
                                                    cashoutObject.cashoutAddress = stdout;
                                                    litecoinClient.sendFrom('dyffy', cashoutObject.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                        if (err) return console.log(err);
                                                        transacter.paidOutDate = new Date();
                                                        transacter.rippleTransactionId = message.hash;
                                                        transacter.transactionId = sendFrom;
                                                        transacter.fromAddress = message.transaction.Account;

                                                        cashoutObject.save(function(err,cashout){
                                                            if (!isEmpty(cashout)){

                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                    }
                                    else {
                                        // TODO : send back
                                        // TODO : failure notification
                                        // TODO : failure email
                                    }
                                }
                            }
                            else if (message.transaction.Amount.currency == 'NMC'){
                                if (!transacter){
                                    var transacted = new Transact({rippleTransactionId : message.transaction.hash, amount : message.transaction.Amount.value, initialDate : new Date()});
                                    child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                        function (error, stdout, stderr) {
                                            if (!error) {
                                                cashoutObject.transactions.push(transacted);
                                                cashoutObject.cashoutAddress = stdout;
                                                cashoutObject.save(function(err,cashoutTwo){
                                                    if (!isEmpty(cashoutTwo)){
                                                        var finalTransaction = cashoutTwo.transactions.id(transacted._id);
                                                        if (cashoutTwo.type == 'NMC' && message.transaction.Amount.currency == 'NMC' && !finalTransaction.paidOutDate){
                                                            finalTransaction.paidOutDate = new Date();
                                                            namecoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                                if (err) return console.log(err);
                                                                finalTransaction.paidOutDate = new Date();
                                                                finalTransaction.rippleTransactionId = message.hash;
                                                                finalTransaction.transactionId = sendFrom;
                                                                finalTransaction.fromAddress = message.transaction.Account;

                                                                cashoutObject.save(function(err,cashout){
                                                                    if (!isEmpty(cashout)){

                                                                    }
                                                                });
                                                            });
                                                        }
                                                        else {
                                                            // TODO : send back
                                                            // TODO : failure notification
                                                            // TODO : failure email
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }
                                else {
                                    if (cashoutObject.type == 'NMC' && message.transaction.Amount.currency == 'NMC' && !transacter.paidOutDate){
                                        child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                            function (error, stdout, stderr) {
                                                if (!error) {
                                                    transacter.paidOutDate = new Date();
                                                    cashoutObject.cashoutAddress = stdout;
                                                    namecoinClient.sendFrom('dyffy', cashoutObject.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                        if (err) return console.log(err);
                                                        transacter.paidOutDate = new Date();
                                                        transacter.rippleTransactionId = message.hash;
                                                        transacter.transactionId = sendFrom;
                                                        transacter.fromAddress = message.transaction.Account;

                                                        cashoutObject.save(function(err,cashout){
                                                            if (!isEmpty(cashout)){

                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                    }
                                    else {
                                        // TODO : send back
                                        // TODO : failure notification
                                        // TODO : failure email
                                    }
                                }
                            }
                            else if (message.transaction.Amount.currency == 'PPC'){
                                if (!transacter){
                                    var transacted = new Transact({rippleTransactionId : message.transaction.hash, amount : message.transaction.Amount.value, initialDate : new Date()});
                                    child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                        function (error, stdout, stderr) {
                                            if (!error) {
                                                cashoutObject.transactions.push(transacted);
                                                cashoutObject.cashoutAddress = stdout;
                                                cashoutObject.save(function(err,cashoutTwo){
                                                    if (!isEmpty(cashoutTwo)){
                                                        var finalTransaction = cashoutTwo.transactions.id(transacted._id);
                                                        if (cashoutTwo.type == 'PPC' && message.transaction.Amount.currency == 'PPC' && !finalTransaction.paidOutDate){
                                                            finalTransaction.paidOutDate = new Date();
                                                            peercoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                                if (err) return console.log(err);
                                                                finalTransaction.paidOutDate = new Date();
                                                                finalTransaction.rippleTransactionId = message.hash;
                                                                finalTransaction.transactionId = sendFrom;
                                                                finalTransaction.fromAddress = message.transaction.Account;

                                                                cashoutObject.save(function(err,cashout){
                                                                    if (!isEmpty(cashout)){

                                                                    }
                                                                });
                                                            });
                                                        }
                                                        else {
                                                            // TODO : send back
                                                            // TODO : failure notification
                                                            // TODO : failure email
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }
                                else {
                                    if (cashoutObject.type == 'PPC' && message.transaction.Amount.currency == 'PPC' && !transacter.paidOutDate){
                                        child_process.exec('python /home/ubuntu/parse_invoice.py ' + encodeURIComponent(message.transaction.InvoiceID),
                                            function (error, stdout, stderr) {
                                                if (!error) {
                                                    transacter.paidOutDate = new Date();
                                                    cashoutObject.cashoutAddress = stdout;
                                                    peercoinClient.sendFrom('dyffy', cashoutObject.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                                        if (err) return console.log(err);
                                                        transacter.paidOutDate = new Date();
                                                        transacter.rippleTransactionId = message.hash;
                                                        transacter.transactionId = sendFrom;
                                                        transacter.fromAddress = message.transaction.Account;

                                                        cashoutObject.save(function(err,cashout){
                                                            if (!isEmpty(cashout)){

                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                    }
                                    else {
                                        // TODO : send back
                                        // TODO : failure notification
                                        // TODO : failure email
                                    }
                                }
                            }
                        }
                        else if (!isEmpty(err) || isEmpty(cashoutObject)){
                            // TODO : send back
                            // TODO : failure notification
                            // TODO : failure email
                        }
                    });
                }
                else if (message.engine_result && message.type == 'transaction' && message.validated && message.engine_result == "tesSUCCESS" && message.transaction && message.transaction.DestinationTag && message.transaction.Amount && (message.transaction.Amount.currency == 'BTC' || message.transaction.Amount.currency == 'LTC' || message.transaction.Amount.currency == 'NXT' || message.transaction.Amount.currency == 'NMC' || message.transaction.Amount.currency == 'PPC' || message.transaction.Amount.currency == 'DOG')){
                    OutsideCashout.findOne({destinationTag : message.transaction.DestinationTag}, function(err,cashoutObject){
                        if (!isEmpty(cashoutObject) && isEmpty(err)){
                            var transacted = new Transact({rippleTransactionId : message.transaction.hash, amount : message.transaction.Amount.value, initialDate : new Date()});
                            cashoutObject.transactions.push(transacted);

                            cashoutObject.save(function(err,cashoutTwo){
                                if (!isEmpty(cashoutTwo)){
                                    var finalTransaction = cashoutTwo.transactions.id(transacted._id);
                                    if (cashoutTwo.type == 'BTC' && message.transaction.Amount.currency == 'BTC' && !finalTransaction.paidOutDate){
                                        finalTransaction.paidOutDate = new Date();
                                        bitcoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                            if (err) return console.log(err);

                                            finalTransaction.paidOutDate = new Date();
                                            finalTransaction.rippleTransactionId = message.hash;
                                            finalTransaction.transactionId = sendFrom;
                                            finalTransaction.fromAddress = message.transaction.Account;

                                            cashoutTwo.save(function(err,cashout){
                                                if (!isEmpty(cashout)){
                                                    withdrawSuccessEmail(cashout.email, message.transaction.Account, message.transaction.Amount.value, cashout.cashoutAddress, message.transaction.Amount.currency);
                                                    dyffy.io.room(cashoutTwo._id.toString()).broadcast('sentWithdraw', {amount : message.transaction.Amount.value, address : cashoutTwo.cashoutAddress, type : cashoutTwo.type});
                                                }
                                            });
                                        });
                                    }
                                    else if (cashoutTwo.type == 'DOG' && message.transaction.Amount.currency == 'DOG' && !finalTransaction.paidOutDate){
                                        finalTransaction.paidOutDate = new Date();
                                        dogecoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                            if (err) return console.log(err);

                                            finalTransaction.paidOutDate = new Date();
                                            finalTransaction.rippleTransactionId = message.hash;
                                            finalTransaction.transactionId = sendFrom;
                                            finalTransaction.fromAddress = message.transaction.Account;

                                            cashoutTwo.save(function(err,cashout){
                                                if (!isEmpty(cashout)){
                                                    withdrawSuccessEmail(cashout.email, message.transaction.Account, message.transaction.Amount.value, cashout.cashoutAddress, message.transaction.Amount.currency);
                                                    dyffy.io.room(cashoutTwo._id.toString()).broadcast('sentWithdraw', {amount : message.transaction.Amount.value, address : cashoutTwo.cashoutAddress, type : cashoutTwo.type});
                                                }
                                            });
                                        });
                                    }
                                    else if (cashoutTwo.type == 'LTC' && message.transaction.Amount.currency == 'LTC' && !finalTransaction.paidOutDate){
                                        finalTransaction.paidOutDate = new Date();
                                        litecoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                            if (err) return console.log(err);

                                            finalTransaction.paidOutDate = new Date();
                                            finalTransaction.rippleTransactionId = message.hash;
                                            finalTransaction.transactionId = sendFrom;
                                            finalTransaction.fromAddress = message.transaction.Account;

                                            cashoutTwo.save(function(err,cashout){
                                                if (!isEmpty(cashout)){
                                                    withdrawSuccessEmail(cashout.email, message.transaction.Account, message.transaction.Amount.value, cashout.cashoutAddress, message.transaction.Amount.currency);
                                                    dyffy.io.room(cashoutTwo._id.toString()).broadcast('sentWithdraw', {amount : message.transaction.Amount.value, address : cashoutTwo.cashoutAddress, type : cashoutTwo.type});
                                                }
                                            });
                                        });
                                    }
                                    else if (cashoutTwo.type == 'NMC' && message.transaction.Amount.currency == 'NMC' && !finalTransaction.paidOutDate){
                                        finalTransaction.paidOutDate = new Date();
                                        namecoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                            if (err) return console.log(err);

                                            finalTransaction.paidOutDate = new Date();
                                            finalTransaction.rippleTransactionId = message.hash;
                                            finalTransaction.transactionId = sendFrom;
                                            finalTransaction.fromAddress = message.transaction.Account;

                                            cashoutTwo.save(function(err,cashout){
                                                if (!isEmpty(cashout)){
                                                    withdrawSuccessEmail(cashout.email, message.transaction.Account, message.transaction.Amount.value, cashout.cashoutAddress, message.transaction.Amount.currency);
                                                    dyffy.io.room(cashoutTwo._id.toString()).broadcast('sentWithdraw', {amount : message.transaction.Amount.value, address : cashoutTwo.cashoutAddress, type : cashoutTwo.type});
                                                }
                                            });
                                        });
                                    }
                                    else if (cashoutTwo.type == 'PPC' && message.transaction.Amount.currency == 'PPC' && !finalTransaction.paidOutDate){
                                        finalTransaction.paidOutDate = new Date();
                                        peercoinClient.sendFrom('dyffy', cashoutTwo.cashoutAddress, Number(message.transaction.Amount.value), function (err, sendFrom) {
                                            if (err) return console.log(err);

                                            finalTransaction.paidOutDate = new Date();
                                            finalTransaction.rippleTransactionId = message.hash;
                                            finalTransaction.transactionId = sendFrom;
                                            finalTransaction.fromAddress = message.transaction.Account;

                                            cashoutTwo.save(function(err,cashout){
                                                if (!isEmpty(cashout)){
                                                    withdrawSuccessEmail(cashout.email, message.transaction.Account, message.transaction.Amount.value, cashout.cashoutAddress, message.transaction.Amount.currency);
                                                    dyffy.io.room(cashoutTwo._id.toString()).broadcast('sentWithdraw', {amount : message.transaction.Amount.value, address : cashoutTwo.cashoutAddress, type : cashoutTwo.type});
                                                }
                                            });
                                        });
                                    }
                                    else if (cashoutTwo.type == 'NXT' && message.transaction.Amount.currency == 'NXT' && !finalTransaction.paidOutDate){
                                        finalTransaction.paidOutDate = new Date();
                                        $.getJSON(
                                            'http://localhost:7874/nxt?requestType=sendMoney&secretPhrase=' + dyffyConfig.mainNXTSecret + '&recipient=' + cashoutObject.cashoutAddress + '&amount=' + (Math.floor(Number(message.transaction.Amount.value) - 1)).toString() + '&fee=1&deadline=900',
                                            function(data) {
                                                console.log(JSON.stringify(data));
                                                finalTransaction.paidOutDate = new Date();
                                                finalTransaction.rippleTransactionId = message.hash;
                                                finalTransaction.transactionId = data.transaction;
                                                finalTransaction.fromAddress = message.transaction.Account;

                                                cashoutObject.save(function(err,cashout){
                                                    if (!isEmpty(cashout)){
                                                        withdrawSuccessEmail(cashout.email, message.transaction.Account, message.transaction.Amount.value, cashout.cashoutAddress, message.transaction.Amount.currency);
                                                        dyffy.io.room(cashoutObject._id.toString()).broadcast('sentWithdraw', {amount : message.transaction.Amount.value, address : cashoutObject.cashoutAddress, type : cashoutObject.type});
                                                    }
                                                });
                                            });
                                    }
                                    else {
                                        // TODO : send back
                                        // TODO : failure notification
                                        // TODO : failure email
                                    }
                                }
                            });
                        }
                        else if (!isEmpty(err) || isEmpty(cashoutObject)){
                            // TODO : send back
                            // TODO : failure notification
                            // TODO : failure email
                        }
                    });
                }
                else if (message.engine_result && message.type == 'transaction' && message.validated && message.engine_result == "tesSUCCESS" && message.transaction && message.transaction.Amount && (message.transaction.Amount.currency == 'BTC' || message.transaction.Amount.currency == 'LTC') && !message.transaction.DestinationTag){
                    // TODO : send back
                    // TODO : failure notification
                    // TODO : failure email
                }
            }
        });
        connection.send(JSON.stringify({command:"subscribe", accounts: [dyffyConfig.iouAddress]}));
    });
    rc.connect(dyffyConfig.rippled);
}

bitcoinLitecoinCashoutSubscribe();
// TODO : include links to prefilled client on QR code
function withdrawEmail(email, cryptoAddress, address, destinationTag, qr, currency){
    var html;
    if (currency == 'BTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a transaction. If successful, a withdrawal should take only a few minutes. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Bitcoin withdrawal pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send BTC to ' + address + '?dt=' + destinationTag + ' for deposit into Bitcoin wallet with address ' + cryptoAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'LTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a transaction. If successful, a withdrawal should take only a few minutes. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Litecoin withdrawal pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send LTC to ' + address + '?dt=' + destinationTag + ' for deposit into Litecoin wallet with address ' + cryptoAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NMC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a transaction. If successful, a withdrawal should take only a few minutes. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Namecoin withdrawal pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send NMC to ' + address + '?dt=' + destinationTag + ' for deposit into Namecoin wallet with address ' + cryptoAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'PPC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a transaction. If successful, a withdrawal should take only a few minutes. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Peercoin withdrawal pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send PPC to ' + address + '?dt=' + destinationTag + ' for deposit into Peercoin wallet with address ' + cryptoAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NXT'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a transaction. If successful, a withdrawal should take only a few minutes. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> NXT withdrawal pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send NXT to ' + address + '?dt=' + destinationTag + ' for deposit into NXT wallet with address ' + cryptoAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'DOG'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a transaction. If successful, a withdrawal should take only a few minutes. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dogecoin withdrawal pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send DOG to ' + address + '?dt=' + destinationTag + ' for deposit into Dogecoin wallet with address ' + cryptoAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    sendEmail(email, "Dyffy " + currency + " withdrawal initiated.", html, "Dyffy Simple Gateway " + currency + " Withdrawal Initiated");
}
function withdrawSuccessEmail(email, address, amount, toAddress, currency){
    var html;
    if (currency == 'BTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Bitcoin withdrawal succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount.toString() + ' BTC successfully withdrawn to ' + toAddress + ' wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'LTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Litecoin withdrawal succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount.toString() + ' LTC successfully withdrawn to ' + toAddress + ' wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NMC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Namecoin withdrawal succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount.toString() + ' NMC successfully withdrawn to ' + toAddress + ' wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'PPC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Peercoin withdrawal succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount.toString() + ' PPC successfully withdrawn to ' + toAddress + ' wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NXT'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> NXT withdrawal succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount.toString() + ' NXT successfully withdrawn to ' + toAddress + ' wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'DOG'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dogecoin withdrawal succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount.toString() + ' DOG successfully withdrawn to ' + toAddress + ' wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    sendEmail(email, "Dyffy " + currency + " withdrawal succeeded.", html, "Dyffy Simple Gateway " + currency + " Withdrawal Succeeded ✔");
}
function withdrawErrorEmail(email, address, amount, toAddress, currency){
    var html;
    if (currency == 'BTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Bitcoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + 'failed. Make sure you include the proper destination tag attached to the Ripple withdrawal address. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'LTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Litecoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + 'failed. Make sure you include the proper destination tag attached to the Ripple withdrawal address. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NMC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Namecoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + 'failed. Make sure you include the proper destination tag attached to the Ripple withdrawal address. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'PPC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Peercoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + 'failed. Make sure you include the proper destination tag attached to the Ripple withdrawal address. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NXT'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> NXT withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + 'failed. Make sure you include the proper destination tag attached to the Ripple withdrawal address. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'DOG'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dogecoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + 'failed. Make sure you include the proper destination tag attached to the Ripple withdrawal address. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    sendEmail(email, "Dyffy " + currency + " withdrawal failed.", html, "Dyffy Simple Gateway " + currency + " Withdrawal Failed");
}
function depositEmail(email, address, qr, currency, rippleAddress){
    var html;
    if (currency == 'BTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a deposit. We must wait 6 Bitcoin confirmations to issue BTC on Ripple which could take 30 minutes to 1 hour depending on the miner fee. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Bitcoin deposit pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send BTC to ' + address + ' for deposit into Ripple wallet with address ' + rippleAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'LTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a deposit. We must wait 6 Litecoin confirmations to issue LTC on Ripple which could take 30 minutes to 1 hour depending on the miner fee. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Litecoin deposit pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send LTC to ' + address + ' for deposit into Ripple wallet with address ' + rippleAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NMC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a deposit. We must wait 6 Namecoin confirmations to issue NMC on Ripple which could take 30 minutes to 1 hour depending on the miner fee. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Namecoin deposit pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send NMC to ' + address + ' for deposit into Ripple wallet with address ' + rippleAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'PPC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a deposit. We must wait 6 Peercoin confirmations to issue PPC on Ripple which could take 30 minutes to 1 hour depending on the miner fee. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Peercoin deposit pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send PPC to ' + address + ' for deposit into Ripple wallet with address ' + rippleAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NXT'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a deposit. We must wait 6 NXT confirmations to issue NXT on Ripple which could take 30 minutes to 1 hour depending on the miner fee. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> NXT deposit pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send NXT to ' + address + ' for deposit into Ripple wallet with address ' + rippleAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'DOG'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have initiated a deposit. We must wait 6 Dogecoin confirmations to issue DOG on Ripple which could take 30 minutes to 1 hour depending on the miner fee. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="' + qr + '" alt="QR Code" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dogecoin deposit pending </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Send DOG to ' + address + ' for deposit into Ripple wallet with address ' + rippleAddress + ' </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    sendEmail(email, "Dyffy " + currency + " deposit initiated.", html, "Dyffy Simple Gateway " + currency + " Deposit Initiated");
}
function depositSuccessEmail(email, address, amount, currency){
    var html;
    if (currency == 'BTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Bitcoin deposit succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount + ' BTC successfully deposited to ' + address + ' Ripple wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'LTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Litecoin deposit succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount + ' LTC successfully deposited to ' + address + ' Ripple wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NMC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Namecoin deposit succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount + ' NMC successfully deposited to ' + address + ' Ripple wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'PPC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Peercoin deposit succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount + ' PPC successfully deposited to ' + address + ' Ripple wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NXT'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> NXT deposit succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount + ' NXT successfully deposited to ' + address + ' Ripple wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'DOG'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have successfully completed a transaction. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/check.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dogecoin deposit succeeded </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> ' + amount + ' DOG successfully deposited to ' + address + ' Ripple wallet. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    sendEmail(email, "Dyffy " + currency + " deposit succeeded.", html, "Dyffy Simple Gateway " + currency + " Deposit Succeeded ✔");
}
function depositErrorEmail(email, address, amount, toAddress, currency){
    var html;
    if (currency == 'BTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Bitcoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + ' failed. Make sure you include a proper destination tag. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'LTC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Litecoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + ' failed. Make sure you include a proper destination tag. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NMC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Namecoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + ' failed. Make sure you include a proper destination tag. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'PPC'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Peercoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + ' failed. Make sure you include a proper destination tag. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'NXT'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> NXT withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + ' failed. Make sure you include a proper destination tag. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    else if (currency == 'DOG'){
        html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <!-- Define Charset --> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <!-- Responsive Meta Tag --> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Dyffy Email</title><!-- Responsive Styles and Valid Styles --> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; } /* ----------- responsivity ----------- */ @media only screen and (max-width: 640px){ /*------ top header ------ */ .header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr><td height="30"></td></tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="2c3e50"><td height="5"></td></tr> <tr bgcolor="2c3e50"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr><td width="30" height="20"></td></tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="2c3e50"><td height="10"></td></tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"><td height="40"></td></tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #18bc9c" href="https://dyffy.com/#/simpleGateway" target="_blank">Simple Gateway</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"><td height="40"></td></tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"><td height="7"></td></tr> <tr bgcolor="ffffff"><td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://s3-us-west-2.amazonaws.com/dyffy/email/simpleGatewayBigBanner.png" alt="large image" class="header-bg" /></td></tr> <tr bgcolor="ffffff"><td height="20"></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Simple Gateway </multiline> </td> </tr> <tr><td height="20"></td></tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> We noticed you have had transaction difficulties. </multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"><td height="25"></td></tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end main section ---------> <tr><td height="35"></td></tr> <!--------- section 1 ---------> <repeater> <layout label="new-feature"> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr><td height="20"></td></tr> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr><td height="6"></td></tr> <tr> <td><a href="" style="width: 128px; display: block; border-style: none !important; border: 0 !important;"><img editable="true" mc:edit="image1" width="128" height="128" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/no.png" alt="image1" class="section-img" /></a></td> </tr> <tr><td height="10"></td></tr> </table> <table border="0" align="left" width="10" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> <tr><td height="30" width="10"></td></tr> </table> <table border="0" width="360" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="section-item"> <tr> <td mc:edit="title2" style="color: #2c3e50; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dogecoin withdrawal failed </multiline> </td> </tr> <tr><td height="15"></td></tr> <tr> <td mc:edit="subtitle2" style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Your ' + currency + ' withdrawal to ' + toAddress + ' failed. Make sure you include a proper destination tag. </multiline> </td> </tr> </table> </td> </tr> <tr><td height="20"></td></tr> </table> </td> </tr> <tr><td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td></tr> </table> </td> </tr><!--------- end section 1 ---------> <tr><td height="35"></td></tr> </layout> <tr><td height="35"></td></tr> </layout> </repeater> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://dyffy.com" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/dyffy-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr><td height="10"></td></tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/Dyffyâ€Ž" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/dyffy" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr><!---------- end prefooter ---------> <tr><td height="30"></td></tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="2c3e50"><td height="14"></td></tr> <tr bgcolor="2c3e50"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Dyffy Inc. Â© Copyright 2013 . All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://s3-us-west-2.amazonaws.com/dyffy/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr><td height="30"></td></tr> </table> </body> </html> ';
    }
    sendEmail(email, "Dyffy " + currency + " deposit failed.", html, "Dyffy Simple Gateway " + currency + " Deposit Failed");
}
// TODO : reformat emit url for prefilled client to link to on QR code
dyffy.io.route('bitcoin', {
    deposit: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.rippleAddress == 'string'){
            bitcoinClient.getNewAddress('dyffy',function (err, address) {
                if (err){
                    return console.log(err);
                }
                var endString;
                if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                    endString = '?amount=' + req.data.amount.toString();
                }
                else {
                    endString = '';
                }
                QRCode.save('/home/ubuntu/dyffy/' + address + '.png','bitcoin:' + address + endString,function(errr,url){
                    require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png'],
                        function(errorr, stdout, stderr){
                            require('imagemagick').convert(['/home/ubuntu/dyffy/' + address + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + address + '.png'],
                                function(err, stdout){
                                    fs.readFile('/home/ubuntu/dyffy/' + address + '.png', function(errrr, original_data){
                                        var base64 = new Buffer(original_data, 'binary').toString('base64');
                                        s3.setBucket('dyffy');
                                        s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {
                                            fs.unlinkSync('/home/ubuntu/dyffy/' + address + '.png');
                                        });
                                        var readcode = 'data:image/png;base64,' + base64;
                                        var cashIn = new OutsideCashin({date: new Date(),cashinAddress: address,rippleAddress: req.data.rippleAddress,type:'BTC',currency: 'bitcoin',qr:readcode,email:req.data.email});
                                        if (req.session && req.session.passport && req.session.passport.user){
                                            cashIn.user = req.session.passport.user;
                                        }
                                        cashIn.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                                        var geoLocation = geoip.lookup(cashIn.ip);
                                        cashIn.countryName = geoLocation.name;
                                        cashIn.countryCode = geoLocation.code;
                                        cashIn.save(function(err,cashins){
                                            if (!isEmpty(cashins)){
                                                req.io.join(cashins._id.toString());
                                                s3.setBucket('dyffy');

                                                s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {

                                                });
                                                depositEmail(cashins.email, address, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + address + '.png', cashins.type, req.data.rippleAddress);
                                                req.io.emit('cryptoDeposit', {type : 'BTC', qr : readcode, address : address});
                                            }
                                        });
                                    });
                                });
                        });
                });
            });
        }
    },
    withdraw: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.bitcoinAddress == 'string'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'bitcoin',type:'BTC',email:req.data.email, cashoutAddress: req.data.bitcoinAddress});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            var endString;
            if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                endString = '&amount=' + req.data.amount.toString() + '%2F' + withdrawObject.type;
            }
            else {
                endString = '&amount=' + '1' + '%2F' + withdrawObject.type;
            }
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    QRCode.save('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png','ripple:' + dyffyConfig.iouAddress + '?dt=' + withdrawObject.destinationTag.toString() + '&name=Dyffy&label=Withdraw%20Bitcoin%20from%20Dyffy' + endString,function(errr,url){
                        require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                            function(errorr, stdout, stderr){
                                require('imagemagick').convert(['/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                                    function(err, stdout){
                                        fs.readFile('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', function(errrr, original_data){
                                            var base64 = new Buffer(original_data, 'binary').toString('base64');
                                            s3.setBucket('dyffy');
                                            s3.putFile('/qr/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', 'public-read', {}, function (error, result) {
                                                fs.unlinkSync('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png');
                                            });
                                            var readcode = 'data:image/png;base64,' + base64;
                                            withdrawObject.qr = readcode;
                                            withdrawObject.address = dyffyConfig.iouAddress;
                                            withdrawObject.save(function(err,withdraw){
                                                if (!isEmpty(withdraw)){
                                                    req.io.join(withdrawObject._id.toString());
                                                    withdrawEmail(withdraw.email, req.data.bitcoinAddress, withdraw.address, withdraw.destinationTag, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + withdrawObject._id.toString() + '.png', withdraw.type);
                                                    var address = dyffyConfig.iouAddress + '?dt=' + withdraw.destinationTag.toString();
                                                    req.io.emit('cryptoWithdraw', {type : 'BTC', qr : readcode, address : address});
                                                }
                                            });
                                        });
                                    });
                            });
                    });
                }
            });
        }
    },
});

dyffy.io.route('dogecoin', {
    deposit: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.rippleAddress == 'string'){
            dogecoinClient.getNewAddress('dyffy',function (err, address) {
                if (err){
                    return console.log(err);
                }
                var endString;
                if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                    endString = '?amount=' + req.data.amount.toString();
                }
                else {
                    endString = '';
                }
                QRCode.save('/home/ubuntu/dyffy/' + address + '.png','dogecoin:' + address + endString,function(errr,url){
                    require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png'],
                        function(errorr, stdout, stderr){
                            require('imagemagick').convert(['/home/ubuntu/dyffy/' + address + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + address + '.png'],
                                function(err, stdout){
                                    fs.readFile('/home/ubuntu/dyffy/' + address + '.png', function(errrr, original_data){
                                        var base64 = new Buffer(original_data, 'binary').toString('base64');
                                        s3.setBucket('dyffy');
                                        s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {
                                            fs.unlinkSync('/home/ubuntu/dyffy/' + address + '.png');
                                        });
                                        var readcode = 'data:image/png;base64,' + base64;
                                        var cashIn = new OutsideCashin({date: new Date(),cashinAddress: address,rippleAddress: req.data.rippleAddress,type:'DOG',currency: 'dogecoin',qr:readcode,email:req.data.email});
                                        if (req.session && req.session.passport && req.session.passport.user){
                                            cashIn.user = req.session.passport.user;
                                        }
                                        cashIn.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                                        var geoLocation = geoip.lookup(cashIn.ip);
                                        cashIn.countryName = geoLocation.name;
                                        cashIn.countryCode = geoLocation.code;
                                        cashIn.save(function(err,cashins){
                                            if (!isEmpty(cashins)){
                                                req.io.join(cashins._id.toString());
                                                s3.setBucket('dyffy');

                                                s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {

                                                });
                                                depositEmail(cashins.email, address, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + address + '.png', cashins.type, req.data.rippleAddress);
                                                req.io.emit('cryptoDeposit', {type : 'DOG', qr : readcode, address : address});
                                            }
                                        });
                                    });
                                });
                        });
                });
            });
        }
    },
    withdraw: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.dogecoinAddress == 'string'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'dogecoin',type:'DOG',email:req.data.email, cashoutAddress: req.data.dogecoinAddress});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            var endString;
            if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                endString = '&amount=' + req.data.amount.toString() + '%2F' + withdrawObject.type;
            }
            else {
                endString = '&amount=' + '1' + '%2F' + withdrawObject.type;
            }
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    QRCode.save('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png','ripple:' + dyffyConfig.iouAddress + '?dt=' + withdrawObject.destinationTag.toString() + '&name=Dyffy&label=Withdraw%20Dogecoin%20from%20Dyffy' + endString,function(errr,url){
                        require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                            function(errorr, stdout, stderr){
                                require('imagemagick').convert(['/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                                    function(err, stdout){
                                        fs.readFile('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', function(errrr, original_data){
                                            var base64 = new Buffer(original_data, 'binary').toString('base64');
                                            s3.setBucket('dyffy');
                                            s3.putFile('/qr/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', 'public-read', {}, function (error, result) {
                                                fs.unlinkSync('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png');
                                            });
                                            var readcode = 'data:image/png;base64,' + base64;
                                            withdrawObject.qr = readcode;
                                            withdrawObject.address = dyffyConfig.iouAddress;
                                            withdrawObject.save(function(err,withdraw){
                                                if (!isEmpty(withdraw)){
                                                    req.io.join(withdrawObject._id.toString());
                                                    withdrawEmail(withdraw.email, req.data.dogecoinAddress, withdraw.address, withdraw.destinationTag, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + withdrawObject._id.toString() + '.png', withdraw.type);
                                                    var address = dyffyConfig.iouAddress + '?dt=' + withdraw.destinationTag.toString();
                                                    req.io.emit('cryptoWithdraw', {type : 'DOG', qr : readcode, address : address});
                                                }
                                            });
                                        });
                                    });
                            });
                    });
                }
            });
        }
    },
});

dyffy.io.route('nextcoin', {
    deposit: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.rippleAddress == 'string'){
            var secretKey = shaValue(crypto.enc.Base64.stringify(crypto.lib.WordArray.random(64)));
            $.getJSON(
                'http://localhost:7874/nxt?requestType=getAccountId&secretPhrase=' + secretKey,
                function(data) {
                    var address = data.accountId;
                    var endString;
                    if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                        endString = '?amount=' + req.data.amount.toString();
                    }
                    else {
                        endString = '';
                    }
                    QRCode.save('/home/ubuntu/dyffy/' + address + '.png','nextcoin:' + address + endString,function(errr,url){
                        require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png'],
                            function(errorr, stdout, stderr){
                                require('imagemagick').convert(['/home/ubuntu/dyffy/' + address + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + address + '.png'],
                                    function(err, stdout){
                                        fs.readFile('/home/ubuntu/dyffy/' + address + '.png', function(errrr, original_data){
                                            var base64 = new Buffer(original_data, 'binary').toString('base64');
                                            s3.setBucket('dyffy');
                                            s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {
                                                fs.unlinkSync('/home/ubuntu/dyffy/' + address + '.png');
                                            });
                                            var readcode = 'data:image/png;base64,' + base64;
                                            var cashIn = new OutsideCashin({date: new Date(),cashinAddress: address,cashinSecret: secretKey,rippleAddress: req.data.rippleAddress,type:'NXT',currency: 'nextcoin',qr:readcode,email:req.data.email});
                                            if (req.session && req.session.passport && req.session.passport.user){
                                                cashIn.user = req.session.passport.user;
                                            }
                                            cashIn.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                                            var geoLocation = geoip.lookup(cashIn.ip);
                                            cashIn.countryName = geoLocation.name;
                                            cashIn.countryCode = geoLocation.code;
                                            cashIn.save(function(err,cashins){
                                                if (!isEmpty(cashins)){
                                                    req.io.join(cashins._id.toString());
                                                    s3.setBucket('dyffy');

                                                    s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {

                                                    });
                                                    depositEmail(cashins.email, address, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + address + '.png', cashins.type, req.data.rippleAddress);
                                                    req.io.emit('cryptoDeposit', {type : 'NXT', qr : readcode, address : address});
                                                }
                                            });
                                        });
                                    });
                            });
                    });
                });
        }
    },
    withdraw: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.nextcoinAddress == 'string'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'nextcoin',type:'NXT',email:req.data.email, cashoutAddress: req.data.nextcoinAddress});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            var endString;
            if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                endString = '&amount=' + req.data.amount.toString() + '%2F' + withdrawObject.type;
            }
            else {
                endString = '&amount=' + '1' + '%2F' + withdrawObject.type;
            }
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    QRCode.save('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png','ripple:' + dyffyConfig.iouAddress + '?dt=' + withdrawObject.destinationTag.toString() + '&name=Dyffy&label=Withdraw%20Nextcoin%20from%20Dyffy' + endString,function(errr,url){
                        require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                            function(errorr, stdout, stderr){
                                require('imagemagick').convert(['/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                                    function(err, stdout){
                                        fs.readFile('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', function(errrr, original_data){
                                            var base64 = new Buffer(original_data, 'binary').toString('base64');
                                            s3.setBucket('dyffy');
                                            s3.putFile('/qr/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', 'public-read', {}, function (error, result) {
                                                fs.unlinkSync('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png');
                                            });
                                            var readcode = 'data:image/png;base64,' + base64;
                                            withdrawObject.qr = readcode;
                                            withdrawObject.address = dyffyConfig.iouAddress;
                                            withdrawObject.save(function(err,withdraw){
                                                if (!isEmpty(withdraw)){
                                                    req.io.join(withdrawObject._id.toString());
                                                    withdrawEmail(withdraw.email, req.data.nextcoinAddress, withdraw.address, withdraw.destinationTag, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + withdrawObject._id.toString() + '.png', withdraw.type);
                                                    var address = dyffyConfig.iouAddress + '?dt=' + withdraw.destinationTag.toString();
                                                    req.io.emit('cryptoWithdraw', {type : 'NXT', qr : readcode, address : address});
                                                }
                                            });
                                        });
                                    });
                            });
                    });
                }
            });
        }
    },
});

dyffy.io.route('namecoin', {
    deposit: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.rippleAddress == 'string'){
            namecoinClient.getNewAddress('dyffy',function (err, address) {
                if (err){
                    return console.log(err);
                }
                var endString;
                if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                    endString = '?amount=' + req.data.amount.toString();
                }
                else {
                    endString = '';
                }
                QRCode.save('/home/ubuntu/dyffy/' + address + '.png','namecoin:' + address + endString,function(errr,url){
                    require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png'],
                        function(errorr, stdout, stderr){
                            require('imagemagick').convert(['/home/ubuntu/dyffy/' + address + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + address + '.png'],
                                function(err, stdout){
                                    fs.readFile('/home/ubuntu/dyffy/' + address + '.png', function(errrr, original_data){
                                        var base64 = new Buffer(original_data, 'binary').toString('base64');
                                        s3.setBucket('dyffy');
                                        s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {
                                            fs.unlinkSync('/home/ubuntu/dyffy/' + address + '.png');
                                        });
                                        var readcode = 'data:image/png;base64,' + base64;
                                        var cashIn = new OutsideCashin({date: new Date(),cashinAddress: address,rippleAddress: req.data.rippleAddress,type:'NMC',currency: 'namecoin',qr:readcode,email:req.data.email});
                                        if (req.session && req.session.passport && req.session.passport.user){
                                            cashIn.user = req.session.passport.user;
                                        }
                                        cashIn.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                                        var geoLocation = geoip.lookup(cashIn.ip);
                                        cashIn.countryName = geoLocation.name;
                                        cashIn.countryCode = geoLocation.code;
                                        cashIn.save(function(err,cashins){
                                            if (!isEmpty(cashins)){
                                                req.io.join(cashins._id.toString());
                                                s3.setBucket('dyffy');

                                                s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {

                                                });
                                                depositEmail(cashins.email, address, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + address + '.png', cashins.type, req.data.rippleAddress);
                                                req.io.emit('cryptoDeposit', {type : 'NMC', qr : readcode, address : address});
                                            }
                                        });
                                    });
                                });
                        });
                });
            });
        }
    },
    withdraw: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.namecoinAddress == 'string'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'namecoin',type:'NMC',email:req.data.email, cashoutAddress: req.data.namecoinAddress});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            var endString;
            if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                endString = '&amount=' + req.data.amount.toString() + '%2F' + withdrawObject.type;
            }
            else {
                endString = '&amount=' + '1' + '%2F' + withdrawObject.type;
            }
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    QRCode.save('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png','ripple:' + dyffyConfig.iouAddress + '?dt=' + withdrawObject.destinationTag.toString() + '&name=Dyffy&label=Withdraw%20namecoin%20from%20Dyffy' + endString,function(errr,url){
                        require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                            function(errorr, stdout, stderr){
                                require('imagemagick').convert(['/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                                    function(err, stdout){
                                        fs.readFile('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', function(errrr, original_data){
                                            var base64 = new Buffer(original_data, 'binary').toString('base64');
                                            s3.setBucket('dyffy');
                                            s3.putFile('/qr/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', 'public-read', {}, function (error, result) {
                                                fs.unlinkSync('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png');
                                            });
                                            var readcode = 'data:image/png;base64,' + base64;
                                            withdrawObject.qr = readcode;
                                            withdrawObject.address = dyffyConfig.iouAddress;
                                            withdrawObject.save(function(err,withdraw){
                                                if (!isEmpty(withdraw)){
                                                    req.io.join(withdrawObject._id.toString());
                                                    withdrawEmail(withdraw.email, req.data.namecoinAddress, withdraw.address, withdraw.destinationTag, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + withdrawObject._id.toString() + '.png', withdraw.type);
                                                    var address = dyffyConfig.iouAddress + '?dt=' + withdraw.destinationTag.toString();
                                                    req.io.emit('cryptoWithdraw', {type : 'NMC', qr : readcode, address : address});
                                                }
                                            });
                                        });
                                    });
                            });
                    });
                }
            });
        }
    },
});

dyffy.io.route('peercoin', {
    deposit: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.rippleAddress == 'string'){
            peercoinClient.getNewAddress('dyffy',function (err, address) {
                if (err){
                    return console.log(err);
                }
                var endString;
                if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                    endString = '?amount=' + req.data.amount.toString();
                }
                else {
                    endString = '';
                }
                QRCode.save('/home/ubuntu/dyffy/' + address + '.png','peercoin:' + address + endString,function(errr,url){
                    require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png'],
                        function(errorr, stdout, stderr){
                            require('imagemagick').convert(['/home/ubuntu/dyffy/' + address + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + address + '.png'],
                                function(err, stdout){
                                    fs.readFile('/home/ubuntu/dyffy/' + address + '.png', function(errrr, original_data){
                                        var base64 = new Buffer(original_data, 'binary').toString('base64');
                                        s3.setBucket('dyffy');
                                        s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {
                                            fs.unlinkSync('/home/ubuntu/dyffy/' + address + '.png');
                                        });
                                        var readcode = 'data:image/png;base64,' + base64;
                                        var cashIn = new OutsideCashin({date: new Date(),cashinAddress: address,rippleAddress: req.data.rippleAddress,type:'PPC',currency: 'peercoin',qr:readcode,email:req.data.email});
                                        if (req.session && req.session.passport && req.session.passport.user){
                                            cashIn.user = req.session.passport.user;
                                        }
                                        cashIn.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                                        var geoLocation = geoip.lookup(cashIn.ip);
                                        cashIn.countryName = geoLocation.name;
                                        cashIn.countryCode = geoLocation.code;
                                        cashIn.save(function(err,cashins){
                                            if (!isEmpty(cashins)){
                                                req.io.join(cashins._id.toString());
                                                s3.setBucket('dyffy');

                                                s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {

                                                });
                                                depositEmail(cashins.email, address, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + address + '.png', cashins.type, req.data.rippleAddress);
                                                req.io.emit('cryptoDeposit', {type : 'PPC', qr : readcode, address : address});
                                            }
                                        });
                                    });
                                });
                        });
                });
            });
        }
    },
    withdraw: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.peercoinAddress == 'string'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'peercoin',type:'PPC',email:req.data.email, cashoutAddress: req.data.peercoinAddress});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            var endString;
            if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                endString = '&amount=' + req.data.amount.toString() + '%2F' + withdrawObject.type;
            }
            else {
                endString = '&amount=' + '1' + '%2F' + withdrawObject.type;
            }
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    QRCode.save('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png','ripple:' + dyffyConfig.iouAddress + '?dt=' + withdrawObject.destinationTag.toString() + '&name=Dyffy&label=Withdraw%20peercoin%20from%20Dyffy' + endString,function(errr,url){
                        require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                            function(errorr, stdout, stderr){
                                require('imagemagick').convert(['/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                                    function(err, stdout){
                                        fs.readFile('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', function(errrr, original_data){
                                            var base64 = new Buffer(original_data, 'binary').toString('base64');
                                            s3.setBucket('dyffy');
                                            s3.putFile('/qr/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', 'public-read', {}, function (error, result) {
                                                fs.unlinkSync('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png');
                                            });
                                            var readcode = 'data:image/png;base64,' + base64;
                                            withdrawObject.qr = readcode;
                                            withdrawObject.address = dyffyConfig.iouAddress;
                                            withdrawObject.save(function(err,withdraw){
                                                if (!isEmpty(withdraw)){
                                                    req.io.join(withdrawObject._id.toString());
                                                    withdrawEmail(withdraw.email, req.data.peercoinAddress, withdraw.address, withdraw.destinationTag, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + withdrawObject._id.toString() + '.png', withdraw.type);
                                                    var address = dyffyConfig.iouAddress + '?dt=' + withdraw.destinationTag.toString();
                                                    req.io.emit('cryptoWithdraw', {type : 'PPC', qr : readcode, address : address});
                                                }
                                            });
                                        });
                                    });
                            });
                    });
                }
            });
        }
    },
});

dyffy.io.route('litecoin', {
    deposit: function(req) {
        console.log(typeof req.data.amount);
        if (req.data && typeof req.data.email == 'string' && typeof req.data.rippleAddress == 'string'){
            litecoinClient.getNewAddress('dyffy', function (err, address) {
                if (err){
                    return console.log(err);
                }
                var endString;
                if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                    endString = '?amount=' + req.data.amount.toString();
                }
                else {
                    endString = '';
                }
                QRCode.save('/home/ubuntu/dyffy/' + address + '.png','litecoin:' + address + endString,function(errr,url){
                    require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png'],
                        function(errorr, stdout, stderr){
                            require('imagemagick').convert(['/home/ubuntu/dyffy/' + address + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + address + '.png'],
                                function(err, stdout){
                                    fs.readFile('/home/ubuntu/dyffy/' + address + '.png', function(errrr, original_data){
                                        var base64 = new Buffer(original_data, 'binary').toString('base64');
                                        s3.setBucket('dyffy');
                                        s3.putFile('/qr/' + address + '.png', '/home/ubuntu/dyffy/' + address + '.png', 'public-read', {}, function (error, result) {
                                            fs.unlinkSync('/home/ubuntu/dyffy/' + address + '.png');
                                        });
                                        var readcode = 'data:image/png;base64,' + base64;
                                        var cashIn = new OutsideCashin({date: new Date(),cashinAddress: address,rippleAddress: req.data.rippleAddress,type:'LTC',currency: 'litecoin',qr:readcode,email:req.data.email});
                                        if (req.session && req.session.passport && req.session.passport.user){
                                            cashIn.user = req.session.passport.user;
                                        }
                                        cashIn.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                                        var geoLocation = geoip.lookup(cashIn.ip);
                                        cashIn.countryName = geoLocation.name;
                                        cashIn.countryCode = geoLocation.code;
                                        cashIn.save(function(err,cashins){
                                            if (!isEmpty(cashins)){
                                                req.io.join(cashins._id.toString());
                                                depositEmail(cashins.email, address, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + address + '.png', cashins.type, req.data.rippleAddress);
                                                req.io.emit('cryptoDeposit', {type : 'LTC', qr : readcode, address : address});
                                            }
                                        });
                                    });
                                });
                        });
                });
            });
        }
    },
    withdraw: function(req) {
        if (req.data && typeof req.data.email == 'string' && typeof req.data.litecoinAddress == 'string'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'litecoin',type:'LTC',email:req.data.email,cashoutAddress: req.data.litecoinAddress});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    var endString;
                    if (typeof req.data.amount == 'string' || typeof req.data.amount == 'number'){
                        endString = '&amount=' + req.data.amount.toString() + '%2F' + withdrawObject.type;
                    }
                    else {
                        endString = '&amount=' + '100' + '%2F' + withdrawObject.type;
                    }
                    QRCode.save('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png','ripple:' + dyffyConfig.iouAddress + '?dt=' + withdrawObject.destinationTag.toString() + '&name=Dyffy&label=Withdraw%20Litecoin%20from%20Dyffy' + endString,function(errr,url){
                        require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                            function(errorr, stdout, stderr){
                                require('imagemagick').convert(['/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png'],
                                    function(err, stdout){
                                        fs.readFile('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', function(errrr, original_data){
                                            var base64 = new Buffer(original_data, 'binary').toString('base64');
                                            s3.setBucket('dyffy');
                                            s3.putFile('/qr/' + withdrawObject._id.toString() + '.png', '/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png', 'public-read', {}, function (error, result) {
                                                fs.unlinkSync('/home/ubuntu/dyffy/' + withdrawObject._id.toString() + '.png');
                                            });
                                            var readcode = 'data:image/png;base64,' + base64;
                                            withdrawObject.qr = readcode;
                                            withdrawObject.address = dyffyConfig.iouAddress;
                                            withdrawObject.save(function(err,withdraw){
                                                if (!isEmpty(withdraw)){
                                                    req.io.join(withdrawObject._id.toString());
                                                    withdrawEmail(withdraw.email, req.data.litecoinAddress, withdraw.address, withdraw.destinationTag, 'https://s3-us-west-2.amazonaws.com/dyffy/qr/' + withdrawObject._id.toString() + '.png', withdraw.type);
                                                    var address = dyffyConfig.iouAddress + '?dt=' + withdraw.destinationTag.toString();
                                                    req.io.emit('cryptoWithdraw', {type : 'LTC', qr : readcode, address : address});
                                                }
                                            });
                                        });
                                    });
                            });
                    });
                }
            });
        }
    },
});

//===================================================== Promotion ====================================================//

dyffy.io.route('submitPromotionValue', function(req) {
    var todayDate = new Date();
    if (req.session.passport && req.session.passport.user && typeof req.data.promotionValue === 'number' && typeof req.data.groupId !== 'undefined'){
        User.findById(req.session.passport.user,function(err,user){
            LossGroup.findById(req.data.groupId, function(err, group){
                if (!isEmpty(group)){
                    if (!group.vested && !group.dissolveDate && typeof group.finalValue === 'undefined' && req.data.promotionValue >= group.rangeStart && req.data.promotionValue <= group.rangeEnd){
                        var ipIn = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                        var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                        ip = geoip.lookup(ip);
                        var sig = new BeforeSignature({ip: ipIn, countryName : ip.name, countryCode : ip.code, userInput: req.data.promotionValue, date: todayDate, user: req.session.passport.user.toString()});

                        group.beforeSignatures.push(sig);
                        group.save(function(err,saveGroup){
                            if (!isEmpty(saveGroup)){
                                var historyExists = false;
                                user.groupHistory.forEach(function(history){
                                    if (history.group == saveGroup._id.toString()){
                                        historyExists = true;
                                    }
                                });
                                if (!historyExists){
                                    var groupHistory = new GroupHistory();
                                    groupHistory.group = saveGroup._id.toString();
                                    groupHistory.dateValueEntered = new Date();
                                    user.groupHistory.push(groupHistory);
                                    user.save(function(err, saveObject2){
                                        dyffy.io.room(saveObject2._id.toString()).broadcast('getRooms');
                                        User.findById(saveObject2._id, "_id username firstName lastName groupHistory joinDate activeDate currentProfileImageUrl verificationBadge rippleInsuranceWallet.publicKey jumioData facebookData linkedinData dwollaData verificationBadge verified",
                                            function (err,doc){
                                                if (!isEmpty(err)){
                                                    req.io.emit('error', {error: "Please forgive the error."});
                                                }
                                                else{
                                                    doc = doc.toObject();
                                                    doc = modSocial(doc);
                                                    doc.groupHistory = _.filter(doc.groupHistory, function(num){ if (num.deleted !== true){return num} });
                                                    dyffy.io.room(saveGroup._id.toString()).broadcast('userFindMany', [doc]);
                                                    if (saveGroup.promotion){
                                                        dyffy.io.broadcast('lossGroupAcquireAll', [saveGroup.toObject()]);
                                                    }
                                                    else {
                                                        dyffy.io.room(saveGroup._id.toString()).broadcast('lossGroupAcquireAll', [saveGroup.toObject()]);
                                                    }
                                                }
                                            });
                                    });
                                }
                                QRCode.save('/home/ubuntu/dyffy/' + sig._id.toString() + '.png','ripple:' + dyffyConfig.masterAddress + '?dt=' + saveGroup.beforeSignatures.id(sig._id).destinationTag.toString() + '&amount=' + (saveGroup.promoteAmount/1000000).toString(),function(errr,url){
                                    require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + sig._id.toString() + '.png', '/home/ubuntu/dyffy/' + sig._id.toString() + '.png'],
                                        function(errorr, stdout, stderr){
                                            require('imagemagick').convert(['/home/ubuntu/dyffy/' + sig._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + sig._id.toString() + '.png'],
                                                function(err, stdout){
                                                    fs.readFile('/home/ubuntu/dyffy/' + sig._id.toString() + '.png', function(errrr, original_data){
                                                        var base64 = new Buffer(original_data, 'binary').toString('base64');
                                                        fs.unlinkSync('/home/ubuntu/dyffy/' + sig._id.toString() + '.png');
                                                        var readcode = 'data:image/png;base64,' + base64;
                                                        req.io.join(group.beforeSignatures.id(sig._id).destinationTag.toString());
                                                        req.io.emit('destinationTag', {address : dyffyConfig.masterAddress + '?dt=' + saveGroup.beforeSignatures.id(sig._id).destinationTag.toString(), groupId : req.data.groupId, readcode: readcode, url: 'ripple:' + dyffyConfig.masterAddress + '?dt=' + saveGroup.beforeSignatures.id(sig._id).destinationTag.toString() + '&amount=' + (saveGroup.promoteAmount/1000000).toString()});
                                                    });
                                                });
                                        });
                                });
                            }
                        });
                    }
                    else {
                        req.io.emit('error', {error: "Value entered needs to be greater than " + group.rangeStart + " and less than " + group.rangeEnd + "."});
                    }
                }
            });
        });
    }
    else if (typeof req.data.promotionValue === 'number' && typeof req.data.groupId !== 'undefined'){
        LossGroup.findById(req.data.groupId, function(err, group){
            if (!isEmpty(group)){
                var ipIn = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
                ip = geoip.lookup(ip);
                var sig = new BeforeSignature({ip: ipIn, countryName : ip.name, countryCode : ip.code, userInput: req.data.promotionValue, date: todayDate});
                group.beforeSignatures.push(sig);
                group.save(function(err,saveGroup){
                    if (!isEmpty(saveGroup)){
                        if (!req.session.flash){
                            req.session.flash = {};
                        }
                        if (req.session.flash.promotionArray){
                            req.session.flash.promotionArray.push(sig._id.toString());
                        }
                        else {
                            req.session.flash.promotionArray = [sig._id.toString()];
                        }
                        req.session.save(function() {
                            QRCode.save('/home/ubuntu/dyffy/' + sig._id.toString() + '.png','ripple:' + dyffyConfig.masterAddress + '?dt=' + saveGroup.beforeSignatures.id(sig._id).destinationTag.toString() + '&amount=' + (saveGroup.promoteAmount/1000000).toString(),function(errr,url){
                                require('imagemagick-composite').composite(['-gravity','center','/home/ubuntu/dyffy/public/img/dyffyqr.png', '/home/ubuntu/dyffy/' + sig._id.toString() + '.png', '/home/ubuntu/dyffy/' + sig._id.toString() + '.png'],
                                    function(errorr, stdout, stderr){
                                        require('imagemagick').convert(['/home/ubuntu/dyffy/' + sig._id.toString() + '.png', '+level-colors', '#2c3e50,#ffffff', '/home/ubuntu/dyffy/' + sig._id.toString() + '.png'],
                                            function(err, stdout){
                                                fs.readFile('/home/ubuntu/dyffy/' + sig._id.toString() + '.png', function(errrr, original_data){
                                                    var base64 = new Buffer(original_data, 'binary').toString('base64');
                                                    fs.unlinkSync('/home/ubuntu/dyffy/' + sig._id.toString() + '.png');
                                                    var readcode = 'data:image/png;base64,' + base64;
                                                    req.io.join(saveGroup.beforeSignatures.id(sig._id).destinationTag.toString());
                                                    req.io.emit('destinationTag', {address : dyffyConfig.masterAddress + '?dt=' + group.beforeSignatures.id(sig._id).destinationTag.toString(), groupId : req.data.groupId, readcode: readcode, url: 'ripple:' + dyffyConfig.masterAddress + '?dt=' + saveGroup.beforeSignatures.id(sig._id).destinationTag.toString() + '&amount=' + (saveGroup.promoteAmount/1000000).toString()});
                                                });
                                            });
                                    });
                            });
                        });
                    }
                });
            }
        });
    }
    else {
        req.io.emit('error', {error : "Need promotion value."});
    }
});
dyffy.io.route('getRate', function(req){
    req.io.emit('uxRate', {rate: average(USDtoXRPRate)});
});
dyffy.io.route('success', function(req){
    req.io.emit('success', {success : req.data});
});
function convertRippledDate(date){
    var rippledDate = new Date("2000-01-01T00:00:00")
    rippledDate = rippledDate.setSeconds(rippledDate.getSeconds() + Number(date));
    rippledDate = new Date(rippledDate);
    return rippledDate;
}
dyffy.io.route('transactionHistory', function(req){
    if (req.session.passport && req.session.passport.user){
        User.findById(req.session.passport.user, function(err,user){
            if (!isEmpty(user)){
                if (user.rippleInsuranceWallet && user.rippleInsuranceWallet[0]){
                    var rc = new WebSocketClient();
                    rc.on('connect', function(connection){
                        connection.on('message', function(message){
                            message = JSON.parse(message.utf8Data);
                            if (message.error){

                            }
                            else {
                                var transactions = message.result.transactions;
                                if (transactions){
                                    transactions = _.filter(transactions,function(transact){
                                        if (transact.tx && transact.tx.TransactionType == "Payment"){
                                            return transact;
                                        }
                                    });
                                    async.map(transactions, function(item, complete){
                                        if (item.tx.Account && item.tx.Destination && (item.tx.Account != user.rippleInsuranceWallet[0].publicKey || item.tx.Destination != user.rippleInsuranceWallet[0].publicKey)){
                                            var addressed = '';
                                            if (item.tx.Account != user.rippleInsuranceWallet[0].publicKey){
                                                addressed = item.tx.Account;
                                            }
                                            else if (item.tx.Destination != user.rippleInsuranceWallet[0].publicKey){
                                                addressed = item.tx.Destination;
                                            }
                                            User.findOne({'rippleInsuranceWallet.publicKey': addressed}, function(err,doc){
                                                var finalObject = {};
                                                if (!isEmpty(doc)){
                                                    var fullName;
                                                    if (doc.firstName || doc.lastName){
                                                        fullName = [doc.firstName, doc.lastName].join(' ').trim();
                                                    }
                                                    else if (typeof doc.linkedinData != 'undefined' && doc.linkedinData != null && doc.linkedinData.firstName && doc.linkedinData.lastName){
                                                        fullName = [doc.linkedinData.firstName, doc.linkedinData.lastName].join(' ').trim();
                                                    }
                                                    else if (doc.facebookData){
                                                        fullName = doc.facebookData.name;
                                                    }
                                                    else if (doc.dwollaData){
                                                        fullName = doc.dwollaData.Name;
                                                    }
                                                    if (!fullName){
                                                        fullName = 'Dyffy Member';
                                                    }
                                                    finalObject._id = doc._id;
                                                    finalObject.id = doc._id;
                                                    finalObject.fullName = fullName;
                                                    finalObject.firstName = doc.firstName;
                                                    finalObject.lastName = doc.lastName;
                                                    finalObject.username = doc.username;
                                                    finalObject.invites = doc.invites;
                                                    finalObject.verified = doc.verified;
                                                    finalObject.joinDate = doc.joinDate;
                                                    finalObject.activeDate = doc.activeDate;
                                                    finalObject.linkedinImage = doc.linkedinImage;
                                                    finalObject.facebookImage = doc.facebookImage;
                                                    finalObject.uploadImage = doc.uploadImage;
                                                    finalObject.currentProfileImageUrl = doc.currentProfileImageUrl;
                                                    finalObject.profileImageSource = doc.profileImageSource;
                                                    if (doc.facebookToken){
                                                        finalObject.facebookData = doc.facebookData;
                                                    }
                                                    if (doc.linkedinToken){
                                                        finalObject.linkedinData = doc.linkedinData;
                                                    }
                                                    if (doc.dwollaToken){
                                                        finalObject.dwollaData = doc.dwollaData;
                                                    }

                                                    finalObject.publicKey = doc.rippleInsuranceWallet[0].publicKey;
                                                    finalObject.connected = true;

                                                    if (doc.facebookToken && doc.facebookData && doc.facebookData.username){
                                                        finalObject.facebookProfileLink = 'https://www.facebook.com/' + doc.facebookData.username;
                                                    }
                                                    else {
                                                        finalObject.facebookProfileLink = '';
                                                    }
                                                    var defaultImage = 'https://s3-us-west-2.amazonaws.com/dyffy/images/profileHolder.jpg';
                                                    var currentProfileImageUrl;
                                                    if (finalObject.currentProfileImageUrl){
                                                        currentProfileImageUrl = finalObject.currentProfileImageUrl;
                                                    }
                                                    else if (finalObject.uploadImage){
                                                        currentProfileImageUrl = finalObject.currentProfileImageUrl;
                                                    }
                                                    else if (finalObject.linkedinImage){
                                                        currentProfileImageUrl = finalObject.linkedinImage;
                                                    }
                                                    else if (finalObject.facebookData && finalObject.facebookData.username){
                                                        currentProfileImageUrl = 'https://graph.facebook.com/' + finalObject.facebookData.username + '/picture?type=large';
                                                    }
                                                    else {
                                                        currentProfileImageUrl = finalObject.currentProfileImageUrl;
                                                    }
                                                    var profileImage = currentProfileImageUrl ? currentProfileImageUrl : defaultImage;
                                                    finalObject.profileImage = profileImage;
                                                    finalObject.profileImageBackgroundStyle = 'background-image: url("' + profileImage + '");';

                                                    if (item.tx.Account != user.rippleInsuranceWallet[0].publicKey){
                                                        item.tx.Account = finalObject;
                                                    }
                                                    else if (item.tx.Destination != user.rippleInsuranceWallet[0].publicKey){
                                                        item.tx.Destination = finalObject;
                                                    }
                                                    complete(err, item);
                                                }
                                                else {
                                                    complete(err, item);
                                                }
                                            });
                                        }
                                        else {
                                            complete(err, item);
                                        }
                                    }, function(err, results){
                                        req.io.emit('rippledHistory', {results: results, marker: message.marker});
                                    });
                                }
                                connection.close();
                            }
                        });
                        if (req.data.marker){
                            connection.send(JSON.stringify({id: req.data.id, command:"account_tx", account:user.rippleInsuranceWallet[0].publicKey, ledger_index_min:"-1", ledger_index_max:"-1", limit:req.data.limit, marker:req.data.marker}));
                        }
                        else {
                            connection.send(JSON.stringify({id: req.data.id, command:"account_tx", account:user.rippleInsuranceWallet[0].publicKey, ledger_index_min:"-1", ledger_index_max:"-1", limit:req.data.limit}));
                        }
                    });
                    rc.on('error', function(error){
                        req.io.emit('error', {error : 'Rippled connection failed.'});
                    });
                    rc.connect(dyffyConfig.rippled);
                }
            }
        });
    }
});
dyffy.io.route('error', function(req){
    req.io.emit('error', {error : req.data});
});
dyffy.io.route('sendXRP', function(req){
    if (req.data.recipient && typeof req.data.recipient == 'string' && !isNaN(Number(req.data.amount))){
        var rc = new WebSocketClient();
        rc.on('connect', function(connection){
            connection.on('message', function(message){
                message = JSON.parse(message.utf8Data);
                if (message.error){
                    req.io.emit('error', {error : 'Transaction failed.'});
                }
                else{
                    //{"result":{"engine_result":"tecUNFUNDED_PAYMENT","engine_result_code":104,"engine_result_message":"Insufficient XRP balance to send.",
                    req.io.emit('sendXRP', message);
                    if (message.result && message.result.engine_result != "tecUNFUNDED_PAYMENT"){
                        req.io.emit('success', {success : message.result && message.result.engine_result_message});
                    }
                    else {
                        req.io.emit('error', {error : message.result.engine_result_message + ' Reserves too low.'});
                    }
                    connection.close();
                }
            });
            var finalAddress = req.data.recipient.split('?dt=');
            if (finalAddress && finalAddress[1]){
                connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"Payment",Account:req.data.publicSender,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:Number(req.data.amount).toFixed(0)},secret:req.data.secret}));
            }
            else {
                connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"Payment",Account:req.data.publicSender,Destination:finalAddress[0],Amount:Number(req.data.amount).toFixed(0)},secret:req.data.secret}));
            }
        });
        rc.on('error', function(error){
            req.io.emit('error', {error : 'Rippled connection failed.'});
        });
        rc.connect(dyffyConfig.rippled);
    }
});
dyffy.io.route('accountInfo', function(req){
    if (req.session.passport && req.session.passport.user){
        var rc = new WebSocketClient();
        var xrp = '';
        rc.on('connect', function(connection){
            connection.on('message', function(message){
                message = JSON.parse(message.utf8Data);
                if (message.error){
                    req.io.emit('accountInfo', {error : 'Account not funded.'});
                    User.findById(req.session.passport.user,function(err,userObject){
                        if (!isEmpty(userObject)){
                            if (userObject.rippleInsuranceWallet[0].funded !== false){
                                userObject.rippleInsuranceWallet[0].funded = false;
                                userObject.save(function(err,userObject2){
                                    if (!isEmpty(userObject2)){
                                        req.io.emit('rippleInsuranceWalletAcquire', userObject2.rippleInsuranceWallet[0].toObject());
                                    }
                                });
                            }
                        }
                    });
                    connection.close();
                }
                else if (message.id == "4"){
                    req.io.emit('accountInfo', message);
                    xrp = message.result.account_data.Balance;
                    connection.send(JSON.stringify({id : "5",command : "account_lines", account : req.data.address}));
                }
                else {
                    User.findById(req.session.passport.user,function(err,userObject){
                        if (!isEmpty(userObject)){
                            if (userObject.rippleInsuranceWallet[0].funded !== true){
                                userObject.rippleInsuranceWallet[0].funded = true;
                                userObject.save(function(err,userObject2){
                                    if (!isEmpty(userObject2)){
                                        var returnObject = userObject2.rippleInsuranceWallet[0].toObject();
                                        returnObject.ious = message.result.lines;
                                        returnObject.xrp = xrp;
                                        req.io.emit('rippleInsuranceWalletAcquire', userObject2.rippleInsuranceWallet[0].toObject());
                                    }
                                });
                            }
                        }
                    });
                    connection.close();
                }
            });
            connection.send(JSON.stringify({id : "4",command : "account_info", account : req.data.address}));
        });
        rc.on('error', function(error){
            req.io.emit('error', {error : 'error'});
        });
        rc.connect(dyffyConfig.rippled);
    }
});
dyffy.io.route('accountLines', function(req){
    if (req.session.passport && req.session.passport.user){
        var rc = new WebSocketClient();
        rc.on('connect', function(connection){
            connection.on('message', function(message){
                message = JSON.parse(message.utf8Data);
                if (message.error){
                    req.io.emit('accountLines', {error : 'Account not funded.'});
                    User.findById(req.session.passport.user,function(err,userObject){
                        if (!isEmpty(userObject)){
                            if (userObject.rippleInsuranceWallet[0].funded !== false){
                                userObject.rippleInsuranceWallet[0].funded = false;
                                userObject.save(function(err,userObject2){
                                    if (!isEmpty(userObject2)){
                                        req.io.emit('rippleInsuranceWalletAcquire', userObject2.rippleInsuranceWallet[0].toObject());
                                    }
                                });
                            }
                        }
                    });
                }
                else {
                    req.io.emit('accountLines', message);
                    User.findById(req.session.passport.user,function(err,userObject){
                        if (!isEmpty(userObject)){
                            if (userObject.rippleInsuranceWallet[0].funded !== true){
                                userObject.rippleInsuranceWallet[0].funded = true;
                                userObject.save(function(err,userObject2){
                                    if (!isEmpty(userObject2)){
                                        req.io.emit('rippleInsuranceWalletAcquire', userObject2.rippleInsuranceWallet[0].toObject());
                                    }
                                });
                            }
                        }
                    });
                }
            });
            connection.send(JSON.stringify({id : req.data.id,command : "account_lines", account : req.data.address}));
        });
        rc.on('error', function(error){
            req.io.emit('error', {error : 'error'});
        });
        rc.connect(dyffyConfig.rippled);
    }
});
dyffy.io.route('sendIOU', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                req.io.emit('sendIOU', message);
            }
        });
        connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"Payment",Account:req.data.publicSender,Destination:req.data.recipient,Amount:{currency:req.data.currency,value:req.data.amount.toString(),issuer:req.data.issuer}},secret:req.data.secret}));
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('sendConvertIOU', function(req){
    if (req.data.recipient && typeof req.data.recipient == 'string'){
        var rc = new WebSocketClient();
        rc.on('connect', function(connection){
            connection.on('message', function(message){
                message = JSON.parse(message.utf8Data);
                if (message.error){
                    req.io.emit('error', {error : 'Insufficient funds or unavailable trust pathway.'});
                }
                else {
                    req.io.emit('sendConvertIOU', message);
                    req.io.emit('success', {success : "Sent Successfully."});
                }
            });
            var finalAddress = req.data.recipient.split('?dt=');
            if (finalAddress && finalAddress[1]){
                connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"Payment",Account:req.data.publicSender,Destination:finalAddress[0],DestinationTag:finalAddress[1],SendMax:{currency:req.data.currency2,value:req.data.amount2,issuer:req.data.issuer},Amount:{currency:req.data.currency,value:req.data.amount.toString(),issuer:req.data.issuer}},secret:req.data.secret}));
            }
            else {
                connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"Payment",Account:req.data.publicSender,Destination:finalAddress[0],SendMax:{currency:req.data.currency2,value:req.data.amount2,issuer:req.data.issuer},Amount:{currency:req.data.currency,value:req.data.amount.toString(),issuer:req.data.issuer}},secret:req.data.secret}));
            }
        });
        rc.on('error', function(error){
            req.io.emit('error', {error : 'error'});
        });
        rc.connect(dyffyConfig.rippled);
    }
});
dyffy.io.route('issueTrust', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                req.io.emit('issueTrust', message);
            }
        });
        connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"TrustSet",Account:req.data.publicTruster,LimitAmount:{currency:req.data.currency,value:req.data.amount,issuer:req.data.issuer}},secret:req.data.secret}));
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('checkExchange', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            var exchange;
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                // exchange is currency2 / currency
                if (req.data.currency == "XRP"){
                    // bid = Math.round(parseFloat(message.result.offers[0].TakerPays.value) * 100.0 / parseFloat(message.result.offers[0].TakerGets.value)) / 100;
                    exchange = Math.round(parseFloat(message.result.offers[0].quality) * 100.0 / 1000000);
                }
                else if (req.data.currency2 == "XRP") {
                    // bid = Math.round(parseFloat(message.result.offers[0].TakerPays) * 100.0 / parseFloat(message.result.offers[0].TakerGets.value)) / 100;
                    exchange = Math.round(parseFloat(message.result.offers[0].quality) * 100.0 / 1000000.0);
                }
                else {
                    exchange = Math.round(parseFloat(message.result.offers[0].quality) * 100.0);
                }
                req.io.emit('checkExchange', message);
            }
        });
        if (req.data.currency == "XRP"){
            connection.send(JSON.stringify({id : req.data.id,command : "book_offers", taker_gets:{currency:req.data.currency}, taker_pays:{currency:req.data.currency2, issuer:req.data.issuer2},taker:req.data.taker}));
        }
        else if (req.data.currency2 == "XRP"){
            connection.send(JSON.stringify({id : req.data.id,command : "book_offers", taker_gets:{currency:req.data.currency, issuer:req.data.issuer}, taker_pays:{currency:req.data.currency2},taker:req.data.taker}));
        }
        else {
            connection.send(JSON.stringify({id : req.data.id,command : "book_offers", taker_gets:{currency:req.data.currency, issuer:req.data.issuer}, taker_pays:{currency:req.data.currency2, issuer:req.data.issuer2},taker:req.data.taker}));
        }
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('subExchange', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            var exchange;
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                // exchange is currency2 / currency
                if (req.data.currency == "XRP"){
                    // bid = Math.round(parseFloat(message.result.offers[0].TakerPays.value) * 100.0 / parseFloat(message.result.offers[0].TakerGets.value)) / 100;
                    exchange = Math.round(parseFloat(message.result.offers[0].quality) * 100.0 / 1000000);
                }
                else if (req.data.currency2 == "XRP") {
                    // bid = Math.round(parseFloat(message.result.offers[0].TakerPays) * 100.0 / parseFloat(message.result.offers[0].TakerGets.value)) / 100;
                    exchange = Math.round(parseFloat(message.result.offers[0].quality) * 100.0 / 1000000.0);
                }
                else {
                    exchange = Math.round(parseFloat(message.result.offers[0].quality) * 100.0);
                }
                req.io.emit('subExchange', message);
            }
        });
        if (req.data.currency == "XRP"){
            connection.send(JSON.stringify({id : req.data.id,command : "subscribe", books : [{taker_gets:{currency:req.data.currency}, taker_pays:{currency:req.data.currency2, issuer:req.data.issuer2},taker:req.data.taker}]}));
        }
        else if (req.data.currency2 == "XRP"){
            connection.send(JSON.stringify({id : req.data.id,command : "subscribe", books : [{taker_gets:{currency:req.data.currency, issuer:req.data.issuer}, taker_pays:{currency:req.data.currency2},taker:req.data.taker}]}));
        }
        else {
            connection.send(JSON.stringify({id : req.data.id,command : "subscribe", books: [{taker_gets:{currency:req.data.currency, issuer:req.data.issuer}, taker_pays:{currency:req.data.currency2, issuer:req.data.issuer2},taker:req.data.taker}]}));
        }
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('offer', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                connection.close();
                req.io.emit('offer', message);
            }
        });
        connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"OfferCreate",Account:req.data.publicKey,TakerPays : { currency : req.data.currency, value : req.data.amount, issuer : req.data.issuer },TakerGets : { currency : req.data.currency2, value : req.data.amount2, issuer : req.data.publicKey }},secret:req.data.secret}));
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('offerCancel', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                connection.close();
                req.io.emit('offerCancel', message);
            }
        });
        connection.send(JSON.stringify({id : req.data.id,command:"submit",tx_json:{TransactionType:"OfferCancel",Account:req.data.publicKey,OfferSequence:req.data.offerSequence},secret:req.data.secret}));
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('returnOffers', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                connection.close();
                req.io.emit('returnOffers', message);
            }
        });
        connection.send(JSON.stringify({id : req.data.id,command : "account_offers", account : req.data.accountId}));
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('returnTxn', function(req){
    var rc = new WebSocketClient();
    rc.on('connect', function(connection){
        connection.on('message', function(message){
            message = JSON.parse(message.utf8Data);
            if (message.error){
                req.io.emit('error', {error : 'error'});
            }
            else{
                connection.close();
                req.io.emit('returnTxn', message);
            }
        });
        connection.send(JSON.stringify({id : req.data.id,command : "account_tx", account : req.data.accountId}));
    });
    rc.on('error', function(error){
        req.io.emit('error', {error : 'error'});
    });
    rc.connect(dyffyConfig.rippled);
});
dyffy.io.route('validation', function(req){
    if (req.session.passport && req.session.passport.user){
        var rc = new WebSocketClient();
        rc.on('connect', function(connection){
            connection.on('message', function(message){
                message = JSON.parse(message.utf8Data);
                if (message.error){
                }
                else {
                    var rc2 = new WebSocketClient();
                    rc2.on('connect', function(connection2){
                        connection2.on('message', function(message){
                            message = JSON.parse(message.utf8Data);
                            if (message.error){
                                req.io.emit('accountInfo', {error : "Account not funded."});
                                User.findById(req.session.passport.user,function(err,userObject){
                                    if (!isEmpty(userObject)){
                                        if (userObject.rippleInsuranceWallet[0].funded !== false){
                                            userObject.rippleInsuranceWallet[0].funded = false;
                                            userObject.save(function(err,userObject2){
                                                if (!isEmpty(userObject2)){
                                                    var wallet  = userObject2.rippleInsuranceWallet[0].toObject();
                                                    delete wallet.ious;
                                                    delete wallet.xrp;
                                                    req.io.emit('rippleInsuranceWalletAcquire', wallet);
                                                }
                                            });
                                        }
                                    }
                                });
                                connection2.close();
                            }
                            else {
                                req.io.emit('accountInfo', message);
                                User.findById(req.session.passport.user,function(err,userObject){
                                    if (!isEmpty(userObject)){
                                        if (userObject.rippleInsuranceWallet[0].funded !== true){
                                            userObject.rippleInsuranceWallet[0].funded = true;
                                            userObject.save(function(err,userObject2){
                                                if (!isEmpty(userObject2)){
                                                    var wallet  = userObject2.rippleInsuranceWallet[0].toObject();
                                                    delete wallet.ious;
                                                    delete wallet.xrp;
                                                    req.io.emit('rippleInsuranceWalletAcquire', wallet);
                                                }
                                            });
                                        }
                                    }
                                });
                                connection2.close();
                            }
                        });
                        connection2.send(JSON.stringify({id : req.data.id, command : "account_info", account : req.data.address}));
                    });
                    rc2.connect(dyffyConfig.rippled);
                    var rc3 = new WebSocketClient();
                    rc3.on('connect', function(connection3){
                        connection3.on('message', function(message){
                            message = JSON.parse(message.utf8Data);
                            if (message.error){
                                req.io.emit('accountLines', {error : "Account not funded."});
                                User.findById(req.session.passport.user,function(err,userObject){
                                    if (!isEmpty(userObject)){
                                        if (userObject.rippleInsuranceWallet[0].funded !== false){
                                            userObject.rippleInsuranceWallet[0].funded = false;
                                            userObject.save(function(err,userObject2){
                                                if (!isEmpty(userObject2)){
                                                    var wallet  = userObject2.rippleInsuranceWallet[0].toObject();
                                                    delete wallet.ious;
                                                    delete wallet.xrp;
                                                    req.io.emit('rippleInsuranceWalletAcquire', wallet);
                                                }
                                            });
                                        }
                                    }
                                });
                                connection3.close();
                            }
                            else {
                                req.io.emit('accountLines', message);
                                User.findById(req.session.passport.user,function(err,userObject){
                                    if (!isEmpty(userObject)){
                                        if (userObject.rippleInsuranceWallet[0].funded !== true){
                                            userObject.rippleInsuranceWallet[0].funded = true;
                                            userObject.save(function(err,userObject2){
                                                if (!isEmpty(userObject2)){
                                                    var wallet  = userObject2.rippleInsuranceWallet[0].toObject();
                                                    delete wallet.ious;
                                                    delete wallet.xrp;
                                                    req.io.emit('rippleInsuranceWalletAcquire', wallet);
                                                }
                                            });
                                        }
                                    }
                                });
                                connection3.close();
                            }
                        });
                        connection3.send(JSON.stringify({id : req.data.id, command : "account_lines", account : req.data.address}));
                    });
                    rc3.connect(dyffyConfig.rippled);
                    req.io.emit('transactionMessage', message);
                }
            });
            connection.send(JSON.stringify({command:"subscribe", id : req.data.id,accounts: [req.data.address]}));
        });
        rc.connect(dyffyConfig.rippled);
    }
});
dyffy.io.route('joinRooms', function(req) {
    if (req.session.passport){
        if (req.session.passport.user){
            User.findById(req.session.passport.user, function(err, user){
                if (user.groupHistory){
                    user.groupHistory.forEach(function(history){
                        req.io.join(history.group);
                    });
                }
                req.io.join(user._id.toString());
            });
        }
    }
});
dyffy.io.route('auth', function(req) {
    if (req.session && req.session.passport){
        if (req.session.passport.user){
            // TODO : send chat threads
            User.findById(req.session.passport.user, "-password -verifyCode -rippleInsuranceWallet", function (err,doc) {
                if (!isEmpty(err)){
                    req.io.emit('error', {error: "Please forgive the error."});
                }
                else {
                    try{
                        if (doc.groupHistory){
                            doc.groupHistory.forEach(function(history){
                                req.io.join(history.group);
                            });
                        }
                        req.io.join(doc._id.toString());
                        doc = doc.toObject();
                        doc.groupHistory = _.filter(doc.groupHistory, function(num){ if (num.deleted !== true){return num} });
                        req.io.emit('auth', {session : req.session, verified : doc.verified, haveWallet: doc.haveWallet, user: doc});
                        ChatThread.find({users : req.session.passport.user},function(err,threads){
                            if (!isEmpty(threads)){
                                req.io.emit('chatThreads', threads);
                            }
                        });
                    }
                    catch(err){
                        req.io.emit('auth', {session : req.session});
                    }
                }
            });
        }
        else {
            req.io.emit('auth', {session : req.session});
        }
    }
    else {
        req.io.emit('auth', {session : req.session});
    }
});

//=========================================  Cron Jobs for Vote and Group End =========================================//
function setupCron (){
    LossGroup.find().exec(function(err, lossGroups){
        lossGroups.forEach(function(group){
            if (group.overDate){
                var job = new cronJob(group.overDate, function(){
                        group.dissolveDate = new Date();
                        group.save(function(err, group){
                            dyffy.io.broadcast('groupEnd', {'group' : group._id});
                        });
                    }, function () {
                    },
                    true,
                    'UTC'
                );
            }
        });
    });
}

//================================================ Templates & Routes ================================================//

dyffy.get('/', function(req, res) {
    if (!req.session.flash){
        req.session.flash = {};
    }
    if (!req.session.passport){
        req.session.passport = {};
    }
    if ('development' == dyffy.get('env')) {
        if (req.get('host') == 'getfirstcoins.com'){
            res.render('indexSimpleDev', {});
        }
        else {
            res.render('indexDev', {});
        }
    }
    else {
        if (req.get('host') == 'getfirstcoins.com'){
            res.render('indexSimple', {});
        }
        else {
            res.render('index', {});
        }
    }
});
dyffy.get('/animation', function(req, res) {
    res.render('animation', {});
});
dyffy.get('/animationTest', function(req, res) {
    res.render('animationTest', {});
});
dyffy.get('/deck', function(req, res) {
    if (req.get('host').toLowerCase() === 'getfirstcoins.com') {
        res.render('newDeck', {});
    }
    else {
        User.count({}, function( err, count){
            res.render('revealPresentation', {memberCount : count});
        });
    }
});
dyffy.get('/walkthrough', function(req, res) {
    res.render('walkthrough', {});
});
dyffy.get('/test', function(req, res) {
    if ('development' == dyffy.get('env')) {
        if (!req.session.flash){
            req.session.flash = {};
        }
        if (!req.session.passport){
            req.session.passport = {};
        }
        res.render('test', {});
    }
    else {
        res.redirect('/');
    }
});
dyffy.get('/auth_close', function(req, res) {
    res.render('auth_close', {req: req});
});
dyffy.get('/jumioCallbackSuccess', function(req, res) {
    res.render('jumioCallbackSuccess', {req: req});
});
dyffy.get('/jumioCallback', function(req, res) {
    res.render('jumioCallback', {req: req});
});
dyffy.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email', 'read_friendlists', 'user_about_me', 'user_activities', 'user_birthday', 'user_education_history', 'publish_actions', 'friends_about_me', 'friends_activities', 'user_checkins', 'friends_birthday', 'friends_education_history', 'friends_checkins', 'user_events', 'friends_events', 'user_groups', 'friends_groups', 'user_hometown', 'friends_hometown', 'user_interests', 'friends_interests', 'user_likes', 'friends_likes', 'user_location', 'friends_location', 'user_status', 'friends_status', 'user_subscriptions', 'friends_subscriptions', 'user_videos', 'user_work_history', 'friends_work_history', 'user_work_history', 'friends_work_history']}));
dyffy.get('/auth/facebook/callback', function(req, res, next) {
    passport.authenticate('facebook', {scope: ['email', 'read_friendlists', 'user_about_me', 'user_activities', 'user_birthday', 'user_education_history', 'publish_actions', 'friends_about_me', 'friends_activities', 'user_checkins', 'friends_birthday', 'friends_education_history', 'friends_checkins', 'user_events', 'friends_events', 'user_groups', 'friends_groups', 'user_hometown', 'friends_hometown', 'user_interests', 'friends_interests', 'user_likes', 'friends_likes', 'user_location', 'friends_location', 'user_status', 'friends_status', 'user_subscriptions', 'friends_subscriptions', 'user_videos', 'user_work_history', 'friends_work_history', 'user_work_history', 'friends_work_history']},
        function(err, user, info) {
            if (!isEmpty(err)){
                res.render('auth_close', {
                    title: "Facebook Auth",
                    body: "<div class='alert alert-error' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Facebook authentication failed.</p></div>"
                });
            }
            else {
                var facebook = false;
                if (req.session.flash){
                    facebook = req.session.flash.facebook;
                }
                if (user == 'done'){
                    res.render('auth_close', {
                        title: "Facebook Auth",
                        body: "<div class='alert alert-error' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Already linked to another account.</p></div>"
                    });
                }
                else if (facebook && !user) {
                    var registerMessage = 'Please register and Dyffy will import your Facebook data.';
                    res.render('auth_close', {
                        title: "Facebook Auth",
                        body: "<div class='alert alert-info' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Please register and Dyffy will import your Facebook data.</p></div>"
                    });
                }
                else {
                    req.logIn(user, function(err) {
                        if (!isEmpty(err)) {
                            res.render('auth_close', {
                                title: "Facebook Auth",
                                body: "<div class='alert alert-error' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Facebook authentication failed.</p></div>"
                            });
                        }
                        else {
                            res.render('auth_close', {
                                title: "Facebook Auth",
                                body: "<div class='alert alert-success' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Facebook authentication successful.</p></div>"
                            });
                        }
                    });
                }
            }
        })(req, res, next);
});
dyffy.get('/auth/linkedin',passport.authenticate('linkedin', { scope: ['r_fullprofile', 'r_emailaddress','r_contactinfo','r_network','rw_groups','rw_nus','w_messages'] }));
dyffy.get('/auth/linkedin/callback', function(req, res, next) {
    passport.authenticate('linkedin', { scope: ['r_fullprofile', 'r_emailaddress','r_contactinfo','r_network','rw_groups','rw_nus','w_messages',] }, function(err, user, info) {
        if (!isEmpty(err)){
            res.render('auth_close', {
                title: "LinkedIn Auth",
                body: "<div class='alert alert-success' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>LinkedIn authentication successful.</p></div>"
            });
        }
        else {
            var linkedin = false;
            if (req.session.flash){
                linkedin = req.session.flash.linkedin;
            }
            if (user == 'done'){
                res.render('auth_close', {
                    title: "LinkedIn Auth",
                    body: "<div class='alert alert-error' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Already linked to another account.</p></div>"
                });
            }
            else if (linkedin && !user){
                res.render('auth_close', {
                    title: "LinkedIn Auth",
                    body: "<div class='alert alert-info' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Please register and Dyffy will import your LinkedIn data.</p></div>"
                });
            }
            else{
                req.logIn(user, function(err) {
                    if (!isEmpty(err)) {
                        res.render('auth_close', {
                            title: "LinkedIn Auth",
                            body: "<div class='alert alert-error' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>LinkedIn authentication failed.</p></div>"
                        });
                    }
                    else {
                        res.render('auth_close', {
                            title: "LinkedIn Auth",
                            body: "<div class='alert alert-success' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>LinkedIn authentication successful.</p></div>"
                        });
                    }
                });
            }
        }
    })(req, res, next);
});
dyffy.get('/auth/dwolla', passport.authenticate('dwolla', { scope: 'AccountInfoFull|Contacts|Transactions|Balance|Send|Request|Funding' }));
dyffy.get('/auth/dwolla/callback', function(req, res, next) {
    passport.authenticate('dwolla', { scope: 'AccountInfoFull|Contacts|Transactions|Balance|Send|Request|Funding' }, function(err, user, info) {
        if (!isEmpty(err)){
            res.render('auth_close', {
                title: "Dwolla Auth",
                body: "<div class='alert alert-info' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Please register and Dyffy will import your Dwolla data.</p></div>"
            });
        }
        else{
            var dwolla = false;
            if (req.session.flash){
                dwolla = req.session.flash.dwolla;
            }
            if (user == 'done'){
                res.render('auth_close', {
                    title: "Dwolla Auth",
                    body: "<div class='alert alert-error' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Already linked to another account.</p></div>"
                });
            }
            else if (dwolla && !user){
                res.render('auth_close', {
                    title: "Dwolla Auth",
                    body: "<div class='alert alert-info' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Please register and Dyffy will import your Dwolla data.</p></div>"
                });
            }
            else{
                req.logIn(user, function(err) {
                    if (!isEmpty(err)) {
                        res.render('auth_close', {
                            title: "Dwolla Auth",
                            body: "<div class='alert alert-error' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Dwolla authentication failed.</p></div>"
                        });
                    }
                    else{
                        res.render('auth_close', {
                            title: "Dwolla Auth",
                            body: "<div class='alert alert-success' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important; '>Dwolla authentication successful.</p></div>"
                        });
                    }
                });
            }
        }
    })(req, res, next);
});
dyffy.io.route('verifyCode',function(req){
    var verifyQuery = req.data.verifyCode;
    if (req.session.passport && verifyQuery && req.session.passport.user){
        User.findOne({ 'verifyCode': verifyQuery },function(err, user) {
            if (!isEmpty(user) && req.session.passport.user == user._id)
            {
                user.verified = true;
                user.save(function(err, data){
                    var success = "Verification successful.";
                    req.io.emit('verifyCode', {success : success});
                });
            }
            else {
                var error = "Wrong verification code.";
                req.io.emit('error', {error : error});
            }
        });
    }
    else{
        var error = "Please login prior to verification.";
        req.io.emit('error', {error : error});
    }
});
dyffy.get('/verifyCode',function(req, res){
    var verifyQuery = decodeURIComponent(req.query.verifyCode);
    if (req.session.passport && verifyQuery && req.session.passport.user){
        User.findOne({ 'verifyCode': verifyQuery },function(err, user) {
            if (!isEmpty(user) && req.session.passport.user == user._id)
            {
                user.verified = true;
                user.save(function(err, data){
                    var success = "Verification successful.";
                    res.redirect('/');
                });
            }
            else {
                var error = "Wrong verification code.";
                res.redirect('/');
            }
        });
    }
    else{
        var error = "Please login prior to verification.";
        res.redirect('/');
    }
});
dyffy.post('/login', function(req, res, next) {passport.authenticate('local', {failureFlash: true}, function(err, user, info) {
    if (err) { return next(err) }
    if (!user) {
        res.send({message : info.message});
    }
    else {
        req.logIn(user, function(err) {
            if (err) { return next(err); }
            res.send({message : info.message});
        });
    }

})(req, res, next);});
dyffy.get('/bridge', function(req, res){
    if (req.query.type == "quote" && req.query.amount){
        var amountArray = req.query.amount.split("/");
        if (amountArray && amountArray[0] && !isNaN(Number(amountArray[0])) && Number(amountArray[0]) > 1 && amountArray[1] && amountArray[1] == 'NXT'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'nextcoin',type:'NXT'});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    res.send({result:"success",quote:{address: dyffyConfig.iouAddress, destination_tag:Number(withdrawObject.destinationTag),
                        send: [
                            {
                                "currency": amountArray[1],
                                "value": (Number(amountArray[0]) + 1).toString(),
                                "issuer": dyffyConfig.iouAddress
                            }
                        ],
                        "expires": 1000000000000000}});
                }
            });
        }
        else if (amountArray && amountArray[0] && !isNaN(Number(amountArray[0])) && Number(amountArray[0]) > 0 && amountArray[1] && amountArray[1] == 'LTC'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'litecoin',type:'LTC'});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    res.send({result:"success",quote:{address: dyffyConfig.iouAddress, destination_tag:Number(withdrawObject.destinationTag),
                        send: [
                            {
                                "currency": amountArray[1],
                                "value": amountArray[0],
                                "issuer": dyffyConfig.iouAddress
                            }
                        ],
                        "expires": 1000000000000000}});
                }
            });
        }
        else if (amountArray && amountArray[0] && !isNaN(Number(amountArray[0])) && Number(amountArray[0]) > 0 && amountArray[1] && amountArray[1] == 'BTC'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'bitcoin',type:'BTC'});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    res.send({result:"success",quote:{address: dyffyConfig.iouAddress, destination_tag:Number(withdrawObject.destinationTag),
                        send: [
                            {
                                "currency": amountArray[1],
                                "value": amountArray[0],
                                "issuer": dyffyConfig.iouAddress
                            }
                        ],
                        "expires": 1000000000000000}});
                }
            });
        }
        else if (amountArray && amountArray[0] && !isNaN(Number(amountArray[0])) && Number(amountArray[0]) > 0 && amountArray[1] && amountArray[1] == 'DOG'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'dogecoin',type:'DOG'});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    res.send({result:"success",quote:{address: dyffyConfig.iouAddress, destination_tag:Number(withdrawObject.destinationTag),
                        send: [
                            {
                                "currency": amountArray[1],
                                "value": amountArray[0],
                                "issuer": dyffyConfig.iouAddress
                            }
                        ],
                        "expires": 1000000000000000}});
                }
            });
        }
        else if (amountArray && amountArray[0] && !isNaN(Number(amountArray[0])) && Number(amountArray[0]) > 0 && amountArray[1] && amountArray[1] == 'NMC'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'namecoin',type:'NMC'});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    res.send({result:"success",quote:{address: dyffyConfig.iouAddress, destination_tag:Number(withdrawObject.destinationTag),
                        send: [
                            {
                                "currency": amountArray[1],
                                "value": amountArray[0],
                                "issuer": dyffyConfig.iouAddress
                            }
                        ],
                        "expires": 1000000000000000}});
                }
            });
        }
        else if (amountArray && amountArray[0] && !isNaN(Number(amountArray[0])) && Number(amountArray[0]) > 0 && amountArray[1] && amountArray[1] == 'PPC'){
            var withdrawObject = new OutsideCashout({date: new Date(),currency:'peercoin',type:'PPC'});
            if (req.session && req.session.passport && req.session.passport.user){
                withdrawObject.user = req.session.passport.user;
            }
            withdrawObject.ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var geoLocation = geoip.lookup(withdrawObject.ip);
            withdrawObject.countryName = geoLocation.name;
            withdrawObject.countryCode = geoLocation.code;
            withdrawObject.save(function(err,withdrawObject){
                if (!isEmpty(withdrawObject)){
                    res.send({result:"success",quote:{address: dyffyConfig.iouAddress, destination_tag:Number(withdrawObject.destinationTag),
                        send: [
                            {
                                "currency": amountArray[1],
                                "value": amountArray[0],
                                "issuer": dyffyConfig.iouAddress
                            }
                        ],
                        "expires": 1000000000000000}});
                }
            });
        }
    }
});
dyffy.io.route('balancedCredit', function(req, res){
    var exchangeRate;
    var exchangePrice; // exchangeRate * $ * 1.055
    exchangeRate = average(USDtoXRPRate);
    exchangePrice = (Number(req.data.amount) * exchangeRate * creditCardFee * 100).toFixed(0);
    if (req.session.passport && req.session.passport.user && !req.data.again && typeof req.data.card === 'string' && typeof req.data.year === 'string' && typeof req.data.month === 'string' && typeof req.data.code === 'string' && req.data.amount && !isNaN(Number(req.data.amount)) && Number(masterFunds)/1000000 >= Number(req.data.amount)){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, function(err,user){
                if (!isEmpty(user)){
                    if (user.jumioVerified){
                        var d = new Date();
                        var todayDate = (d.getTime()-d.getMilliseconds())/1000;
                        var expirationDate = todayDate + 10000;
                        var sellerTo = '';
                        if (req.data.address){
                            sellerTo = req.data.address;
                        }
                        else {
                            if (user.rippleInsuranceWallet && user.rippleInsuranceWallet[0]){
                                sellerTo = user.rippleInsuranceWallet[0].publicKey;
                            }
                        }
                        // TODO : check if valid Ripple/Bitcoin address
                        // TODO : simply write a transaction acquire function that looks up user and addressTo, returning it
                        var transaction = new GoogleWalletTransaction({user : user._id.toString(), dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : sellerTo, amount : req.data.amount, description : req.data.amount.toString() + " XRP from Dyffy", name : "Currency"});
                        transaction.save(function(err,transaction){
                            if (!isEmpty(transaction)){
                                apiBalanced.Cards.create({
                                    card_number: req.data.card,
                                    expiration_year: req.data.year,
                                    expiration_month: req.data.month,
                                    security_code: req.data.code
                                }, function (err, object) {
                                    if (!isEmpty(object)){
                                        var myCard = object;
                                        if (!user.customer){
                                            apiBalanced.Customers.create({

                                            }, function(err, resulter) {
                                                var customer = apiBalanced.Customers.balanced(resulter);
                                                customer.Customers.addCard(object.uri,function(err,resulted){
                                                    apiBalanced.Debits.create({
                                                        amount: exchangePrice,
                                                        customer_uri: resulter.uri,
                                                        source_uri: object.uri
                                                    }, function(err, result) {
                                                        if (result && result.status == "succeeded"){
                                                            user.creditCard = object.uri;
                                                            user.customer = resulter.uri;
                                                            user.save(function(err,customer){
                                                                if (!isEmpty(customer)){
                                                                    var rc2 = new WebSocketClient();
                                                                    rc2.on('connect', function(connectioned){
                                                                        connectioned.on('message', function(message){
                                                                            message = JSON.parse(message.utf8Data);
                                                                            if (message.error){
                                                                            }
                                                                            else {
                                                                                transaction.verified = true;
                                                                                transaction.dateClosed = new Date();
                                                                                transaction.save(function(err,tran){
                                                                                    if (!isEmpty(tran)){
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                        var finalAddress = transaction.to.split('?dt=');
                                                                        var finalAmount = (Number(transaction.amount) * 1000000).toFixed(0);
                                                                        if (finalAddress && finalAddress[1]){
                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                                                        }
                                                                        else {
                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                                                        }
                                                                    });
                                                                    rc2.on('error', function(error){
                                                                    });
                                                                    rc2.connect(dyffyConfig.rippled);
                                                                    req.io.emit('card', {userId: user._id.toString(), creditCard:customer.creditCard, customer:customer.customer});
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            req.io.emit('error', {error: "Card declined. Please contact your credit card provider."});
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                        else {
                                            apiBalanced.Customers.get(user.customer, function(err, resulter) {
                                                var customer = apiBalanced.Customers.balanced(resulter);
                                                customer.Customers.addCard(object.uri,function(err,resulted){
                                                    apiBalanced.Debits.create({
                                                        amount: exchangePrice,
                                                        customer_uri: user.customer,
                                                        source_uri: object.uri
                                                    }, function(err, result) {
                                                        if (result && result.status == "succeeded"){
                                                            user.creditCard = object.uri;
                                                            user.customer = resulter.uri;
                                                            user.save(function(err,customer){
                                                                if (!isEmpty(customer)){
                                                                    var rc2 = new WebSocketClient();
                                                                    rc2.on('connect', function(connectioned){
                                                                        connectioned.on('message', function(message){
                                                                            message = JSON.parse(message.utf8Data);
                                                                            if (message.error){
                                                                            }
                                                                            else {
                                                                                transaction.verified = true;
                                                                                transaction.dateClosed = new Date();
                                                                                transaction.save(function(err,tran){
                                                                                    if (!isEmpty(tran)){
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                        var finalAddress = transaction.to.split('?dt=');
                                                                        var finalAmount = (Number(transaction.amount) * 1000000).toFixed(0);
                                                                        if (finalAddress && finalAddress[1]){
                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                                                        }
                                                                        else {
                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                                                        }
                                                                    });
                                                                    rc2.on('error', function(error){
                                                                    });
                                                                    rc2.connect(dyffyConfig.rippled);
                                                                    req.io.emit('card', {userId: user._id.toString(), creditCard:customer.creditCard, customer:customer.customer});
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            req.io.emit('error', {error: "Card declined. Please contact your credit card provider."});
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                            else {
                                req.io.emit({error:'Transaction save failed.'});
                            }
                        });
                    }
                    else {
                        req.io.emit({error:'Identity not verified.'});
                    }
                }
            });
        }
    }
    else if (req.session.passport && req.session.passport.user && req.data.again && req.data.amount && !isNaN(Number(req.data.amount)) && Number(masterFunds)/1000000 >= Number(req.data.amount)){
        User.findById(req.session.passport.user, function(err,user){
            if (!isEmpty(user) && user.jumioVerified){
                var d = new Date();
                var todayDate = (d.getTime()-d.getMilliseconds())/1000;
                var expirationDate = todayDate + 10000;
                var sellerTo = '';
                if (req.data.address){
                    sellerTo = req.data.address;
                }
                else {
                    if (user.rippleInsuranceWallet && user.rippleInsuranceWallet[0]){
                        sellerTo = user.rippleInsuranceWallet[0].publicKey;
                    }
                }
                var transaction = new GoogleWalletTransaction({user : user._id.toString(), dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : sellerTo, amount : req.data.amount, description : req.data.amount.toString() + " XRP from Dyffy", name : "Currency"});
                transaction.save(function(err,transaction){
                    if (!isEmpty(transaction)){
                        apiBalanced.Debits.create({
                            amount: exchangePrice,
                            customer_uri: user.customer,
                            source_uri: user.creditCard
                        }, function(err, result) {
                            if (result && result.status == "succeeded"){
                                var rc2 = new WebSocketClient();
                                rc2.on('connect', function(connectioned){
                                    connectioned.on('message', function(message){
                                        message = JSON.parse(message.utf8Data);
                                        if (message.error){
                                        }
                                        else {
                                            transaction.verified = true;
                                            transaction.dateClosed = new Date();
                                            transaction.save(function(err,tran){
                                                if (!isEmpty(tran)){
                                                }
                                            });
                                        }
                                    });
                                    var finalAddress = transaction.to.split('?dt=');
                                    var finalAmount = (Number(transaction.amount) * 1000000).toFixed(0);
                                    if (finalAddress && finalAddress[1]){
                                        connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                    }
                                    else {
                                        connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                    }
                                });
                                rc2.on('error', function(error){
                                });
                                rc2.connect(dyffyConfig.rippled);
                            }
                            else {
                                req.io.emit('error', {error: "Card declined. Please contact your credit card provider."});
                            }
                        });
                    }
                });
            }
            else {
                req.io.emit({error:'Identity not verified.'});
            }
        });
    }
    else {
        req.io.emit('error', {error:'Not enough information.'});
    }
});
// TODO:
// bitstamp quote
// pathfind
// global exchange rate return USD/BTC
dyffy.io.route('balancedCreditBitcoin', function(req, res){
    req.io.emit('error', {error: "Credit frozen until reserve increase."});
    var exchangeRate;
    var exchangePrice; // exchangeRate * $ * 1.055
    exchangeRate = average(USDtoXRPRate);
    exchangePrice = (Number(req.data.amount) * exchangeRate * creditCardFee * 100).toFixed(0);
    if (req.session.passport && req.session.passport.user && !req.data.again && typeof req.data.card === 'string' && typeof req.data.year === 'string' && (typeof req.data.bitcoinAmount === 'Number' || typeof req.data.bitcoinAmount === 'String') && typeof req.data.bitcoinAddress === 'String' && typeof req.data.month === 'string' && typeof req.data.code === 'string' && req.data.amount && !isNaN(Number(req.data.amount)) && Number(masterFunds)/1000000 >= Number(req.data.amount)){
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, function(err,user){
                if (!isEmpty(user)){
                    if (user.jumioVerified){
                        var d = new Date();
                        var todayDate = (d.getTime()-d.getMilliseconds())/1000;
                        var expirationDate = todayDate + 10000;
                        var sellerTo = '';
                        if (req.data.address){
                            sellerTo = req.data.address;
                        }
                        else {
                            if (user.rippleInsuranceWallet && user.rippleInsuranceWallet[0]){
                                sellerTo = user.rippleInsuranceWallet[0].publicKey;
                            }
                        }
                        // TODO : check if valid Ripple/Bitcoin address
                        // TODO : simply write a transaction acquire function that looks up user and addressTo, returning it
                        var transaction = new GoogleWalletTransaction({user : user._id.toString(), dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : sellerTo, amount : req.data.amount, description : req.data.amount.toString() + " XRP from Dyffy", name : "Currency"});
                        transaction.save(function(err,transaction){
                            if (!isEmpty(transaction)){
                                apiBalanced.Cards.create({
                                    card_number: req.data.card,
                                    expiration_year: req.data.year,
                                    expiration_month: req.data.month,
                                    security_code: req.data.code
                                }, function (err, object) {
                                    if (!isEmpty(object)){
                                        var myCard = object;
                                        if (!user.customer){
                                            apiBalanced.Customers.create({

                                            }, function(err, resulter) {
                                                var customer = apiBalanced.Customers.balanced(resulter);
                                                customer.Customers.addCard(object.uri,function(err,resulted){
                                                    apiBalanced.Debits.create({
                                                        amount: exchangePrice,
                                                        customer_uri: resulter.uri,
                                                        source_uri: object.uri
                                                    }, function(err, result) {
                                                        if (result && result.status == "succeeded"){
                                                            user.creditCard = object.uri;
                                                            user.customer = resulter.uri;
                                                            var quote;
                                                            user.save(function(err,customer){
                                                                if (!isEmpty(customer)){

//                                                                    var rc2 = new WebSocketClient();
//                                                                    rc2.on('connect', function(connectioned){
//                                                                        connectioned.on('message', function(message){
//                                                                            message = JSON.parse(message.utf8Data);
//                                                                            if (message.error){
//                                                                            }
//                                                                            else {
//                                                                                transaction.verified = true;
//                                                                                transaction.dateClosed = new Date();
//                                                                                transaction.save(function(err,tran){
//                                                                                    if (!isEmpty(tran)){
//                                                                                    }
//                                                                                });
//                                                                            }
//                                                                        });
//                                                                        var finalAddress = transaction.to.split('?dt=');
//                                                                        var finalAmount = (Number(transaction.amount) * 1000000).toFixed(0);
//                                                                        if (finalAddress && finalAddress[1]){
//                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
//                                                                        }
//                                                                        else {
//                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
//                                                                        }
//                                                                    });
//                                                                    rc2.on('error', function(error){
//                                                                    });
//                                                                    rc2.connect(dyffyConfig.rippled);
                                                                    var rc = new WebSocketClient();
                                                                    rc.on('connect', function(connection){
                                                                        connection.on('message', function(message){
                                                                            message = JSON.parse(message.utf8Data);
                                                                            if (message.error){
                                                                            }
                                                                            else if (message.id == '2'){
                                                                                connection.close();
                                                                            }
                                                                            else {
                                                                                var paths = message;
                                                                                if (paths.alternatives){
                                                                                    paths.alternatives.forEach(function(alt){
                                                                                        if (alt.source_amount){
                                                                                            if (alt.source_amount.currency){

                                                                                            }
                                                                                            else {
                                                                                                var finalAmount;
                                                                                                var bitstampDestinationTag;
                                                                                                // Bitstamp API call
//                                                                                                "Destination" : self.get('controller.bitcoinQuote').address,
//                                                                                                "DestinationTag" : self.get('controller.bitcoinQuote').destination_tag,
                                                                                                connection.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:quote.address,Paths:alt.paths_computed,DestinationTag:quote.destination_tag,"InvoiceID": req.data.bitcoinAddress,Amount:{ "currency": "BTC", "value":finalAmount, "issuer" : quote.address }},secret:dyffyConfig.masterSecret}));

                                                                                            }
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                        $.ajax({
                                                                            url: 'https://www.bitstamp.net/ripple/bridge/out/bitcoin/',
                                                                            dataType: 'json',
                                                                            data: {
                                                                                type: "quote",
                                                                                amount: req.data.bitcoinAmount.toString()+"/BTC"
                                                                            },
                                                                            error: function () {

                                                                            },
                                                                            success: function (data) {
                                                                                quote = data.quote;
                                                                                connection.send(JSON.stringify({"command": "path_find", "subcommand": "create", "source_account": dyffyConfig.masterAddress, "source_currencies" : [{"currency" : "XRP"}],"destination_account": data.quote.address, "destination_amount": { "currency": "BTC", "value":req.data.bitcoinAmount.toString(), "issuer" : data.quote.address }}));
                                                                            }
                                                                        });

                                                                    });
                                                                    rc.connect(dyffyConfig.rippled);
                                                                    req.io.emit('card', {userId: user._id.toString(), creditCard:customer.creditCard, customer:customer.customer});
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            req.io.emit('error', {error: "Card declined. Please contact your credit card provider."});
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                        else {
                                            apiBalanced.Customers.get(user.customer, function(err, resulter) {
                                                var customer = apiBalanced.Customers.balanced(resulter);
                                                customer.Customers.addCard(object.uri,function(err,resulted){
                                                    apiBalanced.Debits.create({
                                                        amount: exchangePrice,
                                                        customer_uri: user.customer,
                                                        source_uri: object.uri
                                                    }, function(err, result) {
                                                        if (result && result.status == "succeeded"){
                                                            user.creditCard = object.uri;
                                                            user.customer = resulter.uri;
                                                            user.save(function(err,customer){
                                                                if (!isEmpty(customer)){
                                                                    var rc2 = new WebSocketClient();
                                                                    rc2.on('connect', function(connectioned){
                                                                        connectioned.on('message', function(message){
                                                                            message = JSON.parse(message.utf8Data);
                                                                            if (message.error){
                                                                            }
                                                                            else {
                                                                                transaction.verified = true;
                                                                                transaction.dateClosed = new Date();
                                                                                transaction.save(function(err,tran){
                                                                                    if (!isEmpty(tran)){
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                        var finalAddress = transaction.to.split('?dt=');
                                                                        var finalAmount = (Number(transaction.amount) * 1000000).toFixed(0);
                                                                        if (finalAddress && finalAddress[1]){
                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                                                        }
                                                                        else {
                                                                            connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                                                        }
                                                                    });
                                                                    rc2.on('error', function(error){
                                                                    });
                                                                    rc2.connect(dyffyConfig.rippled);
                                                                    req.io.emit('card', {userId: user._id.toString(), creditCard:customer.creditCard, customer:customer.customer});
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            req.io.emit('error', {error: "Card declined. Please contact your credit card provider."});
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                            else {
                                req.io.emit({error:'Transaction save failed.'});
                            }
                        });
                    }
                    else {
                        req.io.emit({error:'Identity not verified.'});
                    }
                }
            });
        }
    }
    else if (req.session.passport && req.session.passport.user && req.data.again && req.data.amount && !isNaN(Number(req.data.amount)) && Number(masterFunds)/1000000 >= Number(req.data.amount)){
        User.findById(req.session.passport.user, function(err,user){
            if (!isEmpty(user) && user.jumioVerified){
                var d = new Date();
                var todayDate = (d.getTime()-d.getMilliseconds())/1000;
                var expirationDate = todayDate + 10000;
                var sellerTo = '';
                if (req.data.address){
                    sellerTo = req.data.address;
                }
                else {
                    if (user.rippleInsuranceWallet && user.rippleInsuranceWallet[0]){
                        sellerTo = user.rippleInsuranceWallet[0].publicKey;
                    }
                }
                var transaction = new GoogleWalletTransaction({user : user._id.toString(), dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : sellerTo, amount : req.data.amount, description : req.data.amount.toString() + " XRP from Dyffy", name : "Currency"});
                transaction.save(function(err,transaction){
                    if (!isEmpty(transaction)){
                        apiBalanced.Debits.create({
                            amount: exchangePrice,
                            customer_uri: user.customer,
                            source_uri: user.creditCard
                        }, function(err, result) {
                            if (result && result.status == "succeeded"){
                                var rc2 = new WebSocketClient();
                                rc2.on('connect', function(connectioned){
                                    connectioned.on('message', function(message){
                                        message = JSON.parse(message.utf8Data);
                                        if (message.error){
                                        }
                                        else {
                                            transaction.verified = true;
                                            transaction.dateClosed = new Date();
                                            transaction.save(function(err,tran){
                                                if (!isEmpty(tran)){
                                                }
                                            });
                                        }
                                    });
                                    var finalAddress = transaction.to.split('?dt=');
                                    var finalAmount = (Number(transaction.amount) * 1000000).toFixed(0);
                                    if (finalAddress && finalAddress[1]){
                                        connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                    }
                                    else {
                                        connectioned.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                                    }
                                });
                                rc2.on('error', function(error){
                                });
                                rc2.connect(dyffyConfig.rippled);
                            }
                            else {
                                req.io.emit('error', {error: "Card declined. Please contact your credit card provider."});
                            }
                        });
                    }
                });
            }
            else {
                req.io.emit({error:'Identity not verified.'});
            }
        });
    }
    else {
        req.io.emit('error', {error:'Not enough information.'});
    }
});
dyffy.post('/jumioSwipe', function(req, res){
    req.io.emit('error', {error: "Debit frozen until reserve increase."});
//    if (req.body && req.body.merchantPaymentReference){
//        GoogleWalletTransaction.findById(req.body.merchantPaymentReference, function(err,transaction){
//            if (!isEmpty(transaction)){
//                apiBalanced.Cards.create({
//                    card_number: "5105105105105100",
//                    expiration_year: req.body.cardExpiryYear,
//                    expiration_month: req.body.cardExpiryMonth,
//                    security_code: "123"
//                }, function (err, object) {
//                    if (err) {
//                        throw err;
//                    }
//                    var myCard = object;
//                    apiBalanced.Debits.create({
//                        amount: req.body.amount,
//                        source_uri: object.uri
//                    }, function(err, result) {
//                        var rc = new WebSocketClient();
//                        rc.on('connect', function(connection){
//                            connection.on('message', function(message){
//                                message = JSON.parse(message.utf8Data);
//                                if (message.error){
//                                }
//                                else {
//                                    transaction.verified = true;
//                                    transaction.dateClosed = new Date();
//                                    transaction.uri = result.uri;
//                                    transaction.save(function(err,tran){
//                                        if (!isEmpty(tran)){
//                                        }
//                                    });
//                                }
//                            });
//                            var finalAddress = transaction.to.split('?dt=');
//                            var finalAmount = (Number(transaction.amount) * 1000000).toFixed(0);
//                            if (finalAddress && finalAddress[1]){
//                                connection.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
//                            }
//                            else {
//                                connection.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
//                            }
//                        });
//                        rc.on('error', function(error){
//                        });
//                        rc.connect(dyffyConfig.rippled);
//                    });
//                });
//            }
//        });
//    }
});
dyffy.post('/netswipe', function(req, res){
    if (req.body.amount && !isNaN(Number(req.body.amount)) && Number(masterFunds)/1000000 - 100 >= Number(req.body.amount)){
        var exchangeRate;
        var exchangePrice; // exchangeRate * $ * 1.055
        exchangeRate = average(USDtoXRPRate);
        exchangePrice = (Number(req.body.amount) * exchangeRate * creditCardFee).toFixed(2);
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, function(err,user){
                if (!isEmpty(user)){
                    var d = new Date();
                    var todayDate = (d.getTime()-d.getMilliseconds())/1000;
                    var expirationDate = todayDate + 10000;
                    var sellerTo = '';
                    if (req.body.address){
                        sellerTo = req.body.address;
                    }
                    else {
                        if (user.rippleInsuranceWallet && user.rippleInsuranceWallet[0]){
                            sellerTo = user.rippleInsuranceWallet[0].publicKey;
                        }
                    }
                    // TODO : check if valid Ripple/Bitcoin address
                    // TODO : simply write a transaction acquire function that looks up user and addressTo, returning it
                    var transaction = new GoogleWalletTransaction({user : user._id.toString(), dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : sellerTo, amount : req.body.amount, description : req.body.amount.toString() + " XRP from Dyffy", name : "Currency"});
                    transaction.save(function(err,transaction){
                        if (!isEmpty(transaction)){
                            if (sellerTo){
                                res.send({merchant: transaction._id.toString(), user : user._id.toString(), dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : sellerTo, amount : req.body.amount, description : req.body.amount.toString() + " XRP from Dyffy", name : "Currency"});
                            }
                            else {
                                res.send({error:'Transaction failed.'});
                            }
                        }
                        else {
                            res.send({error:'Transaction save failed.'});
                        }
                    });
                }
            });
        }
        else if (req.body.address){
            var d = new Date();
            var todayDate = (d.getTime()-d.getMilliseconds())/1000;
            var expirationDate = todayDate + 10000;
            // TODO : check if valid Ripple/Bitcoin address

            var user = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var transaction = new GoogleWalletTransaction({ipUser : user, dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : req.body.address, amount : req.body.amount, description : "Currency from Dyffy", name : "Currency"});
            transaction.save(function(err,transaction){
                if (!isEmpty(transaction)){
                    res.send({merchant: transaction._id.toString(), ipUser : user, dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : req.body.address, amount : req.body.amount, description : "Currency from Dyffy", name : "Currency"});
                }
                else {
                    res.send({error:'Transaction too long.'});
                }
            });
        }
        else {
            res.send({error:'Transaction failed. No address or login.'});
        }
    }
    else {
        res.send({error:'Insufficient reserves.'});
    }
});
dyffy.post('/googleWalletJWT', function(req, res){
    if (req.body.amount && !isNaN(Number(req.body.amount)) && Number(masterFunds)/1000000 - 100 >= Number(req.body.amount)){
        var exchangeRate;
        var exchangePrice; // exchangeRate * $ * 1.055
        exchangeRate = average(USDtoXRPRate);
        exchangePrice = (Number(req.body.amount) * exchangeRate * creditCardFee).toFixed(2);
        if (req.session && req.session.passport && req.session.passport.user){
            User.findById(req.session.passport.user, function(err,user){
                if (!isEmpty(user)){
                    var d = new Date();
                    var todayDate = (d.getTime()-d.getMilliseconds())/1000;
                    var expirationDate = todayDate + 10000;
                    var sellerTo = '';
                    if (req.body.address){
                        sellerTo = req.body.address;
                    }
                    else {
                        if (user.rippleInsuranceWallet && user.rippleInsuranceWallet[0]){
                            sellerTo = user.rippleInsuranceWallet[0].publicKey;
                        }
                    }
                    // TODO : check if valid Ripple/Bitcoin address
                    // TODO : simply write a transaction acquire function that looks up user and addressTo, returning it
                    var transaction = new GoogleWalletTransaction({user : user._id.toString(), dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : sellerTo, amount : req.body.amount, description : req.body.amount.toString() + " XRP from Dyffy", name : "Currency"});
                    transaction.save(function(err,transaction){
                        if (!isEmpty(transaction)){
                            if (sellerTo){
                                var sellerPacket = "user:" + user._id.toString() + ",to:" + sellerTo + ",amount:" + req.body.amount + ",currency:" + "USD" + ",id:" + transaction._id.toString();
                                if (sellerPacket.length <= 200){
                                    var jwtPacket = jwt.encode({
                                        "iss" : dyffyConfig.googleWalletId,
                                        "aud" : "Google",
                                        "typ" : "google/payments/inapp/item/v1",
                                        "iat" : todayDate,
                                        "exp" : expirationDate,
                                        "request" : {
                                            "currencyCode": "USD",
                                            "price": exchangePrice.toString(),
                                            "name": "Currency",
                                            "sellerData": sellerPacket,
                                            "description": req.body.amount.toString() +" XRP from Dyffy"
                                        }
                                    }, dyffyConfig.googleWalletSecret);
                                    res.send({jwt:jwtPacket});
                                }
                                else {
                                    res.send({error:'Transaction too long.'});
                                }
                            }
                            else {
                                res.send({error:'Transaction failed.'});
                            }
                        }
                        else {
                            res.send({error:'Transaction save failed.'});
                        }
                    });
                }
            });
        }
        else if (req.body.address){
            var d = new Date();
            var todayDate = (d.getTime()-d.getMilliseconds())/1000;
            var expirationDate = todayDate + 10000;
            // TODO : check if valid Ripple/Bitcoin address

            var user = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            var transaction = new GoogleWalletTransaction({ipUser : user, dateReceived : new Date(), currency : "USD", currencyCode : "USD", price : exchangePrice, to : req.body.address, amount : req.body.amount, description : "Currency from Dyffy", name : "Currency"});
            transaction.save(function(err,transaction){
                if (!isEmpty(transaction)){
                    var sellerPacket = "user:" + user._id.toString() + ",to:" + req.body.address + ",amount:" + req.body.amount + ",currency:" + "USD" + ",id:" + transaction._id.toString();
                    if (sellerPacket.length <= 200){
                        var jwtPacket = jwt.encode({
                            "iss" : dyffyConfig.googleWalletId,
                            "aud" : "Google",
                            "typ" : "google/payments/inapp/item/v1",
                            "exp" : expirationDate,
                            "iat" : todayDate,
                            "request" : {
                                "name": "Currency",
                                "description": req.body.amount.toString() +" XRP from Dyffy",
                                "price": exchangePrice.toString(),
                                "currencyCode": "USD",
                                "sellerData": sellerPacket
                            }
                        }, dyffyConfig.googleWalletSecret);
                        res.send({jwt:jwtPacket});
                    }
                    else {
                        res.send({error:'Transaction too long.'});
                    }
                }
                else {
                    res.send({error:'Transaction too long.'});
                }
            });
        }
    }
    else {
        res.send({error:'Insufficient reserves.'});
    }
});
dyffy.post('/googleWallet', function(req, res){
    if (req.body.jwt){
        var returnPacket = jwt.decode(req.body.jwt, dyffyConfig.googleWalletSecret);
        if (returnPacket.response && returnPacket.response.orderId && returnPacket.request && returnPacket.request.sellerData){
            var names = returnPacket.request.sellerData;
            names = names.split(",");
            var objected = {};
            names.forEach(function(name){
                var named = name.split(":");
                var objectname = named[0];
                objected[named[0]] = named[1];
            });
            GoogleWalletTransaction.findById(objected.id, function(err,transaction){
                if (!isEmpty(transaction)){
                    var rc = new WebSocketClient();
                    rc.on('connect', function(connection){
                        connection.on('message', function(message){
                            message = JSON.parse(message.utf8Data);
                            if (message.error){
                            }
                            else {
                                transaction.verified = true;
                                transaction.dateClosed = new Date();
                                transaction.orderId = returnPacket.response.orderId;
                                transaction.save(function(err,tran){
                                    if (!isEmpty(tran)){
                                    }
                                });
                                res.send(returnPacket.response);
                            }
                        });
                        var finalAddress = objected.to.split('?dt=');
                        var finalAmount = (Number(objected.amount) * 1000000).toFixed(0);
                        if (finalAddress && finalAddress[1]){
                            connection.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],DestinationTag:finalAddress[1],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                        }
                        else {
                            connection.send(JSON.stringify({command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:finalAddress[0],Amount:finalAmount},secret:dyffyConfig.masterSecret}));
                        }
                    });
                    rc.on('error', function(error){
                    });
                    rc.connect(dyffyConfig.rippled);
                }
            });
        }
        else {
            res.send({error:'Transaction failed.'});
        }
    }
    else {
        res.send({error:'Transaction failed.'});
    }
});
dyffy.io.route('logout', function(req){
    if (req.session.passport && req.session.passport.user){
        var uniqueId = req.session.passport.user;
        User.findById(uniqueId, function(err, userObject){
            LossGroup.find({'user._id' : uniqueId}).stream().on('data', function (room) {
                req.io.join(room._id.toString());
                if (userObject.username){
                    req.io.room(room._id).broadcast('newOffline', {
                        message: userObject.username + ' is now offline.', room : room._id.toString()
                    });
                }
            }).on('close', function () {
                });
        });
        var success = "Logged Out.";
        req.io.emit('logout', {success : success});
    }
    else {
        req.io.emit('error', {error : 'Already logged off.'});
    }
});
dyffy.post('/logout', function(req, res) {
    req.logout();
    var success = "Logged Out.";
    res.send({success : success});
});
dyffy.io.route('ready', function(req) {
    if (req.session.passport && req.session.passport.user){
        User.findById(req.session.passport.user, function(err, userObject){
            LossGroup.find({'user._id' : req.session.passport.user}).stream().on('data', function (room) {
                req.io.join(room._id.toString());
                if (userObject.username){
                    req.io.room(room._id.toString()).broadcast('newOnline', {
                        message: userObject.username + ' is now online.', room : room._id.toString()
                    });
                }
            }).on('close', function () {
                });
        });
    }
    else {
        req.io.emit('error', {error : 'Not logged in.'});
    }
});
function average(arr){
    return _.reduce(arr, function(memo, num)
    {
        return memo + num;
    }, 0) / arr.length;
}
function issueInvoicesDues(claim, group, req){
    User.find({'groupHistory.group' : group._id.toString()}, function(err, use){
        User.findById(claim.user, function(err, owed){

            $.getJSON(
                'http://openexchangerates.org/api/latest.json?app_id=' + dyffyConfig.exchangeKey,
                function(data) {
                    if ( typeof fx !== "undefined" && fx.rates ) {
                        fx.rates = data.rates;
                        fx.base = data.base;
                    } else {
                        var fxSetup = {
                            rates : data.rates,
                            base : data.base
                        }
                    }

                    var rc = new WebSocketClient();
                    rc.on('connect', function(connection){
                        connection.on('message', function(message){
                            function average(arr){
                                return _.reduce(arr, function(memo, num)
                                {
                                    return memo + num;
                                }, 0) / arr.length;
                            }
                            var exchange;
                            message = JSON.parse(message.utf8Data);
                            if (message.error){
                                req.io.emit('error', {error : 'error'});
                            }
                            else if (message.id == '1') {
                                var xrpExchange
                                if (message.result){
                                    if (message.result.asks && message.result.asks[0]){
                                        xrpExchange = Number(message.result.asks[0].quality) * 1000000;
                                    }
                                }
                                fx.rates.XRP = 1/xrpExchange;
                                connection.send(JSON.stringify({id : "2",command : "book_offers", taker_gets:{currency:"LTC", issuer:"rJHygWcTLVpSXkowott6kzgZU6viQSVYM1"}, taker_pays:{currency:"USD", issuer:"rvYAfWj5gh67oV6fW32ZzP3Aw4Eubs59B"},taker:dyffyConfig.masterAddress}));
                            }
                            else if (message.id == '2') {
                                if (message.result.offers && message.result.offers[0]){
                                    var offerAverage = new Array();
                                    if (message.result.offers){
                                        message.result.offers.forEach(function(offed){
                                            if (offed.taker_pays_funded || offed.taker_gets_funded){
                                                offerAverage.push(Number(offed.taker_pays_funded.value)/Number(offed.taker_gets_funded));
                                            }
                                            else {
                                                offerAverage.push(Number(offed.quality));
                                            }
                                        });
                                    }
                                    fx.rates.LTC = 1/average(offerAverage);
                                }

                                var finalVal = group.portionInput;
                                var valueArray = [];
                                var valueArrayInit = [];
                                var initObject = {};
                                var valueInUSD = 0;
                                group.terms[0].signature.forEach(function(sig){
                                    if (sig.user.toString() != claim.user.toString()){
                                        if (sig.currency != "USD"){
                                            valueInUSD = fx.convert(sig.valueInsured, {from: sig.currency, to: "USD"});
                                            if (valueInUSD){
                                                valueArray.push(valueInUSD);
                                            }
                                        }
                                        else if (sig.currency == "USD"){
                                            valueArray.push(sig.valueInsured);
                                        }
                                    }
                                });
                                valueArrayInit = valueArray;
                                var sumArray = 0;
                                valueArray.forEach(function(num){
                                    sumArray += num;
                                });
                                valueArray = _.map(valueArray, function(num){ return num / sumArray; });
                                var average = 0;
                                valueArray.forEach(function(num){
                                    average += num / valueArray.length;
                                });
                                valueArray = _.map(valueArray, function(num){ return (num - average) * (1 + finalVal) + average; });

                                initObject = _.object(valueArrayInit, valueArray);

                                group.terms[0].signature.forEach(function(sig){
                                    if (sig.user.toString() != claim.user.toString()){
                                        sig.portion = initObject[fx.convert(sig.valueInsured, {from: sig.currency, to: "USD"})];
                                    }
                                });

                                use.forEach(function(used){
                                    if (used._id.toString() != claim.user.toString()){
                                        var history = false;
                                        used.groupHistory.forEach(function(groupHistoried){
                                            if (group._id.toString() == groupHistoried.group && groupHistoried.member){
                                                if (group.terms[0].signature){
                                                    group.terms[0].signature.forEach(function(signed){
                                                        if (used._id.toString() == signed.user){
                                                            history =  signed;
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        if (history){
                                            var payTotal = claim.amountClaimed * history.portion;
                                            var houseAmount = payTotal * dyffyConfig.houseRate;
                                            var payTotalInvoice = houseAmount + payTotal;
                                            if (claim.currencyClaimedIn == 'Dwolla USD'){
                                                payTotalInvoice = parseFloat(payTotalInvoice.toFixed(2));
                                            }
                                            var invoice = new Invoice({claim : claim._id, amount : payTotalInvoice, houseAmount : houseAmount, currency : claim.currencyClaimedIn, fundingSources : owed.rippleInsuranceWallet[0].fundingSources, to : owed._id, from : used._id, recievedDate : new Date(), proportionOfClaim : history.portion, currencyClaimedIn: claim.currencyClaimedIn, type: 'insurance'});
                                            var due = new Invoice({claim : claim._id, amount : payTotal, currency : claim.currencyClaimedIn, fundingSources : owed.rippleInsuranceWallet[0].fundingSources, to : owed._id, from : used._id, recievedDate : new Date(), proportionOfClaim : history.portion, currencyClaimedIn: claim.currencyClaimedIn, type: 'insurance'});
                                            invoice.dueId = due._id.toString();
                                            invoice.group = group._id.toString();
                                            invoice.receivedDate = new Date();
                                            due.invoiceId = invoice._id.toString();
                                            due.group = group._id.toString();
                                            due.receivedDate = new Date();
                                            used.invoices.push(invoice);
                                            owed.dues.push(due);

                                            used.save(function(err, sender){
                                                dyffy.io.room(group._id.toString()).broadcast('invoiceChange', {userId : sender._id.toString(), invoices : sender.invoices});
                                                req.io.emit('invoiceChange', {userId : sender._id.toString(), invoices : sender.invoices});
                                            });
                                        }
                                    }
                                });
                                owed.save(function(err, recipient){
                                    dyffy.io.room(group._id.toString()).broadcast('dueChange', {userId : recipient._id.toString(), dues : recipient.dues});
                                    req.io.emit('dueChange', {userId : recipient._id.toString(), dues : recipient.dues});
                                    connection.close();
                                });

                            }
                        });
                        connection.send(JSON.stringify({"command":"subscribe", "id": "1", "books" : [ {"snapshot": 1, "both": 1, "taker_gets":{"currency":"USD", "issuer":"rvYAfWj5gh67oV6fW32ZzP3Aw4Eubs59B"}, "taker_pays":{"currency":"XRP"}}]}));
                    });
                    rc.on('error', function(error){
                        req.io.emit('error', {error : 'error'});
                    });
                    rc.connect(dyffyConfig.rippled);
                }
            );
        });
    });
}
dyffy.io.route('voteOnClaim', function(req) {
    if (req.session.passport && req.session.passport.user && req.data.claimId && req.data.groupId && typeof req.data.vote !== 'undefined' && typeof req.data.vote !== 'object'){
        User.findById(req.session.passport.user, function(err, userObject){
            userObject.groupHistory.forEach(function(userGroup){
                if (userGroup.group && req.data.groupId.toString() == userGroup.group.toString() && userGroup.member){
                    LossGroup.findById(req.data.groupId, function(err,room){
                        if (room.type !== 'reward'){
                            if (room.vested){
                                var claim = room.claims.id(req.data.claimId);
                                var claimantAdmin = false;
                                User.findById(claim.user, function(err, usered){
                                    usered.groupHistory.forEach(function(history){
                                        if (history.group == req.data.groupId && history.admin){
                                            claimantAdmin = true;
                                        }
                                    });
                                });
                                if ((room.isAdmin === true && userGroup.admin) || room.isAdmin === false || room.claimVote === true || claimantAdmin){
                                    if (claim.voteApprove !== false && claim.voteApprove !== true){
                                        User.find({'groupHistory.group' : room._id.toString()},function(err, groupUser){
                                            var groupMemberCount = 0;
                                            if (room.claimVote === true || room.isAdmin === false || claimantAdmin){
                                                groupUser.forEach(function(us){
                                                    us.groupHistory.forEach(function(userCounter){
                                                        if (userCounter.group == req.data.groupId.toString() && userCounter.member){
                                                            groupMemberCount += 1;
                                                        }
                                                    });
                                                });
                                            }
                                            else {
                                                groupUser.forEach(function(us){
                                                    us.groupHistory.forEach(function(userCounter){
                                                        if (userCounter.group == req.data.groupId.toString() && userCounter.admin && userCounter.member){
                                                            groupMemberCount += 1;
                                                        }
                                                    });
                                                });
                                            }
                                            var votes = claim.votes;
                                            var numberOfVotes = claim.votes.length;
                                            var voteCount = 0;
                                            var flag = false;
                                            votes.forEach(function(vote){
                                                if (vote.vote){
                                                    voteCount += 1;
                                                }
                                                if (vote.user == req.session.passport.user){
                                                    flag = true;
                                                }
                                            });
                                            var thresholdTime = room.voteThresholdClaimApproval * numberOfVotes;
                                            var totalThreshold = room.voteThresholdClaimApproval * groupMemberCount;
                                            var currentDate = new Date();
                                            if (claim.forceEndDate && currentDate >= claim.forceEndDate){
                                                if (voteCount > thresholdTime){
                                                    claim.voteApprove = true;
                                                }
                                                else {
                                                    claim.voteApprove = false;
                                                }
                                                claim.dateVoteEnd = new Date();
                                                room.save(function(err, saveObject){
                                                    req.io.emit('newClaim', {groupId: saveObject._id.toString(), claim: saveObject.claims});
                                                    dyffy.io.room(saveObject._id.toString()).broadcast('newClaim', {groupId: saveObject._id.toString(), claim: saveObject.claims});
                                                    issueInvoicesDues(claim, saveObject, req);
                                                });
                                            }
                                            else if (!flag){
                                                var newVote = new Vote();
                                                newVote.user = req.session.passport.user;
                                                newVote.vote = req.data.vote;
                                                newVote.date = new Date();
                                                claim.votes.push(newVote);
                                                room.save(function(err, saveRoom){
                                                    if (newVote.vote){
                                                        var newCount = voteCount + 1;
                                                    }
                                                    else{
                                                        var newCount = voteCount;
                                                    }
                                                    var claim2 = saveRoom.claims.id(req.data.claimId);
                                                    var claimVoteDifferece = claim2.votes.length - newCount;
                                                    var userMinusThreshold = groupMemberCount - totalThreshold;
                                                    if (req.data.vote && newCount >= totalThreshold){
                                                        claim2.voteApprove = true;
                                                        claim2.dateVoteEnd = new Date();
                                                        saveRoom.save(function(err, saveObject2){
                                                            req.io.emit('newClaim', {groupId: saveObject2._id.toString(), claim: saveObject2.claims});
                                                            dyffy.io.room(saveObject2._id.toString()).broadcast('newClaim', {groupId: saveObject2._id.toString(), claim: saveObject2.claims});
                                                            issueInvoicesDues(claim2, saveObject2, req);
                                                        });
                                                    }
                                                    else if (claimVoteDifferece > userMinusThreshold){
                                                        claim2.voteApprove = false;
                                                        claim2.dateVoteEnd = new Date();
                                                        saveRoom.save(function(err, saveObject2){
                                                            req.io.emit('newClaim', {groupId: saveObject2._id.toString(), claim: saveObject2.claims});
                                                            dyffy.io.room(saveObject2._id.toString()).broadcast('newClaim', {groupId: saveObject2._id.toString(), claim: saveObject2.claims});
                                                        });
                                                    }
                                                    else {
                                                        req.io.emit('newClaim', {groupId: saveRoom._id.toString(), claim: saveRoom.claims});
                                                        dyffy.io.room(saveRoom._id.toString()).broadcast('newClaim', {groupId: saveRoom._id.toString(), claim: saveRoom.claims});
                                                    }
                                                });
                                            }
                                            else {
                                                req.io.emit('error', {error: "Already voted."});
                                            }
                                        });
                                    }
                                    else {
                                        req.io.emit('error', {error: "User is not in group."});
                                    }
                                }
                            }
                        }
                    });
                }
            });
        });
    }
});
dyffy.io.route('voteOnMember', function(req) {
    if (req.session.passport && req.session.passport.user && req.data.memberId && req.data.groupId && typeof req.data.vote !== 'undefined' && typeof req.data.vote !== 'object'){
        User.findById(req.session.passport.user, function(err, userObject){
            userObject.groupHistory.forEach(function(userGroup){
                if (userGroup.group && req.data.groupId.toString() == userGroup.group.toString() && userGroup.member){
                    User.findById(req.data.memberId, function (err, userVotedOn){
                        userVotedOn.groupHistory.forEach(function(decidingGroup){
                            if (decidingGroup.group && decidingGroup.group.toString() == req.data.groupId.toString()){
                                if (decidingGroup.voteApprove !== false && decidingGroup.voteApprove !== true){
                                    LossGroup.findById(req.data.groupId, function(err,groupy){
                                        if (groupy.type !== 'reward'){
                                            if ((groupy.isAdmin === true && userGroup.admin) || (groupy.isAdmin === false)){
                                                User.find({'groupHistory.group' : groupy._id.toString()},function(err, groupUser){
                                                    var groupMemberCount = 0;
                                                    if (groupy.isAdmin === false){
                                                        groupUser.forEach(function(us){
                                                            us.groupHistory.forEach(function(userCounter){
                                                                if (userCounter.group == req.data.groupId.toString() && userCounter.member){
                                                                    groupMemberCount += 1;
                                                                }
                                                            });
                                                        });
                                                    }
                                                    else {
                                                        groupUser.forEach(function(us){
                                                            us.groupHistory.forEach(function(userCounter){
                                                                if (userCounter.group == req.data.groupId.toString() && userCounter.admin && userCounter.member){
                                                                    groupMemberCount += 1;
                                                                }
                                                            });
                                                        });
                                                    }
                                                    var votes = decidingGroup.voteJoin;
                                                    var numberOfVotes = decidingGroup.voteJoin.length;
                                                    var voteCount = 0;
                                                    var flag = false;
                                                    votes.forEach(function(vote){
                                                        if (vote.vote){
                                                            voteCount += 1;
                                                        }
                                                        if (vote.user == req.session.passport.user){
                                                            flag = true;
                                                        }
                                                    });
                                                    var thresholdTime = groupy.voteThresholdNewMembers * numberOfVotes;
                                                    var totalThreshold = groupy.voteThresholdNewMembers * groupMemberCount;
                                                    var currentDate = new Date();
                                                    if (decidingGroup.forceEndDate && currentDate >= decidingGroup.forceEndDate){
                                                        if (voteCount > thresholdTime){
                                                            decidingGroup.voteApprove = true;
                                                            decidingGroup.member = true;
                                                            decidingGroup.voteJoinDateEnd = new Date();
                                                            decidingGroup.voteJoinDate = new Date();
                                                            decidingGroup.dateJoined = new Date();
                                                        }
                                                        else {
                                                            decidingGroup.voteApprove = false;
                                                            decidingGroup.member = false;
                                                            decidingGroup.voteJoinDateEnd = new Date();
                                                        }
                                                        decidingGroup.dateVoteEnd = new Date();
                                                        userVotedOn.save(function(err, saveObject){
                                                            dyffy.io.room(userVotedOn._id.toString()).broadcast('getRooms');
                                                            User.findById(saveObject._id, "_id username firstName lastName groupHistory joinDate activeDate currentProfileImageUrl verificationBadge rippleInsuranceWallet.publicKey jumioData facebookData linkedinData dwollaData verificationBadge verified",
                                                                function (err,doc){
                                                                    if (!isEmpty(err)){
                                                                        req.io.emit('error', {error: "Please forgive the error."});
                                                                    }
                                                                    else{
                                                                        doc = doc.toObject();
                                                                        doc = modSocial(doc);
                                                                        doc.groupHistory = _.filter(doc.groupHistory, function(num){ if (num.deleted !== true){return num} });
                                                                        dyffy.io.room(groupy._id.toString()).broadcast('userFindMany', [doc]);
                                                                    }
                                                                });
                                                        });
                                                    }
                                                    else if (!flag){
                                                        var newVote = new Vote();
                                                        newVote.user = req.session.passport.user;
                                                        newVote.vote = req.data.vote;
                                                        newVote.date = new Date();
                                                        decidingGroup.voteJoin.push(newVote);
                                                        userVotedOn.save(function(err, saveRoom){
                                                            dyffy.io.room(groupy._id.toString()).broadcast('memberVote', {
                                                                groupHistory : decidingGroup, vote : req.data.vote
                                                            });
                                                            var decidingGroup2 = saveRoom.groupHistory.id(decidingGroup._id);
                                                            if (newVote.vote){
                                                                var newCount = voteCount + 1;
                                                            }
                                                            else{
                                                                var newCount = voteCount;
                                                            }
                                                            var claimVoteDifferece = decidingGroup2.voteJoin.length - newCount;
                                                            var userMinusThreshold = groupMemberCount - totalThreshold;
                                                            if (req.data.vote && newCount >= totalThreshold){
                                                                decidingGroup.voteApprove = true;
                                                                decidingGroup.member = true;
                                                                decidingGroup.voteJoinDateEnd = new Date();
                                                                decidingGroup.voteJoinDate = new Date();
                                                                decidingGroup.dateJoined = new Date();
                                                                userVotedOn.save(function(err, saveObject2){
                                                                    dyffy.io.room(userVotedOn._id.toString()).broadcast('getRooms');
                                                                    dyffy.io.room(groupy._id.toString()).broadcast('memberVoteEnd', {groupHistory : decidingGroup});
                                                                    User.findById(saveObject2._id, "_id username firstName lastName groupHistory joinDate activeDate currentProfileImageUrl verificationBadge rippleInsuranceWallet.publicKey jumioData facebookData linkedinData dwollaData verificationBadge verified",
                                                                        function (err,doc){
                                                                            if (!isEmpty(err)){
                                                                                req.io.emit('error', {error: "Please forgive the error."});
                                                                            }
                                                                            else{
                                                                                doc = doc.toObject();
                                                                                doc = modSocial(doc);
                                                                                doc.groupHistory = _.filter(doc.groupHistory, function(num){ if (num.deleted !== true){return num} });
                                                                                dyffy.io.room(groupy._id.toString()).broadcast('userFindMany', [doc]);
                                                                            }
                                                                        });
                                                                });
                                                            }
                                                            else if (claimVoteDifferece > userMinusThreshold){
                                                                decidingGroup.voteApprove = false;
                                                                decidingGroup.member = false;
                                                                decidingGroup.voteJoinDateEnd = new Date();
                                                                userVotedOn.save(function(err, saveObject2){
                                                                    dyffy.io.room(userVotedOn._id.toString()).broadcast('getRooms');
                                                                    dyffy.io.room(groupy._id.toString()).broadcast('memberVoteEnd', {groupHistory : decidingGroup});
                                                                    User.findById(saveObject2._id, "_id username firstName lastName groupHistory joinDate activeDate currentProfileImageUrl verificationBadge rippleInsuranceWallet.publicKey jumioData facebookData linkedinData dwollaData verificationBadge verified",
                                                                        function (err,doc){
                                                                            if (!isEmpty(err)){
                                                                                req.io.emit('error', {error: "Please forgive the error."});
                                                                            }
                                                                            else{
                                                                                doc = doc.toObject();
                                                                                doc = modSocial(doc);
                                                                                doc.groupHistory = _.filter(doc.groupHistory, function(num){ if (num.deleted !== true){return num} });
                                                                                dyffy.io.room(groupy._id.toString()).broadcast('userFindMany', [doc]);
                                                                            }
                                                                        });
                                                                });
                                                            }
                                                            else {
                                                                User.findById(userVotedOn._id, "_id username firstName lastName groupHistory joinDate activeDate currentProfileImageUrl verificationBadge rippleInsuranceWallet.publicKey jumioData facebookData linkedinData dwollaData verificationBadge verified",
                                                                    function (err,doc){
                                                                        if (!isEmpty(err)){
                                                                            req.io.emit('error', {error: "Please forgive the error."});
                                                                        }
                                                                        else{
                                                                            doc = doc.toObject();
                                                                            doc = modSocial(doc);
                                                                            doc.groupHistory = _.filter(doc.groupHistory, function(num){ if (num.deleted !== true){return num} });
                                                                            dyffy.io.room(groupy._id.toString()).broadcast('userFindMany', [doc]);
                                                                        }
                                                                    });
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        req.io.emit('error', {error: "Already voted."});
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                                else {
                                    req.io.emit('error', {error: "User is not in group."});
                                }
                            }
                        });
                    });
                }
            });
        });
    }
    else {
        req.io.emit('error', {error: "Need yay or nay vote."});
    }
});
dyffy.io.route('voteOnGroupVest', function(req) {
    if (req.session.passport && req.session.passport.user && req.data.groupId && typeof req.data.vote !== 'undefined' && typeof req.data.vote !== 'object'){
        User.findById(req.session.passport.user, function(err, userObject){
            userObject.groupHistory.forEach(function(userGroup){
                if (userGroup.group && req.data.groupId.toString() == userGroup.group.toString() && userGroup.member){
                    LossGroup.findById(req.data.groupId, function(err,room){
                        if ((room.isAdmin === true && userGroup.admin) || (room.isAdmin === false) && !room.deleted && room.type !== 'reward'){
                            if (room.terms[0]){
                                var userSigned = false;
                                room.terms[0].signature.forEach(function(sig){
                                    if (sig.user == req.session.passport.user.toString()){
                                        userSigned = true;
                                    }
                                });
                                if (userSigned){
                                    User.find({'groupHistory.group' : room._id},function(err, groupUser){
                                        var groupMemberCount = 0;
                                        groupUser.forEach(function(us){
                                            us.groupHistory.forEach(function(userCounter){
                                                if (userCounter.group == req.data.groupId.toString() && userCounter.member){
                                                    groupMemberCount += 1;
                                                }
                                            });
                                        });
                                        if (!room.minimumUsers || groupMemberCount >= room.minimumUsers){
                                            if (room.isAdmin !== false){
                                                groupMemberCount = 0
                                                groupUser.forEach(function(us){
                                                    us.groupHistory.forEach(function(userCounter){
                                                        if (userCounter.group == req.data.groupId.toString() && userCounter.admin && userCounter.member){
                                                            groupMemberCount += 1;
                                                        }
                                                    });
                                                });
                                            }
                                            var votes = room.vestVote;
                                            var numberOfVotes = room.vestVote.length;
                                            var voteCount = 0;
                                            var flag = false;
                                            votes.forEach(function(vote){
                                                if (vote.vote){
                                                    voteCount += 1;
                                                }
                                                if (vote.user == req.session.passport.user){
                                                    flag = true;
                                                }
                                            });
                                            var thresholdTime = room.voteThresholdGroupVest * numberOfVotes;
                                            var totalThreshold = room.voteThresholdGroupVest * groupMemberCount;
                                            if (!flag){
                                                var newVote = new Vote();
                                                newVote.user = req.session.passport.user;
                                                newVote.vote = req.data.vote;
                                                newVote.date = new Date();
                                                room.vestVote.push(newVote);
                                                room.save(function(err, saveRoom){
                                                    if (newVote.vote){
                                                        var newCount = voteCount + 1;
                                                    }
                                                    else{
                                                        var newCount = voteCount;
                                                    }
                                                    var claimVoteDifferece = room.vestVote.length - newCount;
                                                    var userMinusThreshold = groupMemberCount - totalThreshold;
                                                    if (req.data.vote && newCount >= totalThreshold){
                                                        var allSigned = true;
                                                        groupUser.forEach(function(groupDetect){
                                                            groupDetect.groupHistory.forEach(function(isInGroup){
                                                                if (isInGroup.member && isInGroup.group == req.data.groupId.toString()){
                                                                    var signatureThere = false;
                                                                    saveRoom.terms[0].signature.forEach(function(sigThere){
                                                                        if (sigThere.user == groupDetect._id.toString()){
                                                                            signatureThere = true;
                                                                        }
                                                                    });
                                                                    if (signatureThere == false){
                                                                        allSigned = false;
                                                                        isInGroup.member = false;
                                                                        isInGroup.admin = false;
                                                                        // founding user security logic here, must reexamine other functions and require member && foundingUser for fU security-governed
                                                                        groupDetect.save(function(err,memberNot){
                                                                            dyffy.io.room(room._id.toString()).broadcast('forcedOut', {user : groupDetect._id.toString(), room : room._id.toString()});
                                                                        });

                                                                    }
                                                                }
                                                            });
                                                        });
                                                        saveRoom.vested = true;
                                                        saveRoom.vestDate = new Date();
                                                        saveRoom.endDate = new Date();
                                                        if (typeof saveRoom.durationOfGroup !== 'undefined'){
                                                            saveRoom.endDate.setDate(saveRoom.vestDate.getDate() + saveRoom.durationOfGroup);
                                                        }
                                                        else if (typeof saveRoom.duration !== 'undefined'){
                                                            saveRoom.endDate.setSeconds(saveRoom.endDate.getSeconds() + saveRoom.duration.seconds);
                                                            saveRoom.endDate.setMinutes(saveRoom.endDate.getMinutes() + saveRoom.duration.minutes);
                                                            saveRoom.endDate.setHours(saveRoom.endDate.getHours() + saveRoom.duration.hours);
                                                            saveRoom.endDate.setDate(saveRoom.endDate.getDate() + saveRoom.duration.days);
                                                        }
                                                        saveRoom.save(function(err, group){
                                                            if (!isEmpty(group)){
                                                                $.getJSON(
                                                                    'http://openexchangerates.org/api/latest.json?app_id=' + dyffyConfig.exchangeKey,
                                                                    function(data) {
                                                                        if ( typeof fx !== "undefined" && fx.rates ) {
                                                                            fx.rates = data.rates;
                                                                            fx.base = data.base;
                                                                        } else {
                                                                            var fxSetup = {
                                                                                rates : data.rates,
                                                                                base : data.base
                                                                            }
                                                                        }
                                                                        var rc = new WebSocketClient();
                                                                        rc.on('connect', function(connection){
                                                                            connection.on('message', function(message){
                                                                                function average(arr){
                                                                                    return _.reduce(arr, function(memo, num)
                                                                                    {
                                                                                        return memo + num;
                                                                                    }, 0) / arr.length;
                                                                                }
                                                                                var exchange;
                                                                                message = JSON.parse(message.utf8Data);
                                                                                if (message.error){
                                                                                    req.io.emit('error', {error : 'error'});
                                                                                }
                                                                                else if (message.id == '1') {
                                                                                    var xrpExchange
                                                                                    if (message.result){
                                                                                        if (message.result.asks && message.result.asks[0]){
                                                                                            xrpExchange = Number(message.result.asks[0].quality) * 1000000;
                                                                                        }
                                                                                    }
                                                                                    fx.rates.XRP = 1/xrpExchange;
                                                                                    connection.send(JSON.stringify({id : "2",command : "book_offers", taker_gets:{currency:"LTC", issuer:"rJHygWcTLVpSXkowott6kzgZU6viQSVYM1"}, taker_pays:{currency:"USD", issuer:"rvYAfWj5gh67oV6fW32ZzP3Aw4Eubs59B"},taker:dyffyConfig.masterAddress}));
                                                                                }
                                                                                else if (message.id == '2') {
                                                                                    if (message.result.offers && message.result.offers[0]){
                                                                                        var offerAverage = new Array();
                                                                                        if (message.result.offers){
                                                                                            message.result.offers.forEach(function(offed){
                                                                                                if (offed.taker_pays_funded || offed.taker_gets_funded){
                                                                                                    offerAverage.push(Number(offed.taker_pays_funded.value)/Number(offed.taker_gets_funded));
                                                                                                }
                                                                                                else {
                                                                                                    offerAverage.push(Number(offed.quality));
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                        fx.rates.LTC = 1/average(offerAverage);
                                                                                    }
                                                                                    var finalVal = group.portionInput;
                                                                                    var valueArray = [];
                                                                                    var valueArrayInit = [];
                                                                                    var initObject = {};
                                                                                    var valueInUSD = 0;
                                                                                    group.terms[0].signature.forEach(function(sig){
                                                                                        if (sig.currency != "USD"){
                                                                                            valueInUSD = fx.convert(sig.valueInsured, {from: sig.currency, to: "USD"});
                                                                                            if (valueInUSD){
                                                                                                valueArray.push(valueInUSD);
                                                                                            }
                                                                                        }
                                                                                        else if (sig.currency == "USD"){
                                                                                            valueArray.push(sig.valueInsured);
                                                                                        }
                                                                                    });
                                                                                    valueArrayInit = valueArray;
                                                                                    var sumArray = 0;
                                                                                    valueArray.forEach(function(num){
                                                                                        sumArray += num;
                                                                                    });
                                                                                    valueArray = _.map(valueArray, function(num){ return num / sumArray; });
                                                                                    var average = 0;
                                                                                    valueArray.forEach(function(num){
                                                                                        average += num / valueArray.length;
                                                                                    });
                                                                                    valueArray = _.map(valueArray, function(num){ return (num - average) * (1 + finalVal) + average; });

                                                                                    initObject = _.object(valueArrayInit, valueArray);

                                                                                    group.terms[0].signature.forEach(function(sig){
                                                                                        sig.portion = initObject[fx.convert(sig.valueInsured, {from: sig.currency, to: "USD"})];
                                                                                    });

                                                                                    group.save(function(error,saveGroup){
                                                                                        if (!isEmpty(saveGroup)){
                                                                                            req.io.emit('termAcquire', {contract : saveGroup.terms[0].contract, groupId : req.data.groupId});
                                                                                            dyffy.io.room(saveGroup._id.toString()).broadcast('termAcquire', {contract : saveGroup.terms[0].contract, groupId : req.data.groupId});
                                                                                            connection.close();
                                                                                        }
                                                                                        else {
                                                                                            req.io.emit('error', {error: "Please forgive the error."});
                                                                                            connection.close();
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                            connection.send(JSON.stringify({"command":"subscribe", "id": "1", "books" : [ {"snapshot": 1, "both": 1, "taker_gets":{"currency":"USD", "issuer":"rvYAfWj5gh67oV6fW32ZzP3Aw4Eubs59B"}, "taker_pays":{"currency":"XRP"}}]}));
                                                                        });
                                                                        rc.on('error', function(error){
                                                                            req.io.emit('error', {error : 'error'});
                                                                        });
                                                                        rc.connect(dyffyConfig.rippled);
                                                                    }
                                                                );
                                                                if (room.overDate){
                                                                    var job = new cronJob(room.overDate, function(){
                                                                            LossGroup.findById(room._id, function(err, docc){
                                                                                docc.dissolveDate = new Date();
                                                                                docc.save(function(err, group){
                                                                                    dyffy.io.broadcast('groupEnd', {'group' : group._id});
                                                                                });
                                                                            });
                                                                        }, function () {
                                                                            // second callback
                                                                        },
                                                                        true,
                                                                        'UTC'
                                                                    );
                                                                }
                                                                emitFullGroup(req, group, true);
                                                                req.io.emit('success', {success : 'Group vested.'});
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        var finalRoom = saveRoom.toObject();
                                                        req.io.emit('vestVote', {room : saveRoom._id.toString(), vestVote: finalRoom.vestVote});
                                                        dyffy.io.room(saveRoom._id.toString()).broadcast('vestVote', {room : saveRoom._id.toString(), vestVote: finalRoom.vestVote});
                                                    }
                                                });
                                            }
                                            else {
                                                req.io.emit('error', {error: "Already voted."});
                                            }
                                        }
                                        else {
                                            var needMore = room.minimumUsers - groupMemberCount;
                                            needMore = needMore.toString();
                                            req.io.emit('error', {error: "Need " + needMore + " more users to vest."});
                                        }
                                    });
                                }
                                else {
                                    req.io.emit('error', {error: "Must sign terms before voting."});
                                }
                            }
                            else {
                                req.io.emit('error', {error: "Need terms prior to vest voting."});
                            }
                        }
                    });
                }
            });
        });
    }
});
dyffy.io.route('getLogs', function(req) {
    var directory = '/home/ubuntu/dev.dyffy';
    var stashOutput3 = '';
    var restart2 = child_process.spawn('tail', ['-n100', 'dev.dyffy.log'], {cwd: directory});
    restart2.stdout.setEncoding('utf8');
    restart2.stderr.setEncoding('utf8');
    restart2.stdout.on('data', function(data){
        stashOutput3 += data.toString();
    });
    restart2.stderr.on('data', function(data){
        stashOutput3 += data.toString();
    });
    restart2.on('exit', function(code){
        var splitted = stashOutput3.split(/(\r?\n)/g);
        splitted.reverse();
        var finalString = '';
        for (var i=0; i<splitted.length; i++) {
            if (i != (splitted.length - 1)){
                finalString += splitted[i] + '<br>';
            }
            else {
                finalString += splitted[i];
            }
        }
        req.io.emit('getLogs', {logs: finalString});
    });
});
dyffy.io.route('pullDevBackend', function(req) {
    var directory = '/home/ubuntu/dev.dyffy';
    var git = child_process.spawn('git', ['pull'], {cwd: directory});
    var stashOutput = '';
    git.stdout.setEncoding('utf8');
    git.stderr.setEncoding('utf8');
    git.stdout.on('data', function(data){
        stashOutput += data.toString();
    });
    git.stderr.on('data', function(data){
        stashOutput += data.toString();
    });
    git.on('exit', function(code){
        var stashOutput3 = '';
        var restart2 = child_process.spawn('sudo', ['restart', 'dev.dyffy'], {cwd: directory});
        restart2.stdout.setEncoding('utf8');
        restart2.stderr.setEncoding('utf8');
        restart2.stdout.on('data', function(data){
            stashOutput3 += data.toString();
        });
        restart2.stderr.on('data', function(data){
            stashOutput3 += data.toString();
        });
        restart2.on('exit', function(code){
            console.log(stashOutput3);
        });
    });
});
dyffy.io.route('pullDev', function(req) {
    var directory = '/home/ubuntu/dev.dyffy';
    var git = child_process.spawn('git', ['pull'], {cwd: directory});
    var stashOutput = '';
    git.stdout.setEncoding('utf8');
    git.stderr.setEncoding('utf8');
    git.stdout.on('data', function(data){
        stashOutput += data.toString();
    });
    git.stderr.on('data', function(data){
        stashOutput += data.toString();
    });
    git.on('exit', function(code){
        var stashOutput2 = '';
        var gitCommit = child_process.spawn('sudo', ['grunt'], {cwd: directory});
        gitCommit.stdout.setEncoding('utf8');
        gitCommit.stderr.setEncoding('utf8');
        gitCommit.stdout.on('data', function(data){
        });
        gitCommit.stderr.on('data', function(data){
        });
        gitCommit.on('exit', function(code){
            var stashOutput3 = '';
            var restart2 = child_process.spawn('sudo', ['restart', 'dev.dyffy'], {cwd: directory});
            restart2.stdout.setEncoding('utf8');
            restart2.stderr.setEncoding('utf8');
            restart2.stdout.on('data', function(data){
                stashOutput3 += data.toString();
            });
            restart2.stderr.on('data', function(data){
                stashOutput3 += data.toString();
            });
            restart2.on('exit', function(code){
                console.log(stashOutput3);
            });
        });
    });
});
dyffy.io.route('getLinkedinConnections', function(req) {
    if (req.session.passport && req.session.passport.user){
        User.findById(req.session.passport.user, function(err, userObject){
            if (userObject.linkedinToken && userObject.linkedinSecret){
                getLinkedinConnections(req, userObject.linkedinToken, userObject.linkedinSecret);
            }
        });
    }
    else {
        req.io.emit('error', {error : "Insufficient permissions."});
    }
});
dyffy.io.route('sendLinkedinShare', function(req) {
    if (req.session.passport && req.session.passport.user && req.data.title, typeof req.data.submittedUrl !== 'undefined' && typeof req.data.submittedImageUrl !== 'undefined' && typeof req.data.description !== 'undefined'){
        User.findById(req.session.passport.user, function(err, userObject){
            if (userObject.linkedinToken && userObject.linkedinSecret){
                sendLinkedinShare(req, userObject.linkedinToken, userObject.linkedinSecret, req.data.title, req.data.submittedUrl, req.data.submittedImageUrl, req.data.description);
            }
        });
    }
    else {
        req.io.emit('error', {error : "Insufficient permissions."});
    }
});
dyffy.io.route('sendLinkedinMessaged', function(req) {
    if (req.session.passport && req.session.passport.user && typeof req.data.people !== 'undefined' && typeof req.data.subject !== 'undefined' && typeof req.data.subject !== 'object' && typeof req.data.contented !== 'undefined' && typeof req.data.contented !== 'object'){
        User.findById(req.session.passport.user, function(err, userObject){
            if (userObject.linkedinToken && userObject.linkedinSecret){
                sendLinkedinMessage(req, userObject.linkedinToken, userObject.linkedinSecret, req.data.people, req.data.subject, req.data.contented);
            }
        });
    }
    else {
        req.io.emit('error', {error : "Need subject and content."});
    }
});
dyffy.io.route('failToPay', function(req) {
    if (req.session.passport && req.session.passport.user && req.data.dueId){
        User.findById(req.session.passport.user, function(err, userObject){
            var due = userObject.dues.id(req.data.dueId);
            LossGroup.findById(due.group, function(err, group){
                var grace = 0;
                var fail = false;
                if (group.gracePeriod){
                    grace = group.gracePeriod;
                }
                else {
                    grace = 7;
                }
                var todayDate = new Date();
                var pastDate = new Date();
                pastDate.setDate(due.receivedDate.getDate() + grace);
                if ((!due.type || due.type == 'insurance') && todayDate > pastDate){
                    fail = true;
                }
                if (fail){
                    due.failToPay = true;
                    due.failToPayDate = new Date();
                    User.findOne({'invoices.dueId' : due._id}, function(err,recipient){
                        var invoice = recipient.invoices.id(due.invoiceId);
                        invoice.failToPay = true;
                        invoice.failToPayDate = new Date();
                        userObject.groupHistory(function(grouped){
                            if (grouped.group == due.group){
                                grouped.member = undefined;
                                grouped.admin = undefined;
                            }
                        });
                        userObject.save(function(err, object1){
                            if (!isEmpty(err)) {
                                req.io.emit('error', {error : "Save error."});
                            }
                            else {
                                recipient.save(function(err2, object2){
                                    if (!isEmpty(err2)) {
                                        req.io.emit('error', {error : "Save error."});
                                    }
                                    else {
                                        LossGroup.findOne({'claims._id' : invoice.claim},function(err,group){
                                            User.find({'groupHistory.group' : due.group},function(err,targetUsered){
                                                if (group.isAdmin){
                                                    var adminCount = 0;
                                                    if (targetUsered){
                                                        targetUsered.forEach(function(target){
                                                            target.groupHistory.forEach(function(history){
                                                                if (history.group == req.data.groupId && history.admin){
                                                                    adminCount += 1;
                                                                }
                                                            });
                                                        });
                                                    }
                                                    if (!adminCount && !group.deleted){
                                                        group.isAdmin = false;
                                                    }
                                                }
                                                group.save(function(err, saveGroup){
                                                    var saveGrouped = saveGroup.toObject();
                                                    var userIds  = [];
                                                    var groupHistoryIds = [];
                                                    var memberCount = 0;
                                                    targetUsered.forEach(function(eached){
                                                        userIds.push(eached._id.toString());
                                                        eached.groupHistory.forEach(function(gh) {
                                                            if (gh.group == saveGrouped._id.toString()) {
                                                                groupHistoryIds.push(gh._id);
                                                                if (gh.member){
                                                                    memberCount += 1;
                                                                }
                                                            }
                                                        });
                                                    });
                                                    saveGrouped.groupHistoryIds = groupHistoryIds;
                                                    saveGrouped.userIds = userIds;
                                                    saveGrouped.memberCount = memberCount;
                                                    dyffy.io.room(group._id.toString()).broadcast('lossGroupAcquire', saveGrouped);
                                                    dyffy.io.room(group._id.toString()).broadcast('invoiceChange', {userId : object2._id.toString(), invoices : object2.invoices});
                                                    dyffy.io.room(group._id.toString()).broadcast('dueChange', {userId : object1._id.toString(), dues : object1.dues});
                                                });
                                            });
                                        });
                                    }
                                });
                            }
                        });
                    });
                }
                else if (due.type == 'message' && due.dueDate){
                    if (new Date(due.dueDate) < new Date()){
                        due.failToPay = true;
                        due.failToPayDate = new Date();
                        User.findOne({'invoices.dueId' : due._id}, function(err,recipient){
                            var invoice = recipient.invoices.id(due.invoiceId);
                            invoice.failToPay = true;
                            invoice.failToPayDate = new Date();
                            userObject.save(function(err, object1){
                                if (!isEmpty(err)) {
                                    req.io.emit('error', {error : "Save error."});
                                }
                                else {
                                    recipient.save(function(err2, object2){
                                        if (!isEmpty(err2)) {
                                            req.io.emit('error', {error : "Save error."});
                                        }
                                        else {
                                            req.io.emit('invoiceChange', {userId : object2._id.toString(), invoices : object2.invoices});
                                            dyffy.io.room(object1._id.toString()).broadcast('dueChange', {userId : object1._id.toString(), dues : object1.dues});
                                        }
                                    });
                                }
                            });
                        });
                    }
                }
            });
        });
    }
    else {
        req.io.emit('error', {error : "Insufficient permissions."});
    }
});
function recursMe(param) {
    var indicator = true;
    param.forEach(function(neg){
        if (neg < 0){
            indicator = false;
        }
    });
    if (indicator) {
        return param;
    }
    else {
        var total = 0;
        var absTotal = 0;
        var negativeTotal = 0;
        var positiveLeft = 0;
        param.forEach(function(item){
            total += item;
            if (item < 0){
                negativeTotal += item;
            }
            else {
                positiveLeft += 1;
            }
            absTotal += Math.abs(item);
        });
        var distPortion = negativeTotal/positiveLeft;
        param = _.map(param, function(num){
            if (num < 0){
                return 0;
            }
            else {
                return (num + distPortion);
            }
        });
        return recursMe(param);
    }
}
function promotionOver(group){
    if (group.beforeSignatures){
        var totalPledged = 0;
        var guessedArray = [];
        var amountArray = [];
        var finalPaid = [];
        var final = group.finalValue;
        var multiplier = group.multiplier;
        var eachAmount = group.promoteAmount;
        group.beforeSignatures.forEach(function(sig){
            if (sig.amount){
                totalPledged += sig.amount;
                guessedArray.push(sig.userInput);
                amountArray.push(amountArray);
            }
        });
        if (totalPledged){
            var absDifferenceArray = _.map(guessedArray, function(num){ return Math.abs(num - final); });
            var sumDifference = 0;
            absDifferenceArray.forEach(function(difference){
                sumDifference += difference;
            });
            var averageDifference = sumDifference / absDifferenceArray.length;
            if (averageDifference == 0){
                group.beforeSignatures.forEach(function(sig){
                    if (sig.amount){
                        sig.awarded = eachAmount;
                        sig.awardedDate = new Date();
                        counter += 1;
                    }
                });
            }
            else {
                var finalPaid = _.map(absDifferenceArray, function(num){ return ((((averageDifference - num)/averageDifference) * multiplier) + 1) * eachAmount; });
                var finalValues = recursMe(finalPaid);
                var initObject = _.object(guessedArray, finalValues);
                var counter = 0;
                group.beforeSignatures.forEach(function(sig){
                    if (sig.amount){
                        sig.awarded = initObject[sig.userInput];
                        sig.awardedDate = new Date();
                        counter += 1;
                    }
                });
            }
            var finalCounter = 0;
            group.save(function(err, saveGroup){
                var rc = new WebSocketClient();
                rc.on('connect', function(connection){
                    connection.on('message', function(message){
                        message = JSON.parse(message.utf8Data);
                        if (message.error){

                        }
                        else if (message.id == "1"){
                            // TODO save fee paid date
                        }
                        else {
                            if (!saveGroup.beforeSignatures.id(message.id).paidOutDate){
                                finalCounter += 1;
                                saveGroup.beforeSignatures.id(message.id).paidOutDate = new Date();
                                saveGroup.save(function(err, returnedGroup){
                                    saveGroup = returnedGroup;
                                    if (finalCounter >= counter){
                                        var promotionOffer = false;
                                        var todayDate = new Date();
                                        if (returnedGroup.offers){
                                            returnedGroup.offers.forEach(function(offer){
                                                if (offer.type == 'promotion' && offer.dateRangeEnd > todayDate){
                                                    promotionOffer = true;
                                                }
                                            });
                                        }
                                        if (returnedGroup.promotion || promotionOffer){
                                            dyffy.io.broadcast('lossGroupAcquireAll', [returnedGroup.toObject()]);
                                        }
                                        else {
                                            dyffy.io.room(returnedGroup._id.toString()).broadcast('lossGroupAcquireAll', [returnedGroup.toObject()]);
                                        }
                                    }
                                });
                            }
                        }
                    });
                    saveGroup.beforeSignatures.forEach(function(sig){
                        var founderOffer = false;
                        var todayDate = new Date();
                        if (saveGroup.offers){
                            saveGroup.offers.forEach(function(offer){
                                if (offer.type == 'founder' && offer.dateRangeEnd && offer.dateRangeEnd > todayDate){
                                    founderOffer = true;
                                }
                                else if (offer.type == 'founder' && !offer.dateRangeEnd){
                                    founderOffer = true;
                                }
                            });
                        }
                        if (sig.awarded && (saveGroup.founderDeal || founderOffer) && !sig.paidOutDate){
                            var toFounder = dyffyConfig.houseRate * sig.awarded;
                            var toUser = sig.awarded - toFounder;
                            User.find({'groupHistory.group': saveGroup._id.toString()},function(err,finalUser){
                                finalUser.forEach(function(used){
                                    used.groupHistory.forEach(function(history){
                                        if (history.group == saveGroup._id.toString() && history.foundingUser){
                                            connection.send(JSON.stringify({id : sig._id.toString(),command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:sig.address,Amount:toUser},secret:dyffyConfig.masterSecret}));
                                            connection.send(JSON.stringify({id : "1",command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:used.rippleInsuranceWallet[0].publicKey,Amount:toFounder},secret:dyffyConfig.masterSecret}));
                                        }
                                    });
                                });
                            });
                        }
                        else if (sig.awarded && !sig.paidOutDate){
                            var toFounder = dyffyConfig.houseRate * sig.awarded;
                            var toUser = sig.awarded - toFounder;
                            connection.send(JSON.stringify({id : sig._id.toString(),command:"submit",tx_json:{TransactionType:"Payment",Account:dyffyConfig.masterAddress,Destination:sig.address,Amount:toUser},secret:dyffyConfig.masterSecret}));
                        }
                    });
                });
                rc.on('error', function(error){

                });
                rc.connect(dyffyConfig.rippled);
            });
        }
    }
}
#!/usr/bin/env node
var WebSocketClient = require('websocket').client,
    peercoverConfig = require('../config.js');

// Test websockets
var rc = new WebSocketClient();
rc.on('connect', function (connection) {
    console.log("Connected to " + peercoverConfig.rippled);
    connection.on('message', function (message) {
        message = JSON.parse(message.utf8Data);
        console.log(JSON.stringify(message, null, 3));
    });
    connection.send(JSON.stringify({"command": "server_state"}));
    // connection.send(JSON.stringify({
    //     "command": "subscribe",
    //     "books" : [{
    //         "snapshot": 1,
    //         "both": 1,
    //         "taker_gets": {
    //             "currency": "USD",
    //             "issuer": "rvYAfWj5gh67oV6fW32ZzP3Aw4Eubs59B"
    //         },
    //         "taker_pays": {"currency":"XRP"}
    //     }]
    // }));
});
rc.connect(peercoverConfig.rippled);
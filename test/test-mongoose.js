#!/usr/bin/env node
var child_process = require('child_process'),
    peercoverConfig = require('../config.js'),
    mongoose = require('mongoose'),
    paginate = require('mongoose-paginate'),
    autoIncrement = require('mongoose-auto-increment');

mongoose.connect(peercoverConfig.mongodb);
console.log("Connect to " + peercoverConfig.mongodb);
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
autoIncrement.initialize(mongoose);
var fundingSources = new Schema({
    id    : ObjectId
    , sourceId : String
    , name : String
    , type : String
    , verified : Boolean
    , processingType : String
    , publicKey : String
});
var inviteCodes = new Schema({
    id    : ObjectId
    , code : String
    , user : String
    , used : Boolean
    , usedDate: Date
    , expirationDate : Date
    , issuedDate: Date
    , dateRangeStart : Date
    , dateRangeEnd : Date
    , type : String
    , duration : Schema.Types.Mixed
    , group : String
    , email : String
    , boughtOfferFor : Number
    , boughtOfferForCurrency : String
    , sellOfferFor : Number
    , sellOfferForCurrency : String
    , offerId : String
    , isEnabled : Boolean
    , sellOfferMadeDate : Date
    , sellOfferClosedDate : Date
    , buyOfferFor : Number
    , buyOfferForCurrency : String
    , buyOfferMadeDate : Date
    , buyOfferClosedDate : Date
    , tradeOfferFor : String
    , tradeOfferMadeDate : Date
    , tradeOfferClosedDate : Date
    , transactHistory : Schema.Types.Mixed
});
var offers = new Schema({
    id    : ObjectId
    , description : String
    , startDate : Date
    , expirationDate : Date
    , endDate : Date
    , createdDate : Date
    , updateDate : Date
    , amountLimitMetDate : Date
    , type : String
    , title : String
    , category : String
    , textColor : String
    , buttonColor : String
    , paid : Number
    , paidCurrency : Number
    , paidDate : Date
    , promoted : String
    , promotedUntil : Date
    , madeBy : String
    , imageUrl : String
    , tagline : String
    , color : String
    , active : Boolean
    , hasDuration : Boolean
    , discontinued : Boolean
    , duration : Schema.Types.Mixed
    , amountLimit : Number
    , offerContact : String
    , price : Number
    , pricePerDay : Number
    , isPricePerDay : Boolean
    , currencyPrice : String
    , offers : [inviteCodes]
    , groupTypesAvailable : Array
});
var ious = new Schema({
    id    : ObjectId
    , gateway : String // displayed on wallet view // can issue manually // automatically issued if wallet contains balance claimant trusts at time of claim approval
    , amount : Number // displayed on wallet view
    , currency : String // displayed on wallet view
});
var scans = new Schema({
    id    : ObjectId
    , user : String
});
var creditRequests = new Schema({
    id    : ObjectId
    , dateRequested : Date
    , dateSent : Date
    , dateApproved : Date
    , dateRepayBy : Date
    , amountOwed : Number
    , fees : Number
    , wasAccepted : Boolean
    , transactionId : String
    , messageToSign : String
    , amountRequested : Number
    , address : String
    , issuer : String
    , submitted : Boolean
});
var creditRepayments = new Schema({
    id    : ObjectId
    , date : Date
    , creditRequestsRepaid : Array
    , amount : Number
    , tranactionId : String
});
var creditCurrencies = new Schema({
    id    : ObjectId
    , totalCredit : Number
    , totalCreditOutstanding : Number
    , totalOverdue : Number
    , totalFees : Number
    , averageOverdueDays : Number
    , currency : String
    , creditRequests : [creditRequests]
    , creditRepayments : [creditRepayments]
});
var creditLines = new Schema({
    id    : ObjectId
    , user : String
    , address : String
    , dateOpened : Date
    , creditScore : Number
    , destinationTag : Number
    , creditCurrencies : [creditCurrencies]
});
var trust = new Schema({
    id    : ObjectId
    , gateway : String // displayed on wallet view // can issue manually // automatically issued if wallet contains balance claimant trusts at time of claim approval
    , amount : Number // displayed on wallet view
    , currency : String // displayed on wallet view
});
var trustedGateways = new Schema({
    id    : ObjectId
    , name : String // CRUD by users and displayed on terms/profile views
    , publicKey : String // CRUD by users and displayed on terms/profile views
    , currencies : Array
    , date : Date
});
var googleWalletTransactions = new Schema({
    id    : ObjectId
    , user : String
    , amount : Number
    , currency : String
    , dateReceived : Date
    , dateClosed : Date
    , verified : Boolean
    , rippleTx : String
    , name : String
    , uri : String
    , succeeded : Boolean
    , description : String
    , currencyCode : String
    , price : String
    , transactionId : String
    , orderId : String
    , to : String
    , ipUser : String
});
var rippleInsuredWallets = new Schema({
    id    : ObjectId
    , user : String // identify owner of wallet
    , publicKey : String // verify how much insured/collateral // display on wallet view
    , xrp : Number // amount of XRP, display on wallet view
    , ious : [ious] // display on wallet view
    , trust : [trust] // display on wallet view
    , updateDate : Date
});
var bitcoinInsuredWallets = new Schema({
    id    : ObjectId
    , user : String
    , publicKey : String // used to manually or automatically send BTC when claim approved // display on wallet view
    , btc : String // display on wallet view
    , updateDate : Date
});
var pendingTransactions = new Schema({
    id    : ObjectId
    , invoiceId : String
    , sequenceNumber : Number
    , txHash : String
    , date : Date
});
var invoices = new Schema({
    id    : ObjectId
    , dueId : String
    , messageId : String
    , claim : String
    , group : String
    , failToPay : Boolean
    , amount : Number
    , currencyDesired: String
    , currencyClaimedIn : String
    , houseAmount : Number
    , currency : String
    , fundingSources : [fundingSources]
    , to : String
    , from : String
    , proportionOfClaim : Number
    , paid : Boolean
    , receivedDate : Date
    , paidDate : Date
    , dueDate : Date
    , failToPayDate : Date
    , acceptedInvoiceDate : Date
    , forgivenDate : Date
    , forgiven : Boolean
    , acceptedInvoice : Boolean
    , invoiceId : String
    , type : String
    , txCurrency : String
    , txAmount : String
    , txAmountMax : String
    , txState : String
    , finalStatus : String
    , txHash : String
    , txBlob : String
    , txCurrencyHouse : String
    , txAmountHouse : String
    , txAmountHouseMax : String
    , txStateHouse : String
    , finalStatusHouse : String
    , txHashHouse : String
    , txBlobHouse : String
});
var rippleInsuranceWallets = new Schema({
    id    : ObjectId
    , user : String
    , publicKey : String  // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , privateKey : String // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , walletName : String // CRUD on wallet view
    , password : String // CRUD on wallet view
    , passphrase : String // display on wallet view
    , privateKeyHex : String
    , fundingSources : [fundingSources]
    , xrp : String // display on wallet view
    , ious : [ious] // display on wallet view // issue on wallet view
    , trust : [trust] // display on wallet view // issue on wallet view
    , creationDate : Date // display on wallet view
    , updateDate : Date
    , primary : Boolean
    , funded : Boolean
    , trustedGateways : [trustedGateways]
    , qrcode : String
});
var bitcoinWallets = new Schema({
    id    : ObjectId
    , user : String
    , publicKey : String  // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , privateKey : String // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , btc : String // display on wallet view
    , creationDate : Date // display on wallet view
    , updateDate : Date
    , primary : Boolean
    , funded : Boolean
    , qrcode : String
});
var litecoinWallets = new Schema({
    id    : ObjectId
    , user : String
    , publicKey : String  // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , privateKey : String // used to manually or automatically send IOU or XRP when claim approved // display on wallet view
    , ltc : String // display on wallet view
    , creationDate : Date // display on wallet view
    , updateDate : Date
    , primary : Boolean
    , funded : Boolean
    , qrcode : String
});
var fileUploads = new Schema({
    id    : String
    , user : String
    , date : Date
    , type : String
    , name : String
    , url : String
    , original : String
    , folder : String
    , permissions : String
    , type : String
    , success : Boolean  // set by S3 response
});
var cashOuts = new Schema({
    id    : String
    , user                         : String
    , creationDate                 : Date
    , updateDate                   : Date
    , rippleReceivedDate           : Date
    , balancedDate                 : Date
    , approvedDate                 : Date
    , reversedDate                 : Date
    , nameTo                       : String
    , status                       : String
    , eventURI                     : String
    , transactionURI               : String
    , reversalsURI                 : String
    , amount                       : String
    , transactionId                : String
    , accountId                    : String
    , rippleReceived               : Boolean
    , transferInitiated            : Boolean
    , accountURI                   : String
    , valueIn                      : String
    , appearsAs                    : String
});
var videos = new Schema({
    id    : ObjectId
    , upload : String
    , user : String
    , title : String // CRUD on claim upload view
    , url : String // claim upload view
    , description : String // CRUD on claim upload view
    , creationDate : Date
    , updateDate : Date
    , type : String
    , date : Date
});
var images = new Schema({
    id    : ObjectId
    , upload : String
    , user : String
    , title : String // CRUD on claim upload view
    , url : String // claim upload view
    , description : String // CRUD on claim upload view
    , creationDate : Date
    , updateDate : Date
    , type : String
    , date : Date
});
var documents = new Schema({
    id    : ObjectId
    , upload : String
    , user : String
    , title : String // CRUD on claim upload view
    , url : String // claim upload view
    , description : String // CRUD on claim upload view
    , creationDate : Date
    , updateDate : Date
    , type : String
    , date : Date
});
// reference or embed?
var transactions = new Schema({
    id    : ObjectId
    , userTo : String // displayed on transaction view
    , userFrom : String // display on transaction view
    , embeddedTransaction : Schema.Types.Mixed
    , transaction : String // display on transaction view
    , description : String // CRUD and display on transaction view
    , date : Date // automatically update
    , verified : Boolean // used in conjunction with websocket to display verified transaction in green
    , currency : String
    , amount : String
});
var votes = new Schema({
    id    : ObjectId
    , vote : Boolean
    , shares : Number
    , user : String
    , date : Date
});
var comments = new Schema({
    id    : ObjectId
    , user : String
    , comment : String
    , attachmentUrl : String
    , creationDate : Date
    , updateDate : Date
});

var dividendFees = new Schema({
    id    : ObjectId
    , currency : String
    , amount : Number
    , sharesOutstanding : Number
    , date : Date
    , invoice : String
    , due : String
    , shares : Number
    , amountPerShare : Number
});

var claims = new Schema({
    id    : ObjectId
    , user : String
    , images : [images] // display as attachment on claim view
    , documents : [documents] // display as attachment on claim view
    , videos : [videos] // display as attachment on claim view
    , files : [fileUploads]
    , title : String // CRUD on claim view
    , description : String // CRUD on claim view
    , amountClaimed : Number // CRUD on claim view privileges change depending on state of claim
    , currencyClaimedIn : String // CRUD on claim view privileges change depending on state of claim
    , currencyDesired : String // CRUD on claim view privileges change depending on state of claim
    , voteApprove : Boolean // votes displayed on claims
    , votes : [votes]
    , comments : [comments]
    , forceEndDate : Date
    , vestDate : Date
    , dateVoteEnd : Date
    , creationDate : Date
    , updateDate : Date
    , groupMembersNumberOnFile : Number
    , group: String
});
var groupHistory = new Schema({
    id    : ObjectId
    , user : String
    , group : String
    , dateJoined : Date
    , dateLeft : Date
    , honorableDischarge : Boolean
    , commentsOnMembership : [comments]
    , reviewsOfMember : [comments]
    , voteJoin : [votes]
    , voteJoinDate : Date
    , voteJoinDateEnd : Date
    , voteRescindMembership : [votes]
    , voteRescindMembershipDate : Date
    , voteRescindMembershipDateEnd : Date
    , dateValueEntered : Date
    , roundVoteRescindMembership : Number
    , roundVoteJoin : Number
    , member : Boolean
    , foundingUser : Boolean
    , admin : Boolean
    , voteApprove : Boolean
    , portion : Number
    , forceEndDate : Date
    , voteThresholdMemberApproval : Number
    , deleted : Boolean
    , dateAcceptedReferral : Date
    , valueInsured : Number
});
var invites = new Schema({
    id : ObjectId
    , sentDate : Date
    , acceptedDate : Date
    , declinedDate : Date
    , acceptedDeclined : Boolean
    , sentBy : String
    , receivedBy : String
    , group : String
    , retract : Boolean
    , type : String
});
var signatures = new Schema({
    id    : ObjectId
    , sig : String // signature displayed in profile view
    , user : String // user identification
    , date : Date // current date displayed when signature selected on terms and claims view
    , valueInsured : Number
    , currency : String
    , portion : Number
    , description : String
});
var transacts = new Schema({
    id    : ObjectId
    , transactionId : String
    , amount : Number
    , rippleTransactionId : String
    , fromAddress : String
    , paidOutDate : Date
    , refundDate : Date
    , initialDate : Date
    , shares : Number
    , amountPerShare : Number
});
var outsideCashins = new Schema({
    id    : ObjectId
    , sig : String // signature displayed in profile view
    , user : String // user identification
    , date : Date // current date displayed when signature selected on terms and claims view
    , valueInsured : Number
    , destinationTag : Number
    , currency : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , portion : Number
    , awarded : Number
    , fee : Number
    , address : String
    , cashinAddress : String
    , cashinFromAddress : String
    , cashinSecret : String
    , cashinTransactionId : String
    , rippleTransactionId : String
    , sentFromAddress : String
    , rippleAddress : String
    , type : String
    , transactions : [transacts]
    , ip : String
    , qr : String
    , email : String
    , awardedDate : Date
    , paidDate : Date
    , paidOutDate : Date
    , feePaidOutDate : Date
    , convertedDate : Date
    , refundedDate : Date
    , amount : Number
    , paidTo : String
    , description : String
    , userInput : Number
    , userStringInput : String
});
var outsideCashouts = new Schema({
    id    : ObjectId
    , sig : String // signature displayed in profile view
    , user : String // user identification
    , date : Date // current date displayed when signature selected on terms and claims view
    , valueInsured : Number
    , destinationTag : Number
    , currency : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , portion : Number
    , awarded : Number
    , fee : Number
    , address : String
    , cashoutFromAddress : String
    , cashoutFrom : String
    , cashoutAddress : String
    , cashoutTransactionId : String
    , rippleTransactionId : String
    , sentFromAddress : String
    , rippleAddress : String
    , type : String
    , transactions : [transacts]
    , ip : String
    , qr : String
    , email : String
    , awardedDate : Date
    , paidDate : Date
    , paidOutDate : Date
    , feePaidOutDate : Date
    , convertedDate : Date
    , refundedDate : Date
    , amount : Number
    , paidTo : String
    , description : String
    , userInput : Number
    , userStringInput : String
});
var beforeSignatures = new Schema({
    id    : ObjectId
    , sig : String // signature displayed in profile view
    , user : String // user identification
    , date : Date // current date displayed when signature selected on terms and claims view
    , valueInsured : Number
    , destinationTag : Number
    , currency : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , portion : Number
    , awarded : Number
    , fee : Number
    , address : String
    , ip : String
    , awardedDate : Date
    , paidDate : Date
    , paidOutDate : Date
    , feePaidOutDate : Date
    , convertedDate : Date
    , refundedDate : Date
    , amount : Number
    , paidTo : String
    , description : String
    , userInput : Number
    , userStringInput : String
});
var messages = new Schema({
    id    : ObjectId
    , userFrom : String
    , userTo : String
    , ip : String
    , countryCode : String
    , countryName : String
    , geoLocation : String
    , received : Boolean
    , receivedDate : Date
    , thread : String
    , dateSent : Date
    , dateUpdated : Date
    , dateDeleted : Date
    , deleted : Boolean
    , message : String
    , invoices : [invoices]
    , transactions : [transactions]
    , files : [fileUploads]
});
var chatThreads = new Schema({
    id    : ObjectId
    , users : [String]
    , userInitiated : String
    , group : String
    , dateStarted : Date
    , dateUpdated : Date
    , dateDeleted : Date
    , dateLast : Date
    , deleted : Boolean
    , isGroup : Boolean
    , isStock : Boolean
    , isCurrencyTrade : Boolean
    , messages : [messages]
});
var bankAccounts = new Schema({
    id    : ObjectId
    , dateAdded : Date
    , dateVerified : Date
    , accountURI : String
    , verified : Boolean
    , primary : Boolean
    , accountNumber : String
    , name : String
    , accountType : String
    , bankName : String
});
var connections = new Schema({
    id    : ObjectId
    , sentBy : String
    , receivedBy : String
    , dateSent : Date
    , dateAccepted : Date
    , dateRevoked : Date
    , dateDenied : Date
    , messageAttached : String
    , isAccepted : Boolean
});
var users = new Schema({
    id    : ObjectId
    , email     : String // used for login confirmation
    , username  : String // optional CRUD on profile view // if added display on all headings
    , password  : String // U on profile view
    , joinDate  : Date // display on profile view
    , activeDate : Date // display on profile view
    , firstName : String // CRUD on profile view // if no user name display on all headings
    , lastName  : String // CRUD on profile view // if no user name display on all headings
    , dob       : Date // CRUD on profile
    , superadminDate : Date
    , uploadImage : String // CRUD profile image or select url from facebook, linkedin (make sure SSL intact)
    , facebookImage : String
    , customer : String
    , dailyInstantLimit : Number
    , dailyInstantDate : Date
    , founderDeal: Boolean
    , bitcoinAddress : String
    , litecoinAddress : String
    , auroracoinAddress : String
    , nxtAddress : String
    , etherAddress : String
    , dogecoinAddress : String
    , namecoinAddress : String
    , peercoinAddress : String
    , twoFactorUser : String
    , hasTwoFactor : Boolean
    , walletRecoverCode : String
    , twoFactorDate : Date
    , founderDealDate: Date
    , founderDealDateEnd: Date
    , linkedinImage : String
    , customerServiceAvailable : Boolean
    , haveWallet    : Boolean
    , superadmin : Boolean
    , jumioVerified : Boolean
    , banned : Boolean
    , bankAccounts : [bankAccounts]
    , offers: [inviteCodes]
    , cashOuts: [cashOuts]
    , deposits: [cashOuts]
    , creditReportUrl : String // for V2.0
    , identityProofUrl : String // for V2.0
    , verified: Boolean // must be verified to access any views other than home and about
    , verifyCode : String // used on backend
    , recoverCode : String
    , rippleInsuredWallet : [rippleInsuredWallets] // CRUD on group view after accepted to group
    , bitcoinInsuredWallet : [bitcoinInsuredWallets]
    , bitcoinWallet : [bitcoinWallets] // CRUD on group view after accepted to group V2.0
    , rippleInsuranceWallet : [rippleInsuranceWallets] // CRUD on profile view // required
    , facebookData : Schema.Types.Mixed // display on profile view
    , dwollaData : Schema.Types.Mixed   // display on profile view
    , linkedinData : Schema.Types.Mixed // display on profile view
    , jumioData : Schema.Types.Mixed
    , permissions : String // three levels of permissions for profile display and edit on profile view
    , verificationBadge : Boolean // is identity verified (dwolla bank account valid in name) - display verification badge on profile view
    , facebookToken : String
    , linkedinToken : String
    , customer : String
    , creditCard : String
    , dwollaToken : String
    , facebookRefresh : String
    , linkedinSecret : String
    , dwollaRefresh : String
    , profileImageSource : String
    , currentProfileImageUrl : String
    , updateDate : Date
    , connectionsSent : [connections]
    , connectionsReceived : [connections]
    , connections : Array
    , formerEmail : String
    , chatThreads : [chatThreads]
    , groupHistory : [groupHistory]
    , invites : [invites]
    , invitesSent : [invites]
    , invoices : [invoices]
    , dues : [invoices]
    , signature : [signatures]
});
var usersReferred = new Schema({
    id    : ObjectId
    , user  : String // displayed in new member inbox view
    , referredBy : String // displayed in new member inbox view
    , date : Date // displayed in new member inbox view
    , acceptedDeclined : Boolean
    , dateDecision : Date
});
var termOptions = new Schema({
    id    : ObjectId
    , minInsured : String // CRUD in terms view
    , maxInsured : String // CRUD in terms view
    , trustedGateways : [trustedGateways] // universally group trusted gateways
    , currenciesAllowed : Array // CRUD in terms view
});
var proposedTermsChanges = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , vestDate : Date // group CRUD displayed in terms/group view
    , overDate : Date // group CRUD displayed in terms/group view
    , voteEnd : Date
    , contract : String // group CRUD displayed in terms/group view
    , changesList : String // group CRUD displayed in terms/group view
    , voteApprove : Boolean
    , durationVote : Date
    , comments : [comments]
    , votes : [votes]
    , stashIndex : String
    , commitIndex : String
    , votingShares : Number
    , totalShares : Number
});
var proposedStockIssuances = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , vestDate : Date // group CRUD displayed in terms/group view
    , overDate : Date // group CRUD displayed in terms/group view
    , voteEnd : Date
    , contract : String // group CRUD displayed in terms/group view
    , description : String
    , title : String
    , changesList : String // group CRUD displayed in terms/group view
    , voteApprove : Boolean
    , durationVote : Date
    , ipoDate : Date
    , issued : Boolean
    , dateIssued : Date
    , currency : String
    , issuer : String
    , amount : Number
    , sharesAmount : Number
    , votes : [votes]
    , stashIndex : String
    , commitIndex : String
    , sharesVoting : Boolean
    , sharesTransferable : Boolean
    , sharesPublic : Boolean
});
var templates = new Schema({
    id    : ObjectId
    , name : String
    , type : String
    , markdown : String
    , user : String
    , date : Date
});
var terms = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , updateDate : Date // displayed in terms/group view
    , vestDate : Date // group CRUD displayed in terms/group view
    , overDate : Date // group CRUD displayed in terms/group view
    , contract : String // group CRUD displayed in terms/group view
    , contractUrl : String // group CRUD displayed in terms/group view
    , claimWeight : String // group CRUD displayed in terms/group view
    , typeOfInsurance : String // group CRUD displayed in terms/group view
    , durationGroup : String // group CRUD displayed in terms/group view
    , termOptions : [termOptions] // group CRUD displayed in terms/group view
    , signature : [signatures] // CRUD displayed in terms/group view // signature pad integration // electronic signature
    , durationVote : Date
    , comments : [comments]
    , selectedTerms : Boolean
});
var customerServiceReps = new Schema({
    id    : ObjectId
    , customerServiceUser : String // displayed in terms/group view
    , sellerUser : String
    , merchantCompany : String
    , ip : String
    , ticketRefArray : Array
    , approvedCustomerService : Boolean
    , approvedMerchant : Boolean
    , approvedMerchantDate : Date
    , approvedCustomerServiceDate : Date
});
var customerServiceTickets = new Schema({
    id    : ObjectId
    , customerServiceUser : String
    , customerServiceRep : String
    , chatThreads : [chatThreads]
    , smsEnabled : Boolean
});
var dividends = new Schema({
    id    : ObjectId
    , amount : String
    , currency : String
    , fromAddress : String
    , user : String
    , transaction : String
    , destinationTag : Number
    , toAddress : String
    , transactions : [transacts]
    , date : Date
    , toAddresses : Array
    , toUsers : Array
});
var distributors = new Schema({
    id    : ObjectId
    , distributorUser : String // displayed in terms/group view
    , sellerUser : String
    , merchantCompany : String
    , domain : String
    , name : String
    , description : String
    , ip : String
    , ticketRefArray : Array
    , approvedDistributor : Boolean
    , approvedMerchant : Boolean
    , approvedMerchantDate : Date
    , approvedDistributorDate : Date
    , distributionEnabled : Boolean
    , distributionFee : Number
});
var tickets = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , updateDate : Date // displayed in terms/group view
    , purchasedDate : Date
    , quantity : Number
    , price : Number
    , ip : String
    , purchased : Boolean
    , delivered : Boolean
    , description : String
    , shippingDate : Date
    , shippingEstimate : Date
    , shippingCost : Number
    , isPhysical : Boolean
    , downloadURL : String
    , currency : String
    , customerServiceTickets : [customerServiceTickets]
    , distributor : [distributors]
    , title : String
    , address : String
    , priceOptions : Array
    , transactionNumber : Number
});
var products = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , updateDate : Date // displayed in terms/group view
    , quantity : Number
    , price : Number
    , priceOptions : Array
    , description : String
    , title : String
    , tickets : [tickets]
    , qrCode : String
    , files : [fileUploads]
    , customerServiceEnabled : Boolean
    , customerServiceReps : [customerServiceReps]
    , commission : Number
});
var reports = new Schema({
    id    : ObjectId
    , user  : String // displayed in terms/group view
    , creationDate : Date // displayed in terms/group view
    , releaseDate : Date
    , reportSentDate : Date
    , description : String
    , title : String
    , revenues : Number
    , profit : Number
    , files : [fileUploads]
    , expenses : Number
    , dividends : Number
    , dividendsPerShare : Number
    , sharesOustanding : Number
    , totalSharesPossible : Number
    , marketCap : Number
    , isFinancial : Boolean
    , periodStartDate : Date
    , periodEndDate : Date
    , currency : String
});
var taxFees = new Schema({
    id    : ObjectId
    , currency : String
    , amount : Number
    , sharesOutstanding : Number
    , date : Date
    , invoice : String
    , due : String
    , shares : Number
    , amountPerShare : Number
    , toAddress : String
    , fromAddress : String
});
var taxes = new Schema({
    id    : ObjectId
    , amount : String
    , currency : String
    , fromAddress : String
    , user : String
    , transaction : String
    , destinationTag : Number
    , toAddress : String
    , transactions : [transacts]
    , date : Date
    , toAddresses : Array
    , toUsers : Array
    , dateReleased:  Date
    , type : String
});
var lossGroups = new Schema({
    id    : ObjectId
    , creationDate : Date // display on group view
    , updateDate : Date // display on group view
    , vestDate : Date // display on group view
    , overDate : Date // display on group view
    , dissolveDate : Date
    , minVestDate : Date
    , endDate : Date
    , willVestDate : Date
    , qrCode : String
    , reports : [reports]
    , durationOfGroup : Number
    , minimumUsers : Number
    , commissionDefault : Number
    , customerServiceEnabled : Boolean
    , rangeStart : Number
    , rangeEnd : Number
    , distributionEnabled : Boolean
    , distributionFeeDefault : Number
    , salesTaxRate : Number
    , dividendTaxRate : Number
    , taxAddress : String
    , taxes : [taxes]
    , customerServiceReps : [customerServiceReps]
    , distributors : [distributors]
    , dividends : [dividends]
    , multiplier : Number
    , chatThreads : [chatThreads]
    , files : [fileUploads]
    , dateRangeStart : Date
    , dateRangeEnd : Date
    , eventEndDate : Date
    , signedLabels : Array
    , finalValue : Number
    , promoteAmount : Number
    , maximumShares : Number
    , promoteCurrency : String
    , outsideUrl : String
    , dividendFees : [dividendFees]
    , taxFees : [taxFees]
    , finalValueString : String
    , duration : Schema.Types.Mixed
    , promotion : Boolean
    , promotionEnd : Date
    , type : String
    , name : String
    , ticker : String
    , tickerBoughtDate : Date
    , tickerBoughtFor : Number
    , tickerCurrencyBoughtFor : String
    , brandImage : String
    , uploadImage : String
    , founderDeal : Boolean
    , description : String
    , rewardLabel : String
    , products : [products]
    , tickets : [tickets]
    , offers: [inviteCodes]
    , terms : [terms] // display on group view
    , beforeSignatures : [beforeSignatures]
    , proposedTermsChanges : [proposedTermsChanges]
    , proposedStockIssuances : [proposedStockIssuances]
    , queueTermsProposedChanges : [terms]
    , rippleInsuredWallet : [rippleInsuredWallets] // display on group view
    , bitcoinInsuredWallet : [bitcoinInsuredWallets] // display on group view
    , rippleInsuranceWallet : [rippleInsuranceWallets]
    , userReferred : [usersReferred] // display on group view, display in group members' new member inbox view
    , voteThresholdNewMembers : Number // display on group view // CRUD on terms view
    , voteThresholdTermVest : Number // display on group view
    , voteThresholdShareIssuance : Number
    , voteThresholdGroupVest : Number // display on group view
    , voteThresholdClaimApproval : Number // display on group view
    , voteThresholdAdminApproval : Number // display on group view
    , gracePeriod : Number
    , adminNominations : [usersReferred] // invited users by admins (separate admin vote count)
    , isPublic : Boolean
    , vested : Boolean
    , didVest : Boolean
    , didNotVest : Boolean
    , claims : [claims]
    , vestVote : [votes]
    , isAdmin : Boolean
    , claimVote : Boolean
    , portionInput : Number
    , deleted: Boolean
});

//================================================ Models for MongoDB ================================================//

beforeSignatures.plugin(autoIncrement.plugin, { model: 'BeforeSignature', field: 'destinationTag' });
outsideCashouts.plugin(autoIncrement.plugin, { model: 'OutsideCashout', field: 'destinationTag' });
dividends.plugin(autoIncrement.plugin, { model: 'Dividend', field: 'destinationTag' });
taxes.plugin(autoIncrement.plugin, { model: 'Tax', field: 'destinationTag' });
creditLines.plugin(autoIncrement.plugin, { model: 'CreditLine', field: 'destinationTag' });

var FundingSource = mongoose.model('FundingSource', fundingSources)
    , Iou = mongoose.model('Iou', ious)
    , Trust = mongoose.model('Trust', trust)
    , BankAccount = mongoose.model('BankAccount', bankAccounts)
    , TrustedGateway = mongoose.model('TrustedGateway', trustedGateways)
    , BitcoinWallet  = mongoose.model('BitcoinWallet', bitcoinWallets)
    , LitecoinWallet  = mongoose.model('LitecoinWallet', litecoinWallets)
    , RippleInsuredWallet = mongoose.model('RippleInsuredWallet', rippleInsuredWallets)
    , BitcoinInsuredWallet = mongoose.model('BitcoinInsuredWallet', bitcoinInsuredWallets)
    , PendingTransaction = mongoose.model('PendingTransaction', pendingTransactions)
    , Invoice = mongoose.model('Invoice', invoices)
    , GoogleWalletTransaction = mongoose.model('GoogleWalletTransaction', googleWalletTransactions)
    , RippleInsuranceWallet = mongoose.model('RippleInsuranceWallet', rippleInsuranceWallets)
    , FileUpload = mongoose.model('FileUpload', fileUploads)
    , Transact = mongoose.model('Transact', transacts)
    , Video = mongoose.model('Video', videos)
    , CreditRequest = mongoose.model('CreditRequest', creditRequests)
    , CreditRepayment = mongoose.model('CreditRepayment', creditRepayments)
    , CreditCurrency = mongoose.model('CreditCurrency', creditCurrencies)
    , CreditLine = mongoose.model('CreditLine', creditLines)
    , Imaged = mongoose.model('Imaged', images)
    , OutsideCashin = mongoose.model('OutsideCashin', outsideCashins)
    , OutsideCashout = mongoose.model('OutsideCashout', outsideCashouts)
    , Scan = mongoose.model('Scan', scans)
    , InviteCode = mongoose.model('InviteCode', inviteCodes)
    , Offer = mongoose.model('Offer', offers)
    , Documented = mongoose.model('Documented', documents)
    , Transaction = mongoose.model('Transaction', transactions)
    , Vote = mongoose.model('Vote', votes)
    , Comment = mongoose.model('Comment', comments)
    , Claim = mongoose.model('Claim', claims)
    , CashOut = mongoose.model('CashOut', cashOuts)
    , GroupHistory = mongoose.model('GroupHistory', groupHistory)
    , Invite = mongoose.model('Invite', invites)
    , Signature = mongoose.model('Signature', signatures)
    , BeforeSignature = mongoose.model('BeforeSignature', beforeSignatures)
    , Message = mongoose.model('Message', messages)
    , ChatThread = mongoose.model('ChatThread', chatThreads)
    , Connection = mongoose.model('Connection', connections)
    , User = mongoose.model('User', users)
    , UserReferred = mongoose.model('UserReferred', usersReferred)
    , TermOption = mongoose.model('TermOption', termOptions)
    , Dividend = mongoose.model('Dividend', dividends)
    , ProposedStockIssuance = mongoose.model('ProposedStockIssuance', proposedStockIssuances)
    , ProposedTermsChange = mongoose.model('ProposedTermsChange', proposedTermsChanges)
    , Template = mongoose.model('Template', templates)
    , CustomerServiceRep = mongoose.model('CustomerServiceRep', customerServiceReps)
    , CustomerServiceTicket = mongoose.model('CustomerServiceTicket', customerServiceTickets)
    , Term = mongoose.model('Term', terms)
    , Ticket = mongoose.model('Ticket', tickets)
    , Product = mongoose.model('Product', products)
    , Report = mongoose.model('Report', reports)
    , Tax = mongoose.model('Tax', taxes)
    , TaxFee = mongoose.model('TaxFee', taxFees)
    , DividendFee = mongoose.model('DividendFee', dividendFees)
    , LossGroup = mongoose.model('LossGroup', lossGroups);

// var output = JSON.stringify(Transaction, null, 3);
// var output = Transaction;
// console.dir(output);

mongoose.connection.on('open', function () {
    mongoose.connection.db.collectionNames(function (error, collections) {
        if (error) {
            throw new Error(error);
        }
        else {
            collections.map(function (collection) {
                console.log('found collection %s', collection.name);
            });
        }
    });
    Offer.findOne(function (error, data) {
        console.log(data.title);
    });
    Offer.find().all(function (offer) {
        console.log(offer);
    });
});
 
mongoose.connection.on('error', function (error) {
    throw new Error(error);
});
#!/usr/bin/env node
var child_process = require('child_process'),
    peercoverConfig = require('../config.js');

// Test JSON RPC
var address, serverStateRPC;
address = {
    'masterAddress': peercoverConfig.masterAddress,
    'iouAddress': peercoverConfig.iouAddress,
    'stockAddress': peercoverConfig.stockAddress,
    'marketplaceAddress': peercoverConfig.marketplaceAddress
};
serverStateRPC = [peercoverConfig.rippledRPC, 'server_state'].join(' ');
child_process.exec(serverStateRPC, function (err, stdout) {
    if (err) throw "server_state RPC failed: " + err;
    var serverstate, reserveBase, reserveInc;
    function getAccountInfo(label, rippleAddress, callback) {
        var accountInfoRPC = [
            peercoverConfig.rippledRPC, 'account_info', rippleAddress
        ].join(' ');
        child_process.exec(accountInfoRPC, function (err, stdout) {
            if (err) throw "account_info RPC failed: " + err;
            var accountinfo, balance, ledgerEntries, reserve, data;
            accountinfo = JSON.parse(stdout);
            ledgerEntries = accountinfo.result.account_data.OwnerCount;
            data = {
                xrpReserve: (reserveBase + ledgerEntries*reserveInc) / 1000000,
                xrpBalance: accountinfo.result.account_data.Balance / 1000000
            };
            callback(label, data);
        });
    }
    serverstate = JSON.parse(stdout);
    reserveBase = serverstate.result.state.validated_ledger.reserve_base;
    reserveInc = serverstate.result.state.validated_ledger.reserve_inc;
    for (var label in address) {
        if (address.hasOwnProperty(label)) {
            getAccountInfo(label, address[label], emitter);
        }
    }
});
function emitter(label, data) {
    var emitJSON = {};
    emitJSON[label] = data;
    console.log(JSON.stringify(emitJSON, null, 3));
}
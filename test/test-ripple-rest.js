#!/usr/bin/env node

var http = require('http')
    , peercoverConfig = require('../config')
    , backbone = require('backbone')
    , chokidar = require('chokidar')
    , countryData = require('country-data')
    , crypto = require('crypto-js')
    , cryptoSha = require('crypto')
    , request = require('request')
    , express = require('express')
    , geoip = require('geoip-native')
    , hbs = require('hbs')
    , fx = require('money')
    , ripple = require('ripple-lib')
    , sessions = require('sessions')
    , stylus = require('stylus')
    , twit = require('twit')
    , _ = require('underscore')
    , uuid = require('node-uuid')
    , btoa = require('btoa')
    , cors = require('cors')
    , validation = require('backbone-validation')
    , passport = require('passport')
    , LinkedInStrategy = require('passport-linkedin').Strategy
    , FacebookStrategy = require('passport-facebook').Strategy
    , DwollaStrategy = require('passport-dwolla').Strategy
    , LocalStrategy = require('passport-local').Strategy
    , linkedinClient = require('linkedin-js')(peercoverConfig.inKey, peercoverConfig.inSecret, peercoverConfig.domain)
    , fbapi = require('facebook-api')
    , fb = require('fb')
    , dwolla = require('dwolla')
    , xmlhttp = require('xmlhttprequest')
    , jsdom = require('jsdom')
    , $ = require('jQuery')
    , nodemailer = require('nodemailer')
    , mongoose = require('mongoose')
    , bruteAForce = new (require('connect-bruteforce'))({banFactor: 2000, banMax: 30000})
    , flash = require('connect-flash')
    , https = require('https')
    , fs = require('fs')
    , url = require('url')
    , WebSocketClient = require('websocket').client
    , passportSocketIo = require('passport.socketio')
    , knox = require('knox')
    , mime = require('mime-magic')
    , cronJob = require('cron').CronJob
    , ioClient = require('socket.io-client')
    , path = require('path')
    , redis = require('redis')
    , bitcoin = require('bitcoin')
    , litecoin = require('node-litecoin')
    , solr = require('solr')
    , async = require('async')
    , jStat = require('jStat')
    , marked = require("marked")
    , paginate = require('mongoose-paginate')
    , child_process = require("child_process")
    , RedisStore = require('connect-redis')(express)
    , cluster = require('cluster')
    , numCPUs = require('os').cpus().length
    , moment = require('moment')
    , gravatar = require('gravatar')
    , balanced = require('balanced-official')
    , autoIncrement = require('mongoose-auto-increment')
    , jwt = require('jwt-simple')
    , bcrypt = require('bcrypt')
    , authy = require('authy')(peercoverConfig.authy)
    , pg = require('pg')
    , os = require('os');

/*
ripple-rest full request:
{
  "source_account": "r1...",
  "source_tag": "123",
  "source_amount": {
    "value": "1",
    "currency": "USD",
    "issuer": "r3..."
  },
  "source_slippage": "0.5",
  "destination_account": "r2...",
  "destination_tag": "456",
  "destination_amount": {
    "value": "10",
    "currency": "XRP",
    "issuer": ""
  },
  "invoice_id": "",
  "paths": "[]",
  "partial_payment": false,
  "no_direct_ripple": false
}

minimal request:
{
    "secret": "s...",
    "client_resource_id": uuid.v4(),
    "payment": {
        "source_account": "r...",
        "destination_account": "r...",
        "destination_amount": {
            "value": "1",
            "currency": "XRP",
            "issuer": ""
        }
    }
}
*/

var longjohn = require('longjohn');

function rippleRestHeartbeat() {
    var client, sql, query1, query2;
    var pending = [];
    try {
        client = new pg.Client(peercoverConfig.pgParams);
        client.on('drain', function () {
            client.end.bind(client);
            client.end();
        });
        client.connect();
        query1 = client.query("SELECT uuid, status_url, hash, txtype, txsubtype FROM pending_transactions");
        query2 = client.query("DELETE FROM pending_transactions WHERE validated = 't'");
        query1.on('row', function (row) {
            // Check each transaction for validation
            pending.push(row);
            if (row && row.txtype) {
                // Payment transactions: check using uuid
                if (row.txtype === 'Payment' && row.uuid && row.status_url) {
                    $.getJSON(row.status_url, {}, function (response) {
                        var submitted, validated;
                        if (response.success && response.payment && response.payment.direction === 'outgoing') {
                            if (response.payment.state === 'validated') {
                                console.log(row.hash, "validated");
                                var client2 = new pg.Client(peercoverConfig.pgParams);
                                client2.connect(function (err) {
                                    var sql = "UPDATE pending_transactions SET validated = 't' WHERE uuid = $1";
                                    client2.query(sql, [row.uuid], function (err) {
                                        client2.end();
                                    });
                                });
                            } else {
                                // Resubmit the payment if it originated from us
                                if (response.payment.source_account === peercoverConfig.iouAddress) {
                                    rippleRestPayment({
                                        "secret": peercoverConfig.iouSecret,
                                        "client_resource_id": row.uuid,
                                        "payment": {
                                            "source_account": peercoverConfig.testacct,
                                            "destination_account": peercoverConfig.testtarget,
                                            "destination_amount": response.payment.destination_amount
                                        }
                                    }, row.txsubtype || "Payment");
                                }
                            }
                        }
                    });
                } else {
                    // Other transactions: check using hash
                    logTx(row.txhash);
                }
            }
        });
    } catch (e) {
        console.log("Error Message: " + e.message);
        console.log("Error Code: ");
        console.log(e.number & 0xFFFF);
        console.log("Error Name: " + e.name);
    } finally {
        client.end.bind(client);
    }
}

// Route payments through here
// txSubtype: cashin, cashout, dividend, reward, etc.
function rippleRestPayment(postData, txSubtype) {
    var postUrl = peercoverConfig.rippledRest + "/v1/payments";
    request.post({
        headers: {
            "content-type" : "application/json",
            "Accept": "*/*"
        },
        uri: postUrl,
        body: JSON.stringify(postData)
    }, function (error, response, body) {
        var parsed;
        if (error) console.log(error);
        parsed = JSON.parse(body);
        if (parsed && parsed.success) {
            var status_url;
            if (/127\.0\.0\.1\:5900/.test(parsed.status_url)) {
                status_url = parsed.status_url;
            } else {
                status_url = parsed.status_url.replace("127.0.0.1", "127.0.0.1:5900");
            }
            $.getJSON(status_url, {}, function (response) {
                var submitted, validated, client;
                if (response.success && response.payment && response.payment.direction === 'outgoing') {
                    submitted = (response.payment.result === 'tesSUCCESS') ? true : false;
                    validated = (response.payment.state === 'validated') ? true : false;
                    client = new pg.Client(peercoverConfig.pgParams);
                    client.connect(function (err) {
                        var sql, params;
                        if (err) console.log(err);
                        sql = "INSERT INTO pending_transactions "+
                              "(uuid, status_url, txhash, txtype, txsubtype, validated, "+
                              "success_submit, init_date, last_submit_date, "+
                              "amount, currency, issuer) "+
                              "VALUES ($1, $2, $3, 'Payment', $4, $5, $6, "+
                              "CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, "+
                              "$7, $8, $9)";
                        params = [
                            parsed.client_resource_id,
                            parsed.status_url,
                            response.payment.hash,
                            txSubtype,
                            validated,
                            submitted,
                            response.payment.destination_amount.value,
                            response.payment.destination_amount.currency,
                            response.payment.destination_amount.issuer
                        ];
                        client.query(sql, params, function (err) {
                            if (err) console.log(err);
                            client.end();
                        });
                    });
                }
            });
        }
    });
}

// Log unvalidated transactions in the pending_transactions table.
function logTx(txhash) {
    // Check if the transaction is validated; only add entry
    // to the postgres db if not.
    var endpoint = peercoverConfig.rippledRest + "/v1/transactions/" + txhash;
    $.getJSON(endpoint, {}, function (response) {
        var client, success_submit;
        try {
            if (response && response.tx && response.tx.meta && response.tx.meta.TransactionResult) {
                if (response.tx.validated === false) {
                    client = new pg.Client(peercoverConfig.pgParams);
                    success_submit = (response.tx.meta.TransactionResult === 'tesSUCCESS') ? true : false;
                    client.connect(function (err) {
                        if (err) console.log(err);
                        var amount, currency, issuer;
                        var sql = "INSERT INTO pending_transactions "+
                                  "(uuid, status_url, txhash, txtype, validated, success_submit,"+
                                  "init_date, last_submit_date, source_account, destination_account,"+
                                  "amount, currency, issuer) "+
                                  "VALUES (0, 0, $1, $2, $3, $4, "+
                                  "CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $5, $6, $7)";
                        if (response.tx.Amount) {
                            if (typeof response.tx.Amount === 'object') {
                                amount = response.tx.Amount.value;
                                currency = response.tx.Amount.currency;
                                issuer = response.tx.Amount.issuer;
                            } else {
                                amount = response.tx.Amount / 1000000;
                                currency = 'XRP';
                                issuer = '';
                            }
                        } else if (response.tx.LimitAmount) {
                            amount = response.tx.Amount.value;
                            currency = response.tx.Amount.currency;
                            issuer = response.tx.Amount.issuer;
                        } else {
                            // TODO offers
                            amount = 0;
                            currency = '';
                            issuer = '';
                        }
                        var params = [
                            txhash,
                            response.tx.TransactionType,
                            response.tx.validated,
                            success_submit,
                            amount,
                            currency,
                            issuer
                        ];
                        client.query(sql, params, function (err) {
                            if (err) console.log(err);
                            client.end();
                        });
                    });
                } else if (response.tx.validated === true) {
                    client = new pg.Client(peercoverConfig.pgParams);
                    success_submit = (response.tx.meta.TransactionResult === 'tesSUCCESS') ? true : false;
                    client.connect(function (err) {
                        if (err) console.log(err);
                        var sql = "DELETE FROM pending_transactions WHERE txhash = $1";
                        client.query(sql, [txhash], function (err) {
                            if (err) console.log(err);
                            client.end();
                        });
                    });
                }
            }
        } catch (e) {
            // Missing a field?
            console.log(e.stack);
        }
    });
}

// Listen for notifications on postgres channel "pending".
// Message format:
// txhash,uuid,status_url,txtype,txsubtype,validated,source_account,destination_account,amount,currency,issuer
function pendingTxListener() {
    var client = new pg.Client(peercoverConfig.pgParams);
    client.connect();
    client.query('LISTEN "pending"');
    client.on('notification', function (data) {
        data = data.split(',');
        var tx = {
            txhash: data[0],
            uuid: data[1],
            status_url: data[2],
            txtype: data[3],
            txsubtype: data[4],
            validated: data[5],
            source_account: data[6],
            destination_account: data[7],
            amount: data[8],
            currency: data[9],
            issuer: data[10]
        };
        // Payments
        if (tx.txhash && tx.uuid !== 0) {
            rippleRestPayment({
                "secret": peercoverConfig.iouSecret,
                "client_resource_id": tx.uuid,
                "payment": {
                    "source_account": tx.source_account,
                    "destination_account": tx.destination_account,
                    "destination_amount": {
                        "value": tx.amount,
                        "currency": tx.currency,
                        "issuer": tx.issuer
                    }
                }
            }, row.txsubtype || "Payment")
        }
    });
}

// main
(function () {
    pendingTxListener();   
    logTx("BDC89F1B2D1EFDD1EDED4CAF125D3519E371D2E8EF63BF62B9A87122D1B188C7");
    var postData = {
        "secret": peercoverConfig.testsecret,
        "client_resource_id": uuid.v4(),
        "payment": {
            "source_account": peercoverConfig.testacct,
            "destination_account": peercoverConfig.testtarget,
            "destination_amount": {
                "value": "1",
                "currency": "XRP",
                "issuer": ""
            }
        }
    };
    rippleRestPayment(postData, "dividend");
    setInterval(rippleRestHeartbeat, 10000);
})();

// backbone/AJAX routes
// users Routes
// modify these routes with user checks
// modify find to find by id
peercover.get('/user', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    User.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/user', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new User(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/user', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    User.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/user', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    User.find(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// ious Routes
peercover.get('/iou', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Iou.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/iou', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Iou(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/iou', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Iou.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/iou', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Iou.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// rippleInsuredWallets Routes
peercover.get('/rippleInsuredWallet', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    RippleInsuredWallet.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/rippleInsuredWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new RippleInsuredWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/rippleInsuredWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuredWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/rippleInsuredWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuredWallet.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// bitcoinInsuredWallets Routes
peercover.get('/bitcoinInsuredWallet', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    BitcoinInsuredWallet.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/bitcoinInsuredWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new BitcoinInsuredWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/bitcoinInsuredWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuredWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/bitcoinInsuredWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuredWallet.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// bitcoinInsuranceWallets Routes
peercover.get('/bitcoinInsuranceWallet', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    BitcoinInsuranceWallet.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/bitcoinInsuranceWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new BitcoinInsuranceWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/bitcoinInsuranceWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuranceWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/bitcoinInsuranceWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    BitcoinInsuranceWallet.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// rippleInsuranceWallets Routes
peercover.get('/rippleInsuranceWallet', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    RippleInsuranceWallet.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/rippleInsuranceWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new RippleInsuranceWallet(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/rippleInsuranceWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuranceWallet.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/rippleInsuranceWallet', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    RippleInsuranceWallet.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// facebookData Routes
peercover.get('/facebookDatum', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    FacebookDatum.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/facebookDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new FacebookDatum(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/facebookDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FacebookDatum.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/facebookDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FacebookDatum.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// dwollaData Routes
peercover.get('/dwollaDatum', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    DwollaDatum.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/dwollaDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new DwollaDatum(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/dwollaDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    DwollaDatum.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/dwollaDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    DwollaDatum.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// linkedinData Routes
peercover.get('/linkedinDatum', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    LinkedinDatum.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/linkedinDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new LinkedinDatum(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/linkedinDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LinkedinDatum.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/linkedinDatum', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LinkedinDatum.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// fundingSources routes
peercover.get('/fundingSource', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    FundingSource.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/fundingSource', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new FundingSource(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/fundingSource', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FundingSource.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/fundingSource', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    FundingSource.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// images Routes
peercover.get('/image', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Imaged.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/image', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Imaged(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/image', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Imaged.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/image', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Imaged.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// documents Routes
peercover.get('/document', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Documented.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/document', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Documented(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/document', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Documented.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/document', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Documented.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// videos Routes
peercover.get('/video', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Video.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/video', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Video(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/video', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Video.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/video', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Video.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// claims Routes
peercover.get('/claim', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Claim.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/claim', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Claim(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/claim', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Claim.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/claim', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Claim.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// lossGroups Routes
peercover.get('/lossGroup', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    LossGroup.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/lossGroup', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new LossGroup(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/lossGroup', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LossGroup.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/lossGroup', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    LossGroup.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// usersReferred Routes
peercover.get('/userReferred', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    UserReferred.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/userReferred', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new UserReferred(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/userReferred', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    UserReferred.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/userReferred', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    UserReferred.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// terms Routes
peercover.get('/term', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Term.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/term', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Term(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/term', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Term.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/term', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Term.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// signatures Routes
peercover.get('/signature', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    Signature.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/signature', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new Signature(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/signature', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Signature.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/signature', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    Signature.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// termOptions Routes
peercover.get('/termOption', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    TermOption.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/termOption', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new TermOption(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/termOption', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TermOption.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/termOption', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TermOption.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});

// trustedGateways Routes
peercover.get('/trustedGateway', requireAuth, function(req,res){
    // add object from req.query[""] to include in find
    // if want to limit scope before dumping entire collection
    // to front end
    // add conditionals for security depending on logged-in user
    TrustedGateway.findById(req.params.id).exec(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(doc);
    });
});
peercover.post('/trustedGateway', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    var entry = new TrustedGateway(req.params);
    entry.save(function (err,doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        res.send(req.body);
    });
});
peercover.put('/trustedGateway', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TrustedGateway.findById(req.params.id,function(err, doc) {
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
        doc.update(req.params, function(err,updateObject){});
        res.send(req.body);
    });
});
peercover.delete('/trustedGateway', requireAuth, function(req,res){
    // add conditionals for security purposes and authentication
    // collect errors and send errors back
    TrustedGateway.findById(req.params.id,function(err,doc){
        if (!isEmpty(err)){
            res.send({error: 'Please forgive the deletion error.'});
        }
    }).remove();
});
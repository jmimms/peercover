CREATE OR REPLACE FUNCTION pending_tx_trigger() RETURNS trigger AS $$
DECLARE
BEGIN
    PERFORM pg_notify('pending', NEW.txhash || ',' || NEW.uuid || ',' || NEW.status_url || ',' || NEW.txtype || ',' || NEW.txsubtype || ',' || NEW.validated || ',' || NEW.source_account || ',' || NEW.destination_account || ',' || NEW.amount || ',' || NEW.currency);
    RETURN new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER pending_trigger AFTER INSERT ON pending_transactions
FOR EACH ROW EXECUTE PROCEDURE pending_tx_trigger();

--DROP TABLE pending_transactions;
CREATE TABLE pending_transactions (
    uuid VARCHAR(64),
    status_url VARCHAR(300),
    txhash VARCHAR(1000) NOT NULL,
    txtype VARCHAR(25) NOT NULL,
    txsubtype VARCHAR(25),
    validated BOOLEAN,
    success_submit BOOLEAN,
    source_account VARCHAR(100),
    destination_account VARCHAR(100),
    amount NUMERIC,
    currency CHAR(3),
    issuer VARCHAR(100),
    init_date TIMESTAMP DEFAULT statement_timestamp() NOT NULL,
    last_submit_date TIMESTAMP DEFAULT statement_timestamp()
);
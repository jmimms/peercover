#!/usr/bin/env python
"""
Get transaction data from local rippled via websockets.

(c) Jack Peterson (jack@tinybike.net), Peercover Inc., 2/27/2014
"""
import sys, json, getopt
import websocket
from usage import Usage
from parse_tx import parse_tx, reset_tx_table
from pg_cursor_context import cursor_context
from tickers import get_tickers
from evtlog import error_handler
from datacollector import DataCollector
from dbcleanup import dbcleanup

class Vacuum(DataCollector):
    '''
    Vacuums up data from rippled.
    '''
    def __init__(self, socket_url=None, currencies=None, halt=152370,
                 drop=False, resume=False, logfile=None, verbose=False,
                 update=False, single_ledger=None):
        super(Vacuum, self).__init__(logfile, verbose)
        self.socket_url = socket_url
        self.currencies = currencies
        self.halt = halt
        self.drop = drop
        self.resume = resume
        self.socket = None
        self.update = update
        self.single_ledger = single_ledger
        self.ledger_current_index = None
        self.ledgers_to_read = None
        self.log = logfile

    @error_handler(task="Get current ledger index")
    def get_current_index(self, retry=False):
        try:
            if self.socket is not None:
                self.socket.send(json.dumps({'command': 'ledger_current'}))
                data = json.loads(self.socket.recv())
                if data and data['status'] == 'success':
                    if 'result' in data and 'ledger_current_index' in data['result']:
                        self.ledger_current_index = data['result']['ledger_current_index']
                        print "Current ledger index:", self.ledger_current_index
            else:
                self.get_current_index(retry=True)
        except Exception as e:
            print e
            if retry: return
            self.get_current_index(retry=True)

    @error_handler(task="Check if transaction hash is a duplicate")
    def is_duplicate(self, tx_hash):
        with cursor_context() as cur:
            query = "SELECT count(*) FROM transactions WHERE txhash = %s"
            cur.execute(query, (tx_hash,))
            if cur.rowcount and cur.fetchone()[0]: return True
            return False

    @error_handler(task="Get transaction from rippled")
    def get_tx(self, tx_hash, data):
        try:
            if self.socket is not None:
                self.socket.send(json.dumps({
                    'command': 'tx',
                    'transaction': tx_hash,
                }))
                tx_data = self.socket.recv()
                tx_data = json.loads(tx_data)
                if tx_data['status'] == 'success' and 'result' in tx_data:
                    options = {
                        'ledger_time': data['result']['ledger']['close_time'],
                        'currencies': self.currencies,
                        'past': True,
                        'tx_hash': tx_hash,
                    }
                    return tx_data['result'], options
        except Exception as e:
            print e
        return False, False

    def output_progress(self):
        ledgers_read = self.ledger_current_index - self.ledger_index - 1
        progress = round(float(ledgers_read) / float(self.ledgers_to_read), 3)
        sys.stdout.write("Read " + str(ledgers_read) + "/" +\
                         str(self.ledgers_to_read) + " [" +\
                         str(progress * 100) + "%] ledgers (" +\
                         str(self.stored_tx) + " transactions)\r")
        sys.stdout.flush()

    @error_handler(task="Parse the ledger summary")
    def parse_ledger(self, data):
        accepted = False
        tx_hash_list = None
        if 'result' in data and 'ledger' in data['result'] and \
                        'transactions' in data['result']['ledger']:
            tx_hash_list = data['result']['ledger']['transactions']
            if 'accepted' in data['result']['ledger'] and \
                    data['result']['ledger']['accepted']:
                accepted = True
        return tx_hash_list, accepted

    @error_handler(task="Get the next ledger from rippled")
    def read_next_ledger(self):
        if self.socket is not None:
            self.socket.send(json.dumps({
                'command': 'ledger',
                'ledger_index': self.ledger_index,
                'transactions': True,
                'expand': False,
            }))
            ledger = self.socket.recv()
            return json.loads(ledger)
    
    @error_handler(task="housekeeping")
    def housekeeping(self):
        if self.drop:
            reset_tx_table()
        elif not self.resume and not self.update:
            with cursor_context() as cur:
                query = ("DELETE FROM transactions "
                         "WHERE ledgerindex IS NOT NULL AND historical = 't'")
                cur.execute(query)
                print "Deleted", cur.rowcount, "transactions from database"

    @error_handler(task="Connect to rippled")
    def rippled_connect(self):
        for i in xrange(3):
            try:
                self.socket = websocket.create_connection(self.socket_url)
                print "Connected to", self.socket_url
                return True
            except ValueError as e:
                print "Error: couldn't connect to rippled.", e
        return False

    @error_handler(task="Start vacuuming where we left off")
    def get_leftoff_index(self):
        retval = False

        # Start at the oldest ledger we've collected data for, then walk
        # backwards to the genesis ledger
        if self.resume:
            with cursor_context() as cur:
                cur.execute("SELECT min(ledgerindex) FROM transactions WHERE historical = 't'")
                if cur.rowcount:
                    min_ledger_index = cur.fetchone()[0]
                    if min_ledger_index is not None:
                        self.ledger_current_index = int(min_ledger_index)
                        # print "Starting at ledger index:", self.ledger_current_index
                else:
                    self.get_current_index()
                retval = True

        # Start at the current ledger and walk backward until we get to the
        # newest ledger that we've collected data for
        elif self.update:
            with cursor_context() as cur:
                cur.execute("SELECT max(ledgerindex) FROM transactions WHERE historical = 't'")
                if cur.rowcount:
                    max_ledger_index = cur.fetchone()[0]
                    if max_ledger_index is not None:
                        self.halt = int(max_ledger_index)
                        self.get_current_index()
                        retval = True
                else:
                    print "Error, no ledger data found"
        return retval

    def vacuum_rippled(self):
        self.housekeeping()
        if self.rippled_connect():
            if self.resume or self.update:
                if not self.get_leftoff_index():
                    return False
            else:
                self.get_current_index()

            # Read a single ledger
            if self.single_ledger is not None:
                print "Fetching single ledger:", self.single_ledger
                self.ledger_index = self.single_ledger
                ledger = self.read_next_ledger()
                if ledger is not None:
                    tx_hash_list, accepted = self.parse_ledger(ledger)
                    if tx_hash_list is not None:
                        for tx_hash in tx_hash_list:
                            if self.resume and self.ledger_index == self.ledger_current_index - 1:
                                if self.is_duplicate(tx_hash): continue
                            tx_data_result, options = self.get_tx(tx_hash, ledger)
                            if tx_data_result:
                                self.stored_tx += parse_tx(tx_data_result, accepted, **options)
                dbcleanup()
                self.socket.close()
                return True

            # Read back to the ledger specified in "halt" (by default,
            # this is set to the genesis ledger)
            else:
                print "Halt at ledger:", self.halt
                if self.ledger_current_index is None:
                    self.get_current_index()
                self.ledgers_to_read = self.ledger_current_index - self.halt
                self.ledger_index = self.ledger_current_index - 1
                self.stored_tx = 0
                while self.ledger_index >= self.halt:
                    ledger = self.read_next_ledger()
                    self.output_progress()
                    if ledger is not None:
                        tx_hash_list, accepted = self.parse_ledger(ledger)
                        if tx_hash_list is not None:
                            for tx_hash in tx_hash_list:
                                if self.resume and self.ledger_index == self.ledger_current_index - 1:
                                    if self.is_duplicate(tx_hash): continue
                                tx_data_result, options = self.get_tx(tx_hash, ledger)
                                if tx_data_result:
                                    self.stored_tx += parse_tx(tx_data_result, accepted, **options)
                    if self.ledger_index % 1000 == 0:
                        dbcleanup()
                    self.ledger_index -= 1
                self.socket.close()
                print
                return True

        return False


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            short_opts = 'hdrvus:l:'
            long_opts = ['help', 'drop', 'resume', 'verbose', 'update', 'socket=', 'ledger=']
            opts, args = getopt.getopt(argv[1:], short_opts, long_opts)
        except getopt.GetoptError as e:
            raise Usage(e)

        # Websockets rippled URLS:
        #   - Ripple.com: wss://s1.ripple.com:51233/
        #   - Peercover localhost: ws://127.0.0.1:51236/
        #   - Vent localhost: ws://127.0.0.1:6006/
        parameters = {
            'socket_url': 'wss://s1.ripple.com:51233/',
            'halt': 152370,  # minimum ledger index (genesis ledger)
            'drop': False,  # drop and re-create database if True
            'resume': False,  # start at the minimum ledger found in our db
            'currencies': get_tickers(),  # list of currencies/ticker symbols
            'logfile': 'vacuum.log',
            'verbose': False,
            'single_ledger': None,
            'update': False,
        }
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                print __doc__
                return 0
            elif opt in ('-d', '--drop'):
                parameters['drop'] = True
            elif opt in ('-r', '--resume'):
                parameters['resume'] = True
            elif opt in ('-v', '--verbose'):
                parameters['verbose'] = True
            elif opt in ('-s', '--socket'):
                parameters['socket_url'] = arg
            elif opt in ('-u', '--update'):
                parameters['update'] = True
            elif opt in ('-l', '--ledger'):
                parameters['single_ledger'] = arg
        vac = Vacuum(**parameters)
        vac.vacuum_rippled()
        dbcleanup()
    except Usage as e:
        print >>sys.stderr, e.msg
        print >>sys.stderr, "for help use --help"
        return 2

if __name__ == '__main__':
    sys.exit(main())

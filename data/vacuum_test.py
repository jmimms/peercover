import unittest, json, platform, sys, getopt
import vacuum
from parse_tx import parse_tx, reset_tx_table
from pg_cursor_context import cursor_context
from tickers import get_tickers
from usage import Usage
import psycopg2
from ptestcase import ParametrizedTestCase

class TestVacuum(ParametrizedTestCase):

    def setUp(self):
        parameters = {
            'socket_url': self.param,
            'halt': 152370,
            'drop': False,
            'resume': False,
            'currencies': get_tickers(),
            'logfile': 'pcvdata.log',
            'verbose': False,
        }
        self.vac = vacuum.Vacuum(**parameters)

    def test_cursor_context(self):
        with cursor_context() as cur:
            self.assertTrue(type(cur) == psycopg2._psycopg.cursor)
            cur.execute('SELECT current_database()')
            self.assertEqual(cur.fetchone()[0], 'peercover')

    def test_tickers(self):
        expected_tickers = {
            'AUD', 'BTC', 'CAD', 'CHF', 'EUR', 'GBP', 'JPY', 'USD', 'ILS',
            'NOK', 'XRP', 'LTC', 'NXT', 'DOG', 'NMC', 'PPC', 'PCV', 'CAT',
            'CNY', 'PVC', 'MML', 'MMC'
        }
        tickers = set(get_tickers())
        self.assertTrue(expected_tickers <= tickers)
        self.assertItemsEqual(tickers, expected_tickers)

    def test_housekeeping_drop(self):
        self.vac.drop = True
        self.vac.housekeeping()
        with cursor_context() as cur:
            cur.execute('SELECT * FROM transactions')
            self.assertEqual(cur.rowcount, 0)

    def test_housekeeping_resume(self):
        self.vac.resume = True
        with cursor_context() as cur:
            records = {
                'txid': 1,
                'txhash': 'test',
                'market': 'LOLWUT',
                'currency1': 'LOL',
                'currency2': 'WUT',
                'amount1': 1,
                'amount2': 1,
                'price1': 1,
                'price2': 1,
                'issuer1': 'me',
                'issuer2': 'me',
                'txdate': 946684800,
                'ledgerindex': 1000,
                'historical': True,
                'accepted': False,
            }
            sql = (
                'INSERT INTO transactions '
                '(txid, txhash, market, currency1, currency2, amount1, amount2, '
                'price1, price2, issuer1, issuer2, '
                'txdate, ledgerindex, historical, accepted) '
                'VALUES '
                '(%(txid)s, %(txhash)s, %(market)s, %(currency1)s, %(currency2)s, '
                '%(amount1)s, %(amount2)s, %(price1)s, %(price2)s, '
                '%(issuer1)s, %(issuer2)s, '
                '%(txdate)s, %(ledgerindex)s, %(historical)s, %(accepted)s)'
            )
            cur.execute(sql, records)
        self.vac.housekeeping()
        with cursor_context() as cur:
            cur.execute("SELECT min(ledgerindex) FROM transactions WHERE ledgerindex IS NOT NULL AND historical = 't'")
            self.assertEqual(cur.fetchone()[0], 1000)
        self.vac.drop = True
        self.vac.housekeeping()

    def test_housekeeping_minireset(self):
        with cursor_context() as cur:
            records = {
                'txid': 1,
                'txhash': 'test',
                'market': 'LOLWUT',
                'currency1': 'LOL',
                'currency2': 'WUT',
                'amount1': 1,
                'amount2': 1,
                'price1': 1,
                'price2': 1,
                'issuer1': 'me',
                'issuer2': 'me',
                'txdate': 946684800,
                'ledgerindex': 1,
                'historical': False,
                'accepted': False,
            }
            sql = (
                'INSERT INTO transactions '
                '(txid, txhash, market, currency1, currency2, amount1, amount2, '
                'price1, price2, issuer1, issuer2, '
                'txdate, ledgerindex, historical, accepted) '
                'VALUES '
                '(%(txid)s, %(txhash)s, %(market)s, %(currency1)s, %(currency2)s, '
                '%(amount1)s, %(amount2)s, %(price1)s, %(price2)s, '
                '%(issuer1)s, %(issuer2)s, '
                '%(txdate)s, %(ledgerindex)s, %(historical)s, %(accepted)s)'
            )
            cur.execute(sql, records)
        self.vac.housekeeping()
        with cursor_context() as cur:
            cur.execute('SELECT * FROM transactions')
            self.assertEqual(cur.rowcount, 1)
        with cursor_context() as cur:
            query = ("SELECT count(*) FROM transactions WHERE historical = 't' "
                     "AND ledgerindex IS NOT NULL")
            cur.execute(query)
            self.assertEqual(cur.fetchone()[0], 0)
        self.vac.drop = True
        self.vac.housekeeping()

    def test_rippled_connect(self):
        self.assertTrue(self.vac.rippled_connect())
        self.vac.socket.send(json.dumps({'command': 'ping'}))
        response = self.vac.socket.recv()
        self.assertEqual(type(response), str)
        response = json.loads(response)
        self.assertIn('status', response)
        self.assertEqual(response['status'], 'success')

    def test_get_current_index(self):
        self.vac.rippled_connect()
        self.vac.get_current_index()
        self.assertIsNotNone(self.vac.ledger_current_index)
        self.assertEqual(type(self.vac.ledger_current_index), int)

    def test_read_next_ledger(self):
        self.vac.rippled_connect()
        self.vac.get_current_index()
        self.vac.ledger_index = self.vac.ledger_current_index - 1
        self.vac.stored_tx = 0
        ledger = self.vac.read_next_ledger()
        self.assertEqual(type(ledger), dict)
        self.assertIn('status', ledger)
        self.assertEqual(ledger['status'], 'success')
        self.assertIn('result', ledger)
        self.assertIn('ledger', ledger['result'])
        ledger_fields = ('ledger_hash', 'close_time', 'hash', 'ledger_index',
                         'transactions', 'transaction_hash', 'accepted')
        for field in ledger_fields:
            self.assertIn(field, ledger['result']['ledger'])

    def test_parse_ledger(self):
        self.vac.rippled_connect()
        self.vac.get_current_index()
        self.vac.ledger_index = self.vac.ledger_current_index - 1
        ledger = self.vac.read_next_ledger()
        tx_hash_list, accepted = self.vac.parse_ledger(ledger)
        self.assertEqual(type(tx_hash_list), list)
        self.assertEqual(type(accepted), bool)
        for tx_hash in tx_hash_list:
            self.assertEqual(type(tx_hash), unicode)
            self.assertGreater(len(tx_hash), 10)

    def test_get_tx(self):
        self.vac.rippled_connect()
        self.vac.get_current_index()
        self.vac.halt = self.vac.ledger_current_index - 2
        self.vac.ledger_index = self.vac.ledger_current_index - 1
        ledger = self.vac.read_next_ledger()
        tx_hash_list, accepted = self.vac.parse_ledger(ledger)
        tx_data_result, options = self.vac.get_tx(tx_hash_list[0], ledger)
        self.assertEqual(type(tx_data_result), dict)
        self.assertEqual(type(options), dict)

    def test_parse_tx(self):
        self.vac.rippled_connect()
        self.vac.get_current_index()
        self.vac.ledger_index = self.vac.ledger_current_index - 1
        ledger = self.vac.read_next_ledger()
        tx_hash_list, accepted = self.vac.parse_ledger(ledger)
        tx_data_result, options = self.vac.get_tx(tx_hash_list[0], ledger)
        stored_tx = parse_tx(tx_data_result, accepted, **options)
        self.assertEqual(type(stored_tx), int)
        self.assertGreaterEqual(stored_tx, 0)

    def test_vacuum_rippled(self):
        self.vac.rippled_connect()
        self.vac.get_current_index()
        self.vac.halt = self.vac.ledger_current_index - 5
        self.vac.socket.close()
        self.vac.rippled_connect()
        self.vac.vacuum_rippled()
        self.assertGreaterEqual(self.vac.stored_tx, 0)
        self.assertEqual(self.vac.ledger_index + 1, self.vac.halt)
        with cursor_context() as cur:
            query = ("SELECT * FROM transactions WHERE historical = 't' "
                     "AND ledgerindex IS NOT NULL LIMIT 1")
            cur.execute(query)
            self.assertEqual(cur.rowcount, 1)
            expected_fields = ('pcvid', 'txid', 'txhash', 'market',
                               'currency1', 'currency2', 'price1', 'price2',
                               'amount1', 'amount2', 'issuer1', 'issuer2',
                               'account1', 'account2', 'txdate', 'ledgerindex',
                               'historical', 'accepted')
            self.assertItemsEqual(cur.description, expected_fields)
            self.assertIsNotNone(cur.fetchone()[0])
            query = ("SELECT price1, price2, amount1, amount2 FROM transactions "
                     "WHERE historical = 't' AND ledgerindex IS NOT NULL")
            cur.execute(query)
            for row in cur.fetchall():
                for field in row:
                    self.assertIsNotNone(field)
                    self.assertNotEqual(field, 0)
                    self.assertNotEqual(field, '0')
                    self.assertNotEqual(field, '0.0')
        self.vac.get_leftoff_index()
        self.assertEqual(self.vac.ledger_current_index, self.vac.ledger_index)
        self.vac.drop = True
        self.vac.housekeeping()

    def tearDown(self):
        self.vac.drop = True
        self.vac.housekeeping()
        if self.vac.socket is not None:
            self.vac.socket.close()
        self.vac = None


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            short_opts = 'hdrvs:'
            long_opts = ['help', 'drop', 'resume', 'verbose', 'socket=']
            opts, args = getopt.getopt(argv[1:], short_opts, long_opts)
        except getopt.GetoptError as e:
             raise Usage(e)
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                print __doc__
                return 0
            elif opt in ('-d', '--drop'):
                parameters['drop'] = True
            elif opt in ('-r', '--resume'):
                parameters['resume'] = True
            elif opt in ('-v', '--verbose'):
                parameters['verbose'] = True
            elif opt in ('-s', '--socket'):
                parameters['socket_url'] = arg

        # Default rippled url list, auto-change port on vent
        # Websockets rippled URLS:
        #   - Ripple.com: wss://s1.ripple.com:51233/
        #   - Peercover localhost: ws://127.0.0.1:51236/
        #   - Vent localhost: ws://127.0.0.1:6006/
        rippled_urls = ['ws://127.0.0.1:51236/', 'wss://s1.ripple.com:51233/']
        if platform.node() == 'vent':
            rippled_urls[0] = 'ws://127.0.0.1:6006/'
        for rippled_url in rippled_urls:
            suite = unittest.TestSuite()
            suite.addTest(ParametrizedTestCase.parametrize(TestVacuum, param=rippled_url))
            unittest.TextTestRunner(verbosity=2, buffer=True).run(suite)
            # next_rippled = raw_input('Finished testing ' + rippled_url + ', continue? [Y/n]')
            # if next_rippled.lower() == 'n':
            #     break
    except Usage as e:
        print >>sys.stderr, e.msg
        print >>sys.stderr, "for help use --help"
        return 2

if __name__ == '__main__':
    sys.exit(main())

#!/usr/bin/env python

from pg_cursor_context import cursor_context

def dbcleanup():
    with cursor_context() as cur:
        queries = (
            (
                "DELETE FROM transactions "
                "WHERE price1 = 0 "
                "OR price2 = 0 "
                "OR amount1 = 0 "
                "OR amount2 = 0"
            ),
            (
                "DELETE FROM resampled "
                "WHERE high1 = 0 "
                "OR high2 = 0 "
                "OR low1 = 0 "
                "OR low2 = 0 "
                "OR close1 = 0 "
                "OR close2 = 0 "
                "OR open1 = 0 "
                "OR open2 = 0 "
                "OR volume1 = 0 "
                "OR volume2 = 0"
            ),
            # (
            #     "DROP TABLE IF EXISTS transactions_temp"
            # ),
            # (
            #     "CREATE TABLE transactions_temp AS "
            #     "(SELECT DISTINCT ON (txhash) * FROM transactions) "
            #     "UNION "
            #     "(SELECT * FROM transactions WHERE txhash IS NULL)"
            # ),
            # (
            #     "DROP TABLE transactions"
            # ),
            # (
            #     "ALTER TABLE transactions_temp RENAME TO transactions"
            # ),
        )
        for query in queries:
            cur.execute(query)
            # print cur.rowcount, "rows modified"

if __name__ == '__main__':
    dbcleanup()
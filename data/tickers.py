#!/usr/bin/env python

import json
import websocket
from pg_cursor_context import cursor_context

def update_tickers():
    tickers = ['AUD', 'BTC', 'CAD', 'CHF', 'EUR', 'GBP', 'JPY', 'USD',
               'ILS', 'NOK', 'XRP', 'LTC', 'NXT', 'DOG', 'NMC', 'PPC',
               'PCV', 'AUR']
    socket_url = 'wss://s1.peercover.com:51232/'
    # socket_url = 'wss://s1.ripple.com:51233/'

    # Connect to rippled
    connected = False
    while not connected:
        try:
            socket = websocket.create_connection(socket_url)
            connected = True
            print "Connected to", socket_url
        except Exception as e:
            print e

    # Get tickers from rippled account_lines <addr>
    new_tickers = None
    while new_tickers is None:
        try:
            socket.send(json.dumps({
                'command': 'account_lines',
                'account': 'rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif',
            }))
            data = json.loads(socket.recv())
            if 'result' in data and 'lines' in data['result']:
                print len(data['result']['lines']), 'results found'
                for line in data['result']['lines']:
                    if line['currency'] not in tickers:
                        new_tickers = line['currency']
                        print new_tickers
                        tickers.append(new_tickers)
        except Exception as e:
            print e
            break

    new_tickers2 = None
    while new_tickers2 is None:
        try:
            socket.send(json.dumps({
                'command': 'account_lines',
                'account': 'ra9eZxMbJrUcgV8ui7aPc161FgrqWScQxV',
            }))
            data = json.loads(socket.recv())
            if 'result' in data and 'lines' in data['result']:
                print len(data['result']['lines']), 'results found'
                for line in data['result']['lines']:
                    if line['currency'] not in tickers:
                        new_tickers2 = line['currency']
                        print new_tickers2
                        tickers.append(new_tickers2)
        except Exception as e:
            print e
            break

    # Write tickers to database
    with cursor_context() as cur:
        cur.execute('DROP TABLE IF EXISTS tickers')
        cur.execute('CREATE TABLE tickers (ticker CHAR(3) UNIQUE)')
        query = 'INSERT INTO tickers VALUES (%s)'
        for ticker in tickers:
            cur.execute(query, (ticker,))

def get_tickers():
    with cursor_context() as cur:
        cur.execute("SELECT * FROM tickers")
        tickers = [row[0] for row in cur.fetchall()]
    return tickers

if __name__ == '__main__':
    update_tickers()
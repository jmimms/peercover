import os, datetime, logging
from functools import wraps

class EvtLog(object):
    '''
    Logger acts as singleton; call get_logger with optional name and path 
    to get a logger that can be used for logging.
    '''
    singleinstance = None
    def __init__(self, name, file_or_path, show_messages):
        self.evt_logger = logging.get_logger(name)
        self.show_msgs = show_messages
        if file_or_path:
            if not os.path.isfile(file_or_path):
                logfile = os.path.join(file_or_path, "datacollect.log")
            else:
                logfile = file_or_path
            file_handler = logging.FileHandler(logfile)

            # Format files with timestamp
            file_handler.setFormatter(
                logging.Formatter(fmt="%(asctime)s %(levelname)s %(message)s",
                                  datefmt="%m/%d/%Y %I:%M:%S %p")
            )
            self.evt_logger.addHandler(file_handler)
        else:
            self.evt_logger.addHandler(logging.StreamHandler())
        self.evt_logger.setLevel(logging.INFO)

    @staticmethod
    def get_logger(name="datacollect", file_or_path=None, show_messages=False):
        if not EvtLog.singleinstance:
            EvtLog.singleinstance = EvtLog(name, file_or_path, show_messages)
            if file_or_path:
                EvtLog.singleinstance.log_message(
                    logging.WARN,
                    str(datetime.datetime.now()) + " ***** Log started  with path = " +
                        file_or_path + " and verbose = " + str(show_messages) + " ****"
                )
        return EvtLog.singleinstance

    def log_message(self, level, msg, **kwargs):
        '''Log to log file. If no log file has been given, log to stderr.'''
        try:
            assert self.evt_logger
            self.evt_logger.log(level, msg)
            
            # If running not optimized, log everthing, changing level as needed
            if __debug__ and not self.evt_logger.isEnabledFor(level):
                self.evt_logger.log(self.evt_logger.getEffectiveLevel(), "<debug mode>" + msg)

            # Pass message on to report so it can be shown via web service
            if {'packageId', 'task_name', 'taskDesc'} <= kwargs:
                report, warnings, info = None, None, None
                if level in (logging.INFO, logging.DEBUG):
                    info = msg
                elif level in (logging.WARN, logging.WARNING):
                    warnings = msg
                elif level in (logging.ERROR, logging.CRITICAL):
                    report = msg
                else:
                    raise(Exception("Unknown logging level"))
        except Exception as e:
            print "Exception in log_message"
            raise e


def error_handler(task_name=""):
    def decorate(task_func):
        @wraps(task_func)
        def error_handler_wrapper(*args, **kwargs):
            try:
                if args[0] is None or args[0].logger is None:
                    raise Exception("Logger instance not found")
                logger = args[0].logger
                logger.log_message(logging.INFO, task_name + ":")
                return task_func(*args, **kwargs)
            except Exception as e:
                logger.log_message(logging.WARN, "error type: " + str(type(e)))
                logger.log_message(logging.WARN, e.message)
                task_report = task_name + " terminated: " + e.message
                raise Exception(task_report)
        return error_handler_wrapper
    return decorate

import os, datetime, logging, sys, traceback
from functools import wraps

class EvtLog(object):
    '''
    Logger acts as singleton; call get_logger with optional name and path 
    to get a logger that can be used for logging.
    '''
    singleinstance = None
    def __init__(self, name, file_or_path, show_messages):
        self.evt_logger = logging.getLogger(name)
        self.show_msgs = show_messages
        if file_or_path:
            if not os.path.isfile(file_or_path):
                logfile = os.path.join(file_or_path, "pcvdata.log")
            else:
                logfile = file_or_path
            file_handler = logging.FileHandler(logfile)

            # Format files with timestamp
            file_handler.setFormatter(
                logging.Formatter(fmt="%(asctime)s %(levelname)s %(message)s",
                                  datefmt="%m/%d/%Y %I:%M:%S %p")
            )
            self.evt_logger.addHandler(file_handler)
        else:
            self.evt_logger.addHandler(logging.StreamHandler())
        self.evt_logger.setLevel(logging.WARN)

    @staticmethod
    def getLogger(name="pcvdata", file_or_path=None, show_messages=False):
        if not EvtLog.singleinstance:
            EvtLog.singleinstance = EvtLog(name, file_or_path, show_messages)
            if file_or_path:
                EvtLog.singleinstance.log_message(
                    logging.WARN,
                    str(datetime.datetime.now()) + ": Log started: " +\
                        file_or_path + " (verbose " + str(show_messages) + ")"
                )
        return EvtLog.singleinstance

    def log_message(self, level, msg, **kwargs):
        '''Log to log file. If no log file has been given, log to stderr.'''
        try:
            assert self.evt_logger
            self.evt_logger.log(level, msg)
        except Exception as e:
            print "Exception in log_message"
            raise e


# def error_handler(task=""):
#     def decorate(task_func):
#         @wraps(task_func)
#         def error_handler_wrapper(*args, **kwargs):
#             try:
#                 if args[0] is None or args[0].logger is None:
#                     raise Exception("Logger instance not found")
#                 logger = args[0].logger
#                 logger.log_message(logging.INFO, task + ":")
#                 return task_func(*args, **kwargs)
#             except Exception as e:
#                 # print e
#                 try:
#                     logger.log_message(logging.WARN, "error type: " + str(type(e)))
#                     logger.log_message(logging.WARN, e.message)
#                 except Exception as err:
#                     print e, err
#                 task_report = task + " terminated: " + e.message
#                 raise Exception(task_report)
#         return error_handler_wrapper
#     return decorate

def error_handler(task=""):
    def decorate(task_func):
        @wraps(task_func)
        def wrapper(self, *args, **kwargs):
            try:
                return task_func(self, *args, **kwargs)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                erroroutput = (
                    "Error in task \"" + task + "\" (" + sys.exc_info()[0].__name__ + "/" +
                    fname + "/" + str(exc_tb.tb_lineno) + "):\n--> " + e.message
                )
                with open(self.log, 'a') as logfile:
                    print >>logfile, erroroutput + "\nTraceback:"
                    traceback.print_tb(exc_tb, limit=5, file=logfile)
        return wrapper
    return decorate
#!/usr/bin/env python

from pg_cursor_context import cursor_context

with cursor_context() as cur:
    query = (
        "INSERT INTO resampled (starttime, freq, currency1, currency2, open1, high1, low1, close1, volume1, medprice1, open2, high2, low2, close2, volume2, medprice2, synthetic) "
        "SELECT starttime, freq, currency1, 'LOL', open1, high1, low1, close1, volume1, medprice1, open2, high2, low2, close2, volume2, medprice2, 't' "
        "FROM resampled "
        "WHERE currency1 = 'XRP' AND currency2 = 'PCV'"
    )
    cur.execute(query)
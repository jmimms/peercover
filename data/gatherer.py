#!/usr/bin/env python
"""
Downloads historical trade data from Ripple charts, and current transactions
over websockets from rippled.

Usage: gatherer.py [options]
    -> -r: drop and re-create history and transaction tables
    -> -l: only listen for new transactions
    -> -h: only gather historical data
    -> -n <int>: number of results returned per page for historical charts
            Default: 1000
    -> -b <url>: specify the base url for historical data
            Default: https://ripple.com/chart/
    -> -j <string>: specify a custom REST API suffix
            Default: trades.json?since=
    -> -s <url>: specify a rippled websockets URL

Requires Python 2.7.3+

(c) Jack Peterson (jack@tinybike.net), Peercover Inc., 2/23/2014
"""
import os, sys, json, csv, urllib2, getopt, time, thread
import multiprocessing, platform
import websocket
from decimal import *
from usage import Usage
from pg_cursor_context import cursor_context
from tickers import get_tickers
from tickers import update_tickers
from parse_tx import reset_tx_table
from datacollector import DataCollector
from evtlog import error_handler
from vacuum import Vacuum
from plotbuilder import PlotBuilder
from dbcleanup import dbcleanup

class Gatherer(DataCollector):
    '''
    Gathers historical trade data from the Ripple chart REST API, and listens
    for current rippled transactions via websockets.  Results are stored in a
    PostgreSQL database.
    '''
    def __init__(self, base_url, num_results, currencies, json_suffix,
                 socket_url=None, setup=False, listen_only=True, history_only=False,
                 precision=28, daemon=False, logfile=None, verbose=False):
        super(Gatherer, self).__init__(logfile, verbose)
        self.base_url = base_url
        self.num_results = num_results
        self.currencies = currencies
        self.json_suffix = json_suffix
        self.setup = setup
        self.socket_url = socket_url
        self.blacklist = []
        self.listen_only = listen_only
        self.start_date = None
        self.daemon = daemon
        self.log = logfile
        self.charts = False
        getcontext().prec = precision

    def get_currency_pairs(self):
        '''Generator for combinations of currency pairs.'''
        for i, currency in enumerate(self.currencies):
            for other_currency in self.currencies[i+1:]:
                if currency + other_currency not in self.blacklist:
                    yield (currency, other_currency)

    @error_handler(task="Download JSON formatted data from Ripple charts.")
    def download_trade_data(self, url):
        '''Download JSON formatted data from Ripple charts.'''
        data = None
        try:
            data = urllib2.urlopen(url).read()
            data = json.loads(data)
        except:
            pass
        return data

    @error_handler(task="Get the most recent trade date.")
    def get_last_update(self, cur):
        '''Get the most recent trade date from the historical data already
        stored in our postgres database.
        '''
        cur.execute('SELECT max(txdate) FROM transactions WHERE ledgerindex IS NULL')
        self.start_date = cur.fetchone()[0]

    @error_handler(task="Parse transactions")
    def parse_tx(self, msg):
        '''Parses transactions appearing on the Ripple network.  Checks for
        payment transactions which are applied to an existing offer on the
        network, then extracts the relevant data from these transactions and
        inserts the data into a postgres database.
        '''
        # Verify that this is a transaction
        required_fields = {'transaction', 'engine_result_code'}
        if required_fields <= set(msg) and msg['engine_result_code'] == 0:

            # Verify that the proper payment transaction fields are present
            required_tx_fields = {'TransactionType', 'Account', 'Destination', 'Amount', 'date'}
            tx_fields_ok = required_tx_fields <= set(msg['transaction'])
            tx_type_ok = msg['transaction']['TransactionType'] == 'Payment'
            tx_result_ok = 'meta' in msg and msg['meta']['TransactionResult'] == 'tesSUCCESS'
            if tx_fields_ok and tx_type_ok and tx_result_ok:
                
                # Since we want both ends of the transaction, we have to
                # directly examine the modified/deleted nodes.
                for affected_node in msg['meta']['AffectedNodes']:
                    if 'ModifiedNode' in affected_node:
                        node = affected_node['ModifiedNode']
                    elif 'DeletedNode' in affected_node:
                        node = affected_node['DeletedNode']
                    else:
                        continue
                    is_offer = node['LedgerEntryType'] == 'Offer'
                    has_prev = 'PreviousFields' in node
                    if has_prev:
                        has_pays = 'TakerPays' in node['PreviousFields']
                        has_gets = 'TakerGets' in node['PreviousFields']
                    if is_offer and has_prev and has_pays and has_gets:

                        # Offer parameters before and after the transaction
                        previous = node['PreviousFields']
                        final = node['FinalFields']

                        # Check if the trade is in XRP or another currency
                        #
                        # Note: TakerPays fields also show up in msg['transaction'].
                        # The amount recorded in the transaction is the difference
                        # between the before and after values in the order.
                        adjust_xrp = 10**6
                        if 'currency' in final['TakerGets']:
                            gets = {
                                'currency': final['TakerGets']['currency'],
                                'amount': Decimal(previous['TakerGets']['value']) - Decimal(final['TakerGets']['value']),
                                'issuer': final['TakerGets']['issuer'],
                            }
                        else:
                            gets = {
                                'currency': 'XRP',
                                'amount': (Decimal(previous['TakerGets']) - Decimal(final['TakerGets'])) / adjust_xrp,
                                'issuer': None,
                            }
                        
                        if 'currency' in final['TakerPays']:
                            pays = {
                                'currency': final['TakerPays']['currency'],
                                'amount': Decimal(previous['TakerPays']['value']) - Decimal(final['TakerPays']['value']),
                                'issuer': final['TakerPays']['issuer'],
                            }
                        else:
                            pays = {
                                'currency': 'XRP',
                                'amount': (Decimal(previous['TakerPays']) - Decimal(final['TakerPays'])) / adjust_xrp,
                                'issuer': None,
                            }

                        # Make sure the currency pair is ordered correctly
                        gets_idx, pays_idx = None, None
                        for i, curr in enumerate(self.currencies):
                            if curr == gets['currency']:
                                gets_idx = i
                            if curr == pays['currency']:
                                pays_idx = i
                            if gets_idx is not None and pays_idx is not None:
                                break
                        if gets_idx < pays_idx:
                            temp = gets
                            gets = pays
                            pays = temp

                        if gets['amount'] > 0 and pays['amount'] > 0:

                            # Round to 2 decimal places for NXT, and 8 decimal
                            # places for any other currency
                            pays['quantum'] = Decimal('.01') if pays['currency'] == 'NXT' else Decimal('.00000001')
                            pays['price'] = (gets['amount'] / pays['amount']).quantize(pays['quantum'], rounding=ROUND_HALF_EVEN)
                            gets['quantum'] = Decimal('.01') if gets['currency'] == 'NXT' else Decimal('.00000001')
                            gets['price'] = (pays['amount'] / gets['amount']).quantize(gets['quantum'], rounding=ROUND_HALF_EVEN)
                            pays['amount'] = pays['amount'].quantize(pays['quantum'], rounding=ROUND_HALF_EVEN)
                            gets['amount'] = gets['amount'].quantize(gets['quantum'], rounding=ROUND_HALF_EVEN)
                            
                            # Insert transaction data into database
                            with cursor_context() as cur:
                                txdate = msg['transaction']['date'] + 946684800
                                records = {
                                    'txid': msg['meta']['TransactionIndex'],
                                    'txhash': msg['transaction']['hash'],
                                    'market': pays['currency'] + gets['currency'],
                                    'currency1': pays['currency'],
                                    'currency2': gets['currency'],
                                    'amount1': pays['amount'],
                                    'amount2': gets['amount'],
                                    'price1': pays['price'],
                                    'price2': gets['price'],
                                    'issuer1': pays['issuer'],
                                    'issuer2': gets['issuer'],
                                    'account1': msg['transaction']['Account'],
                                    'account2': msg['transaction']['Destination'],
                                    'txdate': txdate,
                                    'ledgerindex': msg['ledger_index'],
                                    'historical': False,
                                }
                                sql = (
                                    'INSERT INTO transactions '
                                    '(txid, txhash, market, currency1, currency2, amount1, amount2, '
                                    'price1, price2, issuer1, issuer2, account1, account2, '
                                    'txdate, ledgerindex, historical) '
                                    'VALUES '
                                    '(%(txid)s, %(txhash)s, %(market)s, %(currency1)s, %(currency2)s, '
                                    '%(amount1)s, %(amount2)s, %(price1)s, %(price2)s, '
                                    '%(issuer1)s, %(issuer2)s, %(account1)s, %(account2)s, '
                                    '%(txdate)s, %(ledgerindex)s, %(historical)s)'
                                )
                                cur.execute(sql, records)
                    return True
        return False

    @error_handler(task="Parse message received by websocket")
    def on_message(self, socket, message):
        '''Parse the message received by websocket (expects JSON).'''
        try:
            msg = json.loads(message)
            if msg['type'] == 'response':
                return True
            else:
                return self.parse_tx(msg)
        except Exception as e:
            print "Error, expecting JSON, received", e
            return False

    def on_error(self, socket, error):
        print error

    def on_close(self, socket):
        print "Socket closed"

    @error_handler(task="Subscribe to rippled.")
    def on_open(self, socket):
        '''Subscribe to rippled.'''
        def run(*args):
            subscribe = json.dumps({
                'command': 'subscribe',
                'streams': ['transactions'],
            })
            socket.send(subscribe)
        thread.start_new_thread(run, ())

    @error_handler(task="Transaction listener")
    def listen(self):
        '''Set up a persistent websockets listener for new transactions
        submitted to the Ripple network.
        '''
        if self.socket_url is not None:
            websocket.enableTrace = True
            socket = websocket.WebSocketApp(self.socket_url,
                                            on_message=self.on_message,
                                            on_error=self.on_error,
                                            on_close=self.on_close)
            socket.on_open = self.on_open
            socket.run_forever()

    @error_handler(task="Write trade data to database")
    def save_to_database(self, cur, market, data):
        '''Write trade data to database.'''
        for trade in data:
            records = {
                'txid': trade['tid'],
                'market': market[0] + market[1],
                'currency1': market[0],
                'currency2': market[1],
                'amount1': trade['amount'],
                'price1': trade['price'],
                'txdate': trade['date'],
                'historical': True,
            }
            sql = (
                'INSERT INTO transactions '
                '(txid, market, currency1, currency2, amount1, price1, '
                'txdate, historical, collected) '
                'VALUES '
                '(%(txid)s, %(market)s, %(currency1)s, %(currency2)s, '
                '%(amount1)s, %(price1)s, %(txdate)s, %(historical)s, now())'
            )
            cur.execute(sql, records)

    @error_handler(task="Update plots every 15 minutes")
    def plotloop(self, interval=900):
        '''Update plots every 15 minutes.'''
        update_tickers();
        drop = True
        while True:
            time_remaining = interval - time.time() % interval
            time.sleep(time_remaining)
            dbcleanup()
            plotbuilder = PlotBuilder(currencies=self.currencies, drop=drop)
            plotbuilder.datagrind()
            drop = False

    @error_handler(task="Start collecting data")
    def gather(self):
        '''Start collecting data.'''
        # Reset database
        if self.setup:
            with cursor_context() as cur:
                reset_tx_table()

        # Listen for new transactions
        # print "Listening for new transactions"
        listener = multiprocessing.Process(target=self.listen)
        listener.start()
        
        # Get historical data
        if not self.listen_only:
            if self.charts:
                with cursor_context() as cur:
                    if not self.setup:
                        self.get_last_update(cur)
                # print "Gathering historical data..."
                with cursor_context() as cur:
                    for market in self.get_currency_pairs():
                        # sys.stdout.write(market[0] + market[1] + '\r')
                        # sys.stdout.flush()
                        url = self.base_url + market[0] + '/' + market[1] + self.json_suffix
                        since = 0
                        while True:
                            since += self.num_results
                            data = self.download_trade_data(url + str(since))
                            if not data:
                                self.blacklist.append(market)
                                break
                            self.save_to_database(cur, market, data)
                # print "\nHistorical: ok"
            else:
                parameters = {
                    'socket_url': self.socket_url,
                    'halt': 152370,  # minimum ledger index (genesis ledger)
                    'drop': False,
                    'resume': False,
                    'currencies': self.currencies,
                    'logfile': 'vacuum.log',
                    'verbose': False,
                    'single_ledger': None,
                    'update': True,
                }
                if not self.setup:
                    fwd = Vacuum(**parameters)
                    fwdproc = multiprocessing.Process(target=fwd.vacuum_rippled)
                else:
                    parameters['resume'] = True
                    parameters['update'] = False
                    rev = Vacuum(**parameters)
                    revproc = multiprocessing.Process(target=rev.vacuum_rippled)
                    revproc.start()

        # Rebuild plots every 15 minutes
        self.plotloop()
        listener.join()
        if not self.setup:
            fwdproc.join()
        else:
            revproc.join()
    
def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            short_opts = 'rhldn:b:j:s:'
            long_opts = ['--help']
            opts, vals = getopt.getopt(argv[1:], short_opts, long_opts)
        except getopt.GetoptError as e:
             raise Usage(e)

        import logging
        logger = logging.getLogger('gatherer')
        hdlr = logging.FileHandler('/home/ubuntu/dev.peercover/gatherer.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.WARNING)

        logger.error('log')
        catan = get_tickers();
        for catag in catan:
            logger.error(catag)
        # Default parameter values
        parameters = {
            'setup': False,
            'num_results': 1000,
            'base_url': 'https://ripple.com/chart/',
            'json_suffix': '/trades.json?since=',
            'socket_url': 'wss://s1.ripple.com:51233/',
            'listen_only': False,
            'history_only': False,
            'precision': 28,
            'currencies': get_tickers(),
            'logfile': 'gatherer.log',
            'verbose': True,
        }
        if platform.node() == 'vent':
            parameters['socket_url'] = 'wss://s1.ripple.com:51233/'

        # Parse user-specified parameter values
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                print __doc__
                return 0
            elif opt == '-n':
                try:
                    parameters['num_results'] = int(arg)
                except ValueError:
                    print "Error: number of results must be a number, got", arg
                    print "Defaulting to 1000"
            elif opt == '-b':
                parameters['base_url'] = arg
            elif opt == '-j':
                parameters['json_suffix'] = arg
            elif opt == '-l':
                parameters['listen_only'] = True
            elif opt == '-h':
                parameters['history_only'] = True
            elif opt == '-r':
                parameters['setup'] = True
            elif opt == '-s':
                parameters['socket_url'] = arg

        # Gather data
        gatherer = Gatherer(**parameters)
        gatherer.gather()

    except Usage as e:
        print >>sys.stderr, e.msg
        print >>sys.stderr, "for help use --help"
        return 2

if __name__ == '__main__':
    sys.exit(main())

import psycopg2
import csv
from contextlib import contextmanager
import time

@contextmanager
def cursor_context():
    '''Database cursor generator: handles connection, commit, rollback,
    error trapping, and closing connection.  Will propagate exceptions.
    '''
    try:
        conn = None
        with open('pgparams.csv') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            params = list(reader)[0]
        conn = psycopg2.connect(host=params[0], user=params[1],
                                password=params[2], database=params[3],
                                port=params[4])
        if not conn or conn.closed:
            attempt = 1
            while attempt < 5 and (not conn or conn.closed):
                print "Attempt:", attempt
                time.sleep(5)
                attempt += 1
                conn = psycopg2.connect(host=params[0], user=params[1],
                                        password=params[2], database=params[3],
                                        port=params[4])
        if not conn or conn.closed:
            raise Exception("Database connection failed after %d attempts." % attempt)
        cur = conn.cursor()
        yield cur
    except (psycopg2.Error, Exception) as err:
        if conn:
            conn.rollback()
            conn.close()
        print err.message
        raise
    else:
        conn.commit()
        conn.close()

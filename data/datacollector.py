import os
from evtlog import EvtLog
from logging import INFO

class DataCollector(object):

    def __init__(self, logfile, verbose):
        self.logger = None
        self.logfile = logfile
        self.verbose = verbose
        # self.setup_log()

    def setup_log(self):
        try:
            if not os.path.isdir(self.logfile) and not os.path.isfile(self.logfile):
                self.logfile = None
            self.logger = EvtLog.getLogger(file_or_path=self.logfile,
                                           show_messages=self.verbose)
            assert self.logger
            self.logger.log_message(INFO, self.__class__.__name__ + " started")
        except AssertionError as e:
            raise Exception("Unable to create log file: " + e.message)
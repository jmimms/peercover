from decimal import *
from pg_cursor_context import cursor_context

def parse_tx(tx, accepted, ledger_time=None, currencies=None, past=False, tx_hash=None):
    '''Parses Ripple transactions from rippled.  Checks for payment
    transactions which are applied to an existing offer on the network,
    then extracts the relevant data from these transactions and inserts
    the data into the 'transactions' table in a postgres database.
    '''
    stored_tx_count = 0
    getcontext().prec = 28
    if tx['TransactionType'] == 'Payment' and 'meta' in tx and tx['meta']['TransactionResult'] == 'tesSUCCESS':
        for affected_node in tx['meta']['AffectedNodes']:
            if 'ModifiedNode' in affected_node:
                node = affected_node['ModifiedNode']
            elif 'DeletedNode' in affected_node:
                node = affected_node['DeletedNode']
            else:
                continue
            is_offer = node['LedgerEntryType'] == 'Offer'
            has_prev = 'PreviousFields' in node
            if has_prev:
                has_pays = 'TakerPays' in node['PreviousFields']
                has_gets = 'TakerGets' in node['PreviousFields']
            if is_offer and has_prev and has_pays and has_gets:

                # Offer parameters before and after the transaction
                previous = node['PreviousFields']
                final = node['FinalFields']

                # Check if the trade is in XRP or another currency
                adjust_xrp = 10**6
                if 'currency' in final['TakerGets']:
                    gets = {
                        'currency': final['TakerGets']['currency'],
                        'amount': Decimal(previous['TakerGets']['value']) - Decimal(final['TakerGets']['value']),
                        'issuer': final['TakerGets']['issuer'],
                    }
                else:
                    gets = {
                        'currency': 'XRP',
                        'amount': (Decimal(previous['TakerGets']) - Decimal(final['TakerGets'])) / adjust_xrp,
                        'issuer': None,
                    }
                
                if 'currency' in final['TakerPays']:
                    pays = {
                        'currency': final['TakerPays']['currency'],
                        'amount': Decimal(previous['TakerPays']['value']) - Decimal(final['TakerPays']['value']),
                        'issuer': final['TakerPays']['issuer'],
                    }
                else:
                    pays = {
                        'currency': 'XRP',
                        'amount': (Decimal(previous['TakerPays']) - Decimal(final['TakerPays'])) / adjust_xrp,
                        'issuer': None,
                    }

                # Make sure the currency pair is ordered correctly
                if currencies is not None:
                    gets_idx, pays_idx = None, None
                    for i, curr in enumerate(currencies):
                        if curr == gets['currency'] and curr == pays['currency']:
                            break
                        if curr == gets['currency']:
                            gets_idx = i
                        if curr == pays['currency']:
                            pays_idx = i
                        if gets_idx is not None and pays_idx is not None:
                            break
                    if gets_idx < pays_idx:
                        temp = gets
                        gets = pays
                        pays = temp

                if gets['amount'] > 0 and pays['amount'] > 0:

                    # Round to 2 decimal places for NXT, and 8 decimal
                    # places for any other currency
                    pays['quantum'] = Decimal('.01') if pays['currency'] == 'NXT' else Decimal('.00000001')
                    pays['price'] = (gets['amount'] / pays['amount']).quantize(pays['quantum'], rounding=ROUND_HALF_EVEN)
                    gets['quantum'] = Decimal('.01') if gets['currency'] == 'NXT' else Decimal('.00000001')
                    gets['price'] = (pays['amount'] / gets['amount']).quantize(gets['quantum'], rounding=ROUND_HALF_EVEN)
                    pays['amount'] = pays['amount'].quantize(pays['quantum'], rounding=ROUND_HALF_EVEN)
                    gets['amount'] = gets['amount'].quantize(gets['quantum'], rounding=ROUND_HALF_EVEN)
                    
                    # Insert transaction data into database
                    with cursor_context() as cur:
                        txdate = None if ledger_time is None else ledger_time + 946684800
                        records = {
                            'txid': tx['meta']['TransactionIndex'],
                            'txhash': tx_hash,
                            'market': pays['currency'] + gets['currency'],
                            'currency1': pays['currency'],
                            'currency2': gets['currency'],
                            'amount1': pays['amount'],
                            'amount2': gets['amount'],
                            'price1': pays['price'],
                            'price2': gets['price'],
                            'issuer1': pays['issuer'],
                            'issuer2': gets['issuer'],
                            'account1': final['Account'],
                            'txdate': txdate,
                            'ledgerindex': tx['ledger_index'],
                            'historical': past,
                            'accepted': accepted,
                        }
                        sql = (
                            'INSERT INTO transactions '
                            '(txid, txhash, market, currency1, currency2, amount1, amount2, '
                            'price1, price2, issuer1, issuer2, account1, '
                            'txdate, ledgerindex, historical, accepted, collected) '
                            'VALUES '
                            '(%(txid)s, %(txhash)s, %(market)s, %(currency1)s, %(currency2)s, '
                            '%(amount1)s, %(amount2)s, %(price1)s, %(price2)s, '
                            '%(issuer1)s, %(issuer2)s, %(account1)s, '
                            '%(txdate)s, %(ledgerindex)s, %(historical)s, %(accepted)s, now())'
                        )
                        cur.execute(sql, records)
                        stored_tx_count += 1
    return stored_tx_count

def reset_tx_table():
    '''Drop and re-create the transactions table.'''
    queries = (
        'DROP TABLE IF EXISTS transactions',
        (
            'CREATE TABLE transactions ('
            'pcvid SERIAL PRIMARY KEY,'
            'txid INTEGER,'
            'txhash VARCHAR(1000),'
            'market CHAR(6),'
            'currency1 CHAR(3),'
            'currency2 CHAR(3),'
            'price1 DECIMAL(32,8),' 
            'price2 DECIMAL(32,8),'
            'amount1 DECIMAL(32,8),'
            'amount2 DECIMAL(32,8),'
            'issuer1 VARCHAR(1000),'
            'issuer2 VARCHAR(1000),'
            'account1 VARCHAR(1000),'
            'account2 VARCHAR(1000),'
            'txdate BIGINT,'
            'ledgerindex BIGINT,'
            'historical BOOLEAN,'
            'accepted BOOLEAN,'
            'collected TIMESTAMP)'
        ),
    )
    for query in queries:
        with cursor_context() as cur:
            cur.execute(query)
    print "Reset transactions table"

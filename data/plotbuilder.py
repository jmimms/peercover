#!/usr/bin/env python
"""
Data preprocesor to make Peercover's plots really blaze.

(c) Jack Peterson (jack@tinybike.net), Peercover Inc., 3/2/2014
"""
import sys, getopt, csv
from decimal import *
import psycopg2
from usage import Usage
from tickers import get_tickers
import pandas as pd
import pandas.io.sql as psql
import numpy as np
from datacollector import DataCollector
from evtlog import error_handler

pd.set_option('display.width', 500)

class PlotBuilder(DataCollector):
    '''
    Gets data ready for plotting @ Peercover
    '''
    def __init__(self, currencies=None, drop=False):
        super(PlotBuilder, self).__init__('plotbuilder.log', True)
        self.currencies = currencies
        self.drop = drop
        self.log = 'plotbuilder.log'
        self.updates = 0

    @error_handler(task="currency_pairs")
    def currency_pairs(self):
        for i, currency in enumerate(self.currencies):
            for other_currency in self.currencies[i+1:]:
                yield (currency, other_currency)

    @error_handler(task="db_connect")
    def db_connect(self):
        conn = None
        with open('pgparams.csv') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            params = list(reader)[0]
        conn = psycopg2.connect(host=params[0], user=params[1],
                                password=params[2], database=params[3],
                                port=params[4])
        return conn

    @error_handler(task="resampler")
    def resampler(self, df, freq='D'):
        df.txdate = pd.to_datetime(df.txdate, unit='s')
        df = df.set_index(df.txdate)
        rs = [df.price1.resample(freq, how='ohlc'),
              df.price2.resample(freq, how='ohlc')]
        for i, r in enumerate(rs):
            idx = str(i + 1)
            rs[i] = r.join(
                df['amount'+idx].resample(freq, how='sum'), on=r.index).join(
                df['price'+idx].resample(freq, how='median'), on=r.index
            )
        rs = rs[0].join(rs[1], on=rs[0].index, lsuffix=1, rsuffix=2)
        rs.index = rs.index.astype(np.int64) // 10**9
        return rs

    @error_handler(task="write_resampled")
    def write_resampled(self, rs, market, cur, freq='D'):
        nrows = rs.shape[0]
        for i in xrange(nrows):
            row = [rs.index[i], freq, market[0], market[1]]
            vals = rs[i:i+1].values.flatten().tolist()
            if np.isnan(np.sum(vals)):
                continue
            vals = [Decimal(v).quantize(Decimal('.00000001'), 
                    rounding=ROUND_HALF_EVEN) for v in vals]
            row.extend(vals)
            row.extend([False])
            valuestr = ','.join(['%s'] * len(row))
            query = (
                "INSERT INTO resampled "
                "(starttime, freq, currency1, currency2, open1, high1, low1, "
                "close1, volume1, medprice1, open2, high2, low2, close2, "
                "volume2, medprice2, synthetic) "
                "VALUES (%s)"
            ) % valuestr
            cur.execute(query, tuple(row))
            self.updates += 1

    @error_handler(task="setup_resampled_table")
    def setup_resampled_table(self, cur):
        queries = (
            "DROP TABLE IF EXISTS resampled",
            (
                "CREATE TABLE resampled ("
                "starttime BIGINT,"
                "freq VARCHAR(10),"
                "currency1 CHAR(3),"
                "currency2 CHAR(3),"
                "open1 DECIMAL(32,8),"
                "high1 DECIMAL(32,8),"
                "low1 DECIMAL(32,8),"
                "close1 DECIMAL(32,8),"
                "volume1 DECIMAL(32,8),"
                "medprice1 DECIMAL(32,8),"
                "open2 DECIMAL(32,8),"
                "high2 DECIMAL(32,8),"
                "low2 DECIMAL(32,8),"
                "close2 DECIMAL(32,8),"
                "volume2 DECIMAL(32,8),"
                "medprice2 DECIMAL(32,8),"
                "synthetic BOOLEAN)"
            ),
        )
        for query in queries:
            cur.execute(query)

    @error_handler(task="datagrind")
    def datagrind(self):
        freqs = ('D', '8H', '4H', '2H', 'H', '30T', '15T')
        try:
            conn = self.db_connect()
            if conn and not conn.closed:
                cur = conn.cursor()
                if self.drop:
                    self.setup_resampled_table(cur)
                    conn.commit()
                else:
                    cur.execute("SELECT max(starttime) FROM resampled")
                    if cur.rowcount:
                        last_resample = str(cur.fetchone()[0])
                    else:
                        last_resample = 0
                        self.setup_resampled_table(cur)
                        conn.commit()
                # print "PlotBuilder: Resampling time series..."
                for market in self.currency_pairs():
                    # sys.stdout.write("PlotBuilder: " + market[0] + " " + market[1] + "\r")
                    # sys.stdout.flush()

                    # Resample all transactions
                    if self.drop:
                        query = (
                            "SELECT currency1, currency2, price1, price2, "
                            "amount1, amount2, txdate FROM transactions "
                            "WHERE market = '%s' "
                            "ORDER BY txdate"
                        ) % (market[0] + market[1])

                    # Resample transactions from the last resampling
                    # starting timestamp or newer
                    else:
                        query = (
                            "SELECT currency1, currency2, price1, price2, "
                            "amount1, amount2, txdate FROM transactions "
                            "WHERE market = '%s' AND txdate >= '%s' "
                            "ORDER BY txdate"
                        ) % ((market[0] + market[1]), last_resample)
                    df = psql.frame_query(query, conn)
                    if not df.empty:
                        for f in freqs:
                            rs = self.resampler(df, freq=f)
                            self.write_resampled(rs, market, cur, freq=f)
                        conn.commit()
                print
                print "*** PlotBuilder:", self.updates, "records updated ***"
                print
        except (psycopg2.Error, Exception) as e:
            if conn:
                conn.rollback()
                conn.close()
            with open(self.log) as logfile:
                print >>logfile, "psycopg2 error: " + e.message
        else:
            conn.commit()
            conn.close()

        # Index the resampling table


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], 'hd', ['help', '--drop'])
        except getopt.GetoptError as e:
             raise Usage(e)
        parameters = {
            'currencies': get_tickers(),
            'drop': False,
        }
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                print __doc__
                return 0
            elif opt in ('-d', '--drop'):
                parameters['drop'] = True
        plotbuilder = PlotBuilder(**parameters)
        plotbuilder.datagrind()
    except Usage as e:
        print >>sys.stderr, e.msg
        print >>sys.stderr, "for help use --help"
        return 2

if __name__ == '__main__':
    sys.exit(main())

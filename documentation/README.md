bitinsure

Command-line Deployment to Heroku:
Make sure you are in  /492995 directory 
git add . | git commit -m "bitinsure" | git push bitinsure master

Technical Plan:

________________________________________________________________________________
Signature Pad
(for storing signatures from pool members)

http://thomasjbradley.ca/lab/signature-pad/

________________________________________________________________________________
MongoDB

mongodb://peercover:67rhjd3hygGGG6783hjk67438y87483HGJKHGFhhg63hdh37dhd3ud3ddgL@linus.mongohq.com:10016/app15655493
peercover
67rhjd3hygGGG6783hjk67438y87483HGJKHGFhhg63hdh37dhd3ud3ddgL

________________________________________________________________________________
Amazon S3:

jmimms@dycomb.com
Cat@102089

Node.js interfacing - https://aws.amazon.com/sdkfornodejs/

Access Key ID: AKIAJGFZ242LL75BQDEA

Secret Access Key: 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

Root for static files: https://s3-us-west-2.amazonaws.com/dycomb/

________________________________________________________________________________
Heroku:

jmimms@dycomb.com
Cat@102089

Command-line Deployment to Heroku:
Make sure you are in  /492995 directory 
git add . | git commit -m "bitinsure" | git push bitinsure master

Setting up Heroku client on Cloud9 for the first time:
wget http://assets.heroku.com/heroku-client/heroku-client.tgz
tar xzfv heroku-client
cd heroku-client
PATH=$PATH:$PWD
git remote add bitinsure git@heroku.com:bitinsure.git

SSH Key: ssh-rsa AAAAB3NzaC...afFg4x+8wJ jmimms@dycomb.com

API Key: b2540cb7-0c00-44b2-942b-1ef301e15c4e

Node.js deployment, workers, databases, and more:
https://devcenter.heroku.com/articles/nodejs

Heroku S3 Environmental Variable Addition:
https://devcenter.heroku.com/articles/s3

Deployed on:
http://bitinsure.herokuapp.com/

Eventual URL:
http://peercover.com/
(email server still pointing to 98.175.108.40)

Environmental Variable Setup: https://devcenter.heroku.com/articles/s3

________________________________________________________________________________
ClearDB:

Dashboard: https://www.cleardb.com/dashboard

URL: mysql://b994afe9a559ac:d2884491@us-cdbr-east-03.cleardb.com/heroku_cf54c28cea5c078?reconnect=true

Database: heroku_9907703b3f00cab

??? - heroku_cf54c28cea5c078
User: b994afe9a559ac
Password: d2884491

ClearDB SSL and Heroku Commands:
https://devcenter.heroku.com/articles/cleardb

________________________________________________________________________________
Twilio:

jmimms@dycomb.com
Cat@102089

ACCOUNT SID: AC50e14362af8e250d6427e7bfd569a407
AUTH TOKEN: 9fb4d7e308061fff085a420c02f7eeda

Connect Apps so Merchants pay:
https://www.twilio.com/user/account/connect/apps

Node.js helper library: https://www.twilio.com/docs/node/install

REST API: https://www.twilio.com/docs/api/rest

________________________________________________________________________________
Ripple:

Secret Account Key: ssR1fFkCENgekb7yeWjRZt5TNoXje
Public Address: rfCQbQEFScab9ApovCZwNnJUEdEYqZxJAJ

Mimms
Rippler102089
********************************************************************************
Secret Account Key: spkCJXz4woAC1nwGQzwYN5DcHYGJA
Public Address: rMXfbtfwi7gPvGyMFMbo5oaB6nksoynJq1

Mimms2
Rippler102089
********************************************************************************
Secret Account Key: shFznnupvMTRG1WjatWKziC8QjpyW
Public Address: rG8sw8hgtm8bCPYPBuceugj8nEcMLT2Wsp

Dycomb
DycombicRippler102089

________________________________________________________________________________
GoDaddy:

42159551
CCat@102089

________________________________________________________________________________
Bootstrap:
http://twitter.github.io/bootstrap/
S3 directory: https://s3-us-west-2.amazonaws.com/dycomb/bootstrap/

________________________________________________________________________________
Templating:
Going for Jade over ejs - better syntax

________________________________________________________________________________
How to send/check bitcoin:

blockchain-wallet is not enough
http://bitcoin.stackexchange.com/questions/7855/sending-bitcoins-programatically-without-running-a-node
https://github.com/brainwallet/brainwallet.github.com/blob/master/js/brainwallet.js
http://brainwallet.org/#tx
POST to: http://blockchain.info/pushtx
May be able to subscribe to:
http://blockchain.info/api/api_websocket
and detect successful transactions

________________________________________________________________________________
Closure Compiler:

(keep the compiler commands here for all JS)
(attempt to avoid all static JS file imports)
http://closure-compiler.appspot.com/home


See https://npmjs.org/package/package
for package downloads

________________________________________________________________________________
BitPay:

jmimms@dycomb.com
Bitter@102089
Bitter@102089

________________________________________________________________________________
Schedule:

JBM 5/15 12:00AM
    > -Work out and establish database schemas for wallet loss and theft insurance 
    >  terms, pools (groups), users, wallets
    -Establish backbone.js validation models and urls for synchronizing working 
     models to database
        - in progress
    -create basic, barebone view templates for homepage, account signup 
     (utilizing everyauth for basic identity verification), pool management, pool
     creation, member recommendation form, new member inbox, claim inbox, 
     claim file and attachments with bootstrap hosted at 
     https://s3-us-west-2.amazonaws.com/dycomb/bootstrap/
        - in progress
    > -create wallet storage and use safety/security checklist and guide template for 
    >  insurance contract in order to get pool members on same page in terms of 
    >  security and provide standardized criteria for denial of at-fault claims
        - export all wiki information here: https://en.bitcoin.it/wiki/Securing_your_wallet
          checkbox "I have read and will abide by wallet security standards"
        - next to each wallet and amount you wish to insure two check boxes
          designate:
          -hot wallet
          -cold wallet https://en.bitcoin.it/wiki/Cold_storage
            -cold wallet reduced risk factored into proprietary algorithm
                -may change over time when we get better theft/loss data
                -risk may not even come into play with distributed insurance
        - still need Ripple-specific agreements
    > -work out basic algorithms for wallet loss claim distribution with different 
    >  weight option inputs, maximum claim option, and risk adjustment/reweighting 
    >  in response to claims which represent increased risk
        - proprietary trade secret - to be included as final step of build

________________________________________________________________________________
Subprojects:

The Red List:

Abstract:

In the case of theft money will move out of the wallet. If the pool trusts the 
person it will approve the claim. We can track that stolen money across the 
blockchain or ledger, create a public red list of addresses, and flag that money 
as it is transferred. Basically a public record of theft. If directly used to 
buy anything or at an exchange, we can actually catch the thief if legal 
businesses choose to honor our red list. If the thief uses a mixing service, 
we will flag that mixing service as a money launderer and will flag wallets the 
funds ultimately get mixed to, disincentiving users of mixing services 
(because you could inadvertently receive stolen funds and actually feel the 
consequences). Even if we are facing a professional money launderer and cannot 
directly catch a one-time thief, we can establish patterns which will allow us 
to prevent offenders from conducting transactions with legitimate businesses, 
forcing them underground.

Features:

Blockchain/Ledger crawling. 
Data retrieval and storage relating to transmittance of stolen BTC/Ripple.
    -follow transfers of data from primary theft wallet
Automated reporting, storage, and management of flagged addresses.
    -when reports of purchases and accompanying identifying data come through 
    API assign to transfer paths and create custom reports under linked to custom 
    URLs with the ability to forward to relevant authorities
        -sort by specific theft, public addresses, identifying data (multiple 
        thefts/theft purchases)

Storage Fields: simple depth of connection to theft beginning with flagged 
address (1-infinite)**, amount (BTC,XRP), public address, date, block info
    **later on even if mixed and extensively laundered we may be able to detect
    spending patterns from individuals repeatedly closer in the theft chain than
    average if personal information is reported by legitimate companies and
    services via our API on more than one occasion
        **dumb thieves may not mix and we can prosecute if primary or secondary
        theft connection and reported by legitimate business or service
        via our API
        **theft threshold for investigation alpha is (0.05) for statistical
        significance in the 5%
        **repeat criminals may get picked up by our algorithm if use same 
        identity or address to buy things from legitimate businesses who report
        via our API even after extensive laundering (if depth of connection is 
        on average closer to 1 (i.e. depth of connection is 300 and average is 
        1000)) or on average much greater than average transaction depth of 
        connection (went overboard on laundering)
    **we will gather data necesarry to prosecute and take down mixing/laundry
    services using these methods

The Red List API:

Abstract:

Eventually we will get more sophisticated and even provide an API with which to 
check wallets against our Red List. Legal businesses could perform a simple API 
query to know if the wallet they choose to accept funds from is associated with 
stolen goods (first degree, second degree, etc). We could even go so far as to 
allow the legal merchant to send information such as address and name back to 
the registry after detecting a first or second degree red flag attempted wallet 
transaction in order to tie identities to Red List entries.

Features:

Pinging addresses against Red List for anti-theft/anti-fraud verification by 
legitimate businesses and services. 
    -return simple flagged/safe json response if matched against a low-degree 
    theft connection
Submit to API identifying information in standardized format json packet if 
flagged returned by APi ping
    -wallet public address
    -delivery/shipping address
    -name
    -date
    -product/service attempted purchase
    -phone number
    -email
    -spend amount attempt
    

Wolfgang's log
--------------
*) Q: everyauth or passport? A datz: passport
*) Q: more jade or more dynamic javascript? A datz: more javascript
*) Q: should we use !!! (html5) at top of .jade files? A datz: use !!!/html5
*) Attributions:
   Stephen Braitsch, http://node-login.braitsch.io/ (MIT license)

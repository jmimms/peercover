{
    "name": "peercover",
    "version": "0.0.1",
    "dependencies": {
        "underscore": "1.4.x",
        "connect": "2.x.x",
        "express": "3.1.x",
        "mysql": "2.0.x",
        "s3": "0.1.x",
        "twilio": "1.1.x",
        "twilio-heroku": "0.3.x",
        "sessions": "0.x.x",
        "stylus": "0.32.x",
        "heroku": "0.x.x",
        "crypto-js": "3.1.x",
        "backbone": "1.0.x",
        "jade": "0.30.x",
        "ripple-lib": "0.x.x",
        "authnet": "0.0.x",
        "geoip": "0.x.x",
        "country-data": "0.0.x",
        "money": "0.1.x",
        "blockchain-wallet": "0.0.x",
        "blockchain": "x.x.x",
        "packmule": "1.1.x",
        "passport": "0.1.x",
        "passport-facebook": "0.1.x",
        "passport-dwolla": "0.1.x",
        "passport-linkedin": "0.1.x",
        "passport-local": "0.1.x",
        "backbone-validation": "0.7.x",
        "backbone-mysql": "0.2.x",
        "twitter-bootstrap": "2.1.x",
        "xmlhttprequest": "1.5.x",
        "jsdom": "0.6.x",
        "jQuery": "1.7.x",
        "nodemailer": "0.4.x",
        "facebook-api": "0.1.x",
        "dwolla": "0.1.x",
        "linkedin-js": "0.1.x",
        "mongojs": "0.7.x",
        "redis": "0.8.x",
        "mongoose": "3.6.x",
        "connect-flash": "0.1.x",
        "request": "2.x.x",
        "connect-bruteforce": "x.x.x",
        "aws2js": "0.8.x",
        "socket.io": "0.9.x"
    },
    "engines": {
        "node": "0.10.x",
        "npm": "1.2.x"
    }
}
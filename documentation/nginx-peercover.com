map $http_upgrade $upgrade_requested { default Upgrade; '' close; } 

upstream upstream_peercover {
    server 127.0.0.1:4545;
    keepalive 32;
}
server { 
    listen 443 ssl;
    ssl_certificate    /home/ubuntu/EV_SSL/ssl-bundle.crt; 
    ssl_certificate_key     /home/ubuntu/EV_SSL/server.key; 
    add_header Strict-Transport-Security max-age=31536000;

    server_name peercover.com;
    location / { 
        proxy_pass http://upstream_peercover;
        proxy_redirect off; 
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $upgrade_requested; 
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https; 
        proxy_read_timeout 86400;
    } 
}
server { 
    listen 443 ssl;
    ssl_certificate    /home/ubuntu/EV_SSL/ssl-bundle.crt; 
    ssl_certificate_key     /home/ubuntu/EV_SSL/server.key; 
    add_header Strict-Transport-Security max-age=31536000;

    server_name www.peercover.com;
    return 301 https://peercover.com$request_uri;
}
# redirects from peercover.com and www.peercover.com to https://peercover.com
server {
    listen 80;
    server_name peercover.com www.peercover.com;
    return 301 https://peercover.com$request_uri;
}

# dev.peercover.com

upstream upstream_dev {
    server 127.0.0.1:51299;
    keepalive 32;
}
server {
    listen 80;
    server_name dev.peercover.com;
    location / {
        proxy_pass http://upstream_dev;
        proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Host $host ;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $upgrade_requested;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto http;
        proxy_read_timeout 86400;
    }
}

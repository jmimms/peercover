# PeerCover ![Build Status](https://secure.travis-ci.org/joyent/node.png)
## Fixing insurance one line at a time.
===

## Git quirks:

### Before you begin working please 
    git pull bitbucket master
    
### If you delete files run
    git rm <file>
    
### To add, commit, and push after setting up remote git repositories:
    git add . ; git commit -m "peercover" ; git push bitbucket master ; git push heroku master

### To debug application based on console.log(); view Heroku log with:
    heroku logs -t

## Old README.md:

[README.md](https://bitbucket.org/peercover/peercover/src/76e2c09001c0d01987a6fbd50788050760497da7/documentation/README.md?at=master)

## To join Cloud9 IDE:

### visit [https://c9.io/datz/bitinsure](https://c9.io/datz/bitinsure) and wait for approval

## To deploy to Heroku from Cloud9:

    enter /492995 directory 
    git add . ; git commit -m "peercover" ; git push heroku master
    
## To push to BitBucket:

    git add . ; git commit -m "peercover" ; git push bitbucket master
    
### please push to BitBucket and Heroku simultaneously with:

    git push bitbucket master ; git push heroku master
        
## To request invite to BitBucket and Heroku:

### email [jmimms@dycomb.com](jmimms@dycomb.com) or [wspraul@q-ag.de](wspraul@q-ag.de)
    
## To implement signatures for terms:

### [http://thomasjbradley.ca/lab/signature-pad/](http://thomasjbradley.ca/lab/signature-pad/)

## To connect to MongoDB:

    mongodb://peercover:67rhjd3hygGGG6783hjk67438y87483HGJKHGFhhg63hdh37dhd3ud3ddgL@linus.mongohq.com:10016/app15655493peercover
    67rhjd3hygGGG6783hjk67438y87483HGJKHGFhhg63hdh37dhd3ud3ddgL
    
## To connect to Amazon S3:

### jmimms@dycomb.com
### Cat@102089

### Node.js interfacing - [https://aws.amazon.com/sdkfornodejs/](https://aws.amazon.com/sdkfornodejs/)
### Access Key ID: AKIAJGFZ242LL75BQDEA
### Secret Access Key: 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
### Root for static files: https://s3-us-west-2.amazonaws.com/dycomb/
    
## To work with Heroku:

### Setting up Heroku client on Cloud9 for the first time:
    wget http://assets.heroku.com/heroku-client/heroku-client.tgz
    tar xzfv heroku-client
    cd heroku-client
    PATH=$PATH:$PWD
    git remote add heroku git@heroku.com:peercover.git

### Displaying keys: heroku keys

### API Key: b2540cb7-0c00-44b2-942b-1ef301e15c4e

### Node.js deployment, workers, databases, and more:
[https://devcenter.heroku.com/articles/nodejs](https://devcenter.heroku.com/articles/nodejs)

### Heroku S3 Environmental Variable Addition:
[https://devcenter.heroku.com/articles/s3](https://devcenter.heroku.com/articles/s3)

### Deployed on:
[http://peercover.herokuapp.com/](http://peercover.herokuapp.com/)

### Eventual URL:
[http://peercover.com/](http://peercover.com/)
(email server still pointing to 98.175.108.40)

### Environmental Variable Setup: https://devcenter.heroku.com/articles/s3
    
## To work with ClearDB:

### Dashboard: [https://www.cleardb.com/dashboard](https://www.cleardb.com/dashboard)

### URL: mysql://b994afe9a559ac:d2884491@us-cdbr-east-03.cleardb.com/heroku_cf54c28cea5c078?reconnect=true

### Database: heroku_9907703b3f00cab

### ??? - heroku_cf54c28cea5c078
### User: b994afe9a559ac
### Password: d2884491

### ClearDB SSL and Heroku Commands: 
[https://devcenter.heroku.com/articles/cleardb](https://devcenter.heroku.com/articles/cleardb)
    
## To work with Twilio:

### jmimms@dycomb.com
### Cat@102089

### ACCOUNT SID: AC50e14362af8e250d6427e7bfd569a407
### AUTH TOKEN: 9fb4d7e308061fff085a420c02f7eeda

### Connect apps so merchants pay:
[https://www.twilio.com/user/account/connect/apps](https://www.twilio.com/user/account/connect/apps)

### Node.js helper library: 
[https://www.twilio.com/docs/node/install](https://www.twilio.com/docs/node/install)

### REST API: 
[https://www.twilio.com/docs/api/rest](https://www.twilio.com/docs/api/rest)
    
## Ripple testing wallets:
________________________________________________________________________________
    Secret Account Key: ssR1fFkCENgekb7yeWjRZt5TNoXje
    Public Address: rfCQbQEFScab9ApovCZwNnJUEdEYqZxJAJ
    
    Mimms
    Rippler102089
________________________________________________________________________________
    Secret Account Key: spkCJXz4woAC1nwGQzwYN5DcHYGJA
    Public Address: rMXfbtfwi7gPvGyMFMbo5oaB6nksoynJq1
    
    Mimms2
    Rippler102089
________________________________________________________________________________
    Secret Account Key: shFznnupvMTRG1WjatWKziC8QjpyW
    Public Address: rG8sw8hgtm8bCPYPBuceugj8nEcMLT2Wsp
    
    Dycomb
    DycombicRippler102089
________________________________________________________________________________

## GoDaddy information:

### 42159551
### CCat@102089
    
## PeerCover domain registration (courtesy of boomboom):

### wezelvis
### p33rC0v3r

## dnssimple login information (naked domain, SSL enable):
    jmimms@dycomb.com
    cat102089

## SSL/dnssimple
[http://www.jonathandean.com/2012/10/properly-handling-naked-domains-and-ssl-on-heroku/](http://www.jonathandean.com/2012/10/properly-handling-naked-domains-and-ssl-on-heroku)
    
## Bitcoin:
    
### bitcoinjs not working for Heroku v0.10.9
### not enough permissions on Cloud9 - need to reconfigure

### How to send/check bitcoin:
### blockchain-wallet is not enough
[http://bitcoin.stackexchange.com/questions/7855/sending-bitcoins-programatically-without-running-a-node](http://bitcoin.stackexchange.com/questions/7855/sending-bitcoins-programatically-without-running-a-node)
    https://github.com/brainwallet/brainwallet.github.com/blob/master/js/brainwallet.js
    http://brainwallet.org/#tx
    POST to: http://blockchain.info/pushtx
    
### May be able to subscribe to:
    
    http://blockchain.info/api/api_websocket
    and detect successful transactions
    
## Closure compiler:

### (keep the compiler commands here for all JS)
### (attempt to avoid all static JS file imports)
### [http://closure-compiler.appspot.com/home](http://closure-compiler.appspot.com/home)
    
## BitPay:

### jmimms@dycomb.com
### Bitter@102089

Resources for Newcomers
---
  - [The Wiki](https://bitbucket.org/peercover/peercover/wiki/Home)
  - [Issues](https://bitbucket.org/peercover/peercover/issues)
  - [PeerCover mailing list](http://groups.google.com/group/peercover)
  - irc chatroom, [#PeerCover on freenode.net](http://webchat.freenode.net?channels=peercover&uio=d4)


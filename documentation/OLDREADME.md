groupAdmins
    foreign_key -> memberid
    foreign_key -> groupid

//each group only has one set of terms at a given moment

lossGroups
    groupid
    creation_date
    update_date
    vest_date
    over_date
    foreign_key -> terms

//terms may not be defined purely by database cells but by uploaded/google doc
integrated contract - looking for a better solution than Google docs so we 
can allow for collaborative contract editing directly in bitinsure

terms
    foreign_key -> lossGroups
    creation_date
    update_date
    update_agreed
    update_declined
    vest_date
    over_date
    contract_url
    agreed
    declined
    currency_translator
    maximum_insured
    minimum_insured
    claim_weight_algorithm

//what members agreed to what terms when
//must flag term agreements and notify member/prompt for agreement if 
  agree_date of memberTerms < update_date of terms
//signature is json packet stored from http://thomasjbradley.ca/lab/signature-pad/

members
    username
    password
    email
    join_date
    account_active
    first_name (optional)
    last_name (optional)
    date_of_birth (optional)
    profile_picture (optional)
    credit_report_url (optional)
    identity_proof_url (optional scanned passport, birth certificate, driver's 
                        license)
    confirmed (0|1)
    
//make sure any information for everyauth is stored for each member
  
memberTerms
    foreign_key -> members
    foreign_key -> terms
    signature
    agree_date 
    
//keep group ids unique between group columns

memberGroups
    foreign_key -> members
    foreign_key -> lossGroups
    approved
    denied

//for storing/creating Ripple wallets specifically designed for insurance purposes

rippleInsuranceWallets
    account(default: same as member)
    password(default: same as member)
    public_address
    secret_account_key
    ripple_balance (optional)

//may need other tables here such as IOUs, transaction history, ledger transactions,
block chain transactions
//need to think through IOU insurance
    
rippleInsuredWallets
    foreign_key -> members
    public_address
    signature
    ripple_balance
    
//debating whether or not to allow Bitcoin wallets - will not send/receive for them
or use them for claim puposes but probably could use public address/signature for 
anti-fraud purposes

bitcoinInsuredWallets
    foreign_key -> members
    public_address
    signature
    btc_balance
    
insureInstance
    foreign_key -> members
    foreign_key -> lossGroups
    foreign_key -> rippleInsuredWallets
    foreign_key -> bitcoinInsuredWallets
    currency
    insured_amount
    group_weighting (determined by selected algorithm)
    
claims
    foreign_key -> members
    foreign_key -> lossGroups
    foreign_key -> rippleInsuredWallets
    foreign_key -> bitcoinInsuredWallets
    date_filed
    date_approved
    date_declined
    approved
    denied
    date_misfortune
    evidence_description
    claim_total (need the ability to renogotiate claim total in any currency)
    
video
    foreign_key -> claims
    url
    description
    date

images
    foreign_key -> claims
    url
    description
    date
    
documents
    foreign_key -> claims
    url
    description
    date

All information salted with:
rJ/W^3qdnAfi\N;lzT{p$-a(UG'LQAE}MJ$iB_/YzUo@F{DG>;(H7cjXJh7#IbZ

+ data +

OxvUTiapa6e3bsIibtbp3OiPkb6jGi3Vc9A8RR7cAiYCrJgRcie7LHFbuasD7kA

Then, encrypted with AES (need to generate keys for crypto-js).
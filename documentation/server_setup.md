Amazon EC2 instance, US West (Oregon)
  Type m1.xlarge
  Current Instance (2013-09-02)
    Zone us-west-2a
    Instance ID i-d33fc6e4
    Public DNS ec2-54-213-206-120.us-west-2.compute.amazonaws.com
    Public IP 54.213.206.120

DNS
  Pointing to the EC2 IP:
    peercover.com
    s1.peercover.com
    dev.peercover.com

ssh -i private_key.pem ubuntu@peercover.com

* maintenance
 - after restarting the server, make sure mongo, redis, nginx,
   rippled, dev.peercover and peercover stay up.
 - make sure there is enough free disk space
    - todo: how big can the rippled database get?
 - after restarting, update the DNS to point to the new IP
    - todo: namecheap / dnsimple?

apt
  ~# vi /etc/apt/apt.conf.d/50local
  APT::Get::Show-Upgraded "true";
  APT::Get::Show-Versions "true";

ntp
  ~# apt-get install ntp
  * make sure the server time is accurate

nginx
  ~# cp ./nginx-peercover.com /etc/nginx/sites-available/peercover.com
  * private and public key in /home/ubuntu/ssl
  ~# ln -s /etc/nginx/sites-available/peercover.com /etc/nginx/sites-enabled/
  ~# service nginx restart

peercover
  ~# cd /home/ubuntu
  ~# git clone git@bitbucket.org:peercover/peercover.git
  ~# npm rebuild
  ~# cp ./init-peercover.conf /etc/init/peercover.conf
  ~# initctl list|grep peercover
  ~# sudo start peercover
  ~# sudo status peercover

  * if you have to flushdb, remember to: redis-cli FLUSHDB

rippled
  * build
    ~# cd /home/wolfgang
    ~# git clone git@github.com:ripple/rippled.git
    ~# cd rippled
    ~# scons

  ~# cp ./rippled.cfg /home/ubuntu/rippled/rippled.cfg
  * ripple.txt is in peercover.git:static/ripple.txt and served from node
  ~# cp -r ./rippled_ssl_certs /home/ubuntu/rippled/ssl_certs
